/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  * OVERVIEW:
  * =========
  *
  *     - Preprocessor definitions set to define debug in D3 modes.
  *
  *     - Linker script modified to define other RAM areas, so we can just
  *       create a big buffer for writing data over modbus. Not all RAM
  *       areas included in the RAM definition as IAR seems to prefer to not
  *       use DTCM in that case.
  *
  *     - the files "port*.?" are the STM32H745 specific files.
  *
  *     - the files "mb*" are the Modbus specific files. I had to make some fixes
  *       in them. mbconfig.h should be modified to configure which Modbus
  *       features are to be enabled from the library.
  *
  *     - We use "User defined" Modbus function codes for our own payloads for
  *       writing a block of memory. The "file record" standard function codes
  *       are too limited for our needs.
  *
  *     - the files "appModbus.?" contain our own application specific Modbus
  *       handlers, which are just callback functions that the Modbus library
  *       calls.
  *
  *     - You need to register user defined function codes with the library at
  *       startup, and the callbacks are a slightly different signature.
  *
  *     - The library is not designed to have more than 1 Modbus port open at
  *       any time. There are lots of shared globals or statics. Need to move
  *       them into a "channel" data structure for the mainboard so the code
  *       can operate against one of those.
  *
  *     - The library required one UART for communications and one timer for
  *       protocol timeouts.
  *
  *     - In this example I have used USART1 and TIM13, both configured by
  *       CubeMX.
  *
  *     - USART1 is set for 9600 baud, 9 bits (MSB gets replaced by parity),
  *       even parity, 1 stop bit.
  *
  *     - I manually enable the USART1 IRQ in a user code section of usart.c
  *       so that...
  *
  *     - The serial routines do not use the HAL to transmit or receive, as they
  *       are only transfer 1 char at a time. For faster comms we might want to use
  *       the FIFO or DMA (or not faster, but less CPU usage).
  *
  *     - You can connect a FTDI RS232-TTL---WE cable to the following pins,
  *       as set up in the Cube file:
  *           FT232 GND BLACK => NUCLEO GND CN10:5
  *           FT232 RX YELLOW => NUCLEO GND CN10:14 => STM32H745 USART1 TX
  *           FT232 TX ORANGE => NUCLEO GND CN10:16 => STM32H745 USART1 RX
  *           All others N/C
  *
  *     - TIM13 is set so each count is 50 us (100 MHz / 5000) to simply match
  *       the library. The timer is only 1 channel so the time base is started
  *       for a timeout period, and then the callback is used to process the
  *       end of the period. The timer CANNOT be set to One Pulse Mode because
  *       the correct callback does not get called, so the callback must stop
  *       the timer manually.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include <mb.h>
#include "Error_Handler.h"
#include "appModbus.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
    HAL_EnableDBGSleepMode();
    HAL_EnableDBGStopMode();
    HAL_EnableDBGStandbyMode();
    
    HAL_EnableDomain2DBGSleepMode();
    HAL_EnableDomain2DBGStopMode();
    HAL_EnableDomain2DBGStandbyMode();

    HAL_EnableDomain3DBGStopMode();
    HAL_EnableDomain3DBGStandbyMode();

    __HAL_RCC_TIM2_FORCE_RESET();
    __HAL_RCC_TIM2_RELEASE_RESET();
    
    __HAL_DBGMCU_UnFreeze_TIM2();
    __HAL_DBGMCU_FREEZE_TIM2();
    
    __HAL_RCC_TIM13_FORCE_RESET();
    __HAL_RCC_TIM13_RELEASE_RESET();
    
    __HAL_DBGMCU_UnFreeze_TIM13();
    __HAL_DBGMCU_FREEZE_TIM13();
    
    __HAL_RCC_USART1_FORCE_RESET();
    __HAL_RCC_USART1_RELEASE_RESET();

  /* USER CODE END 1 */
/* USER CODE BEGIN Boot_Mode_Sequence_0 */
/* USER CODE END Boot_Mode_Sequence_0 */

  /* Enable I-Cache---------------------------------------------------------*/
  SCB_EnableICache();

  /* Enable D-Cache---------------------------------------------------------*/
  SCB_EnableDCache();

/* USER CODE BEGIN Boot_Mode_Sequence_1 */
/* USER CODE END Boot_Mode_Sequence_1 */
  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();
/* USER CODE BEGIN Boot_Mode_Sequence_2 */
/* USER CODE END Boot_Mode_Sequence_2 */

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM2_Init();
  MX_USART1_UART_Init();
  MX_TIM13_Init();
  /* USER CODE BEGIN 2 */

    //HAL_UART_Transmit(&huart1, (uint8_t*)"\x23\x03\x04\x00\x00\x00\x02\x78\xC0", 9, HAL_MAX_DELAY);
  
    //DBGMCU_APB1PeriphConfig(DBGMCU_TIM2_STOP, ENABLE);

    //DBGMCU->APB1LFZ1 |= DBGMCU_APB1LFZ1_DBG_TIM2;
    //HAL_TIM_PWM_Start_IT(&htim2, TIM_CHANNEL_3);
    
    AppModbusInitialiseReadOnlyHoldingRegisters();

    UCHAR   modbusAddress = 0x20;
    UCHAR   portNumber = 0; // Immaterial here since port does not support multiple ports, but initialisation could be done based on it
    ULONG   baudRate = huart1.Init.BaudRate;
    
    eMBErrorCode mbError;
    mbError = eMBInit(MB_RTU, modbusAddress, portNumber, baudRate, MB_PAR_EVEN);
    mbError = eMBRegisterCB(USER_FUNC_BEGINWRITE, ModbusUserBeginWriteCB);
    mbError = eMBRegisterCB(USER_FUNC_WRITEDATA, ModbusUserWriteDataCB);
    mbError = eMBRegisterCB(USER_FUNC_FINISHWRITE, ModbusUserFinishWriteCB);
    mbError = eMBRegisterCB(USER_FUNC_CANCELWRITE, ModbusUserCancelWriteCB);

    mbError = eMBEnable();
    if (mbError != MB_ENOERR)
    {
        Error_Handler();
    }
    
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
      eMBPoll();
      __WFE();
      __NOP();
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Supply configuration update enable
  */
  HAL_PWREx_ConfigSupply(PWR_DIRECT_SMPS_SUPPLY);
  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 100;
  RCC_OscInitStruct.PLL.PLLP = 4;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  RCC_OscInitStruct.PLL.PLLR = 4;
  RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_3;
  RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
  RCC_OscInitStruct.PLL.PLLFRACN = 0;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV1;
  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

#if 0
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
#endif
void DummyFoo(void)
{
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
