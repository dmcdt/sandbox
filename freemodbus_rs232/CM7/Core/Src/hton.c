/*
 *  Definitions for conversion between host storage order and network storage order.
 *
 *  These in theory should be a bit more intelligent and only perform the
 *  byte swapping IF the host byte order and network byte order are different.
 *  This could be done by the preprocessor if required.
 *  Network byte order is always big endian.
 *  Host byte order for ARM is most commonly (and defaults to) little endian.
 *
 */


#include "hton.h"
#include "endian.h"


// TODO - should have some compile time settings that determine whether host order (depends on host CPU/OS/CPU mode) and network order (always BE) performs conversion or just returns input (or does nothing)
uint32_t htonl(uint32_t hostlong)
{
    return EndianSwap32(hostlong);
}


uint16_t htons(uint16_t hostshort)
{
    return EndianSwap16(hostshort);
}


uint32_t ntohl(uint32_t netlong)
{
    return EndianSwap32(netlong);
}


uint16_t ntohs(uint16_t netshort)
{
    return EndianSwap16(netshort);
}

