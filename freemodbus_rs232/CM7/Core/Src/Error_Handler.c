#include "Error_Handler.h"
#include <intrinsics.h>


/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
    /* User can add his own implementation to report the HAL error return state */
    __disable_interrupt();
    while (1)
    {
    }
}
