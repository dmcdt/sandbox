/*
 *  Function definitions for functions that blindly swap byte orders.
 */


#include "endian.h"
#include "intrinsics.h"


#if !defined(INLINE_ENDIAN_FUNCTIONS)

uint16_t EndianSwap16(uint16_t n)
{
    return (n >> 8) | (n << 8);
    // return __REV16(n);
}


uint32_t EndianSwap32(uint32_t n)
{
    return  ((n >> 24) & 0x000000FFUL) |
            ((n >>  8) & 0x0000FF00UL) |
            ((n <<  8) & 0x00FF0000UL) |
            ((n << 24) & 0xFF000000UL);
    
    // TODO - find some sensible way to force this inline from external modules.
    // would need to check whether inline was enabled, that it is ARM, and then use a static inline in a header
    // Would reduce to a single instruction instead of 8 + function call
    //return __REV(n);
}


#endif
