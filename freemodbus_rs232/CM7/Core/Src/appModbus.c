/*
 *  Modbus callback functions, defined by the application level.
 *
 *  See the declarations in mb.h for some hints on implementation.
 */
 

#include "mb.h"

// Additional custom callbacks for user defined function codes
#include "appModbus.h"
#include "hton.h"
#include "WriteMemory.h"

#include <stdint.h>
#include <stdbool.h>
#include <string.h>


/* Index definitions for the "Holding Registers" that are within our device. */
enum HoldingRegisters
{
    // MBHR = ModBus Holding Register
    MBHR_CompanyId0 = 0,
    MBHR_CompanyId1,
    MBHR_DeviceType,
    MBHR_FirmwareVersion,
    MBHR_FirmwareChecksumHighWord,
    MBHR_FirmwareChecksumLowWord,
    MBHR_SerialNumber0_1,
    MBHR_SerialNumber2_3,
    MBHR_SerialNumber4_5,
    MBHR_SerialNumber6_7,
    MBHR_SerialNumber8_9,
    MBHR_SerialNumber10_11,
    MBHR_SerialNumber12_13,
    MBHR_SerialNumber14_15,
    MBHR_NumberRegisters
};


/* Our table of "Holding Registers". We could make this just a block of
 * USHORTs so that it is easier to just block copy in and out of the data
 * packets, except that's not always best in many cases. So just accept the hit now.
 */
struct HoldingRegister
{
    USHORT  value;
    bool    readOnly;
} gHoldingRegisters[MBHR_NumberRegisters] = {   { 0x4344, true },
                                                { 0x5400, true },
                                                { 0x0001, true},
                                                { 0x0100, true},
                                            };


void AppModbusInitialiseReadOnlyHoldingRegisters(void)
{
    // Set firmware (i.e. current code memory) checksum, it's not a compile time constant
    gHoldingRegisters[MBHR_FirmwareChecksumHighWord] = (struct HoldingRegister){ 0x1234, true };
    gHoldingRegisters[MBHR_FirmwareChecksumLowWord] = (struct HoldingRegister){ 0x5678, true };
}


// FC 04 = Read input register
eMBErrorCode eMBRegInputCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs)
{
    return MB_ENOERR;
}


// FC 06 = Write holding register
// FC 16 = Write multiple holding registers
// FC 03 = Read holding registers
// FC 23 = Read / write multiple holding registers
eMBErrorCode eMBRegHoldingCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs, eMBRegisterMode eMode)
{
    eMBErrorCode errorCode = MB_ENOERR;

    // Modbus device addresses are 1 based (whereas the addresses in the comms
    // are zero based). Internally we want to work with 0 based addresses, so
    // need to subtract one to convert it from a Modbus device address.
    usAddress--;
    if ((usAddress + usNRegs) <= MBHR_NumberRegisters)
    {
        if (eMode == MB_REG_READ)
        {
            for (USHORT registerIndex = 0; registerIndex < usNRegs; registerIndex++)
            {
                USHORT  registerValue = gHoldingRegisters[usAddress + registerIndex].value;
                
                /* Cannot guarantee byte alignment of transmit buffer,
                 * Cortex M4 and M7 should support unaligned accesses, but
                 * accessing a byte at a time is suitable, just slower.
                 * Also allows us to convert to big endian at the same time.
                 */
                *pucRegBuffer++ = (registerValue >> 8);
                *pucRegBuffer++ = registerValue & 0xFF;
            }
        }
        else if (eMode == MB_REG_WRITE)
        {
            /* Some registers are readonly, so check that all the ones we are
             * trying to write are writable.
             */
            bool allWritable = true;
            for (USHORT registerIndex = 0; registerIndex < usNRegs; registerIndex++)
            {
                allWritable &= !gHoldingRegisters[usAddress + registerIndex].readOnly;
            }
            
            if (allWritable)
            {
                /* Go again, but this time write all the new data */
                for (USHORT registerIndex = 0; registerIndex < usNRegs; registerIndex++)
                {
                    /* Cannot guarantee byte alignment of transmit buffer,
                     * Cortex M4 and M7 should support unaligned accesses, but
                     * accessing a byte at a time is suitable, just slower.
                     * Also allows us to convert to big endian at the same time.
                     */
                    gHoldingRegisters[usAddress + registerIndex].value = (pucRegBuffer[registerIndex * 2] << 8) | pucRegBuffer[registerIndex * 2 + 1];
                }
            }
            else
            {
                errorCode = MB_ENOREG;
            }
        }
    }
    else
    {
        errorCode = MB_ENOREG;
    }
    
    return errorCode;
}


// FC 01 = Read coils
// FC 05 = Write single coil
// FC 15 = Write multiple coils
eMBErrorCode eMBRegCoilsCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNCoils, eMBRegisterMode eMode)
{
    if (eMode == MB_REG_READ)
    {
        
    }
    else if (eMode == MB_REG_WRITE)
    {
    }
    
    return MB_ENOERR;
}


// FC 02 = Read discrete inputs
eMBErrorCode eMBRegDiscreteCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNDiscrete)
{
    return MB_ENOERR;
}


enum MemoryArea
{
    Memory_None             = 0x00,     // Reserved to indicate no memory area
    Memory_FirmwareCortexM7 = 0x10,     // Firmware for the Cortex M7 (nominally... really is flash bank 1)
    Memory_FirmwareCortexM4 = 0x11,     // Firmware for the Cortex M4 (nominally... really is flash bank 2)
    Memory_SerialNumber     = 0x20,     // Non-volatile storage for device Serial Number
    Memory_Configuration    = 0x30,     // Configuration BLOB, defines the job acquisition parameters
    Memory_TransmitWaveform0= 0x40,     // Storage space for raw transmit waveform 0
    Memory_TransmitWaveform1= 0x41,     // Storage space for raw transmit waveform 1
};


enum BeginWritePduOffset
{
    BeginWrite_FunctionCode = 0,
    BeginWrite_MemoryArea   = 1,
    BeginWrite_Length       = 2
};


enum WriteDataPduOffset
{
    WriteData_FunctionCode  = 0,
    WriteData_Address       = 1,
    WriteData_Bytes         = 5,
    WriteData_MinimumLength = 6
};


enum FinishWritePduOffset
{
    FinishWrite_FunctionCode    = 0,
    FinishWrite_MemoryArea      = 1,
    FinishWrite_Length          = 2
};


#define WRITEDATA_BUFFER_SIZE   (128 * 1024UL)

struct WriteMemoryContext
{
    enum MemoryArea memoryArea;
    uint32_t        startAddress;
    uint32_t        lastAddress;
    uint8_t         buffer[WRITEDATA_BUFFER_SIZE];
};

#pragma location="AXI_SRAM"
static struct WriteMemoryContext gWriteMemoryContext;


eMBException ModbusUserBeginWriteCB(UCHAR *pucFrame, USHORT *pusLength)
{
    eMBException    exceptionCode = MB_EX_NONE;
    
    if (gWriteMemoryContext.memoryArea == Memory_None)
    {
        if (*pusLength == BeginWrite_Length)
        {
            switch (pucFrame[BeginWrite_MemoryArea])
            {
//                case Memory_FirmwareCortexM7:
//                    break;
//                    
//                case Memory_FirmwareCortexM4:
//                    break;
//                    
                case Memory_SerialNumber:
                    gWriteMemoryContext.memoryArea = Memory_SerialNumber;
                    gWriteMemoryContext.startAddress = 0;
                    gWriteMemoryContext.lastAddress = 0;
                    memset(&gWriteMemoryContext.buffer[0], 0xFF, sizeof gWriteMemoryContext.buffer);
                    //BeginWriteSerialNumber();
                    break;
                    
                case Memory_Configuration:
                    gWriteMemoryContext.memoryArea = Memory_Configuration;
                    gWriteMemoryContext.startAddress = 0;
                    gWriteMemoryContext.lastAddress = 0;
                    memset(&gWriteMemoryContext.buffer[0], 0xFF, sizeof gWriteMemoryContext.buffer);
                    //BeginWriteConfiguration();
                    break;
                    
                default:
                    exceptionCode = MB_EX_ILLEGAL_DATA_ADDRESS;
                    break;
            }
        }
        else
        {
            exceptionCode = MB_EX_ILLEGAL_DATA_VALUE;
        }
    }
    else
    {
        exceptionCode = MB_EX_ILLEGAL_FUNCTION;
    }
    
    return exceptionCode;
}


eMBException ModbusUserWriteDataCB(UCHAR *pucFrame, USHORT *pusLength)
{
    eMBException    exceptionCode = MB_EX_NONE;

    if (gWriteMemoryContext.memoryArea != Memory_None)
    {
        if (*pusLength >= WriteData_MinimumLength)
        {
            ULONG startAddress;
            ULONG length;
            
//            memcpy(&startAddress, &pucFrame[WriteData_Address], sizeof(ULONG));
//            startAddress = ntohl(startAddress);

            startAddress =  (pucFrame[WriteData_Address] << 24) |
                            (pucFrame[WriteData_Address + 1] << 16) |
                            (pucFrame[WriteData_Address + 2] << 8) |
                            pucFrame[WriteData_Address + 3];
            
            // length must be less than 256 bytes, which is the Modbus packet
            // size, so we don't need to check it specifically.
            length = *pusLength - WriteData_Bytes;

            if (gWriteMemoryContext.lastAddress == 0)
            {
                gWriteMemoryContext.startAddress = startAddress;
            }
            
            // Check other combinations fit within buffer
            if ((startAddress >= gWriteMemoryContext.lastAddress) &&
                ((startAddress - gWriteMemoryContext.startAddress) < WRITEDATA_BUFFER_SIZE) &&
                (((startAddress - gWriteMemoryContext.startAddress) + length) <= WRITEDATA_BUFFER_SIZE))
            {
                memcpy(&gWriteMemoryContext.buffer[startAddress], &pucFrame[WriteData_Bytes], length);
                gWriteMemoryContext.lastAddress = gWriteMemoryContext.startAddress + length;
            }
            else
            {
                exceptionCode = MB_EX_ILLEGAL_DATA_ADDRESS;
            }
        }
        else
        {
            exceptionCode = MB_EX_ILLEGAL_DATA_VALUE;
        }
    }
    else
    {
        exceptionCode = MB_EX_ILLEGAL_FUNCTION;
    }
    
    return exceptionCode;
}


eMBException ModbusUserFinishWriteCB(UCHAR *pucFrame, USHORT *pusLength)
{
    eMBException    exceptionCode = MB_EX_NONE;

    if (*pusLength == FinishWrite_Length)
    {
        if ((gWriteMemoryContext.memoryArea != Memory_None) &&
            (pucFrame[FinishWrite_MemoryArea] == gWriteMemoryContext.memoryArea))
        {
            switch (pucFrame[FinishWrite_MemoryArea])
            {
//                case Memory_FirmwareCortexM7:
//                    break;
//                    
//                case Memory_FirmwareCortexM4:
//                    break;
//                    
                case Memory_SerialNumber:
                    FinishWriteSerialNumber(gWriteMemoryContext.startAddress, &gWriteMemoryContext.buffer[0], gWriteMemoryContext.lastAddress - gWriteMemoryContext.startAddress);
                    break;
                    
                case Memory_Configuration:
                    FinishWriteConfiguration(gWriteMemoryContext.startAddress, &gWriteMemoryContext.buffer[0], gWriteMemoryContext.lastAddress - gWriteMemoryContext.startAddress);
                    break;
                    
                case Memory_TransmitWaveform0:
                    FinishWriteWaveform(0, gWriteMemoryContext.startAddress, &gWriteMemoryContext.buffer[0], gWriteMemoryContext.lastAddress - gWriteMemoryContext.startAddress);
                    break;
                    
                case Memory_TransmitWaveform1:
                    FinishWriteWaveform(1, gWriteMemoryContext.startAddress, &gWriteMemoryContext.buffer[0], gWriteMemoryContext.lastAddress - gWriteMemoryContext.startAddress);
                    break;
            }
            
            gWriteMemoryContext.memoryArea = Memory_None;
        }
        else
        {
            exceptionCode = MB_EX_ILLEGAL_DATA_ADDRESS;
        }
    }
    else
    {
        exceptionCode = MB_EX_ILLEGAL_DATA_VALUE;
    }
    
    return exceptionCode;
}


eMBException ModbusUserCancelWriteCB(UCHAR *pucFrame, USHORT *pusLength)
{
    gWriteMemoryContext.memoryArea = Memory_None;
    gWriteMemoryContext.startAddress = 0;
    gWriteMemoryContext.lastAddress = 0;
    memset(&gWriteMemoryContext.buffer[0], 0xFF, sizeof gWriteMemoryContext.buffer);
    
    return MB_EX_NONE;
}

