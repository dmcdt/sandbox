#ifndef APPMODBUS_H
#define APPMODBUS_H

/*
 *  Custom callbacks for our own Modbus user defined function codes.
 */


#include "port.h"
#include "mb.h"
#include "mbproto.h"


/* User defined function code values */
#define USER_FUNC_BEGINWRITE    ((UCHAR)65)
#define USER_FUNC_WRITEDATA     ((UCHAR)66)
#define USER_FUNC_FINISHWRITE   ((UCHAR)67)
#define USER_FUNC_CANCELWRITE   ((UCHAR)68)


/** Just an example so we have some data to reply with */
void AppModbusInitialiseReadOnlyHoldingRegisters(void);


/* Callbacks for user defined function code handlers */
eMBException ModbusUserBeginWriteCB(UCHAR *pucFrame, USHORT *pusLength);
eMBException ModbusUserWriteDataCB(UCHAR *pucFrame, USHORT *pusLength);
eMBException ModbusUserFinishWriteCB(UCHAR *pucFrame, USHORT *pusLength);
eMBException ModbusUserCancelWriteCB(UCHAR *pucFrame, USHORT *pusLength);


#endif
