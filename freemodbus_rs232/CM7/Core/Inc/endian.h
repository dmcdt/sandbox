#ifndef ENDIAN_H
#define ENDIAN_H

/*
 *  Functions that just blindly swap the byte order of variables.
 */

#include <stdint.h>


uint16_t EndianSwap16(uint16_t n);
uint32_t EndianSwap32(uint32_t n);


#if defined(INLINE_ENDIAN_FUNCTIONS)

static inline uint16_t EndianSwap16(uint16_t)
{
    return (n >> 8) | (n << 8);
    // return __REV16(n);
}


static inline uint32_t EndianSwap32(uint32_t n)
{
    return  ((n >> 24) & 0x000000FFUL) |
            ((n >>  8) & 0x0000FF00UL) |
            ((n <<  8) & 0x00FF0000UL) |
            ((n << 24) & 0xFF000000UL);
    
    // TODO - find some sensible way to force this inline from external modules.
    // would need to check whether inline was enabled, that it is ARM, and then use a static inline in a header
    // Would reduce to a single instruction instead of 8 + function call
    //return __REV(n);
}

#endif


#endif
