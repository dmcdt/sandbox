#ifndef WRITEMEMORY_H
#define WRITEMEMORY_H

#include <stdint.h>


void FinishWriteSerialNumber(uint32_t destination, void *source, uint32_t length);
void FinishWriteConfiguration(uint32_t destination, void *source, uint32_t length);
void FinishWriteWaveform(uint32_t waveformIndex, uint32_t destination, void *source, uint32_t length);


#endif
