#ifndef HTON_H
#define HTON_H

/*
 *  Definitions for performing conversion from host storage to network storage.
 */

#include <stdint.h>


uint32_t htonl(uint32_t hostlong);
uint16_t htons(uint16_t hostshort);
uint32_t ntohl(uint32_t netlong);
uint16_t ntohs(uint16_t netshort);


#endif
