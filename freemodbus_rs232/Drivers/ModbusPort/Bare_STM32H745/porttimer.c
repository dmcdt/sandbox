/*
 *  Modbus timer for the STM32 port.
 */

#include "port.h"

#include "mb.h"
#include "mbport.h"

#include "tim.h"

#include "Error_Handler.h"


void ModbusPeriodElapsedCallback(TIM_HandleTypeDef *htim);


/** Handle to the timer used for protocol/packet timeouts */
static TIM_HandleTypeDef *ghMbTimer;


BOOL xMBPortTimersInit( USHORT usTimeOut50us )
{
    BOOL initOk = FALSE;
    HAL_StatusTypeDef status;
    
    // Assume that the timer has been initialised by the CubeMX init code,
    // we just modify it in this function. Take pointer so easy to change
    // timer in future. Only 1 channel is required.
    ghMbTimer = &htim13;
    
    // End value is counted as part of the count, so we need to go up
    // to number of 50us periods - 1 (timer set 1 count == 50 us)
    ghMbTimer->Init.Period = usTimeOut50us - 1;
    status = HAL_TIM_Base_Init(ghMbTimer);
    if (status == HAL_OK)
    {
        // Reset update flag so IRQ doesn't trigger prematurely?
        __HAL_TIM_CLEAR_IT(ghMbTimer, TIM_IT_UPDATE);
        
        status = HAL_TIM_RegisterCallback(ghMbTimer, HAL_TIM_PERIOD_ELAPSED_CB_ID, ModbusPeriodElapsedCallback);
        if (status == HAL_OK)
        {
            initOk = TRUE;
        }
    }
      
    return initOk;
}


inline void vMBPortTimersEnable()
{
    /* Enable the timer with the timeout passed to xMBPortTimersInit( ) */
    HAL_StatusTypeDef status;
    //status = HAL_TIM_OnePulse_Start_IT(ghMbTimer, TIM_CHANNEL_1);
    // TODO - what to do if this fails?
    
    // Need to stop before start, so that timer resets to 0?
    status = HAL_TIM_Base_Stop_IT(ghMbTimer);
    // TODO - what to do sensibly if this fails?
    if (status != HAL_OK)
    {
        Error_Handler();
    }
    
    // Reset counter and update flag so IRQ doesn't trigger prematurely
    ghMbTimer->Instance->CNT = 0;
    __HAL_TIM_CLEAR_IT(ghMbTimer, TIM_IT_UPDATE);
    
    status = HAL_TIM_Base_Start_IT(ghMbTimer);
    // TODO - what to do sensibly if this fails?
    if (status != HAL_OK)
    {
        Error_Handler();
    }
}


inline void vMBPortTimersDisable()
{
    /* Disable any pending timers. */
    HAL_StatusTypeDef status;
    //status = HAL_TIM_OnePulse_Stop_IT(ghMbTimer, TIM_CHANNEL_1);
    status = HAL_TIM_Base_Stop_IT(ghMbTimer);
    // TODO - what to do sensibly if this fails?
    if (status != HAL_OK)
    {
        Error_Handler();
    }
}


/* ISR (or in this case HAL callback) which is called whenever the timer has
 * expired. This function must then call pxMBPortCBTimerExpired( ) to notify the
 * protocol stack that the timer has expired.
 */
void ModbusPeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    HAL_TIM_Base_Stop_IT(ghMbTimer);
    (void)pxMBPortCBTimerExpired();
}

