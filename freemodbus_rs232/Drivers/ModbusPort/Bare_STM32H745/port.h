/*
 * Platform port definitions for STM32H745 using HAL and compiling with IAR.
 */

#ifndef PORT_H
#define PORT_H

#include <stdint.h>
#include <stdbool.h>
#include <intrinsics.h>

#define	INLINE                      inline
#define PR_BEGIN_EXTERN_C           extern "C" {
#define	PR_END_EXTERN_C             }

#define ENTER_CRITICAL_SECTION( )   __disable_interrupt()
#define EXIT_CRITICAL_SECTION( )    __enable_interrupt()

typedef bool BOOL;

typedef unsigned char UCHAR;
typedef char CHAR;

typedef uint16_t USHORT;
typedef int16_t SHORT;

typedef uint32_t ULONG;
typedef int32_t LONG;

#ifndef TRUE
#define TRUE            true
#endif

#ifndef FALSE
#define FALSE           false
#endif

#endif
