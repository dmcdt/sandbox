/*
 *  Modbus FSM event queue, provided by the bare metal demo.
 *
 *  These are used by the Modbus library as a very shallow queue so that
 *  when processing one thing event (like a timeout) it can push the next event
 *  (e.g. need to process a frame) onto this event queue, and then pick it up
 *  in the next mainloop iteration rather than trying to process multiple
 *  events within it's own loop.
 *
 */

#include "mb.h"
#include "mbport.h"

     
static eMBEventType eQueuedEvent;
static BOOL     xEventInQueue;


BOOL xMBPortEventInit(void)
{
    xEventInQueue = FALSE;
    return TRUE;
}


BOOL xMBPortEventPost(eMBEventType eEvent)
{
    xEventInQueue = TRUE;
    eQueuedEvent = eEvent;
    return TRUE;
}


BOOL xMBPortEventGet(eMBEventType * eEvent)
{
    BOOL            xEventHappened = FALSE;

    if(xEventInQueue)
    {
        *eEvent = eQueuedEvent;
        xEventInQueue = FALSE;
        xEventHappened = TRUE;
    }
    return xEventHappened;
}
