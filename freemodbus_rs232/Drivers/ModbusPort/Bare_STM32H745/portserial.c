/*
 * FreeModbus porting layer for STM32H745 using HAL for Cereus tool.
 */

#include "port.h"
#include "mb.h"
#include "mbport.h"


#include "usart.h"


/** Handle to the UART used for Modbus communications, makes it easier to change */
static UART_HandleTypeDef   *ghMbUart;


BOOL xMBPortSerialInit(UCHAR ucPORT, ULONG ulBaudRate, UCHAR ucDataBits, eMBParity eParity)
{
    /*
     * Assume that the HAL MX initialisation code has been called and the port
     * has been configured already. Also assume that the NVIC has been
     * configured for the ISR in the MX Init. Take a copy of the HAL device
     * handle.
     */
    
    ghMbUart = &huart1;
    
    return TRUE;
}


/** Enable or disable the receiver and transmitter.
 *  @param xRXEnable Controls whether serial receive interrupts are disabled/enabled.
 *  @param xTxENable controls whether transmitter empty interrupts are disabled/enabled.
 */
void vMBPortSerialEnable(BOOL xRxEnable, BOOL xTxEnable)
{
    /*
     *  Not using the HAL functions here since the HAL does not deal with
     *  long running background processes. It starts something and then runs to
     *  completion. So there are no functions in the HAL that simply enable
     *  the interrupts and then allow you to put/get your own serial data,
     *  it always tries to do that for you.
     *
     *  Assumes we are using Recieve Not Empty and Transmit Not Full,
     *  i.e. the single byte transmit without FIFO.
     */
    if (xRxEnable)
    {
        SET_BIT(ghMbUart->Instance->CR1, USART_CR1_RXNEIE_RXFNEIE);
    }
    else
    {
        CLEAR_BIT(ghMbUart->Instance->CR1, USART_CR1_RXNEIE_RXFNEIE);
    }
    
    if (xTxEnable)
    {
        SET_BIT(ghMbUart->Instance->CR1, USART_CR1_TXEIE_TXFNFIE);
    }
    else
    {
        CLEAR_BIT(ghMbUart->Instance->CR1, USART_CR1_TXEIE_TXFNFIE);
    }
}


/** Put a byte in the UARTs transmit buffer.
 *
 *  This function is called by the protocol stack if
 *  pxMBFrameCBTransmitterEmpty() has been called.
 */
BOOL xMBPortSerialPutByte(CHAR ucByte)
{
//    HALStatusTypedef status;
//    
//    status = HAL_UART_Transmit(ghMbUart, &ucByte, 1, HAL_MAX_DELAY);
//    if (status != HAL_OK)
//    {
//        return FALSE;
//    }
//    return TRUE;
    
    // Or just the direct code, without the HAL
    ghMbUart->Instance->TDR = ucByte;
    return TRUE;
}


/** Return the byte in the UARTs receive buffer.
 *
 *  This function is called by the protocol stack after pxMBFrameCBByteReceived()
 *  has been called.
 */
BOOL xMBPortSerialGetByte(CHAR * pucByte)
{
//    HALStatusTypedef status;
//    
//    status = HAL_UART_Receive(ghMbUart, pucByte, 1, HAL_MAX_DELAY);
//    if (status != HAL_OK)
//    {
//        return FALSE;
//    }
//    return TRUE;

    // Or just the direct code, without the HAL
    *pucByte = ghMbUart->Instance->RDR;
    return TRUE;
}


/** Interrupt handler for dealing with Modbus transmissions.
 *
 *  Calls the Modbus library functions in response to whether a character
 *  has been recevied or the transmitter is empty.
 *
 *  Shared interrupt handler between every IRQ on the UART so check
 *  whether an interrupt is enabled and the flag before acting on it.
 *
 *  @remarks Create an interrupt handler for the receive interrupt for your target
 *  processor. This function should then call pxMBFrameCBByteReceived( ). The
 *  protocol stack will then call xMBPortSerialGetByte( ) to retrieve the
 *  character.
 *
 *  @remarks Create an interrupt handler for the transmit buffer empty interrupt
 *  (or an equivalent) for your target processor. This function should then
 *  call pxMBFrameCBTransmitterEmpty( ) which tells the protocol stack that
 *  a new character can be sent. The protocol stack will then call 
 *  xMBPortSerialPutByte( ) to send the character.
 */
void USART1_IRQHandler(void)
{
    /* Receive interrupts enabled */
    if (ghMbUart->Instance->CR1 & USART_CR1_RXNEIE_RXFNEIE)
    {
        /* Received something */
        if (ghMbUart->Instance->ISR & USART_ISR_RXNE_RXFNE_Msk)
        {
            /* Inform the Modbus Rx FSM  of the event - it will read the receive
             * register later.
             */
            pxMBFrameCBByteReceived();
        }
        
        /* Check the overrun interrupt, as it is enabled by default.
         * Just clear the flag and ignore it, frame will eventually complete
         * and the CRC will not match, which is all that can be done for corrupt
         * comms with Modbus.
         *
         * The only time we might care to record this is if there is a
         * specific diagnostic counter for overruns.
         */
        if (ghMbUart->Instance->ISR & USART_ISR_ORE_Msk)
        {
            ghMbUart->Instance->ICR |= USART_ICR_ORECF;
        }
    }
    
    /* If Tx interrupt enabled and occurred, use event to drive Modbus Tx FSM. */
    if ((ghMbUart->Instance->CR1 & USART_CR1_TXEIE_TXFNFIE) &&
        ((ghMbUart->Instance->ISR & USART_ISR_TXE_TXFNF_Msk) != 0))
    {
        pxMBFrameCBTransmitterEmpty();
    }
}
