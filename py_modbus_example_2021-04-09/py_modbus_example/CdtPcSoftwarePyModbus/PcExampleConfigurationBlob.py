# Simulation of CDT tool modbus interface, as for running example CDT modbus
# code against.
#
# Requires Pyside2 (Qt) for user interface.
# Requires modbus_tk, pyserial for performing the modbus comms.

import sys
from PySide2.QtWidgets import QApplication, QMainWindow, QMessageBox
from PySide2.QtCore import Slot
import uiLoader
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
import serial
import binascii
import struct
import CustomModbusFunctions
from CustomModbusFunctions import BeginWriteUserModbusRequest
from CustomModbusFunctions import WriteDataUserModbusRequest
from CustomModbusFunctions import FinishWriteUserModbusRequest
from CustomModbusFunctions import CancelWriteUserModbusRequest

# The address of the server on the bus.
serverUnitBusAddress = 0x20

#
class MainWindow(QMainWindow):
    #
    def __init__(self, parent=None):
        #
        QMainWindow.__init__(self, parent)
        self._server = None
        self._serverThread = None
        uiLoader.loadUi('untitled.ui', self)

    # Get the serial configuration to use. Firmware will use 115200,8,E,1
    def extract_serial_parameters(self):
        self._port_path = self.portName.text()
        self._baud_rate = int(self.baud.currentText())
        self._data_bits = int(self.dataBits.currentText())
        self._parity = [serial.PARITY_NONE, serial.PARITY_MARK, serial.PARITY_SPACE, serial.PARITY_ODD, serial.PARITY_EVEN][self.parity.currentIndex()]
        self._stop_bits = [serial.STOPBITS_ONE, serial.STOPBITS_ONE_POINT_FIVE, serial.STOPBITS_TWO][self.stopBits.currentIndex()]

    @Slot()
    def on_queryServer_clicked(self):
        self.extract_serial_parameters()
        client = ModbusClient(method='rtu', timeout=5,
                              port=self._port_path,
                              baudrate=self._baud_rate,
                              parity=self._parity)
        if client.connect():
            self.plainTextEdit.appendPlainText('Read input registers\r\n')
            # These register addresses are PDU  addresses, so are 0 based.
            # This is the opposite of what you set up in the server. WTF!
            rr = client.read_input_registers(6, 8, unit=serverUnitBusAddress)
            self.plainTextEdit.appendPlainText(str(rr))
            for rv in rr.registers:
                self.plainTextEdit.appendPlainText('    0x%04X' % rv)

            client.close()
        else:
            QMessageBox.critical(self, 'Error', 'Could not open port \'' + self._port_path + '\'')

    @Slot()
    def on_writeConfiguration_clicked(self):
        # The dummy configuration blob to send
        address = 0
        
        version_dict = {
                        'BLOB_ID'       : 0x30,
                        'Config_Version': 0x01
        }

        configuration_dict = {
                                'RX_gain_gas_head'      : ("<H",0x0003),
                                'RX_gain_gas_tail'      : ("<H",0x0006),
                                'RX_gain_liquid_head'   : ("<H",0x000C),
                                'RX_gain_liquid_tail'   : ("<H",0x0018),
                                'RX_gain_gas_change'    : ("<H",0x0064),
                                'RX_gain_liquid_change' : ("<H",0x00C8),
                                'RH_PA_enable'          : ("<H",0x000A),
                                'RH_PA_disable'         : ("<H",0x0050),
                                'RH_TX_enable'          : ("<H",0x0064),
                                'RH_XR_TX_start'        : ("<H",0x0028),
                                'RH_TX_disable'         : ("<H",0x0014),
                                'RH_RX_enable'          : ("<H",0x0032),
                                'RH_XR_RX_start'        : ("<H",0x0028),
                                'RH_RX_disable'         : ("<H",0x0014),
                                'RH_pixel_samples'      : ("<H",0x2710),
                                'ToF_default_medium'    : ("<H",0x0000)
        }

        transmit_packet = bytearray(0)

        for key in version_dict.keys():
            transmit_packet.extend(
                version_dict[key].to_bytes(1, byteorder='little'))

        for key in configuration_dict.keys():
            transmit_packet.extend(struct.pack(configuration_dict[key][0],configuration_dict[key][1]))

        CRC_value = (binascii.crc32(transmit_packet) ^ 0xffffffff)

        transmit_packet.extend(CRC_value.to_bytes(4, byteorder='little'))

        # Create the client object
        self.extract_serial_parameters()
        client = ModbusClient(method='rtu', timeout=5,
                              port=self._port_path,
                              baudrate=self._baud_rate,
                              parity=self._parity)

        # We need to tell the client about our custom response messages
        client.register(CustomModbusFunctions.BeginWriteUserModbusResponse)
        client.register(CustomModbusFunctions.WriteDataUserModbusResponse)
        client.register(CustomModbusFunctions.FinishWriteUserModbusResponse)
        client.register(CustomModbusFunctions.CancelWriteUserModbusResponse)

        # Tell the client to connect to the server
        if client.connect():
            # Create the request message to send. Note that it must include
            # the server bus address as the unit named parameter.
            request = BeginWriteUserModbusRequest(CustomModbusFunctions.MEMORY_AREA_CONFIGURATION,
                                                  unit=serverUnitBusAddress)

            # Used in testing  - self.plainTextEdit.appendPlainText(str(request))
            result = client.execute(request)
            if not result.isError():
                self.plainTextEdit.appendPlainText('BeginWrite successful\r\n')
            else:
                self.plainTextEdit.appendPlainText('BeginWrite error\r\n' + str(result))

            # Maximum number of bytes that can be written in one command
            # (as defined by our own custom function code in the firmware)
            #  = Modbus max length (256) - unit address length (1) - 
            #                              function code length (1) -
            #                              function code structure:
            #                               > address (4) 
            #                               > length of packet (1)
            #                               > version number (2) -
            #                              crc length (4)
            #  = 256 - 1 - 1 - (4 + 1 + 2) - 4
            #  = 243
            maximum_bytes = 242 # changed from 245 due to failure on mcu modbus side: mbrtu.c | line 157

            # Just a little something for information purposes
            write_count = 1

            # Loop to write all the configuration data in blocks
            # of up to maximum_bytes
            while address < len(transmit_packet):
                bytes_to_write = len(transmit_packet) - address
                if (bytes_to_write > maximum_bytes):
                    bytes_to_write = maximum_bytes
                
                request = WriteDataUserModbusRequest(address,
                                                     transmit_packet[address:address +
                                                                     bytes_to_write],
                                                     unit=serverUnitBusAddress)
                
                result = client.execute(request)
                if not result.isError():
                    self.plainTextEdit.appendPlainText('SUCCESS ' + str(write_count) +'\r\n' +
                                                        str(bytes_to_write) +
                                                        ' bytes written to address ' +
                                                        str(address) +
                                                        '\r\n')
                else:
                    self.plainTextEdit.appendPlainText('WriteData ' +
                                                        str(bytes_to_write) +
                                                        ' error\r\n' + 
                                                        str(result))
                
                address += bytes_to_write
                write_count += 1

            request = FinishWriteUserModbusRequest(CustomModbusFunctions.MEMORY_AREA_CONFIGURATION,
                                                  unit=serverUnitBusAddress)
            result = client.execute(request)
            if not result.isError():
                self.plainTextEdit.appendPlainText('FinishWrite successful\r\n')
            else:
                self.plainTextEdit.appendPlainText('FinishWrite error\r\n' +
                                                    str(result)+ '\r\n')

            # TODO - probably need a short delay in here while the data is written
            #        to non-volatile memory

            # TODO - other things here, such as polling for status, read back
            #        configuration to check that it has programmed correctly
            rr = client.read_input_registers(24, 2, unit=serverUnitBusAddress)
            received_crc = rr.registers[0] << 16 | rr.registers[1]

            self.plainTextEdit.appendPlainText(
                'Sent CRC       = ' + hex(CRC_value))
            self.plainTextEdit.appendPlainText(
                'Received CRC = ' + hex(received_crc))

            # Close the client to end communications
            client.close()
        else:
            QMessageBox.critical(self, 'Error', 'Could not open port \'' + self._port_path + '\'')

#
def main():
    app = QApplication(sys.argv)
    mainWindow = MainWindow()
    mainWindow.show()
    sys.exit(app.exec_())

#
if (__name__ == '__main__'):
    main()

