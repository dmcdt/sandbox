from pymodbus.pdu import ModbusRequest, ModbusResponse, ModbusExceptions
from pymodbus.compat import int2byte, byte2int
import struct

# User defined function code values
USER_FUNC_BEGINWRITE = 65
USER_FUNC_WRITEDATA = 66
USER_FUNC_FINISHWRITE = 67
USER_FUNC_CANCELWRITE = 68


MEMORY_AREA_FIRMWARE_M7 = 0x10
MEMORY_AREA_FIRMWARE_M4 = 0x11
MEMORY_AREA_SERIAL_NUMBER = 0x20
MEMORY_AREA_CONFIGURATION = 0x30
MEMORY_TRANSMITWAVEFORM0 = 0x40
MEMORY_TRANSMITWAVEFORM1 = 0x41

# Two options for automatic length calculation / checking of packet.
# _rtu_frame_size - set this to the total length of the packet.
# _rtu_byte_count_pos - position within data which encodes the byte length
#                       of the packet. This will be picked up, added to the
#                       CRC, address, FC and length to calculate the total
#                       frame length. So the byte position must be the last
#                       field immediately before the bytes.


#
#
#   Begin Write
#
#
class BeginWriteUserModbusResponse(ModbusResponse):
    function_code = USER_FUNC_BEGINWRITE
    _rtu_frame_size = 6

    def __init__(self, area=None, **kwargs):
        ModbusResponse.__init__(self, **kwargs)
        self._area = area
    
    def encode(self):
        encoded = bytearray(2)
        encoded[0] = self._area
        encoded[1] = 0x01
        return encoded

    def decode(self, data):
        self._area = data[0]


class BeginWriteUserModbusRequest(ModbusRequest):
    function_code = USER_FUNC_BEGINWRITE
    _rtu_frame_size = 5

    def __init__(self, area=None, **kwargs):
        ModbusRequest.__init__(self, **kwargs)
        self._area = area

    def encode(self):
        encoded = bytearray(1)
        encoded[0] = self._area
        return encoded

    def decode(self, data):
        self._area = data[0]

    def execute(self, context):
        #if not (1 <= self.count <= 0x7d0):
        #    return self.doException(ModbusExceptions.IllegalValue)
        #if not context.validate(self.function_code, self.address, self.count):
        #    return self.doException(ModbusExceptions.IllegalAddress)
        #values = context.getValues(self.function_code, self._area)
        #return BeginWriteUserModbusResponse(values)
        if not self._area in [MEMORY_AREA_FIRMWARE_M7,
                              MEMORY_AREA_FIRMWARE_M4,
                              MEMORY_AREA_SERIAL_NUMBER,
                              MEMORY_AREA_CONFIGURATION]:
            return self.doException(ModbusExceptions.IllegalAddress)
        return BeginWriteUserModbusResponse(self._area)



#
#
#   Write Data
#
#
class WriteDataUserModbusResponse(ModbusResponse):
    function_code = USER_FUNC_WRITEDATA
    _rtu_frame_size = 5

    def __init__(self, status=None, **kwargs):
        ModbusResponse.__init__(self, **kwargs)
        self._status = status or 0x01
    
    def encode(self):
        encoded = bytearray(1)
        encoded[0] = self._status
        return encoded

    def decode(self, data):
        self._status = data[0]


class WriteDataUserModbusRequest(ModbusRequest):
    function_code = USER_FUNC_WRITEDATA
    _rtu_byte_count_pos = 6

    def __init__(self, startAddress=None, dataValues=None, **kwargs):
        ModbusRequest.__init__(self, **kwargs)
        self._startAddress = startAddress
        self._dataValues = dataValues or []
    
    def encode(self):
        result = struct.pack('>LB', self._startAddress, len(self._dataValues))
        result += bytes(self._dataValues)
        return result

    def decode(self, data):
        self._startAddress, length = struct.unpack('>LB', data[0:5])
        self._dataValues = data[5:5+length]

    def execute(self, context):
        #if not (1 <= self.count <= 0x7d0):
        #    return self.doException(ModbusExceptions.IllegalValue)
        #if not context.validate(self.function_code, self.address, self.count):
        #    return self.doException(ModbusExceptions.IllegalAddress)
        #values = context.getValues(self.function_code, self._area)
        #return BeginWriteUserModbusResponse(values)
        return WriteDataUserModbusResponse()



#
#
#   Finish Write
#
#
class FinishWriteUserModbusResponse(ModbusResponse):
    function_code = USER_FUNC_FINISHWRITE
    _rtu_frame_size = 6

    def __init__(self, area=None, **kwargs):
        ModbusResponse.__init__(self, **kwargs)
        self._area = area
    
    def encode(self):
        encoded = bytearray(2)
        encoded[0] = self._area
        encoded[1] = 0x01
        return encoded

    def decode(self, data):
        self._area = data[0]


class FinishWriteUserModbusRequest(ModbusRequest):
    function_code = USER_FUNC_FINISHWRITE
    _rtu_frame_size = 5

    def __init__(self, area=None, **kwargs):
        ModbusRequest.__init__(self, **kwargs)
        self._area = area

    def encode(self):
        encoded = bytearray(1)
        encoded[0] = self._area
        return encoded

    def decode(self, data):
        self._area = data[0]

    def execute(self, context):
        #if not (1 <= self.count <= 0x7d0):
        #    return self.doException(ModbusExceptions.IllegalValue)
        #if not context.validate(self.function_code, self.address, self.count):
        #    return self.doException(ModbusExceptions.IllegalAddress)
        #values = context.getValues(self.function_code, self._area)
        #return BeginWriteUserModbusResponse(values)
        if not self._area in [MEMORY_AREA_FIRMWARE_M7,
                              MEMORY_AREA_FIRMWARE_M4,
                              MEMORY_AREA_SERIAL_NUMBER,
                              MEMORY_AREA_CONFIGURATION]:
            return self.doException(ModbusExceptions.IllegalAddress)
        return FinishWriteUserModbusResponse(self._area)



#
#
#   Cancel Write
#
#
class CancelWriteUserModbusResponse(ModbusResponse):
    function_code = USER_FUNC_CANCELWRITE
    _rtu_frame_size = 5

    def __init__(self, area=None, **kwargs):
        ModbusResponse.__init__(self, **kwargs)
        self._area = area
    
    def encode(self):
        encoded = bytearray(1)
        encoded[0] = self._area
        return encoded

    def decode(self, data):
        self._area = data[0]


class CancelWriteUserModbusRequest(ModbusRequest):
    function_code = USER_FUNC_CANCELWRITE
    _rtu_frame_size = 5

    def __init__(self, area=None, **kwargs):
        ModbusRequest.__init__(self, **kwargs)
        self._area = area

    def encode(self):
        encoded = bytearray(1)
        encoded[0] = self._area
        return encoded

    def decode(self, data):
        self._area = data[0]

    def execute(self, context):
        #if not (1 <= self.count <= 0x7d0):
        #    return self.doException(ModbusExceptions.IllegalValue)
        #if not context.validate(self.function_code, self.address, self.count):
        #    return self.doException(ModbusExceptions.IllegalAddress)
        #values = context.getValues(self.function_code, self._area)
        #return BeginWriteUserModbusResponse(values)
        if not self._area in [MEMORY_AREA_FIRMWARE_M7,
                              MEMORY_AREA_FIRMWARE_M4,
                              MEMORY_AREA_SERIAL_NUMBER,
                              MEMORY_AREA_CONFIGURATION]:
            return self.doException(ModbusExceptions.IllegalAddress)
        return CancelWriteUserModbusResponse(self._area)
