# Helper functions for formatting text

import string

printable = string.ascii_letters + string.digits + string.punctuation + ' '
printable_bytes = printable.encode('ascii')

def hex_escape_bytes(s):
    return ''.join(chr(c) if c in printable_bytes else r'\x{0:02x}'.format(c) for c in s)


