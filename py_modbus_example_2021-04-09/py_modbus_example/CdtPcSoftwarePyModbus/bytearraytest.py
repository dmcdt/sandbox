string = "Python is interesting."

# string with encoding 'utf-8'
arr = bytearray(string, 'utf-8')
print(arr)

size = 5

arr = bytearray(size)
print(arr)

rList = [1, 2, 3, 4, 5]

arr = bytearray(rList)
print(arr)

configuration_blob = bytearray(iter(range(16)))
print (configuration_blob)

serial_number = bytearray("fedcba987654321", 'utf-8')