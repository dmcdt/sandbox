# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'CereusTestApp.ui'
##
## Created by: Qt User Interface Compiler version 6.1.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import *  # type: ignore
from PySide6.QtGui import *  # type: ignore
from PySide6.QtWidgets import *  # type: ignore

from pyqtgraph import PlotWidget


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(462, 638)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout_2 = QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.label = QLabel(self.centralwidget)
        self.label.setObjectName(u"label")

        self.horizontalLayout_4.addWidget(self.label)

        self.baud = QComboBox(self.centralwidget)
        self.baud.addItem("")
        self.baud.addItem("")
        self.baud.addItem("")
        self.baud.addItem("")
        self.baud.addItem("")
        self.baud.setObjectName(u"baud")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(3)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.baud.sizePolicy().hasHeightForWidth())
        self.baud.setSizePolicy(sizePolicy)
        self.baud.setEditable(True)

        self.horizontalLayout_4.addWidget(self.baud)

        self.label_3 = QLabel(self.centralwidget)
        self.label_3.setObjectName(u"label_3")

        self.horizontalLayout_4.addWidget(self.label_3)

        self.dataBits = QComboBox(self.centralwidget)
        self.dataBits.addItem("")
        self.dataBits.addItem("")
        self.dataBits.addItem("")
        self.dataBits.addItem("")
        self.dataBits.setObjectName(u"dataBits")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(1)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.dataBits.sizePolicy().hasHeightForWidth())
        self.dataBits.setSizePolicy(sizePolicy1)

        self.horizontalLayout_4.addWidget(self.dataBits)

        self.label_4 = QLabel(self.centralwidget)
        self.label_4.setObjectName(u"label_4")

        self.horizontalLayout_4.addWidget(self.label_4)

        self.parity = QComboBox(self.centralwidget)
        self.parity.addItem("")
        self.parity.addItem("")
        self.parity.addItem("")
        self.parity.addItem("")
        self.parity.addItem("")
        self.parity.setObjectName(u"parity")
        sizePolicy2 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(2)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.parity.sizePolicy().hasHeightForWidth())
        self.parity.setSizePolicy(sizePolicy2)

        self.horizontalLayout_4.addWidget(self.parity)

        self.label_5 = QLabel(self.centralwidget)
        self.label_5.setObjectName(u"label_5")

        self.horizontalLayout_4.addWidget(self.label_5)

        self.stopBits = QComboBox(self.centralwidget)
        self.stopBits.addItem("")
        self.stopBits.addItem("")
        self.stopBits.addItem("")
        self.stopBits.setObjectName(u"stopBits")
        sizePolicy1.setHeightForWidth(self.stopBits.sizePolicy().hasHeightForWidth())
        self.stopBits.setSizePolicy(sizePolicy1)

        self.horizontalLayout_4.addWidget(self.stopBits)


        self.gridLayout_2.addLayout(self.horizontalLayout_4, 1, 0, 1, 1)

        self.closePort = QPushButton(self.centralwidget)
        self.closePort.setObjectName(u"closePort")
        sizePolicy3 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.closePort.sizePolicy().hasHeightForWidth())
        self.closePort.setSizePolicy(sizePolicy3)

        self.gridLayout_2.addWidget(self.closePort, 1, 1, 1, 1)

        self.openPort = QPushButton(self.centralwidget)
        self.openPort.setObjectName(u"openPort")
        sizePolicy3.setHeightForWidth(self.openPort.sizePolicy().hasHeightForWidth())
        self.openPort.setSizePolicy(sizePolicy3)

        self.gridLayout_2.addWidget(self.openPort, 0, 1, 1, 1)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.label_2 = QLabel(self.centralwidget)
        self.label_2.setObjectName(u"label_2")

        self.horizontalLayout_3.addWidget(self.label_2)

        self.portName = QComboBox(self.centralwidget)
        self.portName.setObjectName(u"portName")
        sizePolicy1.setHeightForWidth(self.portName.sizePolicy().hasHeightForWidth())
        self.portName.setSizePolicy(sizePolicy1)
        self.portName.setEditable(True)

        self.horizontalLayout_3.addWidget(self.portName)


        self.gridLayout_2.addLayout(self.horizontalLayout_3, 0, 0, 1, 1)


        self.verticalLayout.addLayout(self.gridLayout_2)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.label_7 = QLabel(self.centralwidget)
        self.label_7.setObjectName(u"label_7")
        sizePolicy4 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.label_7.sizePolicy().hasHeightForWidth())
        self.label_7.setSizePolicy(sizePolicy4)

        self.horizontalLayout_5.addWidget(self.label_7)

        self.modbusAddress = QSpinBox(self.centralwidget)
        self.modbusAddress.setObjectName(u"modbusAddress")
        sizePolicy5 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy5.setHorizontalStretch(1)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.modbusAddress.sizePolicy().hasHeightForWidth())
        self.modbusAddress.setSizePolicy(sizePolicy5)
        self.modbusAddress.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.modbusAddress.setMinimum(1)
        self.modbusAddress.setMaximum(254)
        self.modbusAddress.setValue(32)
        self.modbusAddress.setDisplayIntegerBase(10)

        self.horizontalLayout_5.addWidget(self.modbusAddress)

        self.label_8 = QLabel(self.centralwidget)
        self.label_8.setObjectName(u"label_8")
        sizePolicy4.setHeightForWidth(self.label_8.sizePolicy().hasHeightForWidth())
        self.label_8.setSizePolicy(sizePolicy4)

        self.horizontalLayout_5.addWidget(self.label_8)

        self.modbusAddressHex = QLineEdit(self.centralwidget)
        self.modbusAddressHex.setObjectName(u"modbusAddressHex")
        sizePolicy5.setHeightForWidth(self.modbusAddressHex.sizePolicy().hasHeightForWidth())
        self.modbusAddressHex.setSizePolicy(sizePolicy5)
        self.modbusAddressHex.setAutoFillBackground(False)
        self.modbusAddressHex.setReadOnly(True)

        self.horizontalLayout_5.addWidget(self.modbusAddressHex)

        self.horizontalSpacer_3 = QSpacerItem(150, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_5.addItem(self.horizontalSpacer_3)


        self.verticalLayout.addLayout(self.horizontalLayout_5)

        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.serialNumber = QLineEdit(self.centralwidget)
        self.serialNumber.setObjectName(u"serialNumber")
        self.serialNumber.setAutoFillBackground(False)
        self.serialNumber.setReadOnly(True)

        self.gridLayout.addWidget(self.serialNumber, 1, 1, 1, 1)

        self.readSerialNumber = QPushButton(self.centralwidget)
        self.readSerialNumber.setObjectName(u"readSerialNumber")

        self.gridLayout.addWidget(self.readSerialNumber, 1, 0, 1, 1)


        self.verticalLayout.addLayout(self.gridLayout)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.writeConfiguration = QPushButton(self.centralwidget)
        self.writeConfiguration.setObjectName(u"writeConfiguration")
        sizePolicy6 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy6.setHorizontalStretch(1)
        sizePolicy6.setVerticalStretch(0)
        sizePolicy6.setHeightForWidth(self.writeConfiguration.sizePolicy().hasHeightForWidth())
        self.writeConfiguration.setSizePolicy(sizePolicy6)

        self.horizontalLayout.addWidget(self.writeConfiguration)

        self.queryServer = QPushButton(self.centralwidget)
        self.queryServer.setObjectName(u"queryServer")
        sizePolicy6.setHeightForWidth(self.queryServer.sizePolicy().hasHeightForWidth())
        self.queryServer.setSizePolicy(sizePolicy6)

        self.horizontalLayout.addWidget(self.queryServer)

        self.takeSamples = QPushButton(self.centralwidget)
        self.takeSamples.setObjectName(u"takeSamples")
        sizePolicy6.setHeightForWidth(self.takeSamples.sizePolicy().hasHeightForWidth())
        self.takeSamples.setSizePolicy(sizePolicy6)

        self.horizontalLayout.addWidget(self.takeSamples)

        self.takeQuiescent = QPushButton(self.centralwidget)
        self.takeQuiescent.setObjectName(u"takeQuiescent")

        self.horizontalLayout.addWidget(self.takeQuiescent)

        self.readSamples = QPushButton(self.centralwidget)
        self.readSamples.setObjectName(u"readSamples")
        sizePolicy6.setHeightForWidth(self.readSamples.sizePolicy().hasHeightForWidth())
        self.readSamples.setSizePolicy(sizePolicy6)

        self.horizontalLayout.addWidget(self.readSamples)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.label_6 = QLabel(self.centralwidget)
        self.label_6.setObjectName(u"label_6")
        sizePolicy7 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Minimum)
        sizePolicy7.setHorizontalStretch(0)
        sizePolicy7.setVerticalStretch(0)
        sizePolicy7.setHeightForWidth(self.label_6.sizePolicy().hasHeightForWidth())
        self.label_6.setSizePolicy(sizePolicy7)
        self.label_6.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)

        self.horizontalLayout_2.addWidget(self.label_6)

        self.plainTextEdit = QPlainTextEdit(self.centralwidget)
        self.plainTextEdit.setObjectName(u"plainTextEdit")
        sizePolicy8 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy8.setHorizontalStretch(0)
        sizePolicy8.setVerticalStretch(0)
        sizePolicy8.setHeightForWidth(self.plainTextEdit.sizePolicy().hasHeightForWidth())
        self.plainTextEdit.setSizePolicy(sizePolicy8)

        self.horizontalLayout_2.addWidget(self.plainTextEdit)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.graphicsView = PlotWidget(self.centralwidget)
        self.graphicsView.setObjectName(u"graphicsView")
        sizePolicy9 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy9.setHorizontalStretch(1)
        sizePolicy9.setVerticalStretch(1)
        sizePolicy9.setHeightForWidth(self.graphicsView.sizePolicy().hasHeightForWidth())
        self.graphicsView.setSizePolicy(sizePolicy9)

        self.horizontalLayout_6.addWidget(self.graphicsView)

        self.gridLayout_3 = QGridLayout()
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.runAcquisition = QPushButton(self.centralwidget)
        self.runAcquisition.setObjectName(u"runAcquisition")

        self.gridLayout_3.addWidget(self.runAcquisition, 0, 0, 1, 1)

        self.stopAcquisition = QPushButton(self.centralwidget)
        self.stopAcquisition.setObjectName(u"stopAcquisition")

        self.gridLayout_3.addWidget(self.stopAcquisition, 1, 0, 1, 1)

        self.saveData = QPushButton(self.centralwidget)
        self.saveData.setObjectName(u"saveData")

        self.gridLayout_3.addWidget(self.saveData, 2, 0, 1, 1)


        self.horizontalLayout_6.addLayout(self.gridLayout_3)


        self.verticalLayout.addLayout(self.horizontalLayout_6)


        self.verticalLayout_2.addLayout(self.verticalLayout)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 462, 21))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        self.baud.setCurrentIndex(4)
        self.dataBits.setCurrentIndex(3)
        self.parity.setCurrentIndex(4)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Cereus Test Software", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"Baud", None))
        self.baud.setItemText(0, QCoreApplication.translate("MainWindow", u"9600", None))
        self.baud.setItemText(1, QCoreApplication.translate("MainWindow", u"19200", None))
        self.baud.setItemText(2, QCoreApplication.translate("MainWindow", u"38400", None))
        self.baud.setItemText(3, QCoreApplication.translate("MainWindow", u"57600", None))
        self.baud.setItemText(4, QCoreApplication.translate("MainWindow", u"115200", None))

        self.label_3.setText(QCoreApplication.translate("MainWindow", u"Data", None))
        self.dataBits.setItemText(0, QCoreApplication.translate("MainWindow", u"5", None))
        self.dataBits.setItemText(1, QCoreApplication.translate("MainWindow", u"6", None))
        self.dataBits.setItemText(2, QCoreApplication.translate("MainWindow", u"7", None))
        self.dataBits.setItemText(3, QCoreApplication.translate("MainWindow", u"8", None))

        self.label_4.setText(QCoreApplication.translate("MainWindow", u"Parity", None))
        self.parity.setItemText(0, QCoreApplication.translate("MainWindow", u"None", None))
        self.parity.setItemText(1, QCoreApplication.translate("MainWindow", u"Mark", None))
        self.parity.setItemText(2, QCoreApplication.translate("MainWindow", u"Space", None))
        self.parity.setItemText(3, QCoreApplication.translate("MainWindow", u"Odd", None))
        self.parity.setItemText(4, QCoreApplication.translate("MainWindow", u"Even", None))

        self.label_5.setText(QCoreApplication.translate("MainWindow", u"Stop", None))
        self.stopBits.setItemText(0, QCoreApplication.translate("MainWindow", u"1", None))
        self.stopBits.setItemText(1, QCoreApplication.translate("MainWindow", u"1.5", None))
        self.stopBits.setItemText(2, QCoreApplication.translate("MainWindow", u"2", None))

        self.closePort.setText(QCoreApplication.translate("MainWindow", u"Close", None))
        self.openPort.setText(QCoreApplication.translate("MainWindow", u"Open", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"COM port", None))
        self.portName.setCurrentText(QCoreApplication.translate("MainWindow", u"COM3", None))
        self.label_7.setText(QCoreApplication.translate("MainWindow", u"Device Modbus Address", None))
        self.label_8.setText(QCoreApplication.translate("MainWindow", u"==", None))
        self.serialNumber.setText("")
        self.serialNumber.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Not read yet", None))
        self.readSerialNumber.setText(QCoreApplication.translate("MainWindow", u"Read S/N", None))
        self.writeConfiguration.setText(QCoreApplication.translate("MainWindow", u"Write Configuration", None))
        self.queryServer.setText(QCoreApplication.translate("MainWindow", u"Query Server", None))
        self.takeSamples.setText(QCoreApplication.translate("MainWindow", u"Take Samples", None))
        self.takeQuiescent.setText(QCoreApplication.translate("MainWindow", u"Take Quiet", None))
        self.readSamples.setText(QCoreApplication.translate("MainWindow", u"Read Samples", None))
        self.label_6.setText(QCoreApplication.translate("MainWindow", u"Received", None))
        self.runAcquisition.setText(QCoreApplication.translate("MainWindow", u"Run", None))
        self.stopAcquisition.setText(QCoreApplication.translate("MainWindow", u"Stop", None))
        self.saveData.setText(QCoreApplication.translate("MainWindow", u"Save Data", None))
    # retranslateUi

