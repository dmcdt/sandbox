# Simulation of CDT tool modbus interface, as for running example CDT modbus
# code against.
#
# Requires Pyside2 (Qt) for user interface.
# Requires modbus_tk, pyserial for performing the modbus comms.

import sys
from PySide2.QtWidgets import QApplication, QMainWindow, QMessageBox
from PySide2.QtCore import Slot
import uiLoader
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
import serial
import pandas as pd
import binascii
import CustomModbusFunctions
from CustomModbusFunctions import BeginWriteUserModbusRequest
from CustomModbusFunctions import WriteDataUserModbusRequest
from CustomModbusFunctions import FinishWriteUserModbusRequest
from CustomModbusFunctions import CancelWriteUserModbusRequest

# The address of the server on the bus.
serverUnitBusAddress = 0x10

#
class MainWindow(QMainWindow):
    #
    def __init__(self, parent=None):
        #
        QMainWindow.__init__(self, parent)
        self._server = None
        self._serverThread = None
        uiLoader.loadUi('untitled.ui', self)

    # Get the serial configuration to use. Firmware will use 115200,8,E,1
    def extract_serial_parameters(self):
        self._port_path = self.portName.text()
        self._baud_rate = int(self.baud.currentText())
        self._data_bits = int(self.dataBits.currentText())
        self._parity = [serial.PARITY_NONE, serial.PARITY_MARK, serial.PARITY_SPACE, serial.PARITY_ODD, serial.PARITY_EVEN][self.parity.currentIndex()]
        self._stop_bits = [serial.STOPBITS_ONE, serial.STOPBITS_ONE_POINT_FIVE, serial.STOPBITS_TWO][self.stopBits.currentIndex()]

    @Slot()
    def on_queryServer_clicked(self):
        self.extract_serial_parameters()
        client = ModbusClient(method='rtu', timeout=5,
                              port=self._port_path,
                              baudrate=self._baud_rate,
                              parity=self._parity)
        if client.connect():
            self.plainTextEdit.appendPlainText('Read input registers\r\n')
            # These register addresses are PDU  addresses, so are 0 based.
            # This is the opposite of what you set up in the server. WTF!
            rr = client.read_input_registers(6, 8, unit=serverUnitBusAddress)
            self.plainTextEdit.appendPlainText(str(rr))
            for rv in rr.registers:
                self.plainTextEdit.appendPlainText('    0x%04X' % rv)

            client.close()
        else:
            QMessageBox.critical(self, 'Error', 'Could not open port \'' + self._port_path + '\'')

    @Slot()
    def on_writeConfiguration_clicked(self):
        # The dummy configuration blob to send
        address = 0

        version_dict = {
                        'BLOB_ID'    : 0x40,
                        'EQ_Version' : 0x01
        }
        
        # Read in the file using all the data and giving columns names
        txFile0 = pd.read_csv('ToF - SINC_2.00M _3.50M __EQUALISED_TX.txt',
                              sep='\t', header=None, names=['timebase', 'originalData'])
        # Calculate the bit ratio for the storage in MCU
        txFile0['bitRatio'] = (
            round(((txFile0['originalData'] + 1) / 2) * ((2**14)-1))).astype(int)
        # Change the calculated column into a dictionary to parse as a byte array
        intDict0 = txFile0['bitRatio'].to_dict()
        # Parse the dict into bytearray
        transmit_packet = bytearray(0)

        for key in version_dict.keys():
            transmit_packet.extend(
                version_dict[key].to_bytes(1, byteorder='little'))
        for key in intDict0.keys():
            transmit_packet.extend(
                intDict0[key].to_bytes(2, byteorder='little'))

        CRC_value = (binascii.crc32(transmit_packet) ^ 0xffffffff)

        transmit_packet.extend(CRC_value.to_bytes(4, byteorder='little'))

        # Create the client object
        self.extract_serial_parameters()
        client = ModbusClient(method='rtu', timeout=5,
                              port=self._port_path,
                              baudrate=self._baud_rate,
                              parity=self._parity)

        # We need to tell the client about our custom response messages
        client.register(CustomModbusFunctions.BeginWriteUserModbusResponse)
        client.register(CustomModbusFunctions.WriteDataUserModbusResponse)
        client.register(CustomModbusFunctions.FinishWriteUserModbusResponse)
        client.register(CustomModbusFunctions.CancelWriteUserModbusResponse)

        # Tell the client to connect to the server
        if client.connect():
            # Create the request message to send. Note that it must include
            # the server bus address as the unit named parameter.
            request = BeginWriteUserModbusRequest(CustomModbusFunctions.MEMORY_TRANSMITWAVEFORM1,
                                                  unit=serverUnitBusAddress)

            # Used in testing  - self.plainTextEdit.appendPlainText(str(request))
            result = client.execute(request)
            if not result.isError():
                self.plainTextEdit.appendPlainText('BeginWrite successful\r\n')
            else:
                self.plainTextEdit.appendPlainText('BeginWrite error\r\n' + str(result))

            # Maximum number of bytes that can be written in one command
            # (as defined by our own custom function code in the firmware)
            #  = Modbus max length (256) - unit address length (1) - 
            #                              function code length (1) -
            #                              function code structure:
            #                               > address (4) 
            #                               > length of packet (1)
            #                               > version number (2) -
            #                              crc length (4)
            #  = 256 - 1 - 1 - (4 + 1 + 2) - 4
            #  = 245
            maximum_bytes = 242 # changed from 245 due to failure on mcu modbus side: mbrtu.c | line 157

            # Just a little something for information purposes
            write_count = 1

            # Loop to write all the configuration data in blocks
            # of up to maximum_bytes
            while address < len(transmit_packet):
                bytes_to_write = len(transmit_packet) - address
                if (bytes_to_write > maximum_bytes):
                    bytes_to_write = maximum_bytes
                
                request = WriteDataUserModbusRequest(address,
                                                     transmit_packet[address:address +
                                                                     bytes_to_write],
                                                     unit=serverUnitBusAddress)
                
                result = client.execute(request)
                if not result.isError():
                    self.plainTextEdit.appendPlainText('SUCCESS ' + str(write_count) +'\r\n' +
                                                        str(bytes_to_write) +
                                                        ' bytes written to address ' +
                                                        str(address) +
                                                        '\r\n')
                else:
                    self.plainTextEdit.appendPlainText('WriteData ' +
                                                        str(bytes_to_write) +
                                                        ' error\r\n' + 
                                                        str(result))
                
                address += bytes_to_write
                write_count += 1

            request = FinishWriteUserModbusRequest(CustomModbusFunctions.MEMORY_TRANSMITWAVEFORM1,
                                                  unit=serverUnitBusAddress)
            result = client.execute(request)
            if not result.isError():
                self.plainTextEdit.appendPlainText('FinishWrite successful\r\n')
            else:
                self.plainTextEdit.appendPlainText('FinishWrite error\r\n' +
                                                    str(result)+ '\r\n')

            # TODO - probably need a short delay in here while the data is written
            #        to non-volatile memory

            # TODO - other things here, such as polling for status, read back
            #        configuration to check that it has programmed correctly
            rr = client.read_input_registers(24, 2, unit=serverUnitBusAddress)
            received_crc = rr.registers[0] << 16 | rr.registers[1]

            self.plainTextEdit.appendPlainText(
                'Sent CRC       = ' + hex(CRC_value))
            self.plainTextEdit.appendPlainText(
                'Received CRC = ' + hex(received_crc))

            # Close the client to end communications
            client.close()
        else:
            QMessageBox.critical(self, 'Error', 'Could not open port \'' + self._port_path + '\'')

#
def main():
    app = QApplication(sys.argv)
    mainWindow = MainWindow()
    mainWindow.show()
    sys.exit(app.exec_())

#
if (__name__ == '__main__'):
    main()

