# Simulation of CDT tool modbus interface, as for running example CDT modbus
# code against.
#
# Requires Pyside2 (Qt) for user interface.
# Requires modbus_tk, pyserial for performing the modbus comms.

import sys
from PySide2.QtWidgets import QApplication, QMainWindow, QMessageBox
from PySide2.QtCore import Slot
from serial import serialutil
import uiLoader
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
import serial
import serial.tools.list_ports
import CustomModbusFunctions
from CustomModbusFunctions import BeginWriteUserModbusRequest
from CustomModbusFunctions import WriteDataUserModbusRequest
from CustomModbusFunctions import FinishWriteUserModbusRequest
from CustomModbusFunctions import CancelWriteUserModbusRequest
import MainboardModbusRegisters
import RotatingHeadModbusRegisters


# The address of the servers on the bus.
SERVERBUSADDRESS_MAINBOARD  = 0x10
SERVERBUSADDRESS_ROTATING   = 0x20
serverUnitBusAddress        = 0x20

# Definitions for the Device Type modbus register
DEVICETYPE_INVALID                  = 0x0000
DEVICETYPE_VALIDUS_STATICMAINBOARD  = 0x0001
DEVICETYPE_VALIDUS_ROTATINGHEAD     = 0x0002
DEVICETYPE_MASK_DEVICE              = 0x7FFF
DEVICETYPE_MASK_BOOTLOADER          = 0x8000
DEVICETYPE_FLAG_APPLICATIONACTIVE   = 0x0000
DEVICETYPE_FLAG_BOOTLOADERACTIVE    = 0x8000


#
class MainWindow(QMainWindow):
    #
    def __init__(self, parent=None):
        #
        self._serverUnitBusAddress = SERVERBUSADDRESS_MAINBOARD
        self._port_path = None
        self._baud_rate = 115200
        self._data_bits = 8
        self._parity = serial.PARITY_NONE
        self._stop_bits = serial.STOPBITS_ONE

        QMainWindow.__init__(self, parent)
        self._server = None
        self._serverThread = None
        uiLoader.loadUi('PcExample.ui', self)

        self._populate_comports()

        self.namedAddress.addItem('Quick set ->', 0)
        self.namedAddress.addItem('Mainboard', SERVERBUSADDRESS_MAINBOARD)
        self.namedAddress.addItem('Rotating Head', SERVERBUSADDRESS_ROTATING)
        self.modbusAddress.setValue(self._serverUnitBusAddress)

    def _populate_comports(self):
        available_ports = serial.tools.list_ports.comports()
        for port in available_ports:
            self.portName.addItem(str(port), port.name)

    # Get the serial configuration to use. Firmware will use 115200,8,E,1
    def extract_serial_parameters(self):
        if self.portName.currentIndex() != -1:
            self._port_path = self.portName.itemData(self.portName.currentIndex())
        else:
            self._port_path = self.portName.text()
        self._baud_rate = int(self.baud.currentText())
        self._data_bits = int(self.dataBits.currentText())
        self._parity = [serial.PARITY_NONE, serial.PARITY_MARK, serial.PARITY_SPACE, serial.PARITY_ODD, serial.PARITY_EVEN][self.parity.currentIndex()]
        self._stop_bits = [serial.STOPBITS_ONE, serial.STOPBITS_ONE_POINT_FIVE, serial.STOPBITS_TWO][self.stopBits.currentIndex()]

    def device_description(self, deviceType):
        descriptions = {DEVICETYPE_VALIDUS_STATICMAINBOARD  : 'Validus Mainboard',
                        DEVICETYPE_VALIDUS_ROTATINGHEAD     : 'Validus Rotating Head'}
        
        description = 'Unknown tool'
        deviceOnly = deviceType & DEVICETYPE_MASK_DEVICE
        if deviceOnly in descriptions.keys():
            description = descriptions[deviceOnly]
            if (deviceType & DEVICETYPE_MASK_BOOTLOADER) == DEVICETYPE_FLAG_BOOTLOADERACTIVE:
                description = description + ' Bootloader'

        return description

    @Slot(int)
    def on_namedAddress_currentIndexChanged(self, index):
        if index > 0:
            self.modbusAddress.setValue(self.namedAddress.itemData(index))
            self.namedAddress.setCurrentIndex(0)
    
    @Slot(int)
    def on_modbusAddress_valueChanged(self, value):
        self._serverUnitBusAddress = value
        self.modbusAddressHex.setText(f"0x{self._serverUnitBusAddress:x}")

    @Slot()
    def on_getIdentity_clicked(self):
        self.extract_serial_parameters()
        client = ModbusClient(method='rtu', timeout=5,
                              port=self._port_path,
                              baudrate=self._baud_rate,
                              parity=self._parity)
        if client.connect():
            self.plainTextEdit.appendPlainText('Read identity input registers\r\n')
            # These register addresses are PDU  addresses, so are 0 based.
            # This is the opposite of what you set up in the server. WTF!
            start_register = MainboardModbusRegisters.InputRegisters.MBIR_CompanyId0
            number_registers = MainboardModbusRegisters.InputRegisters.MBIR_SerialNumber14_15 - start_register + 1
            rr = client.read_input_registers(start_register,
                                            number_registers,
                                            unit=self._serverUnitBusAddress)
            self.plainTextEdit.appendPlainText(str(rr))
            for rv in rr.registers:
                self.plainTextEdit.appendPlainText('    0x%04X' % rv)

            # rr = [0x4344, 0x5400, 0x0001, 0x0001, 0x0203, 0xDEAD, 0xC0DE,
            #       0x3132, 0x3334, 0x3536, 0x3738, 0x3930, 0x4142, 0x4344, 0x4546]

            reg = rr.registers
            if len(reg) >= 15:
                cid = (reg[0] << 16) | reg[1]
                company_description = cid.to_bytes(4, 'big').rstrip(b'\x00')

                device_description = self.device_description(reg[2])

                version_major = reg[3] & 0xFF
                version_minor = (reg[4] >> 8) & 0xFF
                version_patch = reg[4] & 0xFF

                firmware_checksum = (reg[5] << 16) | reg[6]

                sn = b''.join(b.to_bytes(2, 'little') for b in reg[7:15])

                identity = '{0} {1} v{2}.{3}.{4}-{5} S/N {6}'.format(
                                    company_description.decode('ascii'),
                                    device_description,
                                    version_major, version_minor, version_patch,
                                    hex(firmware_checksum),
                                    sn.decode('ascii'))

                self.identity.setText(identity)
            else:
                self.identity.setText('Error')

            client.close()
        else:
            QMessageBox.critical(self, 'Error', 'Could not open port \'' + self._port_path + '\'')


    @Slot()
    def on_queryServer_clicked(self):
        self.extract_serial_parameters()
        client = ModbusClient(method='rtu', timeout=5,
                              port=self._port_path,
                              baudrate=self._baud_rate,
                              parity=self._parity)
        if client.connect():
            self.plainTextEdit.appendPlainText('Read input registers\r\n')
            # These register addresses are PDU  addresses, so are 0 based.
            # This is the opposite of what you set up in the server. WTF!
            rr = client.read_input_registers(6, 8, unit=self._serverUnitBusAddress)
            self.plainTextEdit.appendPlainText(str(rr))
            for rv in rr.registers:
                self.plainTextEdit.appendPlainText('    0x%04X' % rv)

            client.close()
        else:
            QMessageBox.critical(self, 'Error', 'Could not open port \'' + self._port_path + '\'')

    @Slot()
    def on_writeConfiguration_clicked(self):
        # The dummy configuration blob to send
        address = 0
        configuration_blob = bytearray(iter(range(256)))*4
        # print (configuration_blob)

        # Create the client object
        self.extract_serial_parameters()
        client = ModbusClient(method='rtu', timeout=5,
                              port=self._port_path,
                              baudrate=self._baud_rate,
                              parity=self._parity)

        # We need to tell the client about our custom response messages
        client.register(CustomModbusFunctions.BeginWriteUserModbusResponse)
        client.register(CustomModbusFunctions.WriteDataUserModbusResponse)
        client.register(CustomModbusFunctions.FinishWriteUserModbusResponse)
        client.register(CustomModbusFunctions.CancelWriteUserModbusResponse)

        # Tell the client to connect to the server
        if client.connect():
            # Create the request message to send. Note that it must include
            # the server bus address as the unit named parameter.
            request = BeginWriteUserModbusRequest(CustomModbusFunctions.MEMORY_AREA_CONFIGURATION,
                                                  unit=self._serverUnitBusAddress)
            result = client.execute(request)
            if not result.isError():
                self.plainTextEdit.appendPlainText('BeginWrite successful\r\n')

            # Maximum number of bytes that can be written in one command
            # (as defined by our own custom function code in the firmware)
            #  = Modbus max length (256) - unit address length (1) - 
            #                              function code length (1) -
            #                              function code structure (4 + 1) -
            #                              crc length (2)
            #  = 256 - 1 - 1 - (4 + 1) - 2
            #  = 247
            maximum_bytes = 247

            # Just a little something for information purposes
            write_count = 0

            # Loop to write all the configuration data in blocks
            # of up to maximum_bytes
            while address < len(configuration_blob):
                bytes_to_write = len(configuration_blob) - address
                if (bytes_to_write > maximum_bytes):
                    bytes_to_write = maximum_bytes
                
                request = WriteDataUserModbusRequest(address,
                                                     configuration_blob[address:address+bytes_to_write],
                                                     unit=self._serverUnitBusAddress)
                result = client.execute(request)
                if not result.isError():
                    self.plainTextEdit.appendPlainText('WriteData ' +
                                                        str(write_count) +
                                                        ' successful\r\n')
                
                address += bytes_to_write
                write_count += 1

            request = FinishWriteUserModbusRequest(CustomModbusFunctions.MEMORY_AREA_CONFIGURATION,
                                                  unit=self._serverUnitBusAddress)
            result = client.execute(request)
            if not result.isError():
                self.plainTextEdit.appendPlainText('FinishWrite successful\r\n')
            else:
                self.plainTextEdit.appendPlainText('FinishWrite error\r\n')

            # TODO - probably need a short delay in here while the data is written
            #        to non-volatile memory

            # TODO - other things here, such as polling for status, read back
            #        configuration to check that it has programmed correctly

            # Close the client to end communications
            client.close()
        else:
            QMessageBox.critical(self, 'Error', 'Could not open port \'' + self._port_path + '\'')

#
def main():
    app = QApplication(sys.argv)
    mainWindow = MainWindow()
    mainWindow.show()
    sys.exit(app.exec_())

#
if (__name__ == '__main__'):
    main()

