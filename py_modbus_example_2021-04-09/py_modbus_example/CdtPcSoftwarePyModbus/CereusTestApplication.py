# Simulation of CDT tool modbus interface, as for running example CDT modbus
# code against.
#
# Requires Pyside2 (Qt) for user interface.
# Requires modbus_tk, pyserial for performing the modbus comms.

import sys
import csv
import PySide2
from PySide2 import QtCore
from PySide2.QtCore import SIGNAL, Slot, QTimer
from PySide2.QtWidgets import QApplication, QMainWindow, QMessageBox, QDialog, QFileDialog
from numpy.core import numeric
import pyqtgraph
import uiLoader
from pymodbus.register_read_message import ReadInputRegistersResponse
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
import serial
import serial.tools.list_ports
import binascii
import CustomModbusFunctions
from CustomModbusFunctions import BeginWriteUserModbusRequest
from CustomModbusFunctions import WriteDataUserModbusRequest
from CustomModbusFunctions import FinishWriteUserModbusRequest
from CustomModbusFunctions import CancelWriteUserModbusRequest
import numpy as np
import string
import time
import os

from RotatingHeadModbusRegisters import InputRegisters, HoldingRegisters, RotatingHeadCommands
from FormattingHelpers import hex_escape_bytes

#
class MainWindow(QMainWindow):
    #
    def __init__(self, parent=None):
        #
        self._server = None
        self._serverThread = None
        self._serverUnitBusAddress = 32
        self._client = None
        self._port_path = None
        self._baud_rate = 115200
        self._data_bits = 8
        self._parity = serial.PARITY_NONE
        self._stop_bits = serial.STOPBITS_ONE
        self._samplesRaw = []
        self._runAcquisition = False
        self._sampleTimer = None
        self._autoIndex = 0

        customWidgets = {
                            'PlotWidget' : pyqtgraph.PlotWidget
        }
        QMainWindow.__init__(self, parent)
        uiLoader.loadUi('CereusTestApp.ui', self, customWidgets)

        self._sampleTimer = QTimer(self)
        self._sampleTimer.setSingleShot(True)
        self.connect(self._sampleTimer, SIGNAL('timeout()'), self.autoAcquire)
        
        self._populate_comports()
        self.on_modbusAddress_valueChanged(self._serverUnitBusAddress)
        self._configure_plot()

        self._set_ui_port_open_state(False)


    def _configure_plot(self):
        #self.graphicsView.disableAutoRange(axis=pyqtgraph.ViewBox.XYAxes)
        #self.graphicsView.setXRange(0, 10)
        #self.graphicsView.setYRange(-1, 1)
        self.graphicsView.getPlotItem().showGrid(True, True)
        #self.graphicsView.getPlotItem().setMouseEnabled(False, False)


    def _populate_comports(self):
        available_ports = serial.tools.list_ports.comports()
        for port in available_ports:
            self.portName.addItem(str(port), port.name)


    # Get the serial configuration to use. Firmware will use 115200,8,E,1
    def extract_serial_parameters(self):
        if self.portName.currentIndex() != -1:
            self._port_path = self.portName.itemData(self.portName.currentIndex())
        else:
            self._port_path = self.portName.text()
        self._baud_rate = int(self.baud.currentText())
        self._data_bits = int(self.dataBits.currentText())
        self._parity = [serial.PARITY_NONE, serial.PARITY_MARK, serial.PARITY_SPACE, serial.PARITY_ODD, serial.PARITY_EVEN][self.parity.currentIndex()]
        self._stop_bits = [serial.STOPBITS_ONE, serial.STOPBITS_ONE_POINT_FIVE, serial.STOPBITS_TWO][self.stopBits.currentIndex()]


    def _get_client(self):
        if self._client is None:
            self.extract_serial_parameters()
            self._client = ModbusClient(method='rtu', timeout=5,
                              port=self._port_path,
                              baudrate=self._baud_rate,
                              parity=self._parity)
            if self._client is None:
                QMessageBox.critical(self, 'Error', 'Could not open port \'' + self._port_path + '\'')
            else:
                self._set_ui_port_open_state(True)
        
        return self._client


    def _set_ui_port_open_state(self, state):
        self.openPort.setEnabled(not state)
        self.closePort.setEnabled(state)

    @Slot()
    def on_openPort_clicked(self):
        self._get_client()
        if self._client is not None:
            self._set_ui_port_open_state(True)

    @Slot()
    def on_closePort_clicked(self):
        if self._client is not None:
            self._client.close()
            self._client = None
        
        self._set_ui_port_open_state(False)

    @Slot(int)
    def on_modbusAddress_valueChanged(self, value):
        self._serverUnitBusAddress = value
        self.modbusAddressHex.setText(f"0x{self._serverUnitBusAddress:x}")

    @Slot()
    def on_readSerialNumber_clicked(self):
        # These register addresses are PDU  addresses, so are 0 based.
        # This is the opposite of what you set up in the server. WTF!
        client = self._get_client()
        if client is not None:
            readResponse = client.read_input_registers(InputRegisters.MBIR_SerialNumber0_1,
                                                       InputRegisters.MBIR_SerialNumber14_15 - InputRegisters.MBIR_SerialNumber0_1,
                                                       unit=self._serverUnitBusAddress)
            self.plainTextEdit.appendPlainText(str(readResponse))

            serialNumber = b''.join([rv.to_bytes(2, 'big') for rv in readResponse.registers])
            self.serialNumber.setText(hex_escape_bytes(serialNumber))
    
    @Slot()
    def on_queryServer_clicked(self):
        self.extract_serial_parameters()
        client = ModbusClient(method='rtu', timeout=5,
                              port=self._port_path,
                              baudrate=self._baud_rate,
                              parity=self._parity)
        if client.connect():
            self.plainTextEdit.appendPlainText('Read input registers\r\n')
            # These register addresses are PDU  addresses, so are 0 based.
            # This is the opposite of what you set up in the server. WTF!
            rr = client.read_input_registers(6, 8, unit=self._serverUnitBusAddress)
            self.plainTextEdit.appendPlainText(str(rr))
            for rv in rr.registers:
                self.plainTextEdit.appendPlainText('    0x%04X' % rv)

            client.close()
        else:
            QMessageBox.critical(self, 'Error', 'Could not open port \'' + self._port_path + '\'')

    @Slot()
    def on_writeConfiguration_clicked(self):
        # The dummy configuration blob to send
        address = 0
        
        version_dict = {
                        'BLOB_ID'    : 0x10,
                        'SN_Version' : 0x01
        }

        transmit_packet = bytearray(0)
        for key in version_dict.keys():
            transmit_packet.extend(version_dict[key].to_bytes(1, byteorder='big'))
        
        serial_number = "fedcba9876"

        transmit_packet.extend(bytearray(serial_number, 'ascii'))
        if (len(serial_number) < 16):
            transmit_packet.extend(bytearray(16-len(serial_number)))
        
        CRC_value = (binascii.crc32(transmit_packet) ^ 0xffffffff)

        transmit_packet.extend(CRC_value.to_bytes(4, byteorder='little'))

        # Create the client object
        self.extract_serial_parameters()
        client = ModbusClient(method='rtu', timeout=5,
                              port=self._port_path,
                              baudrate=self._baud_rate,
                              parity=self._parity)

        # We need to tell the client about our custom response messages
        client.register(CustomModbusFunctions.BeginWriteUserModbusResponse)
        client.register(CustomModbusFunctions.WriteDataUserModbusResponse)
        client.register(CustomModbusFunctions.FinishWriteUserModbusResponse)
        client.register(CustomModbusFunctions.CancelWriteUserModbusResponse)

        # Tell the client to connect to the server
        if client.connect():
            # Create the request message to send. Note that it must include
            # the server bus address as the unit named parameter.
            request = BeginWriteUserModbusRequest(CustomModbusFunctions.MEMORY_AREA_SERIAL_NUMBER,
                                                  unit=self._serverUnitBusAddress)

            # Used in testing  - self.plainTextEdit.appendPlainText(str(request))
            result = client.execute(request)
            if not result.isError():
                self.plainTextEdit.appendPlainText('BeginWrite successful\r\n')
            else:
                self.plainTextEdit.appendPlainText('BeginWrite error\r\n' + str(result))

            # Maximum number of bytes that can be written in one command
            # (as defined by our own custom function code in the firmware)
            #  = Modbus max length (256) - unit address length (1) - 
            #                              function code length (1) -
            #                              function code structure:
            #                               > address (4) 
            #                               > length of packet (1)
            #                               > version number (2) -
            #                              crc length (4)
            #  = 256 - 1 - 1 - (4 + 1 + 2) - 4
            #  = 243
            maximum_bytes = 242 # changed from 245 due to failure on mcu modbus side: mbrtu.c | line 157

            # Just a little something for information purposes
            write_count = 1

            # Loop to write all the configuration data in blocks
            # of up to maximum_bytes
            while address < len(transmit_packet):
                bytes_to_write = len(transmit_packet) - address
                if (bytes_to_write > maximum_bytes):
                    bytes_to_write = maximum_bytes
                
                request = WriteDataUserModbusRequest(address,
                                                     transmit_packet[address:address+bytes_to_write],
                                                     unit=self._serverUnitBusAddress)
                # Used in testing  - self.plainTextEdit.appendPlainText(f'{address =}\r\n')
                result = client.execute(request)
                if not result.isError():
                    self.plainTextEdit.appendPlainText('SUCCESS ' + str(write_count) +'\r\n' +
                                                        str(bytes_to_write) +
                                                        ' bytes written to address ' +
                                                        str(address) +
                                                        '\r\n')
                else:
                    self.plainTextEdit.appendPlainText('WriteData ' +
                                                        str(bytes_to_write) +
                                                        ' error\r\n' + 
                                                        str(result))
                
                address += bytes_to_write
                write_count += 1

            request = FinishWriteUserModbusRequest(CustomModbusFunctions.MEMORY_AREA_SERIAL_NUMBER,
                                                  unit=self._serverUnitBusAddress)
            result = client.execute(request)
            if not result.isError():
                self.plainTextEdit.appendPlainText('FinishWrite successful\r\n')
            else:
                self.plainTextEdit.appendPlainText('FinishWrite error\r\n' +
                                                    str(result)+ '\r\n')

            # TODO - probably need a short delay in here while the data is written
            #        to non-volatile memory

            # TODO - other things here, such as polling for status, read back
            #        configuration to check that it has programmed correctly

            rr = client.read_input_registers(24, 2, unit=self._serverUnitBusAddress)
            received_crc = rr.registers[0] << 16 | rr.registers[1]
            
            self.plainTextEdit.appendPlainText('Sent CRC       = ' + hex(CRC_value))
            self.plainTextEdit.appendPlainText('Received CRC = ' + hex(received_crc))
            

            # Close the client to end communications
            client.close()
        else:
            QMessageBox.critical(self, 'Error', 'Could not open port \'' + self._port_path + '\'')


    @Slot()
    def on_takeSamples_clicked(self):
        command = RotatingHeadCommands.RH_SyncTestTrigger
        if not self.fullSamples.isChecked():
            command = RotatingHeadCommands.RH_SyncQuiescentTrigger
        
        if self._get_client() is not None:
            writeResponse = self._client.write_register(HoldingRegisters.MBHR_RH_Commands,
                                                        command,
                                                        unit=self._serverUnitBusAddress)
            if writeResponse.isError():
                if self._runAcquisition:
                    self.plainTextEdit.appendPlainText('{0}. Error triggering acquisition'.format(self._autoIndex))
                else:
                    QMessageBox.critical(self, 'Error', 'Triggering acquisition')
            else:
                if self._runAcquisition:
                    self.plainTextEdit.appendPlainText('{0}. Triggered acquisition successfully'.format(self._autoIndex))


    @Slot()
    def on_readSamples_clicked(self):
        if self._get_client() is not None:
            self._samplesRaw = []
            registerAddress = InputRegisters.MBIR_PixelDataFirst
            remainingRegistersCount = InputRegisters.MBIR_PixelDataLast - InputRegisters.MBIR_PixelDataFirst + 1
            while remainingRegistersCount > 0:
                readRegisterCount = remainingRegistersCount
                if readRegisterCount > 125:
                    readRegisterCount = 125
                
                rr = self._client.read_input_registers(registerAddress,
                                                        readRegisterCount,
                                                        unit=self._serverUnitBusAddress)
                if rr.isError():
                    if self._runAcquisition:
                        self.plainTextEdit.appendPlainText('{0}. Error reading samples {1} to {2}'.format(self._autoIndex, registerAddress - InputRegisters.MBIR_PixelDataFirst, registerAddress + readRegisterCount - 1 - InputRegisters.MBIR_PixelDataFirst))
                    else:
                        QMessageBox.critical(self, 'Error', 'Exception 0x{0:02x} reading samples {1} to {2}'.format(rr.function_code, registerAddress - InputRegisters.MBIR_PixelDataFirst, registerAddress + readRegisterCount - 1 - InputRegisters.MBIR_PixelDataFirst))
                    break
                
                self._samplesRaw.extend(rr.registers)
                registerAddress = registerAddress + readRegisterCount
                remainingRegistersCount = remainingRegistersCount - readRegisterCount
                time.sleep(0.004)

            #self.graphicsView.plot(np.arange(0, 100, 1), np.random.normal(size=100), clear=True)

            if self._runAcquisition:
                self.plainTextEdit.appendPlainText('{0}. Read samples successfully'.format(self._autoIndex))
            else:
                self.plainTextEdit.appendPlainText('Read samples successfully')
            
            samples = self._samplesRaw.copy()
            overflows = [0] * len(samples)
            parity = [0] * len(samples)

            for i in range(0, len(samples)):
                numericValue, overflowError, parityError = self.extractAdc(samples[i])
                samples[i] = numericValue
                overflows[i] = overflowError
                parity[i] = parityError
            
            self.graphicsView.plot(np.arange(0, 0.001, 0.0000001), samples, clear=True)

    def extractAdc(self, value):
        numericValue = 0
        parityError = 0
        overflowError = 0

        if value & 0x8000:
            # Parity error
            parityError = 1 
        elif value & 0x4000:
            # Overflow, go full scale
            overflowError = 1
            if value & 0x2000:
                numericValue = -32768
            else:
                numericValue = 32767
        else:
            # Normal value, sign extend
            numericValue = (value & 0x1FFF) - (value & 0x2000)
        return numericValue, overflowError, parityError


    def sign_extend(value, bits):
        sign_bit = 1 << (bits - 1)
        return (value & (sign_bit - 1)) - (value & sign_bit)
    

    @Slot()
    def on_singleAcquisition_clicked(self):
        self._runAcquisition = False
        self._sampleTimer.stop()
        self._logBasename = ''


    @Slot()
    def on_runAcquisition_clicked(self):
        self._autoIndex = 0
        self._runAcquisition = True
        self._sampleTimer.start(10)


    @Slot()
    def autoAcquire(self):
        self.on_takeSamples_clicked()
        time.sleep(0.004)
        self.on_readSamples_clicked()
        if self._logBasename != '':
            self._saveSamples(self._logBasename.format(self._autoIndex))
        self._autoIndex = self._autoIndex + 1
        if self._runAcquisition:
            self._sampleTimer.start(10)

    def _saveSamples(self, filename):
        with open(filename, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=',',
                                    quotechar='\"', quoting=csv.QUOTE_MINIMAL)
            for sample in self._samplesRaw:
                writer.writerow(['{0}'.format(sample)])


    @Slot()
    def on_saveData_clicked(self):
        filename = self.saveFileDialog()
        if filename != '':
            # Allow save dialog to pre-configure the save path while running
            parts = os.path.splitext(filename)
            self._logBasename = parts[0] + '_{0:03}' + parts[1]

            # Save the data proper
            self._saveSamples(filename)


    def saveFileDialog(self):
        file_dialog = QFileDialog(self)
        options = file_dialog.options()
        file_dialog.setOptions(options)
        #file_dialog.setText('Select file to save raw data to')
        file_dialog.setNameFilters(['CSV files (*.csv)', 'Text files (*.txt)', 'All files (*)'])
        file_dialog.setAcceptMode(QFileDialog.AcceptSave)
        if file_dialog.exec() == QDialog.Accepted:
            filename = file_dialog.selectedFiles()[0]
        else:
            filename = ''
        return filename

#
def main():
    QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling)
    QApplication.setAttribute(QtCore.Qt.AA_UseHighDpiPixmaps)

    app = QApplication(sys.argv)
    mainWindow = MainWindow()
    mainWindow.show()
    sys.exit(app.exec_())

#
if (__name__ == '__main__'):
    main()

