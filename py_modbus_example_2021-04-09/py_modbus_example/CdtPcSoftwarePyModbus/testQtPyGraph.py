import sys
from PySide2 import QtCore
from PySide2.QtCore import Slot
from PySide2.QtWidgets import QApplication, QMainWindow, QMessageBox
import pyqtgraph
import uiLoader
import numpy as np

#
class MainWindow(QMainWindow):
    #
    def __init__(self, parent=None):
        #
        customWidgets = {
                            'PlotWidget' : pyqtgraph.PlotWidget
        }
        QMainWindow.__init__(self, parent)
        uiLoader.loadUi('CereusTestApp.ui', self, customWidgets)

        self._populate_comports()
        self.on_modbusAddress_valueChanged(self._serverUnitBusAddress)
        self._configure_plot()

        self._set_ui_port_open_state(False)


        self.graphicsView.plot(np.arange(0, 100, 1), np.random.normal(size=100), clear=True)

#
def main():
    QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling)
    QApplication.setAttribute(QtCore.Qt.AA_UseHighDpiPixmaps)

    app = QApplication(sys.argv)
    mainWindow = MainWindow()
    mainWindow.show()
    sys.exit(app.exec_())

#
if (__name__ == '__main__'):
    main()

