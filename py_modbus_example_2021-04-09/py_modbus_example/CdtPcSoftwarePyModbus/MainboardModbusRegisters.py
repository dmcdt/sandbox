# MODBUS definitions for Rotating Head
from enum import IntEnum, auto

# From Rotating Head firmware C code copied 2021-06-18
class InputRegisters(IntEnum):
    # MBIR = ModBus Input Register
    MBIR_CompanyId0 = 0
    MBIR_CompanyId1 = auto()
    MBIR_DeviceType = auto()
    MBIR_FirmwareVersionHighWord = auto()
    MBIR_FirmwareVersionLowWord = auto()
    MBIR_FirmwareChecksumHighWord = auto()
    MBIR_FirmwareChecksumLowWord = auto()
    MBIR_SerialNumber0_1 = auto()
    MBIR_SerialNumber2_3 = auto()
    MBIR_SerialNumber4_5 = auto()
    MBIR_SerialNumber6_7 = auto()
    MBIR_SerialNumber8_9 = auto()
    MBIR_SerialNumber10_11 = auto()
    MBIR_SerialNumber12_13 = auto()
    MBIR_SerialNumber14_15 = auto()
    MBIR_MB_Status = auto()
    MBIR_RH_Status = auto()
    MBIR_MB_Status,
    MBIR_RH_Status,
    MBIR_MB_CorruptPacketsReceived = auto()
    MBIR_SERDES_CRC_ErrorCount = auto()
    MBIR_SERDES_ErrorCount = auto()
    MBIR_SelfTestResults = auto()
    MBIR_PacketsReceived = auto()
    MBIR_RetransmitsRequested = auto()
    MBIR_RH_CorruptPacketsReceived = auto()
    MBIR_SyncPulsesReceived = auto()
    MBIR_ConfigBlobCrcHighWord = auto()
    MBIR_ConfigBlobCrcLowWord = auto()
    # "Real" registers have an entry in the input register storage array.
    MBIR_NumberRealRegisters = auto()

    # "Virtual" registers above this have storage elsewhere
    MBIR_PixelDataFirst = auto()
    MBIR_PixelDataLast = MBIR_PixelDataFirst + 10000 - 1

    # Total number of real and virtual registers
    MBIR_NumberTotalRegisters = auto()


# Index definitions for the "Holding Registers" within the device.
# Copied from Rotating Head firmware C code 2021-06-18
class HoldingRegisters(IntEnum):
    # MBHR = ModBus Holding Register
    MBHR_GetTime = 0
    MBHR_SetTime = auto()
    MBHR_MB_Commands = auto()
    MBHR_RH_Commands = auto()
    MBHR_TX_Equalisation = auto()
    MBHR_NumberRegisters = auto()


# Commands defined that can be written to the MBHR_RH_Commands register
# Copied from Rotating Head firmware C code 2021-06-18
class RotatingHeadCommands(IntEnum):
    RH_DoNothing = 0,
    RH_ClearAllCounters = 10,
    RH_SyncTestTrigger = 100,
    RH_SyncQuiescentTrigger = 110,
    RH_RetransmitPixelData = 200

