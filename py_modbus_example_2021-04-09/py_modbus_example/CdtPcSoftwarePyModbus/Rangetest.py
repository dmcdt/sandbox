# Python program to understand range
  
# creates an iterator
demo = iter(range(6))
  
# print iterator
print ("iterator")
print(demo)
  
# use next
print ("next")
print(next(demo))