# Simulation of CDT tool modbus interface, as for running example CDT modbus
# code against.
#
# Requires Pyside2 (Qt) for user interface.
# Requires pymodbus, pyserial for performing the modbus comms.

import sys
import threading
from PySide2.QtWidgets import QApplication, QMainWindow, QMessageBox
from PySide2.QtCore import Slot
import uiLoader
import serial
from pymodbus.server.sync import ModbusBaseRequestHandler, StartSerialServer
from pymodbus.server.sync import ModbusSerialServer

from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSequentialDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext

from pymodbus.transaction import ModbusRtuFramer

import CustomModbusFunctions
from CustomModbusFunctions import BeginWriteUserModbusRequest
from CustomModbusFunctions import WriteDataUserModbusRequest
from CustomModbusFunctions import FinishWriteUserModbusRequest
from CustomModbusFunctions import CancelWriteUserModbusRequest


#
class MainWindow(QMainWindow):
    # The address of the server on the bus.
    serverUnitBusAddress = 0x20

    #
    def __init__(self, parent=None):
        self._server = None
        self._server_thread = None
        QMainWindow.__init__(self, parent)
        uiLoader.loadUi('untitled.ui', self)

    def create_server(self):
        # Seems the only way to get an exception response from pymodbus to a
        # function code that your device does not support is to reimplement
        # the class ModbusSlaveContext. Best you can do otherwise is to have a
        # single register, otherwise by default it responds to all registers.
        #
        # Also the addresses specified here are the Modbus logical address (1 based),
        # not the PDU address (0 based).
        #
        # Note the dictionary uses the server bus address as the key

        store = {
                self.serverUnitBusAddress: ModbusSlaveContext(
                    di=ModbusSequentialDataBlock(0xFFFF, [0]),
                    co=ModbusSequentialDataBlock(0xFFFF, [0]),
                    hr=ModbusSequentialDataBlock(0xFFFF, [0]),
                    ir=ModbusSequentialDataBlock(1, [0x4344, 0x5400,
                                                    0x0002, 0x0102,
                                                    0xC0DE, 0xBAAD,
                                                    0x3132, 0x3334,
                                                    0x3536, 0x3738,
                                                    0x0000, 0x0000,
                                                    0x0000, 0x0000]))
        }

        # Need to let the modbus library know about the custom requests that get
        # sent to our server. Note, uses the server bus address here too.
        # Register each request using the function code, a unique mnemonic, and
        # a context - information that the request needs access to. I think it has
        # to be present, so just pass in a dummy ModbusSequentialDataBlock().
        store[self.serverUnitBusAddress].register(BeginWriteUserModbusRequest.function_code, 'bw', ModbusSequentialDataBlock(0, [0]))
        store[self.serverUnitBusAddress].register(WriteDataUserModbusRequest.function_code, 'wd', ModbusSequentialDataBlock(0, [0]))
        store[self.serverUnitBusAddress].register(FinishWriteUserModbusRequest.function_code, 'fw', ModbusSequentialDataBlock(0, [0]))
        store[self.serverUnitBusAddress].register(CancelWriteUserModbusRequest.function_code, 'cw', ModbusSequentialDataBlock(0, [0]))

        context = ModbusServerContext(slaves=store, single=False)

        # RTU:
        # Timeout specified here is total read constant timeout. Set to 0.1s
        # whcih is longer than the longest allowed modbus packet at 115200 baud.
        # Also reduces the risk of cutting the packet off, although does not
        # remove it, which pymodbus seems to suffer from. It tries to read a
        # block of data in one operation, even though the total read timeout
        # constant may interrupt a packet but does not signal the end of the packet
        # stream.
        # StartSerialServer(context,
        #                 custom_functions=[BeginWriteUserModbusRequest,
        #                                     WriteDataUserModbusRequest,
        #                                     FinishWriteUserModbusRequest,
        #                                     CancelWriteUserModbusRequest],
        #                 framer=ModbusRtuFramer,
        #                 timeout=0.1,
        #                 port=self._port_path,
        #                 baudrate=self._baud_rate,
        #                 parity=self._parity)
        self._server = self.create_serial_server(context,
                                                 framer=ModbusRtuFramer,
                                                 timeout=0.1,
                                                 port=self._port_path,
                                                 baudrate=self._baud_rate,
                                                 bytesize=self._data_bits,
                                                 parity=self._parity,
                                                 stopbits=self._stop_bits)
        if self._server.socket is None:
            self._server = None
            QMessageBox.critical(self, 'Error', 'Could not open port \'' + self._port_path + '\'')
        
        return self._server is not None

    def runServer(self):
        self.run_serial_server(self._server,
                               custom_functions=[BeginWriteUserModbusRequest,
                                                   WriteDataUserModbusRequest,
                                                   FinishWriteUserModbusRequest,
                                                   CancelWriteUserModbusRequest])

    # Copied out of the pymodbus.sync.StartSerialServer so we can separate the
    # server creation from the serving of the loop.
    def create_serial_server(self, context=None, identity=None,
                             **kwargs):
        framer = kwargs.pop('framer', ModbusRtuFramer)
        server = ModbusSerialServer(context, framer, identity, **kwargs)
        return server

    def run_serial_server(self, server, custom_functions=[]):
        for f in custom_functions:
            server.decoder.register(f)
        server.serve_forever()

    def stop_serial_server(self):
        if self._server != None:
            self._server.server_close()
            self._server_thread.join(2)
            self._server = None
            self._server_thread = None

    def closeEvent(self, event):
        self.stop_serial_server()
        event.accept()

    @Slot()
    def on_startServer_clicked(self):
        self._port_path = self.portName.text()
        self._baud_rate = int(self.baud.currentText())
        self._data_bits = int(self.dataBits.currentText())
        self._parity = [serial.PARITY_NONE, serial.PARITY_MARK, serial.PARITY_SPACE, serial.PARITY_ODD, serial.PARITY_EVEN][self.parity.currentIndex()]
        self._stop_bits = [serial.STOPBITS_ONE, serial.STOPBITS_ONE_POINT_FIVE, serial.STOPBITS_TWO][self.stopBits.currentIndex()]

        if self.create_server():
            self._server_thread = threading.Thread(target=self.runServer)
            self._server_thread.start()
            self.startServer.setEnabled(False)
            self.stopServer.setEnabled(True)

    @Slot()
    def on_stopServer_clicked(self):
        self.stopServer.setEnabled(False)
        self.stop_serial_server()
        self.startServer.setEnabled(True)

#
def main():
    app = QApplication(sys.argv)
    mainWindow = MainWindow()
    mainWindow.show()
    sys.exit(app.exec_())


#
if (__name__ == '__main__'):
    main()

