/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "eth.h"
#include "usart.h"
#include "usb_otg.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include <stdint.h>
#include <stdbool.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#ifndef HSEM_ID_0
#define HSEM_ID_0 (0U) /* HW semaphore 0*/
#endif
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

volatile bool gTransmitInProgress;
volatile bool gReceiveInProgress;

volatile char txBuffer[32];
volatile int txEndPos;

volatile char rxBuffer[32];
volatile int rxStartPos;
volatile int rxEndPos;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MPU_Config(void);
/* USER CODE BEGIN PFP */

void ProcessResponse(char const *buffer, int length);
void TransmitComplete(UART_HandleTypeDef *huart);
void ReceiveComplete(UART_HandleTypeDef *huart);
void UartError(UART_HandleTypeDef *huart);
void ReceiveAborted(UART_HandleTypeDef *huart);
    
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
    
    /* Enable low power debug */
    HAL_EnableDBGSleepMode();
    HAL_EnableDBGStopMode();
    HAL_EnableDBGStandbyMode();
    
    HAL_EnableDomain2DBGSleepMode();
    HAL_EnableDomain2DBGStopMode();
    HAL_EnableDomain2DBGStandbyMode();

    HAL_EnableDomain3DBGStopMode();
    HAL_EnableDomain3DBGStandbyMode();

  /* USER CODE END 1 */
/* USER CODE BEGIN Boot_Mode_Sequence_0 */
/* USER CODE END Boot_Mode_Sequence_0 */

  /* MPU Configuration--------------------------------------------------------*/
  MPU_Config();

  /* Enable I-Cache---------------------------------------------------------*/
  SCB_EnableICache();

  /* Enable D-Cache---------------------------------------------------------*/
  SCB_EnableDCache();

/* USER CODE BEGIN Boot_Mode_Sequence_1 */

/* USER CODE END Boot_Mode_Sequence_1 */
  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();
/* USER CODE BEGIN Boot_Mode_Sequence_2 */

/* USER CODE END Boot_Mode_Sequence_2 */

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ETH_Init();
  MX_USART3_UART_Init();
  MX_USB_OTG_FS_PCD_Init();
  MX_USART1_UART_Init();
  MX_USART2_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
    HAL_StatusTypeDef status;
  
    status = HAL_UART_RegisterCallback(&huart1, HAL_UART_TX_COMPLETE_CB_ID, TransmitComplete);
    status = HAL_UART_RegisterCallback(&huart1, HAL_UART_RX_COMPLETE_CB_ID, ReceiveComplete);
    // On line break, RxHandler gets called then ErrorCallback
    status = HAL_UART_RegisterCallback(&huart1, HAL_UART_ERROR_CB_ID, UartError);
    status = HAL_UART_RegisterCallback(&huart1, HAL_UART_ABORT_RECEIVE_COMPLETE_CB_ID, ReceiveAborted);

    char s1[] = "Hello, world\r\n\0";

    //gTransmitInProgress = false;
    //status = HAL_UART_Transmit_IT(&huart1, (uint8_t *)s1, strlen(s1));

    txEndPos = strlen(s1);
    strcpy((char *)txBuffer, s1);
    gTransmitInProgress = false;
    status = HAL_UART_Transmit_IT(&huart1, (uint8_t *)txBuffer, txEndPos);
    if (status != HAL_OK)
    {
        Error_Handler();
    }

    bool startRx;
    bool startRxWithTimeout;
    bool abortRx;
    
    startRx = false;
    startRxWithTimeout = false;
    abortRx = false;

    uint32_t totalTimeoutStart;
    uint32_t totalTimeoutDuration;
    
    totalTimeoutDuration = 0;
    
    bool lastReceiveInProgress = gReceiveInProgress;
    
    while (1)
    {
        if (totalTimeoutDuration != 0)
        {
            // Keep total timeout running even if rx started? Yes.
            // Code can change if we want an initial no-activity timeout.
            if ((HAL_GetTick() - totalTimeoutStart) > totalTimeoutDuration)
            {
                // Have to just abort, cannot force the timeout flag to be set
                abortRx = true;
                totalTimeoutDuration = 0;
            }
        }
        
        if (startRx)
        {
            startRx = false;
            rxStartPos = 0;
            rxEndPos = 0;
            gReceiveInProgress = true;
            __HAL_UART_CLEAR_FLAG(&huart1, UART_CLEAR_PEF | UART_CLEAR_FEF | UART_CLEAR_NEF | UART_CLEAR_OREF | UART_CLEAR_IDLEF | UART_CLEAR_RTOF);
            __HAL_UART_SEND_REQ(&huart1, UART_RXDATA_FLUSH_REQUEST);
            status = HAL_UART_Receive_IT(&huart1, (uint8_t *)rxBuffer, sizeof rxBuffer);
            if (status != HAL_OK)
            {
                Error_Handler();
            }
        }
        else if (startRxWithTimeout)
        {
            startRxWithTimeout = false;
            rxStartPos = 0;
            rxEndPos = 0;
            gReceiveInProgress = true;
            
            // How convoluted does this need to be to enable the Receiver Timeout?
            // I'd be as well bypassing the HAL, all these do is set the values
            // into the registers. Disable and clear first before re-enabling
            // for a clean start.
            HAL_UART_DisableReceiverTimeout(&huart1);
            HAL_UART_ReceiverTimeout_Config(&huart1, 200);
            __HAL_UART_CLEAR_FLAG(&huart1, UART_CLEAR_RTOF);
            HAL_UART_EnableReceiverTimeout(&huart1);
            SET_BIT(huart1.Instance->CR1, USART_CR1_RTOIE);

            // TODO - I guess if we're using a total timeout we don't really need an interval timeout
            
            // Clear any error flags and the FIFO before we start to receive, 
            // not interested in what has gone before.
            __HAL_UART_CLEAR_FLAG(&huart1, UART_CLEAR_PEF | UART_CLEAR_FEF | UART_CLEAR_NEF | UART_CLEAR_OREF | UART_CLEAR_IDLEF);
            __HAL_UART_SEND_REQ(&huart1, UART_RXDATA_FLUSH_REQUEST);
            status = HAL_UART_Receive_IT(&huart1, (uint8_t *)rxBuffer, sizeof rxBuffer);
            if (status != HAL_OK)
            {
                Error_Handler();
            }
            
            // TODO - should I work the timeout after starting the read? So
            // that it cannot trigger before, if there is ongoing comms.
            
            // Overall timeout just part of the loop rather than a specific timer.
            totalTimeoutDuration = 5000;
            totalTimeoutStart = HAL_GetTick();
        }
        
        if (abortRx)
        {
            abortRx = false;
            status = HAL_UART_AbortReceive_IT(&huart1);

            // need to manually cancel the receive timeout as Abort does not
            // TODO - probably also need to do this for receive complete
            CLEAR_BIT(huart1.Instance->CR1, USART_CR1_RTOIE);
            HAL_UART_DisableReceiverTimeout(&huart1);
        }
        
        // Check if receive has newly completed
        if (lastReceiveInProgress && !gReceiveInProgress)
        {
            ProcessResponse((char *)&rxBuffer[rxStartPos], rxEndPos - rxStartPos);
        }
        lastReceiveInProgress = gReceiveInProgress;
        
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
        __WFE();
        __NOP();
    }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Supply configuration update enable
  */
  HAL_PWREx_ConfigSupply(PWR_DIRECT_SMPS_SUPPLY);
  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}
  /** Macro to configure the PLL clock source
  */
  __HAL_RCC_PLL_PLLSOURCE_CONFIG(RCC_PLLSOURCE_HSE);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 50;
  RCC_OscInitStruct.PLL.PLLP = 2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_3;
  RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
  RCC_OscInitStruct.PLL.PLLFRACN = 0;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV1;
  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART3|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USB;
  PeriphClkInitStruct.PLL3.PLL3M = 1;
  PeriphClkInitStruct.PLL3.PLL3N = 24;
  PeriphClkInitStruct.PLL3.PLL3P = 2;
  PeriphClkInitStruct.PLL3.PLL3Q = 4;
  PeriphClkInitStruct.PLL3.PLL3R = 2;
  PeriphClkInitStruct.PLL3.PLL3RGE = RCC_PLL3VCIRANGE_3;
  PeriphClkInitStruct.PLL3.PLL3VCOSEL = RCC_PLL3VCOWIDE;
  PeriphClkInitStruct.PLL3.PLL3FRACN = 0;
  PeriphClkInitStruct.Usart234578ClockSelection = RCC_USART234578CLKSOURCE_D2PCLK1;
  PeriphClkInitStruct.Usart16ClockSelection = RCC_USART16CLKSOURCE_D2PCLK2;
  PeriphClkInitStruct.UsbClockSelection = RCC_USBCLKSOURCE_PLL3;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  HAL_RCC_MCOConfig(RCC_MCO2, RCC_MCO2SOURCE_SYSCLK, RCC_MCODIV_1);
  /** Enable USB Voltage detector
  */
  HAL_PWREx_EnableUSBVoltageDetector();
}

/* USER CODE BEGIN 4 */

void ProcessResponse(char const *buffer, int length)
{
    if (length > 0)
    {
        if (!gTransmitInProgress)
        {
            HAL_StatusTypeDef status;
            
            memcpy((void *)txBuffer, buffer, length);
            txEndPos = length;
            gTransmitInProgress = true;
            status = HAL_UART_Transmit_IT(&huart1, (uint8_t *)txBuffer, txEndPos);
            if (status != HAL_OK)
            {
                Error_Handler();
            }
        }
    }
}


void TransmitComplete(UART_HandleTypeDef *huart)
{
    gTransmitInProgress = false;
}

void ReceiveComplete(UART_HandleTypeDef *huart)
{
    rxEndPos = huart->RxXferSize - huart->RxXferCount;
    gReceiveInProgress = false;
}

void UartError(UART_HandleTypeDef *huart)
{
    // RxISR==NULL && RxState==Ready when terminal error or non-terminal error on last element of receive (so complete)
    // RxXferCount and huart->RxXferSize valid
    if (huart->ErrorCode & HAL_UART_ERROR_FE)
    {
        // these members only valid on non-terminal error
        if ((huart->RxXferSize - huart->RxXferCount) == (rxStartPos + 1) &&
            (huart->pRxBuffPtr[-1] == 0))
        {
            // Framing error on first byte received, probably a line break
            // so remove it. Except we can't remove it, no ability to change
            // RX buffer
            rxStartPos++;
        }
    }

    if (huart->ErrorCode & HAL_UART_ERROR_RTO)
    {
        // Timeout
        gReceiveInProgress = false;
    }
    
    if ((huart->RxISR==NULL) && (huart->RxXferCount > 0))
    {
        // TODO - need to store how many characters were received, in case we want to do something with the terminal error
        // Timeout or overrun
        // but not parity, framing, or noise - do they keep the value that was detected? must do!
        rxEndPos = huart->RxXferSize - huart->RxXferCount;
        HAL_UART_AbortReceive_IT(huart);
    }
}

void ReceiveAborted(UART_HandleTypeDef *huart)
{
    gReceiveInProgress = false;
}

/* USER CODE END 4 */

/* MPU Configuration */

void MPU_Config(void)
{

  /* Disables the MPU */
  HAL_MPU_Disable();
  /* Enables the MPU */
  HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);

}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
