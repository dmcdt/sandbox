#include "pch.h"
#include "CdtTestLibrary.h"
#include "CdtTestLibraryInternals.h"

#include <cstring>

CDTDLL void WINAPI GetStatistics(void* context, struct Statistics* stats)
{
    CdtToolContext* ctx = reinterpret_cast<CdtToolContext*>(context);

    stats->serialNumber = ctx->serialNumber.c_str();
    stats->firmwareVersion = 1 * 100 + 2;
    stats->capacity = 2 /* Columns */
                    * 8 /* Rows */
                    * 2 /* Devices per bus access */
                    * 4 /* LUNs per device */
                    * 2 /* Planes per block */
                    * 2048 /* Blocks per LUN */
                    * 128 /* Pages per block */
                    * 8192ULL /* Bytes per page */;
    stats->used = ctx->used;
}

