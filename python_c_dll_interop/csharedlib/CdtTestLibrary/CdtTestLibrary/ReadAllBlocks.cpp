#include "pch.h"
#include "CdtTestLibrary.h"
#include "CdtTestLibraryInternals.h"

#include <cmath>
#include <ctime>

enum ChunkTimestampBits
{
    ChunkTimestamp_SecondsUnits_Pos = 0,
    ChunkTimestamp_SecondsUnits_Len = 4,
    ChunkTimestamp_SecondsTens_Pos = 4,
    ChunkTimestamp_SecondsTens_Len = 3,
    ChunkTimestamp_MinutesUnits_Pos = 7,
    ChunkTimestamp_MinutesUnits_Len = 4,
    ChunkTimestamp_MinutesTens_Pos = 11,
    ChunkTimestamp_MinutesTens_Len = 3,
    ChunkTimestamp_HoursUnits_Pos = 14,
    ChunkTimestamp_HoursUnits_Len = 4,
    ChunkTimestamp_HoursTens_Pos = 18,
    ChunkTimestamp_HoursTens_Len = 2,
    ChunkTimestamp_AM24_PM_Pos = 20,
    ChunkTimestamp_AM24_PM_Len = 1,
    ChunkTimestamp_Subseconds_Pos = 21,
    ChunkTimestamp_Subseconds_Len = 10,
    ChunkTimestamp_DayUnits_Pos = 32,
    ChunkTimestamp_DayUnits_Len = 4,
    ChunkTimestamp_DayTens_Pos = 36,
    ChunkTimestamp_DayTens_Len = 2,
    ChunkTimestamp_MonthUnits_Pos = 38,
    ChunkTimestamp_MonthUnits_Len = 4,
    ChunkTimestamp_MonthTens_Pos = 42,
    ChunkTimestamp_MonthTens_Len = 1,
    ChunkTimestamp_Weekday_Pos = 43,
    ChunkTimestamp_Weekday_Len = 3,
    ChunkTimestamp_YearUnits_Pos = 46,
    ChunkTimestamp_YearUnits_Len = 4,
    ChunkTimestamp_YearTens_Pos = 50,
    ChunkTimestamp_YearTens_Len = 4
};


#if 0
enum ChunkTimestampWeekdays
{
    ChunkTimestamp_Monday = (1ULL << ChunkTimestamp_Weekday_Pos),
    ChunkTimestamp_Tuesday = (2ULL << ChunkTimestamp_Weekday_Pos),
    ChunkTimestamp_Wednesday = (3ULL << ChunkTimestamp_Weekday_Pos),
    ChunkTimestamp_Thursday = (4ULL << ChunkTimestamp_Weekday_Pos),
    ChunkTimestamp_Friday = (5ULL << ChunkTimestamp_Weekday_Pos),
    ChunkTimestamp_Saturday = (6ULL << ChunkTimestamp_Weekday_Pos),
    ChunkTimestamp_Sunday = (7ULL << ChunkTimestamp_Weekday_Pos),
};
#endif


extern double rxData[10000];


unsigned long long MakeTimestamp(double fractionalTimeValue)
{
    unsigned long long timestampFields;

    time_t  timestampSeconds;   // Whole seconds of timestamp for conversion
    tm timestampParts;    // Broken down timestamp for the fields

    timestampSeconds = static_cast<time_t>(fractionalTimeValue);
    errno_t err = gmtime_s(&timestampParts, &timestampSeconds);

    timestampFields = 0;

    timestampFields |= static_cast<unsigned long long>(timestampParts.tm_sec % 10) << ChunkTimestamp_SecondsUnits_Pos;
    timestampFields |= static_cast<unsigned long long>(timestampParts.tm_sec / 10) << ChunkTimestamp_SecondsTens_Pos;
    timestampFields |= static_cast<unsigned long long>(timestampParts.tm_min % 10) << ChunkTimestamp_MinutesUnits_Pos;
    timestampFields |= static_cast<unsigned long long>(timestampParts.tm_min / 10) << ChunkTimestamp_MinutesTens_Pos;
    timestampFields |= static_cast<unsigned long long>(timestampParts.tm_hour % 10) << ChunkTimestamp_HoursUnits_Pos;
    timestampFields |= static_cast<unsigned long long>(timestampParts.tm_hour / 10) << ChunkTimestamp_HoursTens_Pos;

    // Work out the subsecond field
    fractionalTimeValue -= timestampSeconds;
    fractionalTimeValue *= 1024;
    int subseconds;
    subseconds = 1023 - (int)fractionalTimeValue;
    if (subseconds > 1023)
    {
        subseconds = 1023;
    }
    if (subseconds < 0)
    {
        subseconds = 0;
    }
    timestampFields |= static_cast<unsigned long long>(subseconds) << ChunkTimestamp_Subseconds_Pos;

    timestampFields |= static_cast<unsigned long long>(timestampParts.tm_mday % 10) << ChunkTimestamp_DayUnits_Pos;
    timestampFields |= static_cast<unsigned long long>(timestampParts.tm_mday / 10) << ChunkTimestamp_DayTens_Pos;
    timestampFields |= static_cast<unsigned long long>((timestampParts.tm_mon + 1) % 10) << ChunkTimestamp_MonthUnits_Pos;
    timestampFields |= static_cast<unsigned long long>((timestampParts.tm_mon + 1) / 10) << ChunkTimestamp_MonthTens_Pos;
    timestampFields |= static_cast<unsigned long long>(timestampParts.tm_wday == 0 ? 7 : timestampParts.tm_wday) << ChunkTimestamp_Weekday_Pos;
    timestampFields |= static_cast<unsigned long long>((timestampParts.tm_year + 1900) % 10) << ChunkTimestamp_YearUnits_Pos;
    timestampFields |= static_cast<unsigned long long>(((timestampParts.tm_year + 1900) / 10) % 10) << ChunkTimestamp_YearTens_Pos;

    return timestampFields;
}


CDTDLL int WINAPI ReadAllBlocks(void* context, struct MultipleArguments* args)
{
    int dataToProcess = 0;

    CdtToolContext* ctx = reinterpret_cast<CdtToolContext*>(context);
    if (ctx && args)
    {
        if (ctx->processedAddress < ctx->used)
        {
            dataToProcess = 1;

            double  timestampFraction;  // Timestamp in numerical format with fractional

            /* Calculate amount of data to read over USB. Try to match the requested amount and then round up to nearest row */
            unsigned long long  pixelChunkLength;
            unsigned long long  ssifChunkLength;
            unsigned long long  imuChunkLength;

            /* Calculate chunk length and round up to nearest multiple of 4 */
            pixelChunkLength = 22 + ctx->numberMainSamples * 2;
            if ((pixelChunkLength % 4) != 0)
            {
                pixelChunkLength = pixelChunkLength + 4 - (pixelChunkLength % 4);
            }

            ssifChunkLength = 46 + ctx->numberTofSamples * 2;
            if ((ssifChunkLength % 4) != 0)
            {
                ssifChunkLength = ssifChunkLength + 4 - (ssifChunkLength % 4);
            }

            imuChunkLength = 20 + ctx->numberImuSamples * 6 * 2;
            if ((imuChunkLength % 4) != 0)
            {
                imuChunkLength = imuChunkLength + 4 - (imuChunkLength % 4);
            }

            /* Calculate the length of data required based on the number of pixels requested
             * (this is only a simulation of what would be in the tool memory).
             * In reality we might read a fixed block size or have the caller specify the number
             * of bytes to read directly. Or maybe we just read until one of the arrays is full?
             * 
             * Pixels are sampled multiple times per revolution, SSIF once, and IMU twice.
             * Assumes 1 revolution == 1 second as the second two are time based.
             */
            unsigned long long numberRevolutions;
            numberRevolutions = args->mainLength / ctx->pixelsPerRevolution;
            if (numberRevolutions > args->secondaryLength)
            {
                numberRevolutions = args->secondaryLength;
            }
            if (numberRevolutions > (args->inertialLength / 2))
            {
                numberRevolutions = (args->inertialLength / 2);
            }

            unsigned long long numberPixels;
            unsigned long long numberSsif;
            unsigned long long numberImu;

            numberPixels = numberRevolutions * ctx->pixelsPerRevolution;
            numberSsif = numberRevolutions;
            numberImu = numberRevolutions * 2;

            unsigned long long  bytesToProcess;
            unsigned long long  bytesToRead;


            bytesToProcess = numberPixels * pixelChunkLength +
                          numberSsif * ssifChunkLength +
                          numberImu * imuChunkLength;
            bytesToRead = ((bytesToProcess - (ctx->readAddress - ctx->processedAddress) + (ctx->downloadMultiple - 1)) / ctx->downloadMultiple) * ctx->downloadMultiple;
            if (bytesToRead > (ctx->used - ctx->readAddress))
            {
                bytesToRead = (ctx->used - ctx->readAddress);
            }

            // TODO - read block of data from tool in smaller blocks than those requested by the caller

            // Maybe in maximum lengths of 2 * 100,000,000 ~~ 200MB
            // That's 1s worth of data transfer at the maximum rate.
            // But also need to keep it aligned to a multiple of the download size.
            // Bearing in mind the download sizes calculated in this example do not include the spare areas.
            // ((200MB + downloadMultiple - 1) / downloadMultiple) * downloadMultiple ==  200,015,872 

            // The following is just something to fill in some dummy data.
            // It would not be done in this structure, rather it would sequentially
            // process whatever was in the buffer as it reads over USB.
            if ((args->mainLength >= numberPixels) && (args->mainData))
            {
                args->mainLength = numberPixels;
                for (int i = 0; i < args->mainLength; i++)
                {
                    timestampFraction = ctx->logStartTime + ctx->revolution + (double)i / (double)ctx->pixelsPerRevolution;

                    args->mainData[i].timestamp = MakeTimestamp(timestampFraction);
                    args->mainData[i].revolutionNumber = ctx->revolution;
                    args->mainData[i].pixelIndex = i;
                    for (int j = 0; j < MAXIMUM_MAIN_SAMPLES; j++)
                    {
                        args->mainData[i].samples[j] = static_cast<short>(rxData[j] * 8192.0);
                    }
                }
            }

            if ((args->secondaryLength >= numberSsif) && (args->secondaryData))
            {
                args->secondaryLength = numberSsif;
                for (int i = 0; i < args->secondaryLength; i++)
                {
                    timestampFraction = ctx->logStartTime + ctx->revolution + (double)i;

                    args->secondaryData[i].timestamp = MakeTimestamp(timestampFraction);
                    args->secondaryData[i].revolutionNumber = ctx->revolution;

                    args->secondaryData[i].pressure = (unsigned short)(100.0 / 10000.0 * 65535.0);
                    args->secondaryData[i].temperature = (unsigned short)((95.0 - -40.0) / (150.0 - -40.0) * 65535.0);

                    args->secondaryData[i].numberTofSamples = 2000;
                    for (int j = 0; j < args->secondaryData[i].numberTofSamples; j++)
                    {
                        args->secondaryData[i].tofSamples[j] = (unsigned short)(((double)rand() * 2.0 / RAND_MAX - 1.0) * 32767.0);
                    }
                }
            }

            if ((args->inertialLength >= numberImu) && (args->inertialData))
            {
                args->inertialLength = numberImu;
                for (int i = 0; i < args->inertialLength; i++)
                {
                    timestampFraction = ctx->logStartTime + ctx->revolution + (double)i / 2.0;

                    args->inertialData[i].timestamp = MakeTimestamp(timestampFraction);
                    args->inertialData[i].temperature = (unsigned short)((95.0 - -40.0) / (150.0 - -40.0) * 65535.0);
                    args->inertialData[i].numberSamples = 50;

                    for (int j = 0; j < args->inertialData[i].numberSamples; j++)
                    {
                        args->inertialData[i].imuSamples[j].ax = (unsigned short)((((double)rand() * 4.0 / RAND_MAX - 2.0) / 100.0) * 16383.0);
                        args->inertialData[i].imuSamples[j].ay = (unsigned short)((((double)rand() * 4.0 / RAND_MAX - 2.0) / 100.0) * 16383.0);
                        args->inertialData[i].imuSamples[j].az = (unsigned short)((1.0 + ((double)rand() * 4.0 / RAND_MAX - 2.0) / 100.0) * 16383.0);
                        args->inertialData[i].imuSamples[j].gx = (unsigned short)((0.0 + 0.01 * ((double)rand() * 2.0 / RAND_MAX)) / 250.0 * 32767.0);
                        args->inertialData[i].imuSamples[j].gy = (unsigned short)((0.0 + 0.01 * ((double)rand() * 2.0 / RAND_MAX)) / 250.0 * 32767.0);
                        args->inertialData[i].imuSamples[j].gz = (unsigned short)((0.0 + 0.01 * ((double)rand() * 2.0 / RAND_MAX)) / 250.0 * 32767.0);
                    }
                }
            }

            ctx->processedAddress += bytesToProcess;
            ctx->readAddress += bytesToRead;
            ctx->revolution += numberRevolutions;
        }
    }

    return dataToProcess;
}

