#include "pch.h"
#include "CdtTestLibrary.h"
#include "CdtTestLibraryInternals.h"


#include <ctime>


CDTDLL void * WINAPI Connect(void)
{
    CdtToolContext *ctx = new CdtToolContext;
    if (ctx)
    {
        ctx->connected = true;
        ctx->serialNumber = "12345678";

        ctx->headSpeedRpm = 60;
        ctx->pixelsPerRevolution = 60;
        ctx->numberMainSamples = MAXIMUM_MAIN_SAMPLES;
        ctx->numberTofSamples = MAXIMUM_SSIF_SAMPLES;
        ctx->numberImuSamples = 50; /* IMU samples taken every half second, so 50 (for 100Hz) */

        ctx->downloadMultiple = 8 /* Rows (or NAND pairs) */ * 2 /* Devices per bus access */ * 8192ULL /* Bytes per page */ /* <- Minimum download unit */;
        ctx->used = ctx->downloadMultiple
                  * 128 /* Pages per block*/ * 16384 /* Blocks/device */ * 2 /* Columns of NAND rows */ /* <- Number of pagerows written */
                  * 14 / 16 /* Ratio to fill up to an example of 448GB, projected full run */;
        ctx->processedAddress = 0;
        ctx->readAddress = 0;
        ctx->revolution = 0;
        time(&ctx->logStartTime); // The base time for generating timestamps
        ctx->logStartTime -= 86400; // Log starts one day ago from connection time
    }

    return reinterpret_cast<void *>(ctx);
}


CDTDLL void WINAPI Disconnect(void *context)
{
    CdtToolContext* ctx = reinterpret_cast<CdtToolContext*>(context);
    if (ctx)
    {
        delete ctx;
    }
}
