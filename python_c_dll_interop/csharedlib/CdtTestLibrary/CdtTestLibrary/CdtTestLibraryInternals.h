#pragma once

#include <cstdint>
#include <string>
#include <ctime>


// This structure is opaque to the caller, so we can use whatever we want
// as it is only ever dealt with from within the library code.
struct CdtToolContext
{
    bool    connected;
    std::string serialNumber;

    int     headSpeedRpm;  /* Speed of rotating head in RPM */
    int     pixelsPerRevolution;   /* Number of pixel stations per revolution of the rotating head */

    int     numberMainSamples;  /* Number of samples at each pixel station for main sensor - would be set from job configuration */
    int     numberTofSamples;   /* Number of samples for each ToF check - would be set from job configuration */
    int     numberImuSamples;   /* Number of samples for each IMU chunk - would be set from job configuration */

    unsigned long long  downloadMultiple;
    unsigned long long  used;               /* Amount of data stored in log memory */
    unsigned long long  readAddress;        /* Amount of data read from the tool so far */
    unsigned long long  processedAddress;   /* Amount of data processed out of the download */

    unsigned long revolution;
    time_t  logStartTime;
};

