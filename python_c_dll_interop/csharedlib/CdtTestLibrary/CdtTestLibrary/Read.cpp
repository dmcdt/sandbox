#include "pch.h"
#include "CdtTestLibrary.h"

CDTDLL void WINAPI ReadNextBlock(unsigned short* data, int length)
{
	for (int i = 0; i < length; i++)
	{
		*data++ = i;
	}
}
