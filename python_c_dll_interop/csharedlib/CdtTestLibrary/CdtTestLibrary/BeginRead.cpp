#include "pch.h"
#include "CdtTestLibrary.h"
#include "CdtTestLibraryInternals.h"


CDTDLL void WINAPI BeginRead(void* context)
{
    CdtToolContext* ctx = reinterpret_cast<CdtToolContext*>(context);

    if (ctx)
    {
        ctx->readAddress = 0;
        ctx->processedAddress = 0;
        ctx->revolution = 0;
    }
}
