#include "pch.h"
#include "CdtTestLibrary.h"
#include "CdtTestLibraryInternals.h"


CDTDLL void WINAPI EndRead(void* context)
{
    CdtToolContext* ctx = reinterpret_cast<CdtToolContext*>(context);

    if (ctx)
    {
        ctx->readAddress = 0xFFFFFFFFFFFFFFFFULL;
    }
}
