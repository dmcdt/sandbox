
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <deque>
#include <cstdint>


enum ChunkTypeId
{
    Chunk_Invalid           = 0,
    Chunk_JobStart          = 10,
    Chunk_Pixel             = 20,
    Chunk_SSIF              = 30,
    Chunk_IMU               = 40,
    Chunk_Event             = 50,
    Chunk_Resynchronisation = 100,
    Chunk_Erased            = 0xFFFF,
};


enum ChunkTimestampBits
{
    ChunkTimestamp_SecondsUnits_Pos     = 0,
    ChunkTimestamp_SecondsUnits_Len     = 4,
    ChunkTimestamp_SecondsTens_Pos      = 4,
    ChunkTimestamp_SecondsTens_Len      = 3,
    ChunkTimestamp_MinutesUnits_Pos     = 7,
    ChunkTimestamp_MinutesUnits_Len     = 4,
    ChunkTimestamp_MinutesTens_Pos      = 11,
    ChunkTimestamp_MinutesTens_Len      = 3,
    ChunkTimestamp_HoursUnits_Pos       = 14,
    ChunkTimestamp_HoursUnits_Len       = 4,
    ChunkTimestamp_HoursTens_Pos        = 18,
    ChunkTimestamp_HoursTens_Len        = 2,
    ChunkTimestamp_AM24_PM_Pos          = 20,
    ChunkTimestamp_AM24_PM_Len          = 1,
    ChunkTimestamp_Subseconds_Pos       = 21,
    ChunkTimestamp_Subseconds_Len       = 10,
    ChunkTimestamp_DayUnits_Pos         = 32,
    ChunkTimestamp_DayUnits_Len         = 4,
    ChunkTimestamp_DayTens_Pos          = 36,
    ChunkTimestamp_DayTens_Len          = 2,
    ChunkTimestamp_MonthUnits_Pos       = 38,
    ChunkTimestamp_MonthUnits_Len       = 4,
    ChunkTimestamp_MonthTens_Pos        = 42,
    ChunkTimestamp_MonthTens_Len        = 1,
    ChunkTimestamp_Weekday_Pos          = 43,
    ChunkTimestamp_Weekday_Len          = 3,
    ChunkTimestamp_YearUnits_Pos        = 46,
    ChunkTimestamp_YearUnits_Len        = 4,
    ChunkTimestamp_YearTens_Pos         = 50,
    ChunkTimestamp_YearTens_Len         = 4
};


enum ChunkTimestampWeekdays
{
    ChunkTimestamp_Monday       = (1ULL << ChunkTimestamp_Weekday_Pos),
    ChunkTimestamp_Tuesday      = (2ULL << ChunkTimestamp_Weekday_Pos),
    ChunkTimestamp_Wednesday    = (3ULL << ChunkTimestamp_Weekday_Pos),
    ChunkTimestamp_Thursday     = (4ULL << ChunkTimestamp_Weekday_Pos),
    ChunkTimestamp_Friday       = (5ULL << ChunkTimestamp_Weekday_Pos),
    ChunkTimestamp_Saturday     = (6ULL << ChunkTimestamp_Weekday_Pos),
    ChunkTimestamp_Sunday       = (7ULL << ChunkTimestamp_Weekday_Pos),
};

enum ChunkStatusBits
{
    ChunkStatus_Pos = 0
};


#pragma pack(1)
struct ChunkHeader
{
    uint16_t    type;
    uint16_t    length;
    uint64_t    timestamp;
    uint16_t    status;
    uint16_t    reserved;
};
#pragma pack()


constexpr auto CONFIGURATION_LENGTH = (1024);

#pragma pack(1)
struct ChunkJobStart
{
    ChunkHeader header;
    uint8_t     configuration[CONFIGURATION_LENGTH];
};
#pragma pack()


constexpr auto MAXIMUM_MAIN_SAMPLES = 10000;

#pragma pack(1)
struct ChunkPixel
{
    ChunkHeader header;
    uint32_t    revolutionNumber;
    uint16_t    pixelIndex;
    uint16_t    samples[MAXIMUM_MAIN_SAMPLES];
};
#pragma pack()


constexpr auto MAXIMUM_TOF_SAMPLES = 2000;

#pragma pack(1)
struct ChunkSSIF
{
    ChunkHeader header;
    uint32_t    revolutionNumber;
    uint16_t    pressure;
    uint16_t    temperatureExternal; // On the P/T sensor
    uint16_t    temperatureInternal; // The PGA305 internal
    uint16_t    temperatureCpuMainboard;
    uint16_t    temperatureCpuRotating;
    float       temperatureMotorController;
    float       motorSpeed;
    float       torque;
    float       calculatedToF;
    uint16_t    tofSamples[MAXIMUM_TOF_SAMPLES];
};
#pragma pack()


constexpr auto MAXIMUM_IMU_SAMPLES = 100;

#pragma pack(1)
struct ChunkIMU
{
    ChunkHeader header;
    uint16_t    temperatureInternal;
    uint16_t    numberSamples;
    struct
    {
        uint16_t    ax, ay, az;
        uint16_t    gx, gy, gz;
    } samples[MAXIMUM_IMU_SAMPLES];
};
#pragma pack()


enum ChunkEventSeverity
{
    ChunkEventSeverity_None = 0,
    ChunkEventSeverity_Error = 10,
    ChunkEventSeverity_Warning = 20,
    ChunkEventSeverity_Info = 30,
    ChunkEventSeverity_Debug = 40,
    ChunkEventSeverity_Trace = 50,
    ChunkEventSeverity_All = 100,
    ChunkEventSeverity_Max = 255
};

enum ChunkEventModule
{
    ChunkEventModule_Mainboard          = 0,
    ChunkEventModule_Mainboard_Power    = 1,
    ChunkEventModule_Mainboard_NAND     = 2,
    ChunkEventModule_RotatingHead       = 64,
    ChunkEventModule_MotorController    = 128,
};

// The the meanings of the eventCode values depends on the module.
enum ChunkEventCode_Mainboard
{
    ChunkEventCode_Mainboard_CpuReset   = 0
};

enum ChunkEventCode_MainboardPower
{
    ChunkEventCode_MainboardPower_PowerFail = 0
};

#pragma pack(1)
struct ChunkEvent
{
    ChunkHeader header;
    uint8_t     severity;
    uint8_t     module;
    uint8_t     eventCode;
};
#pragma pack()


constexpr auto NUMBER_RESYNC_SAMPLES = 8;
enum ChunkResynchronisationFields
{
    ChunkResynchronisation_Field0 = 0xDFFFU,
    ChunkResynchronisation_Field1 = 0xE000U,
};

#pragma pack(1)
struct ChunkResynchronisation
{
    ChunkHeader header;
    uint16_t    sync[NUMBER_RESYNC_SAMPLES];
};
#pragma pack()


constexpr auto NandPageDataSize = 8192; // Number of bytes in the data area of a single page in NAND flash memory device
constexpr auto NandPageExtraSize = 448; // Number of bytes in the spare area of a single page in NAND flash memory device
constexpr auto NandPageTotalSize = (NandPageDataSize + NandPageExtraSize);
constexpr auto NandUnitNumberChips = 2; // Number of chips that are written together as one unit
constexpr auto NandUnitPageSize = NandUnitNumberChips * NandPageTotalSize;

typedef std::vector<uint8_t> Page;

constexpr auto NandArrayRowWidth = 8;   // Number of NAND units within 1 row, a row is the minimum download size


void InterleavePage(std::vector<Page>& dst, std::deque<Page> const& src)
{
    for (size_t i = 0; i < NandPageTotalSize; i++)
    {
    }
}


void WriteToFile(std::ofstream& fstr, std::vector<Page> const& data)
{
    for (std::vector<Page>::const_iterator it = data.begin(); it != data.end(); it++)
    {
        fstr.write(reinterpret_cast<char const*>(&((*it)[0])), (*it).size());
    }
}


void RemoveRow(std::deque<Page>& pages)
{
    for (int i = 0; i < NandArrayRowWidth; i++)
    {
        pages.pop_front();
    }
}


int main()
{
    std::string         filename;
    std::ofstream       fstr;
    std::deque<Page>    pageBuffer;
    std::vector<Page>   writeBuffer;    // Page used to store re-arranged data into and write to file

    filename = "nand_dump.cdt";
    fstr.open(filename, std::ios::binary);
    if (fstr.is_open())
    {
        // Initial page
        pageBuffer.push_back(Page(NandPageTotalSize, 0xFF));

        writeBuffer.resize(NandArrayRowWidth, Page(NandPageTotalSize, 0xFF));

        do
        {
            // Append some data.

            // Check if we have an entire row to write.
            if (pageBuffer.size() > NandArrayRowWidth)
            {
                // Re-arrange page buffer into write buffer
                InterleavePage(writeBuffer, pageBuffer);
                WriteToFile(fstr, writeBuffer);
                RemoveRow(pageBuffer);
            }
        } while (0);

        // Need to make it up to the next multiple of NandArrayRowWidth
        for (uint32_t additionalPages = NandArrayRowWidth - (pageBuffer.size() % NandArrayRowWidth); additionalPages > 0; additionalPages--)
        {
            pageBuffer.push_back(Page(NandPageTotalSize, 0xFF));
        }

        // Any remaining data
        while (!pageBuffer.empty())
        {
            // Re-arrange page buffer into write buffer
            InterleavePage(writeBuffer, pageBuffer);
            WriteToFile(fstr, writeBuffer);
            RemoveRow(pageBuffer);
        }

        fstr.close();
    }
    else
    {
        std::cerr << "Could not open file for writing: \"" << filename << "\"." << std::endl;
    }
}
