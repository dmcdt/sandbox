// CdtLibTestApp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <vector>

#include "CdtTestLibrary.h"

int main()
{
    std::cout << "Size of SecondarySensors == " << sizeof SecondarySensors << std::endl;

#if 0
    unsigned long long timestamp;

    unsigned long long revolutionIndex;
    int numberOfMainSamplesInRevolution; // Could change +/-1 depending on whether interleaved or not

    short pressure;
    short temperature;

    short imuAccelerationX;
    short imuAccelerationY;
    short imuAccelerationZ;

    int numberTofSamples;
    short tofSamples[2000];
#endif

#define DISP(x) std::cout << "SecondarySensors." << #x << " == " << sizeof(((struct SecondarySensors *)0)->x) << " @ " << offsetof(SecondarySensors, x) << std::endl;
    //std::cout << "Offset of SecondarySensors. == " << offsetof(SecondarySensors, ) << std::endl;
    DISP(timestamp);
    DISP(revolutionIndex);
    DISP(numberOfMainSamplesInRevolution);
    DISP(pressure);
    DISP(temperature);
    DISP(imuAccelerationX);
    DISP(imuAccelerationY);
    DISP(imuAccelerationZ);
    DISP(numberTofSamples);
    DISP(tofSamples);
    DISP(tofSamples[0]);


#define DISP2(x,y) std::cout << #x << "." << #y << " == " << sizeof(((struct x *)0)->y) << " @ " << offsetof(x, y) << std::endl;

    std::cout << "Size of MainSensor == " << sizeof MainSensor << std::endl;
    DISP2(MainSensor, timestamp);
    DISP2(MainSensor, revolutionNumber);
    DISP2(MainSensor, pixelIndex);
    DISP2(MainSensor, samples);

    std::cout << "Size of SecondarySensor2 == " << sizeof SecondarySensor2 << std::endl;
    DISP2(SecondarySensor2, timestamp);
    DISP2(SecondarySensor2, revolutionNumber);
    DISP2(SecondarySensor2, pressure);
    DISP2(SecondarySensor2, temperature);
    DISP2(SecondarySensor2, numberTofSamples);
    DISP2(SecondarySensor2, tofSamples);

    std::cout << "Size of InertialSensor == " << sizeof InertialSensor << std::endl;
    DISP2(InertialSensor, timestamp);
    DISP2(InertialSensor, temperature);
    DISP2(InertialSensor, numberSamples);
    DISP2(InertialSensor, imuSamples);


    //int foo = Add(1, 2);

    void* ctx;
    ctx = Connect();
    if (ctx)
    {
        MultipleArguments       args;
        int                     gotData;
        std::vector<MainSensor> mainData;
        std::vector<SecondarySensor2> secondaryData;
        std::vector<InertialSensor> inertialData;

        int rotationalSpeed = 60; /* RPM */
        int imuSampleRate = 100; /* Hz */
        int imuSamplesPerChunk = 50;

        int requestRevolutions; /* vertical steps == number of revolutions */
        int requestDuration;
        
        requestRevolutions = 2000;
        requestDuration = (requestRevolutions * 60 + rotationalSpeed - 1) / rotationalSpeed;

        int pixelsPerRevolution = 60;
        int revolutionsPerSsif = 1;

        int numberMainData;
        int numberSsifData;
        int numberImuData;

        numberMainData = requestRevolutions * pixelsPerRevolution;
        numberSsifData = (requestRevolutions + revolutionsPerSsif - 1) / revolutionsPerSsif;
        numberImuData = requestDuration * imuSampleRate / imuSamplesPerChunk;

        mainData.resize(numberMainData);
        secondaryData.resize(numberSsifData);
        inertialData.resize(numberImuData);

        BeginRead(ctx);

        do
        {
            // Set the output arrays
            args.mainLength = mainData.size();
            args.mainData = &mainData[0];
            args.secondaryLength = secondaryData.size();
            args.secondaryData = &secondaryData[0];
            args.inertialLength = inertialData.size();
            args.inertialData = &inertialData[0];

            // Read next block
            gotData = ReadAllBlocks(ctx, &args);
            if (gotData)
            {
                // TODO - do something with the data
                std::cout << ".";
            }
        } while (gotData);

        EndRead(ctx);

        Disconnect(ctx);
        ctx = 0;
    }

    std::cout << "Press return to exit";
    std::string waitForEnter;
    std::getline(std::cin, waitForEnter);

    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
