#ifndef CDTTESTLIBRARY_H
#define CDTTESTLIBRARY_H

#if defined(_MSC_VER) && _MSC_VER>=1020
#pragma once
#endif

#include <windows.h>

#if defined(CDTTESTLIBRARY_EXPORTS)
#define CDTDLL extern "C" __declspec(dllexport)
#else
#define CDTDLL extern "C" __declspec(dllimport)
#endif


struct Statistics
{
    char const          *serialNumber;
    unsigned short      firmwareVersion;
    unsigned long long  capacity;
    unsigned long long  used;
};


struct SecondarySensors
{
    unsigned long long timestamp;

    unsigned long long revolutionIndex;
    int numberOfMainSamplesInRevolution; // Could change +/-1 depending on whether interleaved or not

    short pressure;
    short temperature;

    short imuAccelerationX;
    short imuAccelerationY;
    short imuAccelerationZ;

    int numberTofSamples;
    short tofSamples[2000];
};


constexpr auto MAXIMUM_MAIN_SAMPLES = 10000;

// NB: The memory layout of these structures may not match exactly the chunk
// structures defined in the NAND Memory description. The reason for that is
// this is the interface from python to the C DLL, NOT a replica of what is
// stored in NAND flash. For example, redundant fields may not be reported here.
struct MainSensor
{
    unsigned long long  timestamp;
    unsigned long       revolutionNumber;
    unsigned short      pixelIndex;
    short               samples[MAXIMUM_MAIN_SAMPLES];
};


constexpr auto MAXIMUM_SSIF_SAMPLES = 2000;

struct SecondarySensor2
{
    unsigned long long  timestamp;
    unsigned long       revolutionNumber;

    unsigned short      pressure;
    unsigned short      temperature;

    int                 numberTofSamples;
    short               tofSamples[MAXIMUM_SSIF_SAMPLES];
};


constexpr auto MAXIMUM_IMU_SAMPLES = 100;

struct ImuSamples
{
    short   ax, ay, az;
    short   gx, gy, gz;
};

struct InertialSensor
{
    unsigned long long  timestamp;
    unsigned short      temperature;

    unsigned short      numberSamples;
    ImuSamples          imuSamples[MAXIMUM_IMU_SAMPLES];
};


// This structure is the one where data is read from the tool.
// The caller passes in the storage and the length of the storage.
// The DLL writes into the storage and updates the length to show
// how many elements have been filled in.
struct MultipleArguments
{
    int mainLength;
    struct MainSensor* mainData;

    int secondaryLength;
    struct SecondarySensor2* secondaryData;

    int inertialLength;
    struct InertialSensor* inertialData;
};

// Simple function to test the interface between python and C
CDTDLL int WINAPI Add(int a, int b);

// Creates a new connection to the tool and returns a newly allocated handle
// for the other DLL functions to use in the communications.
CDTDLL void *WINAPI Connect(void);

// Gets some statistics about the tool
CDTDLL void WINAPI GetStatistics(void *context, struct Statistics* stats);

// Informs the tool that we are to begin reading the log memory.
CDTDLL void WINAPI BeginRead(void* context);

CDTDLL void WINAPI ReadNextBlock(unsigned short* data, int length);
CDTDLL void WINAPI ReadNextBlockBoth(unsigned short* mainData, int mainLength, struct SecondarySensors *secondaryData, int secondaryLength);
CDTDLL int WINAPI ReadAllBlocks(void* context, struct MultipleArguments *args);

// Stops reading the log memory. The connection is still active.
CDTDLL void WINAPI EndRead(void* context);

// Closes a new connection to the tool and frees the memory for the handle.
CDTDLL void WINAPI Disconnect(void *context);

#endif
