#include "pch.h"
#include "Identity.h"

#include <sstream>
#include <vector>


// Treat the uint32_t as a 4 byte string
std::string CompanyDescription(uint32_t companyId)
{
   std::vector<char>    buffer(5, 0);

   buffer[0] = companyId & 0xFF;
   buffer[1] = (companyId >> 8) & 0xFF;
   buffer[2] = (companyId >> 16) & 0xFF;
   buffer[3] = (companyId >> 24) & 0xFF;

   return std::string(&buffer[0]);
}


std::string DeviceTypeDescription(uint16_t deviceId)
{
    std::stringstream   ss;
    bool                recognised = false;

    switch (deviceId)
    {
        case DeviceType::Validus_StaticMainboard:
            ss << "Validus Mainboard";
            recognised = true;
            break;

        case DeviceType::Validus_RotatingHead:
            ss << "Validus Rotating Head";
            recognised = true;
            break;

        default:
            ss << "Unrecognised";
            break;
    }

    if (recognised && (deviceId & DeviceType::BootloaderActive))
    {
        ss << " Bootloader";
    }

    return ss.str();
}

