#ifndef IDENTITY_H
#define IDENTITY_H

/*
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 */


#include <cstdint>
#include <string>


class DeviceType
{
public:
    enum Value : uint16_t
    {
        Invalid                 = 0x0000,
        Validus_StaticMainboard = 0x0001,
        Validus_RotatingHead    = 0x0002,
        BootloaderActive        = 0x8000,
    };
};


std::string CompanyDescription(uint32_t companyId);
std::string DeviceTypeDescription(uint16_t deviceId);


#endif
