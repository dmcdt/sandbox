#include "pch.h"

#include <iostream>
#include <ctime>

#include "CereusDownload.h"
#include "CereusDownloadInternals.h"
#include "BufferSizes.h"
#include "LogAddressing.h"
#include "FT3XXHelper.h"


void ConfigureConnection(CereusToolContext* ctx);


CEREUSDLL void * WINAPI Connect(void)
{
    LogAddressing_Init();

    CereusToolContext *ctx = new CereusToolContext;
    if (ctx)
    {
        // Just open the first FTDI FT3xx device, assume only one physically connected to system
        ctx->connected = ctx->device.Open(0);
        if (ctx->connected)
        {
            ConfigureConnection(ctx);

            ctx->serialNumber = "";

            ctx->badBlockTable.resize(2 * 8 * 2048);
            ctx->bbtIndex = 0;

            ctx->writeBufferUsb.reserve(48 * OVERSAMPLE_RATIO);
            //ctx->readBufferUsb.reserve(USB_READBADBLOCKS_RESPONSE_LENGTH * OVERSAMPLE_RATIO); // If it were only commands.
            ctx->readBufferUsb.reserve(ReceiveBufferSize()); // Enough space for a download from NAND

            ctx->responsePacket.reserve(USB_READBADBLOCKS_RESPONSE_LENGTH + 10);

            ctx->deinterleaved.reserve(ReceiveBufferSize());
            ctx->downloadToProcess.reserve(ReceiveBufferSize());

            ctx->headSpeedRpm = 60;
            ctx->pixelsPerRevolution = 60;
            ctx->numberMainSamples = MAXIMUM_MAIN_SAMPLES;
            ctx->numberTofSamples = MAXIMUM_SSIF_SAMPLES;
            ctx->numberImuSamples = 50; /* IMU samples taken every half second, so 50 (for 100Hz) */

            ctx->downloadMultiple = 8 /* Rows (or NAND pairs) */ * 2 /* Devices per bus access */ * 8192ULL /* Bytes per page */ /* <- Minimum download unit */;
            ctx->memoryInstalled = 0;
            ctx->memoryUsableCapacity = 0;
            ctx->memoryUsed = 0;
            ctx->processedAddress = 0;
            ctx->readAddress = 0;
            ctx->revolution = 0;
            time(&ctx->logStartTime); // The base time for generating timestamps
            ctx->logStartTime -= 86400; // Log starts one day ago from connection time
        }
    }

    return reinterpret_cast<void *>(ctx);
}


void ConfigureConnection(CereusToolContext* ctx)
{
    FT_STATUS ftStatus;

    ftStatus = FT_SetSuspendTimeout(ctx->device.Handle(), 0);
    if (ftStatus != FT_OK)
    {
        /* TODO log error */
        std::cerr << ctx->ftStrings.Format(ftStatus, "seting USB suspend time");
    }

    ftStatus = FT_SetPipeTimeout(ctx->device.Handle(), FT_CHANNEL1_WRITE, 1000);
    if (ftStatus != FT_OK)
    {
        /* TODO log error */
        std::cerr << ctx->ftStrings.Format(ftStatus, "seting timeout on write pipe");
    }

    ftStatus = FT_SetPipeTimeout(ctx->device.Handle(), FT_CHANNEL1_READ, 1000);
    if (ftStatus != FT_OK)
    {
        /* TODO log error */
        std::cerr << ctx->ftStrings.Format(ftStatus, "seting timeout on read pipe");
    }
}


void CycleConnection(CereusToolContext* ctx)
{
    FT_STATUS   ftStatus;

    ftStatus = FT_CycleDevicePort(ctx->device.Handle());
    if (ftStatus != FT_OK)
    {
        /* TODO log error */
    }

    ctx->device.Close();
    ctx->connected = false;

    int openRetries;

    openRetries = RETRY_COUNT_PORT_CYCLE;
    do
    {
        Sleep(500);
        openRetries--;
        ctx->connected = ctx->device.Open(0);
    } while ((!ctx->device.IsOpen()) && (openRetries > 0));

    ConfigureConnection(ctx);

    // Short delay between opening and hitting the device with communications, for starting up.
    Sleep(100);
}


CEREUSDLL void WINAPI Disconnect(void *context)
{
    CereusToolContext* ctx = reinterpret_cast<CereusToolContext*>(context);
    if (ctx)
    {
        ctx->device.Close();
        delete ctx;
    }
}
