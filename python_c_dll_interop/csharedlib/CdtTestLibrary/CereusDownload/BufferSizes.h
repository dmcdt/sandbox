#ifndef BUFFERSIZES_H
#define BUFFERSIZES_H

/*
 *  Copyright (c) 2021 Cereus Ultrasonics Ltd, All rights reserved.
 *
 *  Declarations for dowload buffer sizes.
 */

#pragma once


#include <cstddef>
#include <cstdint>


// TODO these need to be recognised from the tool somehow - queried during identification?
static const size_t PageMainAreaCount = 8192;
static const size_t PageMainAreaSize = PageMainAreaCount * sizeof(uint16_t);

static const size_t PageSpareAreaCount = 448;
static const size_t PageSpareAreaSize = PageSpareAreaCount * sizeof(uint16_t);

static const size_t PageElementCount = PageMainAreaCount + PageSpareAreaCount;
static const size_t PageSize = PageElementCount * sizeof(uint16_t);


static const size_t RowCount = 8;
static const size_t ColumnCount = 2;


size_t ReceiveBufferSize(void);
size_t MemoryContentSize(void);


#endif

