/*
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 */


#include "Ft3xxEnumerator.h"



#if 0
ftStatus = FT_CreateDeviceInfoList(&numberDevices);
if (ftStatus == FT_OK)
{
    std::cout << "There are " << numberDevices << " devices attached." << std::endl;
    if (numberDevices > 0)
    {
        std::vector<FT_DEVICE_LIST_INFO_NODE>   deviceList(numberDevices);
        ftStatus = FT_GetDeviceInfoList(&deviceList[0], &numberDevices);
        if (ftStatus == FT_OK)
        {
            for (size_t deviceIndex = 0; deviceIndex < numberDevices; deviceIndex++)
            {
                std::string serialNumber = GetStringFromArray(deviceList[deviceIndex].SerialNumber, FTD3XXSERIALNUMBER_SIZE);
                std::string description = GetStringFromArray(deviceList[deviceIndex].Description, FTD3XXDESCRIPTION_SIZE);
                std::cout << deviceIndex << ". S/N: " << serialNumber << " - \"" << description << "\"" << std::endl;
            }

            std::cout << "Enter device index to open (0 ... " << (numberDevices - 1) << ") or -1 to exit: ";
            std::cin >> openIndex;
        }
        else
        {
            ShowError(ftStatus, "get device list");
        }
    }
}
else
{
    ShowError(ftStatus, "create device list");
}
#endif


Ft3xxEnumerator::Ft3xxEnumerator()
{
    FT_STATUS   ftStatus;
    ftStatus = FT_CreateDeviceInfoList(&numberDevices);
    if (ftStatus == FT_OK)
    {
        deviceList.resize(numberDevices);
        if (numberDevices)
        {
            ftStatus = FT_GetDeviceInfoList(&deviceList[0], &numberDevices);
            if (ftStatus != FT_OK)
            {
                numberDevices = 0;
                deviceList.clear();
            }
        }
    }
    else
    {
        numberDevices = 0;
        deviceList.clear();
    }
}


std::vector<FT_DEVICE_LIST_INFO_NODE>::const_iterator Ft3xxEnumerator::cbegin()
{
    return deviceList.cbegin();
}


std::vector<FT_DEVICE_LIST_INFO_NODE>::const_iterator Ft3xxEnumerator::cend()
{
    return deviceList.cend();
}
