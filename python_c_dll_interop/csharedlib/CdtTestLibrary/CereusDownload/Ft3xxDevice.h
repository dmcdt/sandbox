#ifndef FT3XXDEVICE_H
#define FT3XXDEVICE_H

/*
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 *
 *  Device access and operating class for FT3XX devices.
 */


#include "FTD3XX.h"


class Ft3xxDevice
{
public:
    Ft3xxDevice(void);
    ~Ft3xxDevice(void);

    bool Open(unsigned long index);
    void Close(void);

    FT_HANDLE Handle(void) const;
    bool IsOpen() const;

private:
    FT_HANDLE   handle;
};

#endif
