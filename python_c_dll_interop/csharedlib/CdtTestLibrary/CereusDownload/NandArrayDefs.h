#ifndef NANDARRAYDEFS_H
#define NANDARRAYDEFS_H

/*
 *  Copyright (c) 2021 Cereus Ultrasonics Ltd, All rights reserved.
 *
 *  Declarations for the NAND array layout.
 */


#pragma once


 /* Number of locations in the NAND array. */
#define NAND_NUMBER_ROWS                (8)
#define NAND_NUMBER_COLUMNS             (2)
#define NAND_NUMBER_LOCATIONS           (NAND_NUMBER_COLUMNS * NAND_NUMBER_ROWS)

/* Number of NAND devices that are accessed simultaneously on the bus. */
#define NAND_ARRAY_SIMULTANEOUSDEVICES  (2)

/* Number of physical devices within the array. */
#define NAND_NUMBER_DEVICES             (NAND_NUMBER_LOCATIONS * NAND_ARRAY_SIMULTANEOUSDEVICES)


/* Assume fixed number of blocks within a device, for fixed storage sizes. */
#define NAND_BLOCKSPERDEVICE            (16384UL)


#endif

