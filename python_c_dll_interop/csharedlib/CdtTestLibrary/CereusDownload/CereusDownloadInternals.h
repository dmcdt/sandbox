#ifndef CEREUSDOWNLOADINTERNALS_H
#define CEREUSDOWNLOADINTERNALS_H

#pragma once

#include <string>
#include <vector>
#include <deque>
#include <fstream>
#include <cstdint>
#include <ctime>

#include "Ft3xxDevice.h"
#include "Ft3xxErrorDescriptions.h"
#include "UsbCommands.h"


// TODO should turn this into a class and have the functions for processing the data as member functions. (The DLL functions would then call the members through the context pointer to the object of the class).
// This structure is opaque to the caller, so we can use whatever we want
// as it is only ever dealt with from within the library code.
struct CereusToolContext
{
    Ft3xxErrorDescriptions  ftStrings;
    Ft3xxDevice             device;
    bool                    connected;

    ExpandedBuffer_t        writeBufferUsb;             // Data to write across USB, with message index in high byte and data in low byte, with duplicates to extend the signals for slower reading by the MCU.
    ExpandedBuffer_t        readBufferUsb;              // Data received over USB, formatted as above

    std::vector<uint8_t>    responsePacket;             // The data extracted from the readBufferUsb that the MCU responded with.

    uint32_t                companyId;
    uint16_t                deviceId;
    std::string             serialNumber;
    int                     firmwareVersionMajor;
    int                     firmwareVersionMinor;
    int                     firmwareVersionRevision;
    uint32_t                firmwareChecksum;

    uint64_t                memoryInstalled;            // Total amount of memory detected in the tool, in pagerows equivalent
    uint64_t                memoryUsableCapacity;       // Amount of usable memory in tool (accounting for undetected devices and bad blocks), in pagerows equivalent
    uint64_t                memoryUsed;                 // Amount of data stored in log memory, in pagerows

    uint64_t                pagerowReadPosition;        // The log memory current read position.

    uint64_t                memoryBytesProcessed;       // Number of bytes processed (read from tool, extracted to output arrays).
    uint64_t                memoryBytesTotalToProcess;  // Total number of bytes that need to be processed from the tool memory.
    uint64_t                progressDivisor;            // Divisor for setting the above into the progress values.


    std::vector<uint32_t>   attachedDevices;            // Table showing devices attached in the NAND array (required to know which data is valid in the download)

    std::vector<uint32_t>   badBlockTable;              // Bad Block Table from device
    uint32_t                bbtIndex;                   // Index to read from while populating BBT

    std::vector<uint16_t>   deinterleaved;              // The deinterleaved pagerow data, with main and spare areas
    std::vector<uint16_t>   downloadToProcess;          // Only the main page area for the log data that needs to be processed
    //size_t                  remainingLength;            // Amount of data remaining in the above buffer after as much data has been processed as possible

    std::deque<uint64_t>    deferredSkipPositions;      // Positions at which the number of bytes processed must be increased by a page, to skip the bad pages, deferred from when it was decided in order to keep the count accurate to the processing position

    std::ofstream           debugDumpFile;              // Dump log memory to raw binary file for debugging purposes

    uint32_t                currentProgress;            // Current progress - set using SetProgress()
    uint32_t                totalProgress;              // Total progress - set using SetProgress()

    int     headSpeedRpm;  /* Speed of rotating head in RPM */
    int     pixelsPerRevolution;   /* Number of pixel stations per revolution of the rotating head */

    int     numberMainSamples;  /* Number of samples at each pixel station for main sensor - would be set from job configuration */
    int     numberTofSamples;   /* Number of samples for each ToF check - would be set from job configuration */
    int     numberImuSamples;   /* Number of samples for each IMU chunk - would be set from job configuration */

    unsigned long long  downloadMultiple;
    unsigned long long  readAddress;        /* Amount of data read from the tool so far */
    unsigned long long  processedAddress;   /* Amount of data processed out of the download */

    unsigned long revolution;
    time_t  logStartTime;
};

#endif
