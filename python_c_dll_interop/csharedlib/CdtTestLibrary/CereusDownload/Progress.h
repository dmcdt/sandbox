#ifndef PROGRESS_H
#define PROGRESS_H

/*
 *  Copyright (c) 2021 Cereus Ultrasonics Ltd, All rights reserved.
 *
 *  Declarations for reporting progress, that are available
 *  to other source files within the DLL, but not exposed at the DLL
 *  interface (they are declared in the DLL public header).
 */


#pragma once

#include <cstdint>

#include "CereusDownloadInternals.h"

void SetProgress(CereusToolContext* ctx, uint32_t current, uint32_t total);


#endif

