/*
 *  Copyright (c) 2021 Cereus Ultrasonics Ltd, All rights reserved.
 *
 *  Definitions for dowload buffer sizes.
 */

#include "pch.h"
#include "BufferSizes.h"


// 8640 bytes per page
// 2 NAND devices per pair = 2 * 8640 = 17,280 bytes per page
// 8 NAND pairs per row = 17,280 * 8 = 138,240 bytes per row
// Read 2 bytes for each read so Number of NAND reads = 138,240 / 2 = 69,120
// 8 FTCLKS per NAND read = 69,120 * 8 = 552,960 FTCLKs, so number of elements in target array must match
// True read length is xxxx bytes, we want to read extra for MCU <-> NAND switching times

// Receive buffer size in uint16_t's (as that is transfer size of FT600 FIFO Master Bus)
size_t ReceiveBufferSize(void)
{
    const size_t            pageRowReceiveSize = MemoryContentSize();
    const size_t            pageRowExtraSize = 100; // Some additional buffer to catch slow to start transfers
    const size_t            pageRowBufferSize = pageRowReceiveSize + pageRowExtraSize;
    const size_t            maximumPageRowsToRead = 1; // Maximum number of pageRows to read in one read operation. Can only read one at a time with the FT600 timeout and interaction required between the CPLD, FT600, and MCU to swap page reads.

    return maximumPageRowsToRead * pageRowBufferSize;
}


size_t MemoryContentSize(void)
{
    const size_t    numberFtclksPerElementRead = 8; // This is the fudge factor in the CPLD to allow the data to be read reliably. It is the number of FTCLKs taken to perform one read of a NAND chip pair.

    return PageElementCount * RowCount * numberFtclksPerElementRead;
}


