#ifndef TLVCFILE_H
#define TLVCFILE_H

/*
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 *
 *  Declarations for a Tag-Length-Value-Checksum style of file format.
 */


#include <fstream>


class TlvcFile
{
    std::fstream&   fs;

public:
    TlvcFile(std::fstream& stream);

    void Append(uint32_t type, char const *value, uint32_t length);
};

#endif
