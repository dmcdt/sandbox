/*
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 */


#include "pch.h"
#include "Ft3xxDeviceInfo.h"


namespace
{
    size_t my_strlen_s(const char* str, size_t strsz)
    {
        size_t  len = 0;

        if (str)
        {
            while ((*str++ != '\0') && (strsz--))
            {
                ++len;
            }
        }

        return len;
    }


    std::string GetStringFromArray(char const* arr, size_t maximumLength)
    {
        size_t actualLength = my_strlen_s(arr, maximumLength);
        return std::string(arr, actualLength);
    }
}


#define FTD3XXSERIALNUMBER_SIZE (sizeof(((FT_DEVICE_LIST_INFO_NODE*)0)->SerialNumber) / sizeof(((FT_DEVICE_LIST_INFO_NODE*)0)->SerialNumber[0]))
#define FTD3XXDESCRIPTION_SIZE  (sizeof(((FT_DEVICE_LIST_INFO_NODE*)0)->Description) / sizeof(((FT_DEVICE_LIST_INFO_NODE*)0)->Description[0]))



Ft3DeviceInfo::Ft3DeviceInfo(FT_DEVICE_LIST_INFO_NODE const& infoNode)
{
    flags = infoNode.Flags;
    type = infoNode.Type;
    id = infoNode.ID;
    locId = infoNode.LocId;
    serialNumber = GetStringFromArray(infoNode.SerialNumber, FTD3XXSERIALNUMBER_SIZE);
    description = GetStringFromArray(infoNode.Description, FTD3XXDESCRIPTION_SIZE);
}


ULONG Ft3DeviceInfo::Flags() const
{
    return flags;
}


ULONG Ft3DeviceInfo::Type() const
{
    return type;
}


ULONG Ft3DeviceInfo::ID() const
{
    return id;
}


DWORD Ft3DeviceInfo::LocId() const
{
    return locId;
}


std::string Ft3DeviceInfo::SerialNumber() const
{
    return serialNumber;
}


std::string Ft3DeviceInfo::Description() const
{
    return description;
}
