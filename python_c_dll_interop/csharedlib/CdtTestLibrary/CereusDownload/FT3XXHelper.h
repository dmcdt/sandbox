#ifndef FT3XXHELPER_H
#define FT3XXHELPER_H

/*
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 */


#include <map>
#include <string>

#include <FTD3XX.h>

#define FT_USER_PIPE_FIRST  (2)
#define FT_USER_PIPE_LAST   (5)

#define FT_PIPE_TYPE_WRITE  (0x00)
#define FT_PIPE_TYPE_READ   (0x80)

#define FT_CHANNEL1_WRITE   (FT_PIPE_TYPE_WRITE | (FT_USER_PIPE_FIRST + 0))
#define FT_CHANNEL1_READ    (FT_PIPE_TYPE_READ | (FT_USER_PIPE_FIRST + 0))

#define FT_CHANNEL2_WRITE   (FT_PIPE_TYPE_WRITE | (FT_USER_PIPE_FIRST + 1))
#define FT_CHANNEL2_READ    (FT_PIPE_TYPE_READ | (FT_USER_PIPE_FIRST + 1))

#define FT_CHANNEL3_WRITE   (FT_PIPE_TYPE_WRITE | (FT_USER_PIPE_FIRST + 2))
#define FT_CHANNEL3_READ    (FT_PIPE_TYPE_READ | (FT_USER_PIPE_FIRST + 2))

#define FT_CHANNEL4_WRITE   (FT_PIPE_TYPE_WRITE | (FT_USER_PIPE_FIRST + 3))
#define FT_CHANNEL4_READ    (FT_PIPE_TYPE_READ | (FT_USER_PIPE_FIRST + 3))


#endif
