#include "pch.h"
#include "CereusDownload.h"
#include "CereusDownloadInternals.h"

#include <iostream>
#include <algorithm>
#include <functional>
#include <cmath>
#include <ctime>

#include <Windows.h>

#include "UsbCommands.h"
#include "FT3XXHelper.h"
#include "usleep.h"
#include "BufferSizes.h"
#include "LogMemory.h"
#include "BadBlockTable.h"
#include "LogAddressing.h"
#include "Progress.h"
#include "DataHeader.h"


enum ChunkTimestampBits
{
    ChunkTimestamp_SecondsUnits_Pos = 0,
    ChunkTimestamp_SecondsUnits_Len = 4,
    ChunkTimestamp_SecondsTens_Pos = 4,
    ChunkTimestamp_SecondsTens_Len = 3,
    ChunkTimestamp_MinutesUnits_Pos = 7,
    ChunkTimestamp_MinutesUnits_Len = 4,
    ChunkTimestamp_MinutesTens_Pos = 11,
    ChunkTimestamp_MinutesTens_Len = 3,
    ChunkTimestamp_HoursUnits_Pos = 14,
    ChunkTimestamp_HoursUnits_Len = 4,
    ChunkTimestamp_HoursTens_Pos = 18,
    ChunkTimestamp_HoursTens_Len = 2,
    ChunkTimestamp_AM24_PM_Pos = 20,
    ChunkTimestamp_AM24_PM_Len = 1,
    ChunkTimestamp_Subseconds_Pos = 21,
    ChunkTimestamp_Subseconds_Len = 10,
    ChunkTimestamp_DayUnits_Pos = 32,
    ChunkTimestamp_DayUnits_Len = 4,
    ChunkTimestamp_DayTens_Pos = 36,
    ChunkTimestamp_DayTens_Len = 2,
    ChunkTimestamp_MonthUnits_Pos = 38,
    ChunkTimestamp_MonthUnits_Len = 4,
    ChunkTimestamp_MonthTens_Pos = 42,
    ChunkTimestamp_MonthTens_Len = 1,
    ChunkTimestamp_Weekday_Pos = 43,
    ChunkTimestamp_Weekday_Len = 3,
    ChunkTimestamp_YearUnits_Pos = 46,
    ChunkTimestamp_YearUnits_Len = 4,
    ChunkTimestamp_YearTens_Pos = 50,
    ChunkTimestamp_YearTens_Len = 4
};


#if 0
enum ChunkTimestampWeekdays
{
    ChunkTimestamp_Monday = (1ULL << ChunkTimestamp_Weekday_Pos),
    ChunkTimestamp_Tuesday = (2ULL << ChunkTimestamp_Weekday_Pos),
    ChunkTimestamp_Wednesday = (3ULL << ChunkTimestamp_Weekday_Pos),
    ChunkTimestamp_Thursday = (4ULL << ChunkTimestamp_Weekday_Pos),
    ChunkTimestamp_Friday = (5ULL << ChunkTimestamp_Weekday_Pos),
    ChunkTimestamp_Saturday = (6ULL << ChunkTimestamp_Weekday_Pos),
    ChunkTimestamp_Sunday = (7ULL << ChunkTimestamp_Weekday_Pos),
};
#endif


bool ReadPagerow(CereusToolContext* ctx, uint32_t pagerow);
bool SendReadPagerowCommand(CereusToolContext* ctx, uint32_t pagerow);
bool ReceiveReadPagerowResponse(CereusToolContext* ctx);
bool IsBlockGood(CereusToolContext* ctx, uint32_t logicalPageNumber, uint16_t const* page);
bool IsChunkOutputFull(ChunkHeader_t const* chunk, struct MultipleArguments* args, int mainDataIndex, int ssifDataIndex, int imuDataIndex);


unsigned long long MakeTimestamp(double fractionalTimeValue)
{
    unsigned long long timestampFields;

    time_t  timestampSeconds;   // Whole seconds of timestamp for conversion
    tm timestampParts;    // Broken down timestamp for the fields

    timestampSeconds = static_cast<time_t>(fractionalTimeValue);
    errno_t err = gmtime_s(&timestampParts, &timestampSeconds);

    timestampFields = 0;

    timestampFields |= static_cast<unsigned long long>(timestampParts.tm_sec % 10) << ChunkTimestamp_SecondsUnits_Pos;
    timestampFields |= static_cast<unsigned long long>(timestampParts.tm_sec / 10) << ChunkTimestamp_SecondsTens_Pos;
    timestampFields |= static_cast<unsigned long long>(timestampParts.tm_min % 10) << ChunkTimestamp_MinutesUnits_Pos;
    timestampFields |= static_cast<unsigned long long>(timestampParts.tm_min / 10) << ChunkTimestamp_MinutesTens_Pos;
    timestampFields |= static_cast<unsigned long long>(timestampParts.tm_hour % 10) << ChunkTimestamp_HoursUnits_Pos;
    timestampFields |= static_cast<unsigned long long>(timestampParts.tm_hour / 10) << ChunkTimestamp_HoursTens_Pos;

    // Work out the subsecond field
    fractionalTimeValue -= timestampSeconds;
    fractionalTimeValue *= 1024;
    int subseconds;
    subseconds = 1023 - (int)fractionalTimeValue;
    if (subseconds > 1023)
    {
        subseconds = 1023;
    }
    if (subseconds < 0)
    {
        subseconds = 0;
    }
    timestampFields |= static_cast<unsigned long long>(subseconds) << ChunkTimestamp_Subseconds_Pos;

    timestampFields |= static_cast<unsigned long long>(timestampParts.tm_mday % 10) << ChunkTimestamp_DayUnits_Pos;
    timestampFields |= static_cast<unsigned long long>(timestampParts.tm_mday / 10) << ChunkTimestamp_DayTens_Pos;
    timestampFields |= static_cast<unsigned long long>((timestampParts.tm_mon + 1) % 10) << ChunkTimestamp_MonthUnits_Pos;
    timestampFields |= static_cast<unsigned long long>((timestampParts.tm_mon + 1) / 10) << ChunkTimestamp_MonthTens_Pos;
    timestampFields |= static_cast<unsigned long long>(timestampParts.tm_wday == 0 ? 7 : timestampParts.tm_wday) << ChunkTimestamp_Weekday_Pos;
    timestampFields |= static_cast<unsigned long long>((timestampParts.tm_year + 1900) % 10) << ChunkTimestamp_YearUnits_Pos;
    timestampFields |= static_cast<unsigned long long>(((timestampParts.tm_year + 1900) / 10) % 10) << ChunkTimestamp_YearTens_Pos;

    return timestampFields;
}


uint64_t UpdateProcessedLength(uint64_t processedLength, uint32_t incrementLength)
{
    if (incrementLength)
    {
        uint64_t    baseProcessedFullPages;

        // Round down current position to multiple of complete pages
        baseProcessedFullPages = (processedLength / PageSize) * PageSize;

        // Increase by adjustment
        processedLength += incrementLength;

        // Calculate number of complete new pages processed and add on the spare area
        uint64_t completePagesProcessed = (processedLength - baseProcessedFullPages) / PageMainAreaSize;
        if (completePagesProcessed)
        {
            processedLength += completePagesProcessed * PageSpareAreaSize;
        }
    }

    return processedLength;
}

template<class InIt> size_t ErasedCount(InIt first, InIt last)
{
    size_t erasedCount = std::distance(first,
                                       std::find_if_not(first, last,
                                                        std::bind(std::equal_to<uint16_t>(), Chunk_Erased, std::placeholders::_1)));

    return erasedCount;
}


uint64_t SkipDeferredBadBlocks(uint64_t processedSize, std::deque<uint64_t>& deferredPositions)
{
    while ((!deferredPositions.empty()) &&
        (deferredPositions.front() <= processedSize))
    {
        processedSize += PageSize;
        deferredPositions.pop_front();
    }

    return processedSize;
}


CEREUSDLL int WINAPI ReadAllBlocks(void* context, struct MultipleArguments* args)
{
    int dataToProcess = 0;
    bool checkData = false;

    CereusToolContext* ctx = reinterpret_cast<CereusToolContext*>(context);
    if (ctx && args)
    {
        int     mainDataIndex = 0;
        int     ssifDataIndex = 0;
        int     imuDataIndex = 0;
        bool    outputFull = false;

        while ((ctx->memoryBytesProcessed < ctx->memoryBytesTotalToProcess) &&
               (!outputFull))
        {
            // Process the log memory contents.
            size_t processPosition = 0;
            ChunkHeader_t const* chunk = 0;

            // Find first non-erased data
            size_t erasedCount = ErasedCount(ctx->downloadToProcess.begin(), ctx->downloadToProcess.end());
            processPosition += erasedCount;
            ctx->memoryBytesProcessed = UpdateProcessedLength(ctx->memoryBytesProcessed, erasedCount * sizeof(uint16_t));
            ctx->memoryBytesProcessed = SkipDeferredBadBlocks(ctx->memoryBytesProcessed, ctx->deferredSkipPositions);

            // Set the chunk pointer if we still have some data.
            if (processPosition < ctx->downloadToProcess.size())
            {
                chunk = reinterpret_cast<ChunkHeader_t const*>(&ctx->downloadToProcess[processPosition]);
                outputFull = IsChunkOutputFull(chunk, args, mainDataIndex, ssifDataIndex, imuDataIndex);
            }

            // If we have data to process
            if ((processPosition < ctx->downloadToProcess.size()) && (chunk) &&
                (chunk->length / sizeof(uint16_t)) <= ((ctx->downloadToProcess.size() - processPosition)) &&
                (!outputFull))
            {
                while ((processPosition < ctx->downloadToProcess.size()) && (chunk) &&
                    (chunk->length / sizeof(uint16_t)) <= ((ctx->downloadToProcess.size() - processPosition)) &&
                    (!outputFull))
                {
                    if (chunk->length == 0)
                    {
                        std::cerr << "Chunk length zero." << std::endl;
                        DebugBreak();
                        // TODO need to scan forward for a resync chunk
                    }

                    // TODO process chunk data
                    switch (chunk->type)
                    {
                    case Chunk_Pixel:
                        ChunkPixel_t const* pixel;
                        pixel = reinterpret_cast<ChunkPixel_t const*>(chunk);

                        // TODO verify the chunk has same contents as what was programmed into memory - verifies download is correct
                        if ((checkData) &&
                            (pixel->chunk.header.length != (22 + ctx->numberMainSamples * sizeof(pixel->samples[0]) + (1 - (ctx->numberMainSamples % 2)) * 2)))
                        {
                            std::cerr << "Main length not as expected." << std::endl;
                            DebugBreak();
                        }
                        pixel->chunk.header.timestamp;
                        pixel->chunk.header.status;
                        pixel->chunk.header.reserved;
                        pixel->chunk.revolutionNumber;
                        pixel->chunk.pixelIndex;
                        if ((checkData) &&
                            (memcmp(mainSensor, pixel->samples, ctx->numberMainSamples * sizeof(pixel->samples[0]))))
                        {
                            std::cerr << "Main sample data not as expected." << std::endl;
                            DebugBreak();
                        }

                        // Copy chunk data to output arrays.
                        args->mainData[mainDataIndex].timestamp = pixel->chunk.header.timestamp;
                        args->mainData[mainDataIndex].revolutionNumber = pixel->chunk.revolutionNumber;
                        args->mainData[mainDataIndex].pixelIndex = pixel->chunk.pixelIndex;
                        memcpy(args->mainData[mainDataIndex].samples, pixel->samples, ctx->numberMainSamples * sizeof(pixel->samples[0]));
                        mainDataIndex++;
                        break;

                    case Chunk_SSIF:
                        ChunkSSIF_t const* ssif;
                        ssif = reinterpret_cast<ChunkSSIF_t const*>(chunk);
                        
                        // Verify the chunk has same contents as what was programmed into memory - verifies download is correct
                        if ((checkData) &&
                            (ssif->chunk.header.length != (46 + ctx->numberTofSamples * sizeof(ssif->tofSamples[0]) + (1 - (ctx->numberTofSamples % 2)) * 2)))
                        {
                            std::cerr << "SSIF length not as expected." << std::endl;
                            DebugBreak();
                        }
                        ssif->chunk.header.timestamp;
                        ssif->chunk.header.status;
                        ssif->chunk.header.reserved;
                        ssif->chunk.revolutionNumber;
                        ssif->chunk.pressure;
                        ssif->chunk.temperatureExternal;
                        ssif->chunk.temperatureInternal;
                        ssif->chunk.temperatureCpuMainboard;
                        ssif->chunk.temperatureCpuRotating;
                        ssif->chunk.temperatureMotorController;
                        ssif->chunk.motorSpeed;
                        ssif->chunk.torque;
                        ssif->chunk.calculatedToF == 46.5;
                        if (checkData)
                        {
                            for (int sampleIndex = 0; sampleIndex < ctx->numberTofSamples; ++sampleIndex)
                            {
                                if (tofSensor[sampleIndex] != ssif->tofSamples[sampleIndex])
                                {
                                    std::cerr << "ToF sample data not as expected." << std::endl;
                                    DebugBreak();
                                }
                            }
                        }

                        // Copy SSIF data to output array.
                        args->secondaryData[ssifDataIndex].timestamp = ssif->chunk.header.timestamp;
                        args->secondaryData[ssifDataIndex].revolutionNumber = ssif->chunk.revolutionNumber;
                        args->secondaryData[ssifDataIndex].pressure = ssif->chunk.pressure;
                        args->secondaryData[ssifDataIndex].temperature = ssif->chunk.temperatureExternal;
                        args->secondaryData[ssifDataIndex].numberTofSamples = ctx->numberTofSamples;
                        memcpy(args->secondaryData[ssifDataIndex].tofSamples, ssif->tofSamples, ctx->numberTofSamples * sizeof(ssif->tofSamples[0]));
                        ssifDataIndex++;
                        break;

                    case Chunk_IMU:
                        ChunkIMU_t const* imu;
                        imu = reinterpret_cast<ChunkIMU_t const*>(chunk);
                        
                        // TODO verify the chunk has same contents as what was programmed into memory - verifies download is correct
                        if ((checkData) &&
                            (imu->chunk.header.length != (20 + imu->chunk.numberSamples * sizeof(imu->samples[0]))))
                        {
                            std::cerr << "IMU length not as expected." << std::endl;
                            DebugBreak();
                        }
                        imu->chunk.header.timestamp;
                        imu->chunk.header.status;
                        imu->chunk.header.reserved;
                        imu->chunk.temperatureInternal;
                        imu->chunk.numberSamples;

                        if (checkData)
                        {
                            uint16_t gx = 7;
                            uint16_t gy = 0xFFF9; // -7
                            uint16_t gz = 3;

                            for (int i = 0; i < imu->chunk.numberSamples; i++)
                            {
                                if ((imu->samples[i].ax != 164) ||
                                    (imu->samples[i].ay != 0xFF5C) ||
                                    (imu->samples[i].az != 16384) ||
                                    (imu->samples[i].gx != gx) ||
                                    (imu->samples[i].gy != gy) ||
                                    (imu->samples[i].gz != gz))
                                {
                                    std::cerr << "IMU data not as expected." << std::endl;
                                    DebugBreak();
                                }

                                gx = 0x10000 - gx;
                                gy = 0x10000 - gy;
                                gz = 0x10000 - gz;
                            }
                        }

                        args->inertialData[imuDataIndex].timestamp = imu->chunk.header.timestamp;
                        args->inertialData[imuDataIndex].temperature = imu->chunk.temperatureInternal;
                        args->inertialData[imuDataIndex].numberSamples = imu->chunk.numberSamples;
                        memcpy(args->inertialData[imuDataIndex].imuSamples, imu->samples, imu->chunk.numberSamples * sizeof(imu->samples[0]));
                        imuDataIndex++;
                        break;

                        // TODO process other chunk types, maybe for DLL internal usage or other reporting to the caller.
                    }

                    // Move onto next chunk and check that there is space to process it.
                    processPosition += (chunk->length / sizeof(uint16_t));
                    ctx->memoryBytesProcessed = UpdateProcessedLength(ctx->memoryBytesProcessed, chunk->length);
                    ctx->memoryBytesProcessed = SkipDeferredBadBlocks(ctx->memoryBytesProcessed, ctx->deferredSkipPositions);

                    // Find first non-erased data, starting from current position
                    size_t erasedCount = ErasedCount(ctx->downloadToProcess.begin() + processPosition, ctx->downloadToProcess.end());
                    processPosition += erasedCount;
                    ctx->memoryBytesProcessed = UpdateProcessedLength(ctx->memoryBytesProcessed, erasedCount * sizeof(uint16_t));
                    ctx->memoryBytesProcessed = SkipDeferredBadBlocks(ctx->memoryBytesProcessed, ctx->deferredSkipPositions);

                    if (processPosition < ctx->downloadToProcess.size())
                    {
                        chunk = reinterpret_cast<ChunkHeader_t const*>(&ctx->downloadToProcess[processPosition]);
                        outputFull = IsChunkOutputFull(chunk, args, mainDataIndex, ssifDataIndex, imuDataIndex);
                    }
                    else
                    {
                        chunk = nullptr;
                    }
                }

                dataToProcess = 1;
            }
            else
            {
                // Need more data. Read next pagerow. Iterate to process.
                if (ctx->pagerowReadPosition < ctx->memoryUsed)
                {
                    if (ReadPagerow(ctx, ctx->pagerowReadPosition))
                    {
                        UsbCommand_ExtractAndDeinterleaveLogMemory(ctx->readBufferUsb, ctx->deinterleaved);

                        if (ctx->debugDumpFile.is_open())
                        {
                            ctx->debugDumpFile.write(reinterpret_cast<char*>(&ctx->deinterleaved[0]), ctx->deinterleaved.size() * sizeof(ctx->deinterleaved[0]));
                            // TODO check for file errors and report
                        }

                        // TODO - process downloaded page structure, ECC etc
                        for (int row = 0; row < RowCount; row++)
                        {
                            // TODO check that device is attached

                            uint32_t logicalPageNumber = LogAddressing_GetLogicalPageFromPageRow(ctx->pagerowReadPosition) + row;

                            if (IsBlockGood(ctx, logicalPageNumber, &ctx->deinterleaved[row * PageElementCount]))
                            {
                                size_t  currentLength = ctx->downloadToProcess.size();
                                ctx->downloadToProcess.resize(currentLength + PageMainAreaCount);
                                memcpy(&ctx->downloadToProcess[currentLength], &ctx->deinterleaved[row * PageElementCount], PageMainAreaSize);
                            }
                            else
                            {
                                // TODO log fact that block is bad? - to update and send back to the tool
                                    
                                // Skip it
                                //ctx->memoryBytesProcessed += PageSize;
                                ctx->deferredSkipPositions.push_back((uint64_t)logicalPageNumber * PageSize);
                            }
                        }

                        ctx->pagerowReadPosition++;
                    }
                }
                else
                {
                    // Need more data but there is none in the file. Corrupt end of file for chunk not to complete?
                    // TODO log error state
                    // Move to end of data to terminate processing.
                    uint64_t countToEnd = ctx->downloadToProcess.size() - processPosition;
                    processPosition = ctx->downloadToProcess.size();
                    ctx->memoryBytesProcessed = UpdateProcessedLength(ctx->memoryBytesProcessed, countToEnd * sizeof(uint16_t));
                    ctx->memoryBytesProcessed = SkipDeferredBadBlocks(ctx->memoryBytesProcessed, ctx->deferredSkipPositions);
                }
            }

            // Remove the processed data.
            if (processPosition)
            {
                size_t remainingLength;
                remainingLength = ctx->downloadToProcess.size() - processPosition;
                if (remainingLength)
                {
                    memmove(&ctx->downloadToProcess[0], &ctx->downloadToProcess[processPosition], remainingLength * sizeof(ctx->downloadToProcess[0]));
                }
                ctx->downloadToProcess.resize(remainingLength);
            }

            // Processed count so far updated at the point where the data is processed rather than once here.
            // Allows us to track down the position within the memory dump / download that the chunk is at
            // immediately at the expense of a few extra counter updates.
            SetProgress(ctx, ctx->memoryBytesProcessed / ctx->progressDivisor, ctx->memoryBytesTotalToProcess / ctx->progressDivisor);
        }

        /* Update output count of how many array elements have been filled in. */
        args->mainLength = mainDataIndex;
        args->secondaryLength = ssifDataIndex;
        args->inertialLength = imuDataIndex;
    }

    return dataToProcess;
}


bool ReadPagerow(CereusToolContext* ctx, uint32_t pagerow)
{
    int     attempts = RETRY_COUNT_COMMAND_RESEND;
    bool    commandOk;

    do
    {
        commandOk = false;
        attempts--;

        if (SendReadPagerowCommand(ctx, pagerow))
        {
            if (ReceiveReadPagerowResponse(ctx))
            {
                commandOk = true;
            }
        }

        if (!commandOk)
        {
            std::cerr << "Retrying pagerow " << pagerow << std::endl;
        }
    } while ((!commandOk) && (attempts > 0));

    // TODO see also FT_CycleDevicePort() to power cycle the port if automated reset needs to be performed
    // Close handle afterware, Sleep for 500ms, poll the number of devices until it reappears, then reopen handle

    return commandOk;
}


bool SendReadPagerowCommand(CereusToolContext* ctx, uint32_t pagerow)
{
    bool                    writeOk = false;
    FT_STATUS               ftStatus;

    std::vector<uint8_t> command = UsbCommand_ReadPageRows(pagerow, 1);
    CreateWriteBuffer(command, ctx->writeBufferUsb);

    ULONG ulBytesToWrite = ctx->writeBufferUsb.size() * sizeof(ctx->writeBufferUsb[0]) / sizeof(UCHAR);
    ULONG ulBytesWritten;
    ftStatus = FT_WritePipe(ctx->device.Handle(), FT_CHANNEL1_WRITE, reinterpret_cast<PUCHAR>(&ctx->writeBufferUsb[0]), ulBytesToWrite, &ulBytesWritten, NULL);
    if (ftStatus == FT_OK)
    {
        //usbFile.Append(UsbFile::DataOut, reinterpret_cast<char*>(&writeBuffer[0]), writeBuffer.size() * sizeof(writeBuffer[0]));
        writeOk = true;
    }
    else
    {
        /* TODO log error */

        ftStatus = FT_AbortPipe(ctx->device.Handle(), FT_CHANNEL1_WRITE);
        if (ftStatus != FT_OK)
        {
            /* TODO log error */
        }
    }

    return writeOk;
}


bool ReceiveReadPagerowResponse(CereusToolContext* ctx)
{
    bool readOk = false;
    FT_STATUS               ftStatus;

    ctx->readBufferUsb.resize(ReceiveBufferSize());

    ULONG ulBytesRead;
    ftStatus = FT_ReadPipe(ctx->device.Handle(), FT_CHANNEL1_READ, reinterpret_cast<PUCHAR>(&ctx->readBufferUsb[0]), ctx->readBufferUsb.size() * sizeof(ctx->readBufferUsb[0]) / sizeof(UCHAR), &ulBytesRead, NULL);

    // The read size seems to come through as 552690 elements, which is less than 552960+100. But it's repeatable, and probably ties
    // in with the number of bytes dropped at each TXE_N since it works out to 69086.25 bytes which is 34 bytes smaller than the page size of 69120.
    // 34 is a familiar number to be short from the

    ULONG ulElementsRead = ulBytesRead / sizeof(ctx->readBufferUsb[0]);
    //QMessageBox::information(this, "Information", QString("Received %1 bytes, %2 elements").arg(ulBytesRead).arg(ulElementsRead));
    if (ftStatus == FT_OK)
    {
        size_t offset = 0;
        if (ulElementsRead == 552691)
        {
            ulElementsRead--;
            offset = 1;
        }

        if (ulElementsRead == 552690)
        {
            ctx->readBufferUsb.resize(offset + ulElementsRead);
            readOk = true;
        }
        else
        {
            /* TODO log error */
        }
    }
    else
    {
        /* TODO log error */

        ftStatus = FT_AbortPipe(ctx->device.Handle(), FT_CHANNEL1_READ);
        if (ftStatus != FT_OK)
        {
            /* TODO log error */
        }
    }

    return readOk;
}


// TODO should make this a class member
// TODO should download the bad block table and use it to base the decisions off - tool might not have time to update flash based BBT (although it will have to for it to work).
bool IsBlockGood(CereusToolContext* ctx, uint32_t logicalPageNumber, uint16_t const *page)
{
    bool        blockGood = false;

    uint32_t    row = LogAddressing_GetRow(logicalPageNumber);
    uint32_t    deviceBlockIndex = LogAddressing_GetPhysicalBlockInDevice(logicalPageNumber);
    uint32_t    column = LogAddressing_GetColumn(logicalPageNumber);

    uint32_t    bbtIndex = (((column * NAND_NUMBER_ROWS) + row) * NAND_BBT_TABLESIZE) + NAND_BBT_INDEX(deviceBlockIndex);
    uint32_t    bbtShift = NAND_BBT_SHIFT(deviceBlockIndex);
    uint32_t    bbtCell = ctx->badBlockTable[bbtIndex];
    uint32_t    bbtEntry = bbtCell >> bbtShift;

    blockGood = ((bbtEntry & NAND_BBT_SIMULTANEOUS_MASK) == NAND_BBT_SIMULTANEOUS_GOOD);

    return blockGood;
}


bool IsChunkOutputFull(ChunkHeader_t const *chunk, struct MultipleArguments* args, int mainDataIndex, int ssifDataIndex, int imuDataIndex)
{
    bool    outputFull = false;

    
    if (chunk && args)
    {
        switch (chunk->type)
        {
            case Chunk_Pixel:
                outputFull = (mainDataIndex >= args->mainLength);
                break;

            case Chunk_SSIF:
                outputFull = (ssifDataIndex >= args->secondaryLength);
                break;

            case Chunk_IMU:
                outputFull = (imuDataIndex >= args->inertialLength);
                break;
        }
    }

    return outputFull;
}

