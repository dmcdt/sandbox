/*
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 */

#include "pch.h"
#include "TlvcFile.h"

#include <stdint.h>

#include "crc32.h"


TlvcFile::TlvcFile(std::fstream& stream) :
    fs(stream)
{
}


void TlvcFile::Append(uint32_t type, char const *value, uint32_t length)
{
    std::streampos  lastGoodPosition;
    lastGoodPosition = fs.tellp();

    uint32_t    crc = crc32(value, length);

    fs.write(reinterpret_cast<char *>(&type), sizeof type);
    fs.write(reinterpret_cast<char *>(&length), sizeof length);
    fs.write(value, length);
    fs.write(reinterpret_cast<char *>(&crc), sizeof crc);

    if (!fs.good())
    {
        fs.clear();
        fs.seekp(lastGoodPosition, std::ios::beg);
        // TODO - truncate file to the last known good position? Means closing the filestream, truncating and re-opening, which might not be straightforward from this class as need path not just file handle.
    }
}
