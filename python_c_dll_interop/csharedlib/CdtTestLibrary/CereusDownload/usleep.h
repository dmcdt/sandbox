#ifndef USLEEP_H
#define USLEEP_H

#pragma once

#include <cstdint>


void usleep(int64_t usec);

#endif

