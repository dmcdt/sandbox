#ifndef FT3XXDEVICEINFO_H
#define FT3XXDEVICEINFO_H

/*
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 *
 *  Class to represent FTD3XX device information. Basically just an extraction
 *  from the FTDI C structure to store it in a C++ class/types.
 */


#include "FTD3XX.h"

#include <string>



class Ft3DeviceInfo
{
public:
    Ft3DeviceInfo(FT_DEVICE_LIST_INFO_NODE const& infoNode);

    ULONG Flags() const;
    ULONG Type() const;
    ULONG ID() const;
    DWORD LocId() const;

    std::string SerialNumber() const;
    std::string Description() const;

private:
    ULONG flags;
    ULONG type;
    ULONG id;
    DWORD locId;
    std::string serialNumber;
    std::string description;
    // Handle not extracted for now
};

#endif
