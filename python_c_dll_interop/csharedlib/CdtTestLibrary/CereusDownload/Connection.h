#ifndef CONNECTION_H
#define CONNECTION_H

/*
 *  Copyright (c) 2021 Cereus Ultrasonics Ltd, All rights reserved.
 * 
 *  Declarations for managing connection to device, that are available
 *  to other source files within the DLL, but not exposed at the DLL
 *  interface (they are declared in the DLL public header).
 */


#pragma once


#include "CereusDownloadInternals.h"


void CycleConnection(CereusToolContext* ctx);


#endif

