#include "pch.h"

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

#include "CereusDownload.h"
#include "CereusDownloadInternals.h"

#include <FTD3XX.h>
#include "FT3XXHelper.h"
#include "Ft3xxErrorDescriptions.h"
#include "Ft3xxEnumerator.h"
#include "Ft3xxDeviceInfo.h"

#include "UsbCommands.h"
#include "Identity.h"
#include "crc16_modbus.h"
#include "UsbFile.h"
#include "crc32.h"
#include "Connection.h"


bool GetIdentity(CereusToolContext* ctx);
bool GetMemoryUsage(CereusToolContext* ctx);
bool SendAndReceiveIdentify(CereusToolContext* ctx);
bool SendGetPageRowCountsCommand(CereusToolContext* ctx);
bool ReceiveGetPageRowCountsResponse(CereusToolContext* ctx);


CEREUSDLL void WINAPI GetStatistics(void* context, struct Statistics* stats)
{
    CereusToolContext* ctx = reinterpret_cast<CereusToolContext*>(context);

    GetIdentity(ctx);
    //Sleep(1);
    GetMemoryUsage(ctx);

    stats->serialNumber = ctx->serialNumber.c_str();
    stats->firmwareVersion = ctx->firmwareVersionMajor * 100 + ctx->firmwareVersionMinor;
    stats->capacity = ctx->memoryUsableCapacity;
    stats->used = ctx->memoryUsed;
}


bool GetIdentity(CereusToolContext* ctx)
{
    int     portResetAttempts;          // Retries for cycling the device port
    int     communicationsAttempts;     // Retries for sending and receiving the command
    bool    commandOk;

    portResetAttempts = RETRY_COUNT_PORT_CYCLE;
    do
    {
        communicationsAttempts = RETRY_COUNT_COMMAND_RESEND;

        do
        {
            communicationsAttempts--;

            commandOk = SendAndReceiveIdentify(ctx);

            if (!commandOk)
            {
                std::cerr << "GetIdentity - retrying command." << std::endl;
                Sleep(100);
            }
        } while ((!commandOk) && (communicationsAttempts > 0));

        if ((!commandOk) && (portResetAttempts > 0))
        {
            std::cerr << "GetIdentity - cycle port." << std::endl;
            portResetAttempts--;
            CycleConnection(ctx);
        }
    } while ((!commandOk) && (portResetAttempts > 0));

    return commandOk;
}


bool SendAndReceiveIdentify(CereusToolContext* ctx)
{
    bool commandOk = false;
    FT_STATUS               ftStatus;
    //ExpandedBuffer_t        writeBuffer;
    std::vector<uint8_t>    command = UsbCommand_Identify();

    CreateWriteBuffer(command, ctx->writeBufferUsb);

    ULONG ulBytesToWrite = ctx->writeBufferUsb.size() * sizeof(ctx->writeBufferUsb[0]) / sizeof(UCHAR);
    ULONG ulBytesWritten;
    ftStatus = FT_WritePipe(ctx->device.Handle(), FT_CHANNEL1_WRITE, reinterpret_cast<PUCHAR>(&ctx->writeBufferUsb[0]), ulBytesToWrite, &ulBytesWritten, NULL);
    if (ftStatus == FT_OK)
    {
        ExpandedBuffer_t        readBuffer;
        ULONG                   ulElementsRead;
        ULONG                   ulBytesRead;
        std::vector<uint8_t>    response;

        //Sleep(1);

        readBuffer.resize(OVERSAMPLE_RATIO * USB_IDENTIFY_RESPONSE_LENGTH + 10);
        ftStatus = FT_ReadPipe(ctx->device.Handle(), FT_CHANNEL1_READ, reinterpret_cast<PUCHAR>(&readBuffer[0]), readBuffer.size() * sizeof(readBuffer[0]) / sizeof(UCHAR), &ulBytesRead, NULL);

        ulElementsRead = ulBytesRead / sizeof(readBuffer[0]);
#if 0
        received = QString("Received %1 bytes, %2 elements:").arg(ulBytesRead).arg(ulElementsRead);

        for (DWORD i = 0; i < ulElementsRead; i++)
        {
            if ((i % 16) == 0)
            {
                received += "\r\n";
            }
            received += QString("%1 ").arg((uint)readBuffer[i], 4, 16, QChar('0'));
        }
        received += "\r\n";
        ui->log->appendPlainText(received);
#endif

        if (ftStatus == FT_OK)
        {
            readBuffer.resize(ulElementsRead);
            ExtractResponse(readBuffer, response);
            if (response.size() > 2)
            {
                uint16_t    crcCalculated = CRC16Modbus_Calculate(&response[0], response.size() - 2);
                uint16_t    crcReceived = (response[response.size() - 1] << 8) | response[response.size() - 2];
                if (crcCalculated == crcReceived)
                {
#if 0
                    QString extracted;
                    extracted = "Extracted: ";
                    for (size_t index = 0; index < response.size(); index++)
                    {
                        extracted += QString("0x%1 ").arg((uint)response[index], 2, 16, QChar('0'));
                    }
                    extracted += "\r\n";
                    ui->log->appendPlainText(extracted);
#endif

                    // Parse identity from extracted bytes
                    if (response.size() == USB_IDENTIFY_RESPONSE_LENGTH)
                    {
                        if (response[1] == UsbResponseStatus_Ok)
                        {
                            ctx->companyId = response[2] | (response[3] << 8) | (response[4] << 16) | (response[5] << 24);
                            ctx->deviceId = response[6] | (response[7] << 8);

                            // Fixed length string extraction. Need to remove trailing nulls and blanks (0xFF)
                            ctx->serialNumber = std::string(reinterpret_cast<char*>(&response[15]), USB_IDENTIFY_RESPONSE_SERIAL_LENGTH);
                            std::string::size_type snLastPos = ctx->serialNumber.find_last_not_of("\x00\xFF");
                            if (snLastPos != ctx->serialNumber.npos)
                            {
                                ctx->serialNumber.erase(snLastPos + 1);
                            }

                            ctx->firmwareVersionMajor = response[8];
                            ctx->firmwareVersionMinor = response[9];
                            ctx->firmwareVersionRevision = response[10];
                            ctx->firmwareChecksum = response[11] | (response[12] << 8) | (response[13] << 16) | (response[14] << 24);

                            commandOk = true;
                        }
                        else
                        {
                            //ui->log->appendPlainText(QString("Response status 0x%1 is not OK").arg(response[1], 16));
                            ctx->serialNumber = "Response status is not OK.";
                        }
                    }
                    else
                    {
                        //ui->log->appendPlainText("Response length does not match expected.");
                        ctx->serialNumber = "Response length not expected.";
                    }
                }
                else
                {
#if 0
                    QString message;
                    message = QString("CRC mismatch calc %1 != recv %2\r\n").arg((uint)crcCalculated, 4, 16, QChar('0')).arg((uint)crcReceived, 4, 16, QChar('0'));
                    ui->log->appendPlainText(message);
#endif
                    ctx->serialNumber = "Response packet CRC mismatch.";
                }
            }
            else
            {
#if 0
                QString message;
                message = QString("Packet too short for valid response %1 bytes\r\n").arg(response.size());
                ui->log->appendPlainText(message);
#endif
                ctx->serialNumber = "Response packet too short";
            }
        }
        else
        {
            ctx->serialNumber = ctx->ftStrings.Format(ftStatus, "read from USB");

            /* TODO send OOB abort command to FIFO master */
            /* TODO wait some duration for FIFO master to abort */
            ftStatus = FT_AbortPipe(ctx->device.Handle(), FT_CHANNEL1_READ);
            if (ftStatus != FT_OK)
            {
                /* TODO log */
            }
        }
    }
    else
    {
        ctx->serialNumber = ctx->ftStrings.Format(ftStatus, "write to USB");

        /* TODO send OOB abort command to FIFO master */
        /* TODO wait some duration for FIFO master to abort */
        ftStatus = FT_AbortPipe(ctx->device.Handle(), FT_CHANNEL1_WRITE);
        if (ftStatus != FT_OK)
        {
            /* TODO log */
        }
    }

    return commandOk;
}


bool GetMemoryUsage(CereusToolContext* ctx)
{
    int     portResetAttempts;          // Retries for cycling the device port
    int     communicationsAttempts;     // Retries for sending and receiving the command
    bool    commandOk;

    portResetAttempts = RETRY_COUNT_PORT_CYCLE;
    do
    {
        communicationsAttempts = RETRY_COUNT_COMMAND_RESEND;

        do
        {
            commandOk = false;
            communicationsAttempts--;

            if (SendGetPageRowCountsCommand(ctx))
            {
                Sleep(1);
                if (ReceiveGetPageRowCountsResponse(ctx))
                {
                    commandOk = true;
                }
            }

            if (!commandOk)
            {
                std::cerr << "GetMemoryUsage - retrying command." << std::endl;
                Sleep(100);
            }
        } while ((!commandOk) && (communicationsAttempts > 0));

        if ((!commandOk) && (portResetAttempts > 0))
        {
            std::cerr << "GetMemoryUsage - cycle port." << std::endl;
            portResetAttempts--;
            CycleConnection(ctx);
        }
    } while ((!commandOk) && (portResetAttempts > 0));

    return commandOk;
}


bool SendGetPageRowCountsCommand(CereusToolContext* ctx)
{
    bool                    writeOk;
    FT_STATUS               ftStatus;
    //ExpandedBuffer_t        writeBuffer;
    std::vector<uint8_t>    command = UsbCommand_GetPagerowCounts();

    writeOk = false;

    CreateWriteBuffer(command, ctx->writeBufferUsb);

    ULONG ulBytesToWrite = ctx->writeBufferUsb.size() * sizeof(ctx->writeBufferUsb[0]) / sizeof(UCHAR);
    ULONG ulBytesWritten;
    ftStatus = FT_WritePipe(ctx->device.Handle(), FT_CHANNEL1_WRITE, reinterpret_cast<PUCHAR>(&ctx->writeBufferUsb[0]), ulBytesToWrite, &ulBytesWritten, NULL);
    if (ftStatus == FT_OK)
    {
        writeOk = true;
    }
    else
    {
        //QMessageBox::critical(this, "Error", QString::fromStdString(ftStrings.Format(ftStatus, "Send GetPageRowCounts")));

        /* TODO send OOB abort command to FIFO master */
        /* TODO wait some duration for FIFO master to abort */
        ftStatus = FT_AbortPipe(ctx->device.Handle(), FT_CHANNEL1_WRITE);
        if (ftStatus != FT_OK)
        {
            /* TODO log */
        }
    }

    return writeOk;
}


bool ReceiveGetPageRowCountsResponse(CereusToolContext* ctx)
{
    bool                            readOk;
    FT_STATUS                       ftStatus;
    ExpandedBuffer_t                readBuffer;
    ULONG                           ulBytesRead;
    std::vector<uint8_t>            responsePacket;
    UsbParseResult                  parseResult;
    UsbResponseGetPagerowCounts_t   response;

    readOk = false;
    readBuffer.resize(1024);
    ftStatus = FT_ReadPipe(ctx->device.Handle(), FT_CHANNEL1_READ, reinterpret_cast<PUCHAR>(&readBuffer[0]), readBuffer.size() * sizeof(readBuffer[0]) / sizeof(UCHAR), &ulBytesRead, NULL);
    if (ftStatus == FT_OK)
    {
        ExtractResponse(readBuffer, responsePacket);
        parseResult = UsbParseResponse_GetPagerowCounts(responsePacket, response);
        if (parseResult == UsbParseResult::OkGoodResponse)
        {
            ctx->memoryInstalled = response.pagerowsInstalled;
            ctx->memoryUsableCapacity = response.pagerowsCapacity;
            ctx->memoryUsed = response.pagerowsUsed;

            readOk = true;
        }
        else
        {
            //QString message;
            //message = QString("Error in GetPageRowCounts response = 0x%1\r\n").
            //    arg(response.responseStatus, 2, 16, QChar('0'));
            //ui->log->appendPlainText(message);
        }
    }
    else
    {
        //QMessageBox::critical(this, "Error", QString::fromStdString(ftStrings.Format(ftStatus, "reading")));

        /* TODO send OOB abort command to FIFO master */
        /* TODO wait some duration for FIFO master to abort */
        ftStatus = FT_AbortPipe(ctx->device.Handle(), FT_CHANNEL1_READ);
        if (ftStatus != FT_OK)
        {
            /* TODO log */
        }
    }

    return readOk;
}


void WriteGetBadBlockCountsCommand(CereusToolContext* ctx)
{
}


void ReadGetBadBlockCountsResponse(CereusToolContext* ctx)
{
}


