#ifndef LOGADDRESSING_H
#define LOGADDRESSING_H

/*
 *  Copyright (c) 2021 Cereus Ultrasonics Ltd, All rights reserved.
 *
 *  Declarations for addressing into the log memory.
 */


#pragma once


#include <cstdint>


void LogAddressing_Init(void);
uint32_t LogAddressing_GetRow(uint32_t logicalPageAddress);
uint32_t LogAddressing_GetPhysicalBlockInDevice(uint32_t logicalPageAddress);
uint32_t LogAddressing_GetColumn(uint32_t logicalPageAddress);
uint32_t LogAddressing_GetLogicalPageFromPageRow(uint32_t pageRow);


#endif

