/*
 *  Copyright (c) 2021 Cereus Ultrasonics Ltd, All rights reserved.
 *
 *  Declarations for querying and setting progress of long running
 *  operations.
 */


#include "pch.h"

#include "CereusDownload.h"
#include "CereusDownloadInternals.h"


CEREUSDLL void WINAPI Progress(void* context, struct ProgressReport *progress)
{
    CereusToolContext* ctx = reinterpret_cast<CereusToolContext*>(context);
    if (ctx && progress)
    {
        progress->current = ctx->currentProgress;
        progress->total = ctx->totalProgress;
    }
}


void SetProgress(CereusToolContext *ctx, uint32_t current, uint32_t total)
{
    ctx->currentProgress = current;
    ctx->totalProgress = total;
}

