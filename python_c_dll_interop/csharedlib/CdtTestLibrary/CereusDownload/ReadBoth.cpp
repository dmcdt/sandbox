
#include "pch.h"
#include "CereusDownload.h"

#include <cmath>

#define M_PI (3.14159265358979323846264338327950288)

CEREUSDLL void WINAPI ReadNextBlockBoth(unsigned short* mainData, int mainLength, struct SecondarySensors* secondaryData, int secondaryLength)
{
	// In reality this code would read a block of data from the USB interface,
	// write it to disk, and then separate from the single block of data (from tool memory)
	// into the separate arrays to make python processing easier.
	for (int i = 0; i < mainLength; i++)
	{
		*mainData++ = i;
	}

	for (int i = 0; i < secondaryLength; i++)
	{
        secondaryData[i].timestamp = 1606406849 + i / 2;

        secondaryData[i].revolutionIndex = i;
        if ((i % 1) == 0)
        {
            secondaryData[i].numberOfMainSamplesInRevolution = 60;
        }
        else
        {
            secondaryData[i].numberOfMainSamplesInRevolution = 59;
        }

        secondaryData[i].pressure = 8000 - i;
        secondaryData[i].temperature = 125 - i;

        secondaryData[i].imuAccelerationX = 0;
        secondaryData[i].imuAccelerationY = 0;
        secondaryData[i].imuAccelerationZ = -32768;

        secondaryData[i].numberTofSamples = 2000;
        for (int tofSampleIndex = 0; tofSampleIndex < secondaryData[i].numberTofSamples; ++tofSampleIndex)
        {
            secondaryData[i].tofSamples[tofSampleIndex] = (short)(10000.0 * std::sin(2.0 * M_PI * 10.0 * (double)tofSampleIndex / (double)secondaryData[i].numberTofSamples));
        }
    }
}
