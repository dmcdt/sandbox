/*
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 */

#include "pch.h"
#include "LogAddressing.h"
#include "NandArrayDefs.h"
#include "BufferSizes.h"

struct NandContext
{
    /* Information about the NAND devices, all need to be the same. */
    uint16_t    pageSize;           /* bytes in page main area */
    uint16_t    pageSpareSize;      /* bytes in page spare area */
    uint16_t    blockSize;          /* pages per block */
    uint16_t    blockShift;         /* Same as above, but used to bit shift between the page and block, rather than multiply and divide. */
    uint16_t    numberPlanes;       /* number of planes (each plane contains consecutive blocks) */
    uint16_t    planeSize;          /* Number of blocks in each plane */
    uint16_t    lunSize;            /* Number of blocks in a LUN */
    uint16_t    numberLuns;         /* Number of LUNs per target/die */
    uint16_t    numberChipEnables;  /* Number of chip enables/targets/dies per package/chip/device */

    ///* Information about the size of the NAND array. */
    //uint16_t    busWidthDevices;    /* Number of devices accessed simultaneously on the bus */
    //uint16_t    numberRows;         /* Number of rows in entire NAND array. */
    //uint16_t    numberColumns;      /* Number of columns in entire NAND array. */

    /* The following fields are used to break up an array-wide logical page address */
    uint16_t    addrRowPos;         /* Bit position for the row number within the NAND array. */
    uint16_t    addrRowBits;        /* Number of bits used to represent the row number */
    uint32_t    addrRowMask;        /* Bit mask for the row bit, to be applied before shifting */
    uint16_t    addrPagePos;
    uint16_t    addrPageBits;       /* Page within the target. */
    uint32_t    addrPageMask;
    uint16_t    addrCePos;
    uint16_t    addrCeBits;         /* Chip enable/select number for the target. */
    uint32_t    addrCeMask;
    uint16_t    addrColumnPos;
    uint16_t    addrColumnBits;     /* Column within the NAND array. */
    uint32_t    addrColumnMask;
};


static NandContext gsNandContext;


void LogAddressing_Init(void)
{
    // For Intel JS29F16B08JCNE1
    gsNandContext.pageSize = PageMainAreaCount;
    gsNandContext.pageSpareSize = PageSpareAreaCount;
    gsNandContext.blockSize = 128;      /* 128 pages in a block. */
    gsNandContext.blockShift = 7;
    gsNandContext.numberPlanes = 2;
    gsNandContext.planeSize = 2048;
    gsNandContext.lunSize = gsNandContext.numberPlanes * gsNandContext.planeSize;
    gsNandContext.numberLuns = 1;
    gsNandContext.numberChipEnables = 4;

    //gsNandContext.busWidthDevices = 2; /* 16-bit bus, high and low bytes come from separate devices. */
    //gsNandContext.numberRows = NAND_NUMBER_ROWS;
    //gsNandContext.numberColumns = NAND_NUMBER_COLUMNS;

    gsNandContext.addrRowPos = 0;
    gsNandContext.addrRowBits = 3;
    gsNandContext.addrRowMask = ((1UL << gsNandContext.addrRowBits) - 1) << gsNandContext.addrRowPos;
    gsNandContext.addrPagePos = gsNandContext.addrRowBits + gsNandContext.addrRowPos;
    gsNandContext.addrPageBits = 19;
    gsNandContext.addrPageMask = ((1UL << gsNandContext.addrPageBits) - 1) << gsNandContext.addrPagePos;
    gsNandContext.addrCePos = gsNandContext.addrPageBits + gsNandContext.addrPagePos;
    gsNandContext.addrCeBits = 2;
    gsNandContext.addrCeMask = ((1UL << gsNandContext.addrCeBits) - 1) << gsNandContext.addrCePos;
    gsNandContext.addrColumnPos = gsNandContext.addrCeBits + gsNandContext.addrCePos;
    gsNandContext.addrColumnBits = 1;
    gsNandContext.addrColumnMask = ((1UL << gsNandContext.addrColumnBits) - 1) << gsNandContext.addrColumnPos;
}


uint32_t LogAddressing_GetRow(uint32_t logicalPageAddress)
{
    return (logicalPageAddress >> gsNandContext.addrRowPos) & (gsNandContext.addrRowMask >> gsNandContext.addrRowPos);
}


uint32_t LogAddressing_GetPhysicalBlockInDevice(uint32_t logicalPageAddress)
{
    return (logicalPageAddress & (gsNandContext.addrCeMask | gsNandContext.addrPageMask)) >> (gsNandContext.blockShift + gsNandContext.addrPagePos);
}


uint32_t LogAddressing_GetColumn(uint32_t logicalPageAddress)
{
    return (logicalPageAddress >> gsNandContext.addrColumnPos) & (gsNandContext.addrColumnMask >> gsNandContext.addrColumnPos);
}


uint32_t LogAddressing_GetLogicalPageFromPageRow(uint32_t pageRow)
{
    return pageRow << gsNandContext.addrPagePos;
}


