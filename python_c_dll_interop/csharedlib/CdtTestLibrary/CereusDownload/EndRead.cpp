#include "pch.h"
#include "CereusDownload.h"
#include "CereusDownloadInternals.h"


CEREUSDLL void WINAPI EndRead(void* context)
{
    CereusToolContext* ctx = reinterpret_cast<CereusToolContext*>(context);

    if (ctx)
    {
        ctx->readAddress = 0xFFFFFFFFFFFFFFFFULL;

        // Max out all the counters
        ctx->pagerowReadPosition = ctx->memoryUsed;
        ctx->memoryBytesProcessed = ctx->memoryBytesTotalToProcess;
    }
}
