/*
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 */


#include "pch.h"
#include "Ft3xxDevice.h"

#include <cassert>


Ft3xxDevice::Ft3xxDevice() :
    handle(0)
{
}


Ft3xxDevice::~Ft3xxDevice()
{
    Close();
}


bool Ft3xxDevice::Open(unsigned long index)
{
    Close();

    bool    openOk = false;

    FT_STATUS ftStatus;
    ftStatus = FT_Create(reinterpret_cast<PVOID>(index), FT_OPEN_BY_INDEX, &handle);
    if (ftStatus == FT_OK)
    {
        openOk = true;
    }

    return openOk;
}


void Ft3xxDevice::Close()
{
    if (handle)
    {
        FT_STATUS ftStatus;

        ftStatus = FT_Close(handle);
        if (ftStatus != FT_OK)
        {
            // TODO - log in some way?. Cannot throw as this is called from dtor.
        }

        handle = 0;
    }
}


FT_HANDLE Ft3xxDevice::Handle() const
{
    return handle;
}


bool Ft3xxDevice::IsOpen() const
{
    return handle != 0;
}
