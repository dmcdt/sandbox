/*
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 */


#include "pch.h"
#include "Ft3xxErrorDescriptions.h"

#include <sstream>


Ft3xxErrorDescriptions::Ft3xxErrorDescriptions()
{
    registerErrorDescriptions();
}


void Ft3xxErrorDescriptions::registerErrorDescriptions(void)
{
    statusDescription.insert(std::make_pair(FT_OK, "No error"));
    statusDescription.insert(std::make_pair(FT_INVALID_HANDLE, "Invalid handle"));
    statusDescription.insert(std::make_pair(FT_DEVICE_NOT_FOUND, "Device not found"));
    statusDescription.insert(std::make_pair(FT_DEVICE_NOT_OPENED, "Device not opened"));
    statusDescription.insert(std::make_pair(FT_IO_ERROR, "I/O error"));
    statusDescription.insert(std::make_pair(FT_INSUFFICIENT_RESOURCES, "Insufficient resources"));
    statusDescription.insert(std::make_pair(FT_INVALID_PARAMETER, "Invalid parameter"));
    statusDescription.insert(std::make_pair(FT_INVALID_BAUD_RATE, "Invalid baud rate"));
    statusDescription.insert(std::make_pair(FT_DEVICE_NOT_OPENED_FOR_ERASE, "Device not opened for erase"));
    statusDescription.insert(std::make_pair(FT_DEVICE_NOT_OPENED_FOR_WRITE, "Device not opened for write"));
    statusDescription.insert(std::make_pair(FT_FAILED_TO_WRITE_DEVICE, "Failed to write to device"));
    statusDescription.insert(std::make_pair(FT_EEPROM_READ_FAILED, "EEPROM read failed"));
    statusDescription.insert(std::make_pair(FT_EEPROM_WRITE_FAILED, "EEPROM write failed"));
    statusDescription.insert(std::make_pair(FT_EEPROM_ERASE_FAILED, "EEPROM erase failed"));
    statusDescription.insert(std::make_pair(FT_EEPROM_NOT_PRESENT, "EEPROM not present"));
    statusDescription.insert(std::make_pair(FT_EEPROM_NOT_PROGRAMMED, "EEPROM not programmed"));
    statusDescription.insert(std::make_pair(FT_INVALID_ARGS, "Invalid args"));
    statusDescription.insert(std::make_pair(FT_NOT_SUPPORTED, "Not supported"));
    statusDescription.insert(std::make_pair(FT_NO_MORE_ITEMS, "No more items"));
    statusDescription.insert(std::make_pair(FT_TIMEOUT, "Timeout"));
    statusDescription.insert(std::make_pair(FT_OPERATION_ABORTED, "Operation aborted"));
    statusDescription.insert(std::make_pair(FT_RESERVED_PIPE, "Reserved pipe"));
    statusDescription.insert(std::make_pair(FT_INVALID_CONTROL_REQUEST_DIRECTION, "Invalid control request direction"));
    statusDescription.insert(std::make_pair(FT_INVALID_CONTROL_REQUEST_TYPE, "Invalid control request type"));
    statusDescription.insert(std::make_pair(FT_IO_PENDING, "I/O pending"));
    statusDescription.insert(std::make_pair(FT_IO_INCOMPLETE, "I/O complete"));
    statusDescription.insert(std::make_pair(FT_HANDLE_EOF, "Handle EOF"));
    statusDescription.insert(std::make_pair(FT_BUSY, "Busy"));
    statusDescription.insert(std::make_pair(FT_NO_SYSTEM_RESOURCES, "No system resources"));
    statusDescription.insert(std::make_pair(FT_DEVICE_LIST_NOT_READY, "Device list not ready"));
    statusDescription.insert(std::make_pair(FT_DEVICE_NOT_CONNECTED, "Device not connected"));
    statusDescription.insert(std::make_pair(FT_INCORRECT_DEVICE_PATH, "Incorrect device path"));
    statusDescription.insert(std::make_pair(FT_OTHER_ERROR, "Other error"));
}


std::string Ft3xxErrorDescriptions::Format(FT_STATUS status, std::string const& msg)
{
    std::stringstream ss;
    std::string description;

    StatusDescriptionLookup_t::const_iterator lookup = statusDescription.find(status);
    if (lookup != statusDescription.cend())
    {
        description = lookup->second;
    }
    else
    {
        description = "Unknown error";
    }

    ss << "Error " << status << " during " << msg.c_str() << ": " << description << std::endl;

    return ss.str();
}

