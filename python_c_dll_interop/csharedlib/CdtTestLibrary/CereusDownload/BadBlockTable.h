#ifndef BADBLOCKTABLE_H
#define BADBLOCKTABLE_H

/*
 *  Copyright (c) 2021 Cereus Ultrasonics Ltd, All rights reserved.
 *
 *  Declarations for bad block table from tool.
 */


#pragma once


#include <cstdint>

#include "NandArrayDefs.h"
#include "LogAddressing.h"


#define NAND_BBT_BITS_PER_ENTRY     (2)
#define NAND_BBT_TABLESIZE          ((NAND_BLOCKSPERDEVICE * NAND_ARRAY_SIMULTANEOUSDEVICES * NAND_BBT_BITS_PER_ENTRY) / (sizeof (uint32_t) * CHAR_BIT))
#define NAND_BBT_INDEX(block)       ((block * NAND_ARRAY_SIMULTANEOUSDEVICES * NAND_BBT_BITS_PER_ENTRY) / (sizeof (uint32_t) * CHAR_BIT))
#define NAND_BBT_SHIFT(block)       ((block * NAND_ARRAY_SIMULTANEOUSDEVICES * NAND_BBT_BITS_PER_ENTRY) % (sizeof (uint32_t) * CHAR_BIT))

#define NAND_BBT_MASK               ((1UL << NAND_BBT_BITS_PER_ENTRY) - 1)
#define NAND_BBT_BAD_FACTORY        (0UL)
#define NAND_BBT_BAD_WORNOUT        (1UL)
#define NAND_BBT_RESERVEDBLOCK      (2UL)
#define NAND_BBT_GOOD               (3UL)

#define NAND_BBT_SIMULTANEOUS_MASK  ((NAND_BBT_MASK << NAND_BBT_BITS_PER_ENTRY) | NAND_BBT_MASK)
#define NAND_BBT_SIMULTANEOUS_GOOD  ((NAND_BBT_GOOD << NAND_BBT_BITS_PER_ENTRY) | NAND_BBT_GOOD)


#endif
