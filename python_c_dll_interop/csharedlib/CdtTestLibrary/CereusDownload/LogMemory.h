#ifndef LOGMEMORY_H
#define LOGMEMORY_H

/*
 *  Copyright (c) 2021 Cereus Ultrasonics Ltd, All rights reserved.
 *
 *  Declarations for log memory data structures.
 */

#pragma once


#include <cstdint>


#define CONFIGURATION_LENGTH (1024)
#define RESYNC_SAMPLES_COUNT (8)

#define MAXIMUM_ALIGNMENT   (4)

#define MAXIMUM_TOF_SAMPLES_COUNT (2000)
#define MAXIMUM_IMU_SAMPLES_COUNT (100)
#define MAXIMUM_MAIN_SAMPLES_COUNT (10000)


enum ChunkStatusBits_t
{
    ChunkStatus_Medium_Pos = 0,
    ChunkStatus_Medium_Len = 1,

    ChunkStatus_Running_Pos = 1,
    ChunkStatus_Running_Len = 1,
};


/* As agreed in email trail "Pixel data - Waveform file". */
#define CHUNKSTATUS_MEDIUM_MASK     (((1U << ChunkStatus_Medium_Len) - 1) << ChunkStatus_Medium_Pos)
#define CHUNKSTATUS_MEDIUM_GAS      (0U << ChunkStatus_Medium_Pos)
#define CHUNKSTATUS_MEDIUM_OIL      (1U << ChunkStatus_Medium_Pos)

#define CHUNKSTATUS_RUNNING_MASK    (((1U << ChunkStatus_Running_Len) - 1) << ChunkStatus_Running_Pos)
#define CHUNKSTATUS_NOT_RUNNING     (0U << ChunkStatus_Running_Pos)
#define CHUNKSTATUS_RUNNING         (1U << ChunkStatus_Running_Pos)


enum ChunkEventSeverity_t
{
    ChunkEventSeverity_None = 0,
    ChunkEventSeverity_Error = 10,
    ChunkEventSeverity_Warning = 20,
    ChunkEventSeverity_Info = 30,
    ChunkEventSeverity_Debug = 40,
    ChunkEventSeverity_Trace = 50,
    ChunkEventSeverity_All = 100,
    ChunkEventSeverity_Max = 255
};


enum ChunkEventModule_t
{
    ChunkEventModule_Mainboard = 0,
    ChunkEventModule_Mainboard_Power = 1,
    ChunkEventModule_Mainboard_NAND = 2,
    ChunkEventModule_Mainboard_System = 3,
    ChunkEventModule_RotatingHead = 64,
    ChunkEventModule_MotorController = 128,
};

/* The the meanings of the eventCode values depends on the module. */
enum ChunkEventCode_Mainboard_t
{
    ChunkEventCode_Mainboard_None = 0,
    ChunkEventCode_Mainboard_CpuReset = 1,
};


enum ChunkEventCode_MainboardPower_t
{
    ChunkEventCode_MainboardPower_None = 0,
    ChunkEventCode_MainboardPower_PowerFail = 1,
};


enum ChunkEventModule_MainboardNAND_t
{
    ChunkEventModule_MainboardNAND_None = 0,
    ChunkEventModule_MainboardNAND_BadBlock = 1,    /* New bad block discovered. */
};


enum ChunkEventModule_MainboardSystem_t
{
    ChunkEventModule_MainboardSystem_None = 0,
    ChunkEventModule_MainboardSystem_EnterArmedState = 1,
    ChunkEventModule_MainboardSystem_EnterRunState = 2,
    ChunkEventModule_MainboardSystem_EnterIdleState = 3,
};


#pragma pack(2)
struct ImuSampleFrame_t
{
    uint16_t    ax, ay, az;
    uint16_t    gx, gy, gz;
};
#pragma pack()


enum ChunkTypeId_t
{
    Chunk_Invalid = 0,
    Chunk_JobStart = 10,
    Chunk_Pixel = 20,
    Chunk_SSIF = 30,
    Chunk_IMU = 40,
    Chunk_Event = 50,
    Chunk_Resynchronisation = 100,
    Chunk_Erased = 0xFFFF,
};


#pragma pack(1)
struct ChunkHeader_t
{
    uint16_t    type;
    uint16_t    length;
    uint64_t    timestamp;
    uint16_t    status;
    uint16_t    reserved;
};
#pragma pack()


#pragma pack(1)
struct ChunkJobStart_t
{
    struct ChunkHeader_t    header;
    uint8_t                 configuration[CONFIGURATION_LENGTH];
};
#pragma pack()


#pragma pack(1)
/*  Same as the following structure, but without the samples or any padding for
 *  alignment, so the chunk can be created as an automatic variable.
 */
struct ChunkPixelWithoutSamples_t
{
    struct ChunkHeader_t    header;
    uint32_t                revolutionNumber;
    uint16_t                pixelIndex;
};
#pragma pack()


#pragma pack(1)
struct ChunkPixel_t
{
    struct ChunkPixelWithoutSamples_t   chunk;
    uint16_t                            samples[MAXIMUM_MAIN_SAMPLES_COUNT];
    uint16_t                            padding;
};
#pragma pack()


#pragma pack(1)
struct ChunkSSIFWithoutSamples_t
{
    struct ChunkHeader_t    header;
    uint32_t                revolutionNumber;
    uint16_t                pressure;
    uint16_t                temperatureExternal; // On the P/T sensor
    uint16_t                temperatureInternal; // The PGA305 internal
    uint16_t                temperatureCpuMainboard;
    uint16_t                temperatureCpuRotating;
    float                   temperatureMotorController;
    float                   motorSpeed;
    float                   torque;
    float                   calculatedToF;
};
#pragma pack()


#pragma pack(1)
struct ChunkSSIF_t
{
    struct ChunkSSIFWithoutSamples_t    chunk;
    uint16_t                            tofSamples[MAXIMUM_TOF_SAMPLES_COUNT];
};
#pragma pack()


#pragma pack(1)
struct ChunkIMUWithoutSamples_t
{
    struct ChunkHeader_t    header;
    uint16_t                temperatureInternal;
    uint16_t                numberSamples;
};
#pragma pack()


#pragma pack(1)
struct ChunkIMU_t
{
    struct ChunkIMUWithoutSamples_t chunk;
    struct ImuSampleFrame_t         samples[MAXIMUM_IMU_SAMPLES_COUNT];
};
#pragma pack()


#pragma pack(1)
struct ChunkEvent_t
{
    struct ChunkHeader_t    header;
    uint8_t                 severity;
    uint8_t                 module;
    uint16_t                eventCode;
};
#pragma pack()


#pragma pack(1)
struct ChunkResynchronisation_t
{
    struct ChunkHeader_t    header;
    uint16_t                sync[RESYNC_SAMPLES_COUNT];
};
#pragma pack()


enum ChunkResynchronisationFields_t
{
    ChunkResynchronisation_Field0 = 0xDFFFU,
    ChunkResynchronisation_Field1 = 0xE000U,
};


#endif

