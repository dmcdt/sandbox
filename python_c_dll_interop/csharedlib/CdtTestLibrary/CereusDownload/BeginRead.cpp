#include "pch.h"

#include <iostream>
#include <iomanip>
#include <filesystem>

#include "CereusDownload.h"
#include "CereusDownloadInternals.h"
#include "UsbCommands.h"
#include "FT3XXHelper.h"
#include "crc16_modbus.h"
#include "Connection.h"
#include "Progress.h"
#include "LogAddressing.h"
#include "BufferSizes.h"


void ReadBadBlockTable(CereusToolContext* ctx);
bool ReadBadBlockTablePart(CereusToolContext* ctx, uint32_t bbtIndex);
bool SendReadBadBlockTablePart(CereusToolContext* ctx, uint32_t bbtIndex);
bool ReceiveReadBadBlockTablePartResponse(CereusToolContext* ctx);


CEREUSDLL void WINAPI BeginRead(void* context)
{
    CereusToolContext* ctx = reinterpret_cast<CereusToolContext*>(context);

    if (ctx)
    {
        ctx->pagerowReadPosition = 0;

        ctx->memoryBytesProcessed = 0;
        ctx->memoryBytesTotalToProcess = LogAddressing_GetLogicalPageFromPageRow(ctx->memoryUsed) * PageSize;
        ctx->progressDivisor = (ctx->memoryBytesTotalToProcess + UINT32_MAX - 1) / UINT32_MAX;
        if (ctx->progressDivisor == 0)
        {
            ctx->progressDivisor = 1;
        }

        ctx->readAddress = 0;
        ctx->processedAddress = 0;
        ctx->revolution = 0;

        std::filesystem::path debugDumpPath = std::filesystem::temp_directory_path();
        debugDumpPath.append("Cereus");
        debugDumpPath.append("Download");
        std::filesystem::create_directories(debugDumpPath);

        debugDumpPath.append("download.bin");
        ctx->debugDumpFile.open(debugDumpPath.string(), std::ios::binary | std::ios::trunc | std::ios::out);
        if (!ctx->debugDumpFile.is_open())
        {
            /* TODO better logging and some kind of error recovery? */
            std::cerr << "Could not create file \"" << debugDumpPath.string() << "\"." << std::endl;
        }

        ReadBadBlockTable(ctx);
    }
}


void ReadBadBlockTable(CereusToolContext* ctx)
{
    bool        commsOk;

    do
    {
        commsOk = ReadBadBlockTablePart(ctx, ctx->bbtIndex);
        SetProgress(ctx, ctx->bbtIndex, ctx->badBlockTable.size());
    } while ((commsOk) && (ctx->bbtIndex != USB_READBADBLOCKS_INVALIDINDEX));

    /* TODO what to do on failure */
}


bool ReadBadBlockTablePart(CereusToolContext* ctx, uint32_t bbtIndex)
{
    int     portResetAttempts;          // Retries for cycling the device port
    int     communicationsAttempts;     // Retries for sending and receiving the command
    bool    commandOk;

    portResetAttempts = RETRY_COUNT_PORT_CYCLE;
    do
    {
        communicationsAttempts = RETRY_COUNT_COMMAND_RESEND;

        do
        {
            commandOk = false;
            communicationsAttempts--;

            Sleep(1);
            if (SendReadBadBlockTablePart(ctx, bbtIndex))
            {
                Sleep(1);
                if (ReceiveReadBadBlockTablePartResponse(ctx))
                {
                    commandOk = true;
                }
            }

            if (!commandOk)
            {
                std::cerr << "ReadBadBlockTable - retrying command bbtIndex=" << bbtIndex << std::endl;
                Sleep(100);
            }
        } while ((!commandOk) && (communicationsAttempts > 0));

        if ((!commandOk) && (portResetAttempts > 0))
        {
            std::cerr << "ReadBadBlockTable - cycle port" << std::endl;
            portResetAttempts--;
            CycleConnection(ctx);
        }
    } while ((!commandOk) && (portResetAttempts > 0));

    return commandOk;
}


bool SendReadBadBlockTablePart(CereusToolContext* ctx, uint32_t bbtIndex)
{
    bool                    writeOk = false;
    FT_STATUS               ftStatus;

    std::vector<uint8_t> command = UsbCommand_ReadBadBlocksTable(bbtIndex);
    CreateWriteBuffer(command, ctx->writeBufferUsb);

    ULONG ulBytesToWrite = ctx->writeBufferUsb.size() * sizeof(ctx->writeBufferUsb[0]) / sizeof(UCHAR);
    ULONG ulBytesWritten;
    ftStatus = FT_WritePipe(ctx->device.Handle(), FT_CHANNEL1_WRITE, reinterpret_cast<PUCHAR>(&ctx->writeBufferUsb[0]), ulBytesToWrite, &ulBytesWritten, NULL);
    if (ftStatus == FT_OK)
    {
        writeOk = true;
    }
    else
    {
        ftStatus = FT_AbortPipe(ctx->device.Handle(), FT_CHANNEL1_WRITE);
        if (ftStatus != FT_OK)
        {
            /* TODO log */
        }
    }


    return writeOk;
}


bool ReceiveReadBadBlockTablePartResponse(CereusToolContext* ctx)
{
    bool                                readOk = false;
    FT_STATUS                           ftStatus;
    std::vector<uint8_t>                responsePacket;
    UsbResponse_ReadBadBlocksTable_t    response;
    UsbParseResult                      parseResult;

    ctx->readBufferUsb.resize(OVERSAMPLE_RATIO * USB_READBADBLOCKS_RESPONSE_LENGTH + 10);

    ULONG ulBytesRead;
    ftStatus = FT_ReadPipe(ctx->device.Handle(), FT_CHANNEL1_READ, reinterpret_cast<PUCHAR>(&ctx->readBufferUsb[0]), ctx->readBufferUsb.size() * sizeof(ctx->readBufferUsb[0]) / sizeof(UCHAR), &ulBytesRead, NULL);

    ULONG ulElementsRead = ulBytesRead / sizeof(ctx->readBufferUsb[0]);
    if (ftStatus == FT_OK)
    {
        ctx->readBufferUsb.resize(ulElementsRead);
        ExtractResponse(ctx->readBufferUsb, responsePacket);
        parseResult = UsbParseResponse_ReadBadBlocksTable(responsePacket, response);
        if (parseResult == UsbParseResult::OkGoodResponse)
        {
            memcpy(&ctx->badBlockTable[response.startIndex], &response.bbtPart[0], response.bbtPartCount * sizeof(response.bbtPart[0]));
            ctx->bbtIndex = response.nextIndex;
            readOk = true;
        }
        else
        {
            std::cerr << "Error in ReadBadBlocks response parse result=" << (int)parseResult << " status=0x" << std::hex << std::setw(2) << std::setfill('0') << response.basicResponse.responseStatus << std::endl;
            std::cerr << std::dec << std::setw(1) << std::setfill(' ');
        }
    }
    else
    {
        ftStatus = FT_AbortPipe(ctx->device.Handle(), FT_CHANNEL1_READ);
        if (ftStatus != FT_OK)
        {
            /* TODO log */
        }
    }

    return readOk;
}
