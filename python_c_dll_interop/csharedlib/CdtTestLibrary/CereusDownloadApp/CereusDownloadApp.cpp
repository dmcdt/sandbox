// Test application in C++ for Cereus Ultrasonics download DLL.
//


#include <iostream>
#include <string>
#include <vector>
#include <ctime>

#include <CereusDownload.h>


int main()
{
    void* cereusTool = Connect();
    if (cereusTool)
    {
        Statistics          stats;
        time_t              allTransferStartTime;
        time_t              downloadStartTime;
        time_t              downloadEndTime;
        double              transferDuration;
        double              downloadDuration;
        time_t              lastUpdateDisplayTime;
        time_t              updateDisplayTime;
        double              updateDisplayDuration;
        int                 dataToProcess;


        // Data structures for storing main log memory data into.
        MultipleArguments               allBlockArgs;
        std::vector<MainSensor>         mainData;
        std::vector<SecondarySensor2>   secondaryData;
        std::vector<InertialSensor>     inertialData;


        std::cout << "Connected to tool." << std::endl;

        time(&allTransferStartTime);

        GetStatistics(cereusTool, &stats);
        std::cout << "Serial number: " << stats.serialNumber << "." << std::endl;
        std::cout << "Firmware version: " << stats.firmwareVersion << std::endl;
        std::cout << "Used " << stats.used << " of " << stats.capacity << " pagerows." << std::endl;

        std::cout << "Reading Bad Block Table." << std::endl;
        BeginRead(cereusTool);

        time(&downloadStartTime);

        std::cout << "Starting download of log memory." << std::endl;
        lastUpdateDisplayTime = downloadStartTime;

        // Set up the output data structure for a fresh call to ReadAllBlocks()
        mainData.resize(600);
        secondaryData.resize(10);
        inertialData.resize(10);
        allBlockArgs.mainData = &mainData[0];
        allBlockArgs.mainLength = mainData.size();
        allBlockArgs.secondaryData = &secondaryData[0];
        allBlockArgs.secondaryLength = secondaryData.size();
        allBlockArgs.inertialData = &inertialData[0];
        allBlockArgs.inertialLength = inertialData.size();

        dataToProcess = ReadAllBlocks(cereusTool, &allBlockArgs);
        while (dataToProcess)
        {
            // TODO process output data

            // Set up for next call to ReadAllBlocks()
            allBlockArgs.mainData = &mainData[0];
            allBlockArgs.mainLength = mainData.size();
            allBlockArgs.secondaryData = &secondaryData[0];
            allBlockArgs.secondaryLength = secondaryData.size();
            allBlockArgs.inertialData = &inertialData[0];
            allBlockArgs.inertialLength = inertialData.size();

            dataToProcess = ReadAllBlocks(cereusTool, &allBlockArgs);

            time(&updateDisplayTime);
            updateDisplayDuration = difftime(updateDisplayTime, lastUpdateDisplayTime);
            if (updateDisplayDuration > 1)
            {
                struct ProgressReport   progress;

                Progress(cereusTool, &progress);

                std::cout << "Download progress " << (uint64_t)100 * progress.current / progress.total << "% (" << progress.current << " of " << progress.total << ")" << std::endl;
                lastUpdateDisplayTime = updateDisplayTime;
            }
        }

        EndRead(cereusTool);
        time(&downloadEndTime);

        transferDuration = difftime(downloadEndTime, allTransferStartTime);
        downloadDuration = difftime(downloadEndTime, downloadStartTime);

        double downloadSize = stats.used * 8640 * 2 * 8;
        std::cout << "Downloaded " << (downloadSize/(1024.0 * 1024)) << " MiB complete in " << downloadDuration << " seconds (" << (downloadSize / (downloadDuration * 1024.0 * 1024)) << " MiB/s)." << std::endl;
        std::cout << "Total duration " << transferDuration << " seconds." << std::endl;

        Disconnect(cereusTool);
        cereusTool = nullptr;
    }
    else
    {
        std::cerr << "Could not open device." << std::endl;
    }

    std::cout << "Press enter to exit";
    std::string waitForEnter;
    std::getline(std::cin, waitForEnter);

    return 0;
}
