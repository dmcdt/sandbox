# Example of using ctypes in python to exchange data with a DLL written in C/C++.
# Note just an example, not a concrete definition of how the data will be
# structured, or how the interfacing will work.
#
# Background info
#       python ctypes documentation
#       numpy documentation
#       https://stackoverflow.com/questions/5862915/passing-numpy-arrays-to-a-c-function-for-input-and-output

import ctypes
import _ctypes
import numpy

# Open DLL, Windows python will append .dll extension
path = "CdtTestLibrary"

# ctypes can load either a windll or a cdll, depending on the calling convention
# of the functions in the libraryes, either "stdcall" or "C" respectively.
#
# I think it does not make a difference on 64 bit Windows (the "stdcall"
# calling convention has been dropped for 64 bit architectures)
# but the way parameters are passed is different between the two on 32 bit
# Windows. Best make sure to match the C function declaration.
#
# In our example I have used "WINAPI" to specify the calling convntion in the C
# code, which would nominally translate to using windll in python.
cdtdll = ctypes.windll.LoadLibrary(path)

# Verify handle has been opened (by just printing it)
print(cdtdll._handle)



# The C signature of the example functions are:
#       void ReadNextBlock(unsigned short* data, int length);
#       void ReadNextBlockBoth(unsigned short* mainData, int mainLength, struct SecondarySensors *secondaryData, int secondaryLength);
#
# By default, loading the library as a cdll assumes that the function returns
# an "int", so we have to set the cdll object to show that the function does not
# return anything.
#
# If we are running 64 bit Windows then windll works the same way as cdll so
# we need to set the result type as well.
#
# Comment these lines out if running on 32 bit Windows.
cdtdll.ReadNextBlock.restype = None
cdtdll.ReadNextBlockBoth.restype = None


# Define the datatype used for the secondary sensor information, which is only
# stored once per revolution, not all the time like the main transducer samples.
# See definition of "struct SecondarySensors" in "include\CdtTestLibrary.h"
    # unsigned long long timestamp;

    # unsigned long long revolutionIndex;
    # int numberOfMainSamplesInRevolution; // Could change +/-1 depending on whether interleaved or not

    # short pressure;
    # short temperature;

    # short imuAccelerationX;
    # short imuAccelerationY;
    # short imuAccelerationZ;

    # int numberTofSamples;
    # short tofSamples[2000];
    
# Note the align=True at the end of the struct! It makes the in-memory alignment
# for python match the C compiler alignment.
secondarySensorType = numpy.dtype([('timestamp', 'u8'),
    ('revolutionIndex', 'u8'),
    ('numberOfMainSamplesInRevolution', 'i4'),
    ('pressure', 'i2'),
    ('temperature', 'i2'),
    ('imuAccelerationX', 'i2'),
    ('imuAccelerationY', 'i2'),
    ('imuAccelerationZ', 'i2'),
    ('numberTofSamples', 'i4'),
    ('tofSamples', 'i2', 2000)],
    align=True);

# Test calling a simple function, int Add(int a, int b). returns a+b
print(cdtdll.Add(ctypes.c_int(1), ctypes.c_int(2)))


# Create a numpy array with a specific data type that the C function can handle
data = numpy.zeros(238, dtype=numpy.uint16)
print(data)


# Call the function. Fills in the array with the values 0 to length - 1.
#
# data.ctypes.data - is the pointer to the underlying memory, using the 
#                    ndarray.ctypes attributes which are specifically for
#                    python/numpy and C interoperability.
# ctypes.c_void_p(x) - converts 'x' to a pointer (makes sure the value isn't truncated)
# ctypes.c_int(x) - converts 'x' to an integer, probably not required here but shown for example
# other data types are available in ctypes to force conversion from a python
# variable to a data type that will match the C DLL function.
cdtdll.ReadNextBlock(ctypes.c_void_p(data.ctypes.data), ctypes.c_int(data.size));
print(data)


# Set up array to receive the slower data and call function to read both
# (as an example of reading structured data)
slowData = numpy.ndarray(4, dtype=secondarySensorType)
cdtdll.ReadNextBlockBoth(ctypes.c_void_p(data.ctypes.data), ctypes.c_int(data.size), ctypes.c_void_p(slowData.ctypes.data), ctypes.c_int(slowData.size));
print(data)
print(slowData)


# Free the library
handle = cdtdll._handle
del cdtdll
_ctypes.FreeLibrary(handle)
handle = 0
