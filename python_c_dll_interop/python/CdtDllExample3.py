# Third example of using ctypes in python to exchange data with a DLL written
# in C/C++. This is probably close to how the DLL functions would work.
#   * This version fill as much data as possible and continues until around 448GiB
#     has been read.
#   * DLL loading has been changed in python 3.8 so a fix for that.
#
# The functions are arranged to be used in the following order and manner:
#       1) Create a connection to the tool (Connect())
#       2) Get some basic statistics from the tool, if required (GetStatistics())
#       3) Tell the DLL to start downloading (BeginRead())
#       4) While there is still data to be processed:
#           4.1) Fill in structure to tell DLL where to store data extracted
#                from tool. This includes:
#                   * the numpy ndarrays for each type of data chunk (main,
#                   * SSIF, IMU, etc).
#                   * the number of elements in each array. This will be updated
#                     to show how many elements worth of data were stored in
#                     in the array.
#           4.2) Call ReadAllBlocks() to read from tool and fill in data
#           4.3) Process the data returned by ReadAllBlocks(). Note that the
#                amount of data returned from reading the tool will have been
#                updated in the structure.
#       5) Call the EndRead() function to tell the DLL to stop downloading.
#       6) Call the Disconnect() function to end communications with the tool.
#
# Also need to manage opening and closing the DLL.
#
# Background info
#       python ctypes documentation
#       numpy documentation
#       https://stackoverflow.com/questions/5862915/passing-numpy-arrays-to-a-c-function-for-input-and-output

import os
import ctypes
import _ctypes
import numpy
import matplotlib.pyplot as plt


# Define the equivalent structures in python that are used in the DLL.
# See definitions in "include\CdtTestLibrary.h"
    

# One way we can do it is to define a class based on a ctypes.Structure
class Statistics(ctypes.Structure):
    _fields_ = [
        ("serialNumber", ctypes.c_char_p),
        ("firmwareVersion", ctypes.c_uint16),
        ("capacity", ctypes.c_ulonglong),
        ("used", ctypes.c_ulonglong)]
    
    def __repr__(self):
        return (f'{self.__class__.__name__}('
                f'{self.serialNumber!r}, {self.firmwareVersion!r}, '
                f'{self.capacity!r}, {self.used!r})')


# Another method is to define a numpy dtype for each element in what would
# be returned in an array for numerical processing.
#
# I don't know which is better, we could change it as required.
#
# Note the align=True at the end of the struct! It makes the in-memory alignment
# for python match the C compiler alignment.

# Create datatype for numpy to have native access to this structure.
# Although the sub-array samples has 10000 elements, only the first N
# elements are filled in by the DLL, where N is the number of samples
# set in the job configuration.
MainSensorType = numpy.dtype([('timestamp', numpy.uint64),
                              ('revolutionNumber', numpy.uint32),
                              ('pixelIndex', numpy.uint16),
                              ('samples', numpy.int16, (10000,))],
                             align=True);


SecondarySensor2Type = numpy.dtype([('timestamp', numpy.uint64),
                                    ('revolutionNumber', numpy.uint32),
                                    ('pressure', numpy.uint16),
                                    ('temperature', numpy.uint16),
                                    ('numberTofSamples', numpy.int32),
                                    ('tofSamples', numpy.int16, (2000,))],
                                   align=True);

ImuSamplesType = numpy.dtype([('ax', numpy.int16), ('ay', numpy.int16), ('az', numpy.int16),
                              ('gx', numpy.int16), ('gy', numpy.int16), ('gz', numpy.int16)],
                             align=True);

InertialType = numpy.dtype([('timestamp', numpy.uint64),
                            ('temperature', numpy.uint16),
                            ('numberSamples', numpy.uint16),
                            ('samples', ImuSamplesType, (100,))],
                           align=True);


# Another class based on ctypes.Structure. This seemed a better
# choice for a structure that is only a one off rather than held multiple
# times in a numpy ndarray.
class MultipleArguments(ctypes.Structure):
    _fields_ = [
        ("mainLength", ctypes.c_int),
        ("mainData", ctypes.c_void_p),
        ("secondaryLength", ctypes.c_int),
        ("secondaryData", ctypes.c_void_p),
        ("inertialLength", ctypes.c_int),
        ("inertialData", ctypes.c_void_p)]
    # TODO - Other chunk types need to be added here
    
    def __repr__(self):
        return (f'{self.__class__.__name__}('
                f'{self.mainLength!r}, {self.mainData!r}\n'
                f'{self.secondaryLength!r}, {self.secondaryData!r}\n'
                f'{self.inertialLength!r}, {self.inertialData!r})')


# For python >= 3.8 on Windows we need to specifically add the DLL directory
# for it to be found. See https://docs.python.org/3/whatsnew/3.8.html#bpo-36085-whatsnew
if hasattr(os, 'add_dll_directory'):
    os.add_dll_directory(os.path.dirname(os.path.abspath(__file__)))
    

# Open DLL, Windows python will append .dll extension
path = "CdtTestLibrary"


# ctypes can load either a windll or a cdll, depending on the calling convention
# of the functions in the libraryes, either "stdcall" or "C" respectively.
#
# I think it does not make a difference on 64 bit Windows (the "stdcall"
# calling convention has been dropped for 64 bit architectures)
# but the way parameters are passed is different between the two on 32 bit
# Windows. Best make sure to match the C function declaration.
#
# In our example I have used "WINAPI" to specify the calling convntion in the C
# code, which would nominally translate to using windll in python.
cdtdll = ctypes.windll.LoadLibrary(path)


# Verify handle has been opened (by just printing it)
print(cdtdll._handle)



# Define functions in the DLL. Arguments and result type. These are the DLL
# function signatures:
#       CDTDLL void *WINAPI Connect(void);
#       CDTDLL void WINAPI GetStatistics(void *context, struct Statistics* stats);
#       CDTDLL void WINAPI BeginRead(void* context);
#       CDTDLL int WINAPI ReadAllBlocks(void* context, struct MultipleArguments *args);
#       CDTDLL void WINAPI EndRead(void* context);
#       CDTDLL void WINAPI Disconnect(void *context);
#
# By default, loading the library as a cdll assumes that the function returns
# an "int", so we have to set the cdll object to show that the function does not
# return anything.
#
# If we are running 64 bit Windows then windll works the same way as cdll so
# we need to set the result type as well.
#
# The result type needs to be specified. By default it is an int. If running
# on 32-bit Windows the result is passed back on the stack, and so even if the
# DLL function does not return anything (it has a "void" before the function
# name), it needs to have a restype of int so that the stack is cleaned up
# correctly.
cdtdll.Connect.restype = ctypes.c_void_p
cdtdll.Connect.argtypes = None

cdtdll.GetStatistics.restype = None
cdtdll.GetStatistics.argtypes = [ctypes.c_void_p, ctypes.c_void_p];

cdtdll.BeginRead.restype = None
cdtdll.BeginRead.argtypes = [ctypes.c_void_p]

cdtdll.ReadAllBlocks.restype = ctypes.c_int
cdtdll.ReadAllBlocks.argtypes = [ctypes.c_void_p, ctypes.c_void_p];

cdtdll.EndRead.restype = None
cdtdll.EndRead.argtypes = [ctypes.c_void_p]

cdtdll.Disconnect.restype = None
cdtdll.Disconnect.argtypes = [ctypes.c_void_p]


# Create a connection to the tool. In the real DLL this would probably open the
# device handle to communicate over USB, so might need to check whether
# the connection returned is not 0.
cdtConnection = cdtdll.Connect()


# Get some statistics from the tool. This would contain information useful to
# performing the download. Create and pass an object that the DLL can fill in.
stats = Statistics()
cdtdll.GetStatistics(cdtConnection, ctypes.byref(stats))
print("Serial number = " + stats.serialNumber.decode())
print("Firmware version = " + str(stats.firmwareVersion // 100) + "." + str(stats.firmwareVersion % 100))
print("Storage capacity = " + str(stats.capacity))
print("Storage used     = " + str(stats.used))


# Let the DLL/tool know we are about to start download.
cdtdll.BeginRead(cdtConnection)


# Create structure to take all arguments for calling function in DLL
args = MultipleArguments()


# Create numpy ndarrays for storage of the data
numberRevolutions = 2000; # Number of revolutions / vertical steps

# 2000 vertical steps * 60 pixels per revolution
mainDataLength = numberRevolutions * 60;
mainData = numpy.ndarray(mainDataLength, dtype=MainSensorType);

# Corresponding SSIF length, assumes 1 SSIF per revolution
secondaryDataLength = numberRevolutions;
secondaryData = numpy.ndarray(secondaryDataLength, dtype=SecondarySensor2Type);

# Corresponding IMU length - currently assumed to be 60 RPM == 1 RPs,
# and 2 IMU chunks/s with 50 samples in each. 
inertialDataLength = numberRevolutions * 2; 
inertialData = numpy.ndarray(inertialDataLength, dtype=InertialType);


dataToProcess = 1
numberLoops = 0 # Just for tracking how many times the loop is called


mainFig = plt.figure('Main sensor')
imuFig = plt.figure('IMU axes')


while dataToProcess:
    # Fill in the structure with the arguments to the call
    args.mainLength = mainData.size;
    args.mainData = mainData.ctypes.data;
    args.secondaryLength = secondaryData.size;
    args.secondaryData = secondaryData.ctypes.data;
    args.inertialLength = inertialData.size;
    args.inertialData = inertialData.ctypes.data;
    
    
    # Call the function, making sure to specify byref on the class so it passes by reference.
    dataToProcess = cdtdll.ReadAllBlocks(cdtConnection, ctypes.byref(args));
    if (dataToProcess):
        # Display the fields updated by the DLL.
        print(str(numberLoops) + '. ' +
              str(args.mainLength) + ' ' +
              str(args.secondaryLength) + ' ' +
              str(args.inertialLength))
        #print(args.mainLength)
        #print(args.secondaryLength)
        #print(args.inertialLength)
        print('')
        
        
        # Display some of the information returned from the DLL in the arrays.
        # Need to access the numpy arrays from the numpy array objects defined in python.
        # Cannot access it through the args class/structure because it is just a void
        # pointer from ctypes and has no structure to it.
        # Integers are OK, such as args.mainLength.
        plt.figure(mainFig.number)
        plt.clf()
        #plt.plot(mainData[0:args.mainLength]['revolutionNumber'])
        plt.plot(mainData[0]['samples'])
        #plt.plot(secondaryData[0]['tofSamples'][0:secondaryData[0]['numberTofSamples']])
        plt.figure(imuFig.number)
        plt.clf()
        plt.subplot(3,1,1)
        plt.plot(inertialData[0]['samples']['ax'][0:inertialData[0]['numberSamples']] / 16384)
        plt.subplot(3,1,2)
        plt.plot(inertialData[0]['samples']['ay'][0:inertialData[0]['numberSamples']] / 16384)
        plt.subplot(3,1,3)
        plt.plot(inertialData[0]['samples']['az'][0:inertialData[0]['numberSamples']] / 16384)
        
        numberLoops = numberLoops + 1


# Stop the download.
cdtdll.EndRead(cdtConnection)


# Need to remember to free the connection once we no longer need it.
# This should probably be wrapped up into a class for automatic management.
# The connection is no longer valid after calling Disconnect() and MUST not
# be used afterwards. Any data associated with the connection that has not
# been copied (e.g. mostly pointers) also MUST not be used afterwards.
cdtdll.Disconnect(cdtConnection)
cdtConnection = 0


# Free the library
handle = cdtdll._handle
del cdtdll
_ctypes.FreeLibrary(handle)
handle = 0
