/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usart.h"
#include "gpio.h"
#include "fmc.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "ecc/bch.h"
#include "ecc/hamming.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#define BUFFER_SIZE (64)
#define NUMBER_SAMPLES  (5000)

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

uint8_t gBuffer[BUFFER_SIZE];
uint8_t gBufferGood[BUFFER_SIZE];

union SampleStorage
{
    uint16_t samples[NUMBER_SAMPLES];
    uint32_t dualSamples[NUMBER_SAMPLES/2];
    
} gSamples;


// Flag needed to or in to achieve even parity
const uint16_t evenParity[16] = {   0x0000, 0x8000, 0x0000, 0x8000,
                                    0x0000, 0x8000, 0x0000, 0x8000, 
                                    0x0000, 0x8000, 0x0000, 0x8000, 
                                    0x0000, 0x8000, 0x0000, 0x8000 };

/* Number of 1 bits in a uint8_t */
//const uint8_t numberBits[256] = {   0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4,
//                                    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
//                                    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
//                                    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
//                                    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
//                                    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
//                                    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
//                                    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
//                                    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
//                                    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
//                                    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
//                                    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
//                                    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
//                                    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
//                                    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
//                                    4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8
//                                };

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MPU_Config(void);
/* USER CODE BEGIN PFP */
void Sleep(uint32_t Delay);
void CalculateParity(void);
extern void MyCalculateParity(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
#if VECT_TAB_ITCM
    /* If we are remapping execution into ITCM then we need to do it ASAP after
     * the system has started, and before any interrupts are used.
     */
    SCB->VTOR = D1_ITCMRAM_BASE; //| VECT_TAB_OFFSET;        /* Vector Table Relocation in ITCM RAM */
#endif

    //const struct bch_def *bch = &bch_4bit;
    const struct bch_def *bch = &bch_3bit;
    uint8_t ecc[BCH_MAX_ECC];
    int verify;
    uint32_t startCyclesGen;
    uint32_t endCyclesGen;
    uint32_t startCyclesVer;
    uint32_t endCyclesVer;
    uint32_t startCyclesRep;
    uint32_t endCyclesRep;

    uint32_t totalCyclesGen;
    uint32_t totalCyclesVer;
    uint32_t totalCyclesRep;
    
    char    str[64];
    

    HAL_EnableDBGSleepMode();
    HAL_EnableDBGStopMode();
    HAL_EnableDBGStandbyMode();
    
    HAL_EnableDomain2DBGSleepMode();
    HAL_EnableDomain2DBGStopMode();
    HAL_EnableDomain2DBGStandbyMode();

    HAL_EnableDomain3DBGStopMode();
    HAL_EnableDomain3DBGStandbyMode();
    
    
  /* USER CODE END 1 */
/* USER CODE BEGIN Boot_Mode_Sequence_0 */
/* USER CODE END Boot_Mode_Sequence_0 */

  /* MPU Configuration--------------------------------------------------------*/
  MPU_Config();

  /* Enable I-Cache---------------------------------------------------------*/
  SCB_EnableICache();

  /* Enable D-Cache---------------------------------------------------------*/
  SCB_EnableDCache();

/* USER CODE BEGIN Boot_Mode_Sequence_1 */
/* USER CODE END Boot_Mode_Sequence_1 */
  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();
/* USER CODE BEGIN Boot_Mode_Sequence_2 */
/* USER CODE END Boot_Mode_Sequence_2 */

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_FMC_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  
    /* Turn on CPU cycel counter */
    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
  
    /* Generate data over which to protect with ECC */
    // Sequential fill
//    for (int i = 0; i < BUFFER_SIZE; i++)
//    {
//        gBuffer[i] = i;
//    }

    // Stride sequential fill
//    for (int i = 0; i < BUFFER_SIZE; i++)
//    {
//        gBuffer[i] = i * 17;
//    }

    // Larger stride modulo sequence
//    gBuffer[0] = 0x36;
//    for (int i = 1; i < BUFFER_SIZE; i++)
//    {
//        gBuffer[i] = i * 17 + gBuffer[i] * 3;
//    }
    
    
    // Random fill
//    for (int i = 0; i < BUFFER_SIZE; i++)
//    {
//        gBuffer[i] = rand();
//    }

    // All 1's, should have longest code path?
    memset(gBuffer, 0x01, BUFFER_SIZE);
    
#if 0    
    /* Generate ECC bytes for the given page. */
    memset(ecc, 0x00, sizeof(ecc));
    startCyclesGen = DWT->CYCCNT;
    bch_generate(bch, gBuffer, BUFFER_SIZE, ecc);
    endCyclesGen = DWT->CYCCNT;

    /* Can verify by itself */
    startCyclesVer = DWT->CYCCNT;
    verify = bch_verify(bch, gBuffer, BUFFER_SIZE, ecc);
    endCyclesVer = DWT->CYCCNT;
    
    /* Introduce an error in the data */
    gBuffer[37] = 0x24;
    
    /* Or go for the full bhuna and repair the page, plus verify again
     * afterwards to check for unrecoverable errors.
     */
    startCyclesRep = DWT->CYCCNT;
    bch_repair(bch, gBuffer, BUFFER_SIZE, ecc);
    endCyclesRep = DWT->CYCCNT;
    verify = bch_verify(bch, gBuffer, BUFFER_SIZE, ecc);
    
    totalCyclesGen = endCyclesGen - startCyclesGen;
    totalCyclesVer = endCyclesVer - startCyclesVer;
    totalCyclesRep = endCyclesRep - startCyclesRep;
#endif
    
#if 0
    /* Test operation of dhara hamming implementation */
    int totalNoErrors = 0;
    int totalOneBitErrors = 0;
    int totalTwoBitErrors = 0;
    int totalOtherErrors = 0;
    int totalBadRepair = 0;
    
    hamming_ecc_t   hammSyndrome;
    int             hammRepairStatus;
    
    /* For each possible bit in data and ECC */
    for (int byteIndex = 0; byteIndex < (BUFFER_SIZE + 3); byteIndex++)
    {
        for (int bitIndex = 0; bitIndex < 8; bitIndex++)
        {
            /* Generate some test data */
            memset(gBuffer, 0x00, sizeof(gBuffer));

            for (int i = 0; i < BUFFER_SIZE; i++)
            {
                gBuffer[i] = rand();
            }
            memcpy(gBufferGood, gBuffer, BUFFER_SIZE);
            memset(ecc, 0x00, sizeof(ecc));
            hamming_generate(gBuffer, BUFFER_SIZE, ecc);

            /* All bits in the ECC are valid, even
             * if the data length is lower.
             * Cannot base this on the other encoder
             * where 64 bytes only needs 10 bits.
             * Because implementation always written to use all bits.
             *
             * Luckily, 8192 / 64 * 3 = 128 * 3 = 384 bytes of ECC.
             *
             * That fits OK into the 448 spare area, with some spare for
             * formatting plus it's own ECC.
             */
//            ecc[1] &= 0x03;
//            ecc[2] = 0;
            
            /* Introduce an error */
            if (byteIndex < BUFFER_SIZE)
            {
                gBuffer[byteIndex] ^= 1 << bitIndex;
            }
            else
            {
                ecc[byteIndex - BUFFER_SIZE] ^= 1 << bitIndex;
            }
            
            hammSyndrome = hamming_syndrome(gBuffer, BUFFER_SIZE, ecc);
//            hammSyndrome &= 0x3FF;

            /* Attempt to repair ECC errors. Returns 0 if successful, -1 if an error
             * occurs.
             */
            hammRepairStatus = hamming_repair(gBuffer, BUFFER_SIZE, hammSyndrome);
            if (hammRepairStatus == 0)
            {
                if (hammSyndrome == 0)
                {
                    totalNoErrors++;
                }
                else
                {
                    totalOneBitErrors++;
                }
            
                if (memcmp(gBufferGood, gBuffer, BUFFER_SIZE) != 0)
                {
                    totalBadRepair++;
                }
            }
            else
            {
                // Cannot repair, but we know there are errors
                totalTwoBitErrors++;
            }
        }
    }

    if ((totalOneBitErrors != (512 + 24)) && (totalBadRepair == 0))
    {
        // Should detect all combinations of two bit errors
        strncpy(str, "ERROR: Did not detect all 1 bit errors\r\n", sizeof(str));
        str[sizeof(str) - 1] = '\0';
        HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
        Error_Handler();
    }
    
    totalOneBitErrors = 0;

    /* Check two bit errors across the data and ECC */
    // All combinations of 2 bit errors in data
    for (int byteIndex1 = 0; byteIndex1 < (BUFFER_SIZE + 3); byteIndex1++)
    {
        for (int bitIndex1 = 0; bitIndex1 < 8; bitIndex1++)
        {
            for (int byteIndex2 = 0; byteIndex2 < (BUFFER_SIZE + 3); byteIndex2++)
            {
                for (int bitIndex2 = 0; bitIndex2 < 8; bitIndex2++)
                {
                    // Cannot toggle same bit twice, that would be no error
                    if ((byteIndex1 != byteIndex2) || (bitIndex1 != bitIndex2))
                    {
                        for (int i = 0; i < BUFFER_SIZE; i++)
                        {
                            gBuffer[i] = rand();
                        }
                        memcpy(gBufferGood, gBuffer, BUFFER_SIZE);
                        memset(ecc, 0x00, sizeof(ecc));
                        hamming_generate(gBuffer, BUFFER_SIZE, ecc);
                        
                        /* Introduce an error */
                        if (byteIndex1 < BUFFER_SIZE)
                        {
                            gBuffer[byteIndex1] ^= 1 << bitIndex1;
                        }
                        else
                        {
                            ecc[byteIndex1 - BUFFER_SIZE] ^= 1 << bitIndex1;
                        }
                        
                        if (byteIndex2 < BUFFER_SIZE)
                        {
                            gBuffer[byteIndex2] ^= 1 << bitIndex2;
                        }
                        else
                        {
                            ecc[byteIndex2 - BUFFER_SIZE] ^= 1 << bitIndex2;
                        }
                        
                        hammSyndrome = hamming_syndrome(gBuffer, BUFFER_SIZE, ecc);

                        /* Attempt to repair ECC errors. Returns 0 if successful, -1 if an error
                         * occurs.
                         */
                        hammRepairStatus = hamming_repair(gBuffer, BUFFER_SIZE, hammSyndrome);
                        if (hammRepairStatus == 0)
                        {
                            if (hammSyndrome == 0)
                            {
                                totalNoErrors++;
                            }
                            else
                            {
                                totalOneBitErrors++;
                            }
                            
                            if (memcmp(gBufferGood, gBuffer, BUFFER_SIZE) != 0)
                            {
                                totalBadRepair++;
                            }
                        }
                        else
                        {
                            // Cannot repair, but we know there are errors
                            totalTwoBitErrors++;
                        }
                    }
                }
            }
        }
    }

    if ((totalTwoBitErrors != (536 * 536 - 536)) && (totalBadRepair == 0))
    {
        // Should detect all combinations of two bit errors
        strncpy(str, "ERROR: Did not detect all 2 bit errors\r\n", sizeof(str));
        str[sizeof(str) - 1] = '\0';
        HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
        Error_Handler();
    }
    totalTwoBitErrors = 0;
    
    /* Three bit errors get incorrectly corrected and not reported */
    
#endif
    
    
#if 0
    /* Test the speed of dhara hamming implementation */
    //gBuffer[0] = 0x01;
    memset(ecc, 0x00, sizeof(ecc));
    
    startCyclesGen = DWT->CYCCNT;
    hamming_generate(gBuffer, BUFFER_SIZE, ecc);
    endCyclesGen = DWT->CYCCNT;
    
    startCyclesVer = DWT->CYCCNT;
    endCyclesVer = DWT->CYCCNT;
    startCyclesRep = DWT->CYCCNT;
    endCyclesRep = DWT->CYCCNT;
    
    totalCyclesGen = endCyclesGen - startCyclesGen;
    sprintf(str, "%lu\r\n", totalCyclesGen);
    HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
#endif
    
    
#if 0
    /* Use the ECC hardware in the NAND flash to see how quick it is and how
     * the result compares.
     */
    __IO uint16_t *nandCommonData = (void *)NAND_DEVICE;
    uint16_t const *nandData = (uint16_t *)gBuffer;

    /* Cycles are essentially free as the calculation is done while writing to
     * the NAND flash through the FMC. Not sure how I'd do a little mini-ECC
     * on the OOB bytes though, probably cannot.
     */
    HAL_StatusTypeDef status;
    uint32_t eccValue;
    
    memset(ecc, 0x00, sizeof(ecc));
    
    status = HAL_NAND_ECC_Enable(&hnand1);
    for (int i = 0; i < BUFFER_SIZE / 2; i++)
    {
        *nandCommonData = *nandData++;
    }
    startCyclesGen = DWT->CYCCNT;
    /* The wait for FIFO empty is included here */
    status = HAL_NAND_GetECC(&hnand1, &eccValue, HAL_MAX_DELAY);
    status = HAL_NAND_ECC_Disable(&hnand1);
    status = HAL_NAND_ECC_Enable(&hnand1);
    endCyclesGen = DWT->CYCCNT;
    /* Extra enable at the end is to include all calls in the time calculation */
    status = HAL_NAND_ECC_Disable(&hnand1);
    
    
    totalCyclesGen = endCyclesGen - startCyclesGen;
    sprintf(str, "%lu %lu\r\n", eccValue, totalCyclesGen);
    HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
    
#endif
    
#if 1
    /* Test the speed of calculating the parity bit
     * Using methods from http://graphics.stanford.edu/~seander/bithacks.html#ParityParallel
     */
    
    /* Dummy data fill */
//    for (int i = 0; i < NUMBER_SAMPLES; i++)
//    {
//        gSamples.samples[i] = rand() & 0x7FFF;
//    }
//    
//    startCyclesGen = DWT->CYCCNT;
//    CalculateParity();
//    endCyclesGen = DWT->CYCCNT;
//    
//    totalCyclesGen = endCyclesGen - startCyclesGen;
//    sprintf(str, "%lu\r\n", totalCyclesGen);
//    HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);

    for (int i = 0; i < NUMBER_SAMPLES; i++)
    {
        gSamples.samples[i] = rand() & 0x7FFF;
    }

    startCyclesGen = DWT->CYCCNT;
    CalculateParity();
    endCyclesGen = DWT->CYCCNT;
    
    totalCyclesGen = endCyclesGen - startCyclesGen;
    sprintf(str, "%lu\r\n", totalCyclesGen);
    HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
#endif
    
    
    while (1)
    {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

        Sleep(1000);
    }
  /* USER CODE END 3 */
}


/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Supply configuration update enable
  */
  HAL_PWREx_ConfigSupply(PWR_DIRECT_SMPS_SUPPLY);
  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 75;
  RCC_OscInitStruct.PLL.PLLP = 2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_3;
  RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
  RCC_OscInitStruct.PLL.PLLFRACN = 0;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV2;
  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_FMC;
  PeriphClkInitStruct.FmcClockSelection = RCC_FMCCLKSOURCE_D1HCLK;
  PeriphClkInitStruct.Usart16ClockSelection = RCC_USART16CLKSOURCE_D2PCLK2;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* CPU low power version of HAL_Delay */
void Sleep(uint32_t Delay)
{
    uint32_t tickstart = HAL_GetTick();
    uint32_t wait = Delay;

    /* Add a freq to guarantee minimum wait */
    if (wait < HAL_MAX_DELAY)
    {
        wait += (uint32_t)(uwTickFreq);
    }

    while ((HAL_GetTick() - tickstart) < wait)
    {
        __WFE();
        __NOP();
    }

    __NOP();
}

void CalculateParity(void)
{
    for (int i = 0; i < NUMBER_SAMPLES/4; i++)
    {
//        uint16_t v = gSamples[i];
//        v ^= v >> 8;
//        v ^= v >> 4;
//        v &= 0x0F;
//        //gSamples[i] |= evenParity[v];
//        gSamples[i] |= ((0x6996 >> v) & 1) << 15;
        
        // 32 bit load / mod / store takes 31275 cycles
//        uint32_t v = gSamples.dualSamples[i];
//        uint16_t v0;
//        uint16_t v1;
//
//        v ^= v >> 8;
//        v ^= v >> 4;
//        v &= 0x000F000F;
//        
//        v0 = v;
//        v1 = v >> 16;
//
//        gSamples.dualSamples[i] |= (((0x6996 >> v1) & 1) << 31) | (((0x6996 >> v0) & 1) << 15);


        // Modified 32 bit, so that it is more aligned. Takes 28775 clocks.
//        uint32_t v = gSamples.dualSamples[i];
//        uint16_t v0;
//        uint16_t v1;
//
//        v ^= v >> 8;
//        v ^= v >> 4;
//        v &= 0x000F000F;
//        
//        v0 = v;
//        v1 = v >> 16;
//        
//        gSamples.dualSamples[i] |= ((0x69960000UL << v1) | (0x6996 << v0)) & 0x80008000UL;
        
        // 32 bit shift and XOR, no lookup. Takes 28771 cycles
//        uint32_t v = gSamples.dualSamples[i];
//        uint16_t v0;
//        uint16_t v1;
//
//        v ^= v << 8;
//        v ^= v << 4;
//        v ^= v << 2;
//        v ^= v << 1;
//        v &= 0x80008000UL;
//        
//        gSamples.dualSamples[i] |= v;
        
        // 2*32 bit shift and XOR, no lookup, to try to reduce data dependancies. Takes 17521 cycles
        uint32_t v0 = gSamples.dualSamples[2*i];
        uint32_t v1 = gSamples.dualSamples[2*i+1];

        v0 ^= v0 << 8;
        v0 ^= v0 << 4;
        v0 ^= v0 << 2;
        v0 ^= v0 << 1;
        v0 &= 0x80008000UL;
        
        v1 ^= v1 << 8;
        v1 ^= v1 << 4;
        v1 ^= v1 << 2;
        v1 ^= v1 << 1;
        v1 &= 0x80008000UL;

        gSamples.dualSamples[2*i] |= v0;
        gSamples.dualSamples[2*i+1] |= v1;
    }
}

/* USER CODE END 4 */

/* MPU Configuration */

void MPU_Config(void)
{
  MPU_Region_InitTypeDef MPU_InitStruct = {0};

  /* Disables the MPU */
  HAL_MPU_Disable();
  /** Initializes and configures the Region and the memory to be protected
  */
  MPU_InitStruct.Enable = MPU_REGION_ENABLE;
  MPU_InitStruct.Number = MPU_REGION_NUMBER0;
  MPU_InitStruct.BaseAddress = 0x80000000;
  MPU_InitStruct.Size = MPU_REGION_SIZE_256MB;
  MPU_InitStruct.SubRegionDisable = 0x0;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL2;
  MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
  MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_DISABLE;
  MPU_InitStruct.IsShareable = MPU_ACCESS_NOT_SHAREABLE;
  MPU_InitStruct.IsCacheable = MPU_ACCESS_NOT_CACHEABLE;
  MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);
  /* Enables the MPU */
  HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);

}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
