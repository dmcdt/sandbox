//   \                                 In section .text, align 2, keep-with-next
//    614          void CalculateParity(void)
//    615          {
//    616              for (int i = 0; i < NUMBER_SAMPLES/4; i++)
//   \                     CalculateParity: (+1)
//   \        0x0   0x....             LDR.N    R1,??DataTable4_2
//   \        0x2   0xF240 0x40E2      MOVW     R0,#+1250
//    617              {
//    618          //        uint16_t v = gSamples[i];
//    619          //        v ^= v >> 8;
//    620          //        v ^= v >> 4;
//    621          //        v &= 0x0F;
//    622          //        //gSamples[i] |= evenParity[v];
//    623          //        gSamples[i] |= ((0x6996 >> v) & 1) << 15;
//    624                  
//    625                  // 32 bit load / mod / store takes 31275 cycles
//    626          //        uint32_t v = gSamples.dualSamples[i];
//    627          //        uint16_t v0;
//    628          //        uint16_t v1;
//    629          //
//    630          //        v ^= v >> 8;
//    631          //        v ^= v >> 4;
//    632          //        v &= 0x000F000F;
//    633          //        
//    634          //        v0 = v;
//    635          //        v1 = v >> 16;
//    636          //
//    637          //        gSamples.dualSamples[i] |= (((0x6996 >> v1) & 1) << 31) | (((0x6996 >> v0) & 1) << 15);
//    638          
//    639          
//    640                  // Modified 32 bit, so that it is more aligned. Takes 28775 clocks.
//    641          //        uint32_t v = gSamples.dualSamples[i];
//    642          //        uint16_t v0;
//    643          //        uint16_t v1;
//    644          //
//    645          //        v ^= v >> 8;
//    646          //        v ^= v >> 4;
//    647          //        v &= 0x000F000F;
//    648          //        
//    649          //        v0 = v;
//    650          //        v1 = v >> 16;
//    651          //        
//    652          //        gSamples.dualSamples[i] |= ((0x69960000UL << v1) | (0x6996 << v0)) & 0x80008000UL;
//    653                  
//    654                  // 32 bit shift and XOR, no lookup. Takes 28771 cycles
//    655          //        uint32_t v = gSamples.dualSamples[i];
//    656          //        uint16_t v0;
//    657          //        uint16_t v1;
//    658          //
//    659          //        v ^= v << 8;
//    660          //        v ^= v << 4;
//    661          //        v ^= v << 2;
//    662          //        v ^= v << 1;
//    663          //        v &= 0x80008000UL;
//    664          //        
//    665          //        gSamples.dualSamples[i] |= v;
//    666                  
//    667                  // 2*32 bit shift and XOR, no lookup, to try to reduce data dependancies. Takes  cycles
//    668                  uint32_t v0 = gSamples.dualSamples[2*i];
//   \                     ??CalculateParity_0: (+1)
//   \        0x6   0x680A             LDR      R2,[R1, #+0]
//    669                  uint32_t v1 = gSamples.dualSamples[2*i+1];
//   \        0x8   0x684B             LDR      R3,[R1, #+4]
//    670          
//    671                  v0 ^= v0 << 8;
//    672                  v0 ^= v0 << 4;
//    673                  v0 ^= v0 << 2;
//    674                  v0 ^= v0 << 1;
//    675                  v0 &= 0x80008000UL;
//    676                  
//    677                  v1 ^= v1 << 8;
//    678                  v1 ^= v1 << 4;
//    679                  v1 ^= v1 << 2;
//    680                  v1 ^= v1 << 1;
//    681                  v1 &= 0x80008000UL;
//    682          
//    683                  gSamples.dualSamples[2*i] |= v0;
//   \        0xA   0xF8D1 0xC000      LDR      R12,[R1, #+0]
//   \        0xE   0xEA82 0x2202      EOR      R2,R2,R2, LSL #+8
//   \       0x12   0xEA83 0x2303      EOR      R3,R3,R3, LSL #+8
//   \       0x16   0xEA82 0x1202      EOR      R2,R2,R2, LSL #+4
//   \       0x1A   0xEA83 0x1303      EOR      R3,R3,R3, LSL #+4
//   \       0x1E   0xEA82 0x0282      EOR      R2,R2,R2, LSL #+2
//   \       0x22   0xEA83 0x0383      EOR      R3,R3,R3, LSL #+2
//   \       0x26   0xEA82 0x0242      EOR      R2,R2,R2, LSL #+1
//    684                  gSamples.dualSamples[2*i+1] |= v1;
//   \       0x2A   0xEA83 0x0343      EOR      R3,R3,R3, LSL #+1
//   \       0x2E   0xF002 0x2280      AND      R2,R2,#0x80008000
//   \       0x32   0xEA42 0x020C      ORR      R2,R2,R12
//   \       0x36   0x600A             STR      R2,[R1, #+0]
//   \       0x38   0x684A             LDR      R2,[R1, #+4]
//   \       0x3A   0xF003 0x2380      AND      R3,R3,#0x80008000
//   \       0x3E   0x4313             ORRS     R3,R3,R2
//   \       0x40   0x604B             STR      R3,[R1, #+4]
//    685              }
//   \       0x42   0x3108             ADDS     R1,R1,#+8
//   \       0x44   0x1E40             SUBS     R0,R0,#+1
//   \       0x46   0xD1DE             BNE.N    ??CalculateParity_0
//    686          }
//   \       0x48   0x4770             BX       LR               ;; return

        EXTERN      gSamples
        PUBLIC      MyCalculateParity
        
        SECTION .text:CODE (2)
        
MyCalculateParity
        LDR.N       R1,??gSamplesAddress
        MOVW        R0,#+1250
??CalculateParity_0:
        LDR         R2,[R1, #+0]
        LDR         R3,[R1, #+4]
        LDR         R12,[R1, #+0]
        EOR         R2,R2,R2, LSL #+8
        EOR         R3,R3,R3, LSL #+8
        EOR         R2,R2,R2, LSL #+4
        EOR         R3,R3,R3, LSL #+4
        EOR         R2,R2,R2, LSL #+2
        EOR         R3,R3,R3, LSL #+2
        EOR         R2,R2,R2, LSL #+1
        EOR         R3,R3,R3, LSL #+1
        AND         R2,R2,#0x80008000
        ORR         R2,R2,R12
        STR         R2,[R1, #+0]
        LDR         R2,[R1, #+4]
        AND         R3,R3,#0x80008000
        ORRS        R3,R3,R2
        STR         R3,[R1, #+4]

        ADDS        R1,R1,#+8
        SUBS        R0,R0,#+1
        BNE.N       ??CalculateParity_0

        BX          LR               ;; return

        SECTION .text:CODE (2)
        DATA
??gSamplesAddress:
        DC32        gSamples
        
        END
        
        