// TODO - need to have some way for these functions to time out or fail cleanly if the I2C device fails to respond

#include "pga305.h"

#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#include "i2c.h"
#include "Error_Handler.h"
#include "sleep.h"


#define PGA305_CRC_IV   (0xFFU)


static I2C_HandleTypeDef    *ghPga305;
//static uint16_t             gDeviceBaseAddress;
//static uint16_t             gDeviceDataPageAddress;
//static uint16_t             gDeviceControlPageAddress;
//static uint16_t             gDeviceEepromPageAddress;


void Pga305_Initialise(void)
{
    uint8_t     regValue;
    bool        eepromChanged = false;
    
    // Assume the I2C device is already initialised by Cube
    ghPga305 = &hi2c1;

//    gDeviceBaseAddress = 0x40 << 1; // STM just replaces the LSb with r/w and leaves you to shift the address up
//    gDeviceDataPageAddress = gDeviceBaseAddress + 0;
//    gDeviceControlPageAddress = gDeviceBaseAddress + 2;
//    gDeviceEepromPageAddress = gDeviceBaseAddress + 5;

#if 0    
    /* Configure some default settings for our application. Need to put the
     * PGA305 onboard MCU into reset and allow the digital interface to use
     * the PGA305 resources.
     */
    Pga305_WriteRegister(PGA305_DATA_PAGE, Pga305_DataReg_CompensationControl,
                         PGA305_COMPENSATIONCTRL_COMPENSATIONRESET | PGA305_COMPENSATIONCTRL_IFSEL_DIGITALINTERFACE);
    
    // TODO - set PGA305 into 400KHz I2C. But how to cleanly reset the I2C peripheral?
    
    /* Set pressure gain to be 10V/V, to match pressure sensors used in tool */
    Pga305_WriteRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_P_GAIN_SELECT, PGA305_PGAINSELECT_NOTINVERTED | PGA305_PGAINSELECT_10V);
    
    /*  Both internal and external temperature sensors here are single ended, but
     *  that is an EEPROM setting, so do not continually reconfigure it here.
     */
    regValue = Pga305_ReadRegister(PGA305_EEPROM_PAGE, Pga305_EEAddr_TEMP_SE);
    if ((regValue & PGA305_TEMPSE_TEMPSE_Msk) != PGA305_TEMPSE_SINGLEENDED)
    {
        // TODO - cache and update the SE flag in EEPROM
        //Pga305_WriteRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_TEMP_SE, PGA305_TEMPSE_SINGLEENDED);
        //eepromChanged = true;
    }
    
    /* Set temperature gain to 5V/V, suitable for both channels */
    Pga305_WriteRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_T_GAIN_SELECT, PGA305_TGAINSELECT_NOTINVERTED | PGA305_TGAINSELECT_5V);
    
    /* Save up all the EEPROM changes until the end before recalculating
     * and programming the CRC.
     */
    if (eepromChanged)
    {
        Pga305_WriteEepromCrc(Pga305_CalculateCrcOverEeprom());
        eepromChanged = false;
    }
    
    /* Allow the onboard MCU to run again and perform compensations */
    Pga305_WriteRegister(PGA305_DATA_PAGE, Pga305_DataReg_CompensationControl,
                         PGA305_COMPENSATIONCTRL_COMPENSATION_RUNNING|PGA305_COMPENSATIONCTRL_IFSEL_CALCULATIONENGINE);
#endif
}


/** Read compensated pressure output while PGA305 is running.
 */
uint32_t Pga305_ReadCompensated(void)
{
    uint32_t    compensatedValue = 0;
    
    HAL_StatusTypeDef    status;
    
    // WTF HAL, why do I need this?
    //status = HAL_I2C_IsDeviceReady(&hi2c1, 
    
    uint8_t     txbuffer[16];
    uint16_t    txlength;
    uint8_t     rxbuffer[16];
    uint16_t    rxlength;
    
    /*
     *  Follow example from datasheet to read compensated output value 24 bits
     *
        Description             MCU                         PGA305
        1. Send cmd 4, read     0x40 - Start Addr Write     Ack
        compensated output      0x09 - Register = Command   Ack
                                0x04 - Command              Ack
                                                            
        2. Read bits 23:16      0x40 - Start Addr Write     Ack
                                0x04 - Register = Data LSB  Ack
                                0x41 - RptStart Addr Read   0xhh
                                Ack(?)

        3. Send read trail      0x40 - Start Addr Write     Ack
        words command           0x09 - Register = Command   Ack
                                0x70 - Command              Ack

        4. Read bits 16:9       0x40 - Start Addr Write     Ack
                                0x05 - Register = Data MSB  Ack
                                0x41 - Start Addr Read      0xhh
                                Ack(?)
        
        5. Read bits 8:0        0x40 - Start Addr Write     Ack
                                0x04 - Register = Data LSB  Ack
                                0x41 - Start Addr Read      0xhh
                                Ack(?)
     */
    
    // 1. Send read compensated output value command
    txbuffer[0] = Pga305_DataReg_Command;
    txbuffer[1] = Pga305_DataCmd_ReadCompensated;
    txlength = 2;
    status = HAL_I2C_Master_Transmit(ghPga305, PGA305_DATAPAGE_ADDRESS, txbuffer, txlength, HAL_MAX_DELAY);
    if (status != HAL_OK)
    {
        Error_Handler();
    }

    
    // 2. Read bits 23:16
    txbuffer[0] = Pga305_DataReg_DataLSB;
    txlength = 1;
    status = HAL_I2C_Master_Transmit(ghPga305, PGA305_DATAPAGE_ADDRESS, txbuffer, txlength, HAL_MAX_DELAY);
    if (status != HAL_OK)
    {
        Error_Handler();
    }
    
    rxlength = 1;
    status = HAL_I2C_Master_Receive(ghPga305, PGA305_DATAPAGE_ADDRESS, rxbuffer, rxlength, HAL_MAX_DELAY);
    if (status != HAL_OK)
    {
        Error_Handler();
    }
    
    compensatedValue = rxbuffer[0] << 16;
    
    
    // 3. Send read trail word
    txbuffer[0] = Pga305_DataReg_Command;
    txbuffer[1] = Pga305_DataCmd_ReadTrailWord;
    txlength = 2;
    status = HAL_I2C_Master_Transmit(ghPga305, PGA305_DATAPAGE_ADDRESS, txbuffer, txlength, HAL_MAX_DELAY);
    if (status != HAL_OK)
    {
        Error_Handler();
    }
    
    
    // 4. Read bits 16:9
    txbuffer[0] = Pga305_DataReg_DataMSB;
    txlength = 1;
    status = HAL_I2C_Master_Transmit(ghPga305, PGA305_DATAPAGE_ADDRESS, txbuffer, txlength, HAL_MAX_DELAY);
    if (status != HAL_OK)
    {
        Error_Handler();
    }
    
    rxlength = 1;
    status = HAL_I2C_Master_Receive(ghPga305, PGA305_DATAPAGE_ADDRESS, rxbuffer, rxlength, HAL_MAX_DELAY);
    if (status != HAL_OK)
    {
        Error_Handler();
    }
    
    compensatedValue |= rxbuffer[0] << 8;

    
    // 5. Read bits 8:0
    txbuffer[0] = Pga305_DataReg_DataLSB;
    txlength = 1;
    status = HAL_I2C_Master_Transmit(ghPga305, PGA305_DATAPAGE_ADDRESS, txbuffer, txlength, HAL_MAX_DELAY);
    if (status != HAL_OK)
    {
        Error_Handler();
    }
    
    rxlength = 1;
    status = HAL_I2C_Master_Receive(ghPga305, PGA305_DATAPAGE_ADDRESS, rxbuffer, rxlength, HAL_MAX_DELAY);
    if (status != HAL_OK)
    {
        Error_Handler();
    }

    compensatedValue |= rxbuffer[0];

    return compensatedValue;
}


/** Read temperature ADC while compensation engine running.
 *
 *  This feature of the PGA305 does not seem so reliable. It seems better to
 *  leave the compensation engine in reset and to read the ADC over the
 *  digital interface using \sa Pga305_ReadPressureAdc().
 */
uint32_t Pga305_ReadPressureRaw(void)
{
    uint32_t    adcValue;
    uint8_t     high, mid, low;

    Pga305_WriteRegister(PGA305_DATA_PAGE, Pga305_DataReg_Command, Pga305_DataCmd_ReadPAdc);
    Sleep(1);
    
    high = Pga305_ReadRegister(PGA305_DATA_PAGE, Pga305_DataReg_DataLSB);
    Sleep(1);

    Pga305_WriteRegister(PGA305_DATA_PAGE, Pga305_DataReg_Command, Pga305_DataCmd_ReadTrailWord);
    Sleep(1);
    
    low = Pga305_ReadRegister(PGA305_DATA_PAGE, Pga305_DataReg_DataLSB);
    Sleep(1);

    mid = Pga305_ReadRegister(PGA305_DATA_PAGE, Pga305_DataReg_DataMSB);
    Sleep(1);
    
    adcValue = (high << 16) | (mid << 8) | low;
    
    return adcValue;
}


/** Read temperature ADC output from control and data page, while compensation engine not running. */
uint32_t Pga305_ReadPressureAdc(void)
{
    uint32_t    adcValue;
    uint8_t     high, mid, low;

    low = Pga305_ReadRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_PADC_DATA1);
    mid = Pga305_ReadRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_PADC_DATA2);
    high = Pga305_ReadRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_PADC_DATA3);

    adcValue = (high << 16) | (mid << 8) | low;
    
    return adcValue;
}


/** Select which temperature channel is enabled
 *
 *  Enables either the external or internal temperature channel by selecting
 *  which signals get routed through the multiplexer.
 *
 *  @param channel - 0 for external, 1 (or anything else) for internal.
 */
void Pga305_SetTemperatureChannel(int channel)
{
    if (channel == 0)
    {
        Pga305_WriteRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_TEMP_CTRL, PGA305_TEMPCTRL_CURRENT50UA | PGA305_TEMPCTRL_EXTERNAL);
    }
    else
    {
        Pga305_WriteRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_TEMP_CTRL, PGA305_TEMPCTRL_CURRENTOFF | PGA305_TEMPCTRL_INTERNAL);
    }
}


/** Read temperature ADC while compensation engine running.
 *
 *  This feature of the PGA305 does not seem so reliable. It seems better to
 *  leave the compensation engine in reset and to read the ADC over the
 *  digital interface using \sa Pga305_ReadTemperatureAdc().
 */
uint32_t Pga305_ReadTemperatureRaw(void)
{
    uint32_t    adcValue;
    uint8_t     high, mid, low;

    Pga305_WriteRegister(PGA305_DATA_PAGE, Pga305_DataReg_Command, Pga305_DataCmd_ReadTAdc);
    Sleep(1);
    
    high = Pga305_ReadRegister(PGA305_DATA_PAGE, Pga305_DataReg_DataLSB);
    Sleep(1);

    Pga305_WriteRegister(PGA305_DATA_PAGE, Pga305_DataReg_Command, Pga305_DataCmd_ReadTrailWord);
    Sleep(1);
    
    low = Pga305_ReadRegister(PGA305_DATA_PAGE, Pga305_DataReg_DataLSB);
    Sleep(1);

    mid = Pga305_ReadRegister(PGA305_DATA_PAGE, Pga305_DataReg_DataMSB);
    Sleep(1);
    
    adcValue = (high << 16) | (mid << 8) | low;
    
    return adcValue;
}


/** Read temperature ADC output from control and data page, while compensation engine not running. */
uint32_t Pga305_ReadTemperatureAdc(void)
{
    uint32_t    adcValue;
    uint8_t     high, mid, low;

    low = Pga305_ReadRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_TADC_DATA1);
    mid = Pga305_ReadRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_TADC_DATA2);
    high = Pga305_ReadRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_TADC_DATA3);

    adcValue = (high << 16) | (mid << 8) | low;
    
    return adcValue;
}


uint8_t Pga305_ReadEeprom(uint8_t address)
{
    assert(address < PGA305_EEPROM_SIZE);
    
    return Pga305_ReadRegister(PGA305_EEPROM_PAGE, address);
}


void Pga305_WriteEepromBlock(uint8_t address, uint8_t const data[PGA305_EEPROM_CACHE_SIZE])
{
    assert(address < PGA305_EEPROM_SIZE);
    
    /* Set upper 4 bits of EEPROM address */
    address >>= 3;
    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_PageAddress, address);
    
    /* Write bytes into the cache */
    for (int i = 0; i < PGA305_EEPROM_CACHE_SIZE; i++)
    {
        Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Cache0 + i, data[i]);
    }
    
    /* Erase and program */
    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Control, PGA305_EEPROMCTRL_ERASEPROGRAM);
    
    /* Wait for operations to finish */
    uint8_t status;
    do
    {
        status = Pga305_ReadRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Status);
    } while (status & (PGA305_EEPROMSTATUS_ERASE_Msk|PGA305_EEPROMSTATUS_PROGRAM_Msk));
    
    /* Once all blocks have been written to EEPROM the CRC value should be updated */
}


uint8_t Pga305_ReadTempCtrl(void)
{
    return Pga305_ReadRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_TEMP_CTRL);
}


uint8_t Pga305_ReadRegister(uint8_t page, uint8_t offset)
{
    uint8_t registerValue;
    
    HAL_StatusTypeDef    status;
    
    status = HAL_I2C_Master_Transmit(ghPga305, PGA305_ADDRESS_TRANSMIT(page), &offset, 1, HAL_MAX_DELAY);
    if (status != HAL_OK)
    {
        Error_Handler();
    }
    
    status = HAL_I2C_Master_Receive(ghPga305, PGA305_ADDRESS_TRANSMIT(page), &registerValue, 1, HAL_MAX_DELAY);
    if (status != HAL_OK)
    {
        Error_Handler();
    }
    
    return registerValue;
}


void Pga305_WriteRegister(uint8_t page, uint8_t offset, uint8_t value)
{
    HAL_StatusTypeDef    status;
    
    uint8_t     txbuffer[2];
    
    txbuffer[0] = offset;
    txbuffer[1] = value;
    status = HAL_I2C_Master_Transmit(ghPga305, PGA305_ADDRESS_TRANSMIT(page), txbuffer, sizeof txbuffer, HAL_MAX_DELAY);
    if (status != HAL_OK)
    {
        Error_Handler();
    }
}


uint8_t Pga305_UpdateCrc(uint8_t currentCrc, uint8_t dataValue)
{
    uint8_t nextCrc;
    uint8_t dc;

    /* All the XORs are symmetric on corresponding bits and then shifted into
     * place and XORed with itself.
     *
     * Naive implementation, relies on compiler to optimise away the unnecessary
     * shifts.
     */
    dc = dataValue ^ currentCrc;
    nextCrc = (0x01 & ((dc >> 7) ^ (dc >> 6) ^ (dc >> 0))) |
              (0x02 & (((dc >> 6) ^ (dc >> 1) ^ (dc >> 0))) << 1) |
              (0x04 & (((dc >> 6) ^ (dc >> 2) ^ (dc >> 1) ^ (dc >> 0))) << 2) |
              (0x08 & (((dc >> 7) ^ (dc >> 3) ^ (dc >> 2) ^ (dc >> 1))) << 3) |
              (0x10 & (((dc >> 4) ^ (dc >> 3) ^ (dc >> 2))) << 4) |
              (0x20 & (((dc >> 5) ^ (dc >> 4) ^ (dc >> 3))) << 5) |
              (0x40 & (((dc >> 6) ^ (dc >> 5) ^ (dc >> 4))) << 6) |
              (0x80 & (((dc >> 7) ^ (dc >> 6) ^ (dc >> 5))) << 7);
    
    return nextCrc;
}


/* Calculate the CRC for the EEPROM area contents */
/*
Example: should be 0x91
01 00 20 00 00 00 00 00 
00 00 00 00 00 00 10 00 
00 00 00 00 00 00 00 00 
00 00 00 00 00 00 00 00 
00 00 00 00 00 00 00 00 
00 00 00 00 00 00 00 00 
66 01 00 08 01 80 02 43 
00 01 00 00 00 00 FF 3F 
00 00 FF 3F 01 00 00 00 
00 00 00 00 00 00 00 00 
00 00 00 00 00 00 00 00 
07 73 FF 3F FF 3F 01 00 
00 00 00 00 00 00 00 00 
01 00 FF FF FF FF FF FF 
FF FF FF FF FF FF FF FF 
FF FF FF FF FF FF FF 91 

Example: should be 0x90
00 00 20 00 00 00 00 00 
00 00 00 00 00 00 10 00 
00 00 00 00 00 00 00 00 
00 00 00 00 00 00 00 00 
00 00 00 00 00 00 00 00 
00 00 00 00 00 00 00 00 
66 01 00 08 01 80 02 43 
00 01 00 00 00 00 FF 3F 
00 00 FF 3F 01 00 00 00 
00 00 00 00 00 00 00 00 
00 00 00 00 00 00 00 00 
07 73 FF 3F FF 3F 01 00 
00 00 00 00 00 00 00 00 
01 00 FF FF FF FF FF FF 
FF FF FF FF FF FF FF FF 
FF FF FF FF FF FF FF 90 

    // Psuedocode from datasheet
//    currentCRC8 = 0xFF; // Current value of CRC8
//
//    for NextData
//
//    D = NextData;
//
//    C = currentCRC8;
//
//    begin
//
//    nextCRC8_BIT0 = D_BIT7 ^ D_BIT6 ^ D_BIT0 ^ C_BIT0 ^ C_BIT6 ^ C_BIT7;
//
//    nextCRC8_BIT1 = D_BIT6 ^ D_BIT1 ^ D_BIT0 ^ C_BIT0 ^ C_BIT1 ^ C_BIT6;
//
//    nextCRC8_BIT2 = D_BIT6 ^ D_BIT2 ^ D_BIT1 ^ D_BIT0 ^ C_BIT0 ^ C_BIT1 ^ C_BIT2 ^ C_BIT6;
//
//    nextCRC8_BIT3 = D_BIT7 ^ D_BIT3 ^ D_BIT2 ^ D_BIT1 ^ C_BIT1 ^ C_BIT2 ^ C_BIT3 ^ C_BIT7;
//
//    nextCRC8_BIT4 = D_BIT4 ^ D_BIT3 ^ D_BIT2 ^ C_BIT2 ^ C_BIT3 ^ C_BIT4;
//
//    nextCRC8_BIT5 = D_BIT5 ^ D_BIT4 ^ D_BIT3 ^ C_BIT3 ^ C_BIT4 ^ C_BIT5;
//
//    nextCRC8_BIT6 = D_BIT6 ^ D_BIT5 ^ D_BIT4 ^ C_BIT4 ^ C_BIT5 ^ C_BIT6;
//
//    nextCRC8_BIT7 = D_BIT7 ^ D_BIT6 ^ D_BIT5 ^ C_BIT5 ^ C_BIT6 ^ C_BIT7;
//
//    end
//
//    currentCRC8 = nextCRC8_D8;
//
//    endfor
    
*/
/** Calculates the CRC over a block of memory using the PGA305 algorithm.
 *
 *  @param data - Address of an array containing (PGA305_EEPROM_SIZE - 1) elements to calculate the CRC over.
 */
uint8_t Pga305_CalculateCrc(uint8_t const *data)
{
    uint8_t crc = PGA305_CRC_IV;

    /* Do not include the CRC byte in the calculation */
    for (int i = 0; i < (PGA305_EEPROM_SIZE - 1); i++)
    {
        crc = Pga305_UpdateCrc(crc, data[i]);
    }
    
    return crc;
}


/** Calculates the CRC over the current contents of PGA305 EEPROM.
 */
uint8_t Pga305_CalculateCrcOverEeprom(void)
{
    uint8_t crc = PGA305_CRC_IV;

    /* Do not include the CRC byte in the calculation */
    for (int i = 0; i < (PGA305_EEPROM_SIZE - 1); i++)
    {
        uint8_t eepromData;
        
        eepromData = Pga305_ReadRegister(PGA305_EEPROM_PAGE, i);
        crc = Pga305_UpdateCrc(crc, eepromData);
    }
    
    return crc;
}


/** Writes a new CRC value into EEPROM.
 *
 *  Gets the EEPROM line containing the CRC, writes the data
 *  back into the cache with the new CRC, erases and programs the line, and
 *  waits for the programming to complete.
 */
void Pga305_WriteEepromCrc(uint8_t crc)
{
    uint8_t eepromLine[PGA305_EEPROM_CACHE_SIZE];
    
    /* Get existing contents of other bytes */
    for (int i = 0; i < (PGA305_EEPROM_CACHE_SIZE - 1); i++)
    {
        uint8_t eepromValue;
        
        eepromValue = Pga305_ReadRegister(PGA305_EEPROM_PAGE, PGA305_EEPROM_SIZE - PGA305_EEPROM_CACHE_SIZE + i);
        Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Cache0 + i, eepromValue);
    }
    
    /* Include CRC value */
    eepromLine[PGA305_EEPROM_CACHE_SIZE - 1] = crc;
    
    /* Write line (cache, erase, program, wait) to EEPROM */
    Pga305_WriteEepromBlock(PGA305_EEPROM_SIZE - PGA305_EEPROM_CACHE_SIZE, eepromLine);
}


/* Temperature conversion coefficients */
#define PGA305_TEMPERATURE_ADC_LOW  (0x16C900UL)
#define PGA305_TEMPERATURE_ADC_MID  (0x1ACF00UL)
#define PGA305_TEMPERATURE_ADC_HIGH (0x29E500UL)

#define PGA305_TEMPERATURE_DEG_LOW  (-40.0)
#define PGA305_TEMPERATURE_DEG_MID  (0.0)
#define PGA305_TEMPERATURE_DEG_HIGH (150.0)

#define PGA305_TEMPERATURE_m0       ((PGA305_TEMPERATURE_DEG_MID - PGA305_TEMPERATURE_DEG_LOW) / ((double)PGA305_TEMPERATURE_ADC_MID - PGA305_TEMPERATURE_ADC_LOW))
#define PGA305_TEMPERATURE_C0       (PGA305_TEMPERATURE_DEG_MID - PGA305_TEMPERATURE_m0 * PGA305_TEMPERATURE_ADC_MID)

#define PGA305_TEMPERATURE_m1       ((PGA305_TEMPERATURE_DEG_HIGH - PGA305_TEMPERATURE_DEG_MID) / ((double)PGA305_TEMPERATURE_ADC_HIGH - PGA305_TEMPERATURE_ADC_MID))
#define PGA305_TEMPERATURE_C1       (PGA305_TEMPERATURE_DEG_MID - PGA305_TEMPERATURE_m1 * PGA305_TEMPERATURE_ADC_MID)


double Pga305_CalculateInternalTemperature(uint32_t temperatureAdc)
{
    double temperatureDegC;
    
    /* Need to linear interpolate from the following:
    
        -40 C == 0x16C900
          0 C == 0x1ACF00
        150 C == 0x29E500
    */
    
    if (temperatureAdc < PGA305_TEMPERATURE_ADC_MID)
    {
        
        temperatureDegC = PGA305_TEMPERATURE_m0 * temperatureAdc + PGA305_TEMPERATURE_C0;
    }
    else
    {
        temperatureDegC = PGA305_TEMPERATURE_m1 * temperatureAdc + PGA305_TEMPERATURE_C1;
    }
        
    return temperatureDegC;
}
