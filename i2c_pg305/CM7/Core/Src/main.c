/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
#include <stdio.h>
#include "pga305.h"
#include "sleep.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MPU_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


//enum PGA305_Data_Registers
//{
//    PGA305_DataReg_DataLSB  = 0x04,     // LSB of requested data
//    PGA305_DataReg_DataMSB  = 0x05,     // MSB of requested data
//    PGA305_DataReg_Command  = 0x09,     // Command to execute
//};
//
//
//// These are the commands that can be sent to the PGA305, by writing
//// them to the register PGA305_DataReg_Command.
//enum PGA305_Data_Commands
//{
//    PGA305_DataCmd_ReadPAdc         = 0x00,     // Read one 24-bit sample from the Pressure ADC channel
//    PGA305_DataCmd_ReadTAdc         = 0x02,     // Read one 24-bit sample from the Temperature ADC channel
//    PGA305_DataCmd_ReadCompensated  = 0x04,     // Read compensated output value
//    PGA305_DataCmd_ReadDiagnostics  = 0x06,
//    PGA305_DataCmd_ReadTrailWord    = 0x70,     // Read the lower 16 bits of any previously executed command
//};


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
/* USER CODE BEGIN Boot_Mode_Sequence_0 */
/* USER CODE END Boot_Mode_Sequence_0 */

  /* MPU Configuration--------------------------------------------------------*/
  MPU_Config();

  /* Enable I-Cache---------------------------------------------------------*/
  SCB_EnableICache();

  /* Enable D-Cache---------------------------------------------------------*/
  SCB_EnableDCache();

/* USER CODE BEGIN Boot_Mode_Sequence_1 */
/* USER CODE END Boot_Mode_Sequence_1 */
  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();
/* USER CODE BEGIN Boot_Mode_Sequence_2 */
/* USER CODE END Boot_Mode_Sequence_2 */

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_USART3_UART_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
    HAL_StatusTypeDef    status;
    
    // WTF HAL, why do I need this?
    //status = HAL_I2C_IsDeviceReady(&hi2c1, 
    
//    uint16_t    deviceBaseAddress = 0x40; // STM just replaces the LSb with r/w and leaves you to shift the address up
//    uint16_t    deviceDataPageAddress = (deviceBaseAddress + 0) << 1;
//    //uint16_t    deviceControlPageAddress = (deviceBaseAddress + 2) << 1;
//    //uint16_t    deviceEepromPageAddress = (deviceBaseAddress + 5) << 1;
//    uint8_t     txbuffer[16];
//    uint16_t    txlength;
//    uint8_t     rxbuffer[16];
//    uint16_t    rxlength;
//    
//    /*
//     *  Follow example from datasheet to read compensated output value 24 bits
//     *
//        Description             MCU                         PGA305
//        1. Send cmd 4, read     0x40 - Start Addr Write     Ack
//        compensated output      0x09 - Register = Command   Ack
//                                0x04 - Command              Ack
//                                                            
//        2. Read bits 23:16      0x40 - Start Addr Write     Ack
//                                0x04 - Register = Data LSB  Ack
//                                0x41 - RptStart Addr Read   0xhh
//                                Ack(?)
//
//        3. Send read trail      0x40 - Start Addr Write     Ack
//        words command           0x09 - Register = Command   Ack
//                                0x70 - Command              Ack
//
//        4. Read bits 16:9       0x40 - Start Addr Write     Ack
//                                0x05 - Register = Data MSB  Ack
//                                0x41 - Start Addr Read      0xhh
//                                Ack(?)
//        
//        5. Read bits 8:0        0x40 - Start Addr Write     Ack
//                                0x04 - Register = Data LSB  Ack
//                                0x41 - Start Addr Read      0xhh
//                                Ack(?)
//     */
//    
//    // 1. Send read compensated output value command
//    txbuffer[0] = Pga305_DataReg_Command;
//    txbuffer[1] = Pga305_DataCmd_ReadCompensated;
//    txlength = 2;
//    status = HAL_I2C_Master_Transmit(&hi2c1, deviceDataPageAddress, txbuffer, txlength, HAL_MAX_DELAY);
//    if (status != HAL_OK)
//    {
//        Error_Handler();
//    }
//
//    
//    // 2. Read bits 23:16
//    txbuffer[0] = Pga305_DataReg_DataLSB;
//    txlength = 1;
//    status = HAL_I2C_Master_Transmit(&hi2c1, deviceDataPageAddress, txbuffer, txlength, HAL_MAX_DELAY);
//    if (status != HAL_OK)
//    {
//        Error_Handler();
//    }
//    
//    rxlength = 1;
//    status = HAL_I2C_Master_Receive(&hi2c1, deviceDataPageAddress, rxbuffer, rxlength, HAL_MAX_DELAY);
//    if (status != HAL_OK)
//    {
//        Error_Handler();
//    }
//    
//    
//    // 3. Send read trail word
//    txbuffer[0] = Pga305_DataReg_Command;
//    txbuffer[1] = Pga305_DataCmd_ReadTrailWord;
//    txlength = 2;
//    status = HAL_I2C_Master_Transmit(&hi2c1, deviceDataPageAddress, txbuffer, txlength, HAL_MAX_DELAY);
//    if (status != HAL_OK)
//    {
//        Error_Handler();
//    }
//    
//    
//    // 4. Read bits 16:9
//    txbuffer[0] = Pga305_DataReg_DataMSB;
//    txlength = 1;
//    status = HAL_I2C_Master_Transmit(&hi2c1, deviceDataPageAddress, txbuffer, txlength, HAL_MAX_DELAY);
//    if (status != HAL_OK)
//    {
//        Error_Handler();
//    }
//    
//    rxlength = 1;
//    status = HAL_I2C_Master_Receive(&hi2c1, deviceDataPageAddress, rxbuffer, rxlength, HAL_MAX_DELAY);
//    if (status != HAL_OK)
//    {
//        Error_Handler();
//    }
//    
//    
//    // 5. Read bits 8:0
//    txbuffer[0] = Pga305_DataReg_DataLSB;
//    txlength = 1;
//    status = HAL_I2C_Master_Transmit(&hi2c1, deviceDataPageAddress, txbuffer, txlength, HAL_MAX_DELAY);
//    if (status != HAL_OK)
//    {
//        Error_Handler();
//    }
//    
//    rxlength = 1;
//    status = HAL_I2C_Master_Receive(&hi2c1, deviceDataPageAddress, rxbuffer, rxlength, HAL_MAX_DELAY);
//    if (status != HAL_OK)
//    {
//        Error_Handler();
//    }
//
//    while (1)
//    {
//        __WFE();
//        __NOP();
//    }
    
    Pga305_Initialise();
    
    
    char    str[128];
    uint8_t tempCtrl;
    
    uint8_t eeprom[PGA305_EEPROM_SIZE];

    // To use the digital interface you need to enable the digital interface
    // and reset the compensation by writing 0x03 to the compensation control
    // register.
    
//    tempCtrl = Pga305_ReadRegister(PGA305_DATA_PAGE, 0x0C);
//    sprintf(str, "COMP_CTRL = 0x%02X\r\n", tempCtrl);
//    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//
//    tempCtrl = Pga305_ReadTempCtrl();
//    sprintf(str, "TEMP_CTRL = 0x%02X\r\n", tempCtrl);
//    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//    
//    tempCtrl = Pga305_ReadRegister(PGA305_CONTROL_PAGE, 0x03B);
//    sprintf(str, "OP_STAGE_CTRL = 0x%02X\r\n", tempCtrl);
//    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);

    strcpy(str, "Write comp ctrl\r\n");
    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
    Pga305_WriteRegister(PGA305_DATA_PAGE, Pga305_DataReg_CompensationControl, PGA305_COMPENSATIONCTRL_COMPENSATIONRESET | PGA305_COMPENSATIONCTRL_IFSEL);
    
//    tempCtrl = Pga305_ReadRegister(PGA305_DATA_PAGE, 0x0C);
//    sprintf(str, "COMP_CTRL = 0x%02X\r\n", tempCtrl);
//    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//
//    tempCtrl = Pga305_ReadTempCtrl();
//    sprintf(str, "TEMP_CTRL = 0x%02X\r\n", tempCtrl);
//    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//    
//    tempCtrl = Pga305_ReadRegister(PGA305_CONTROL_PAGE, 0x03B);
//    sprintf(str, "OP_STAGE_CTRL = 0x%02X\r\n", tempCtrl);
//    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//
//    tempCtrl = Pga305_ReadRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_CalculateCrc);
//    sprintf(str, "EE CRC = 0x%02X\r\n", tempCtrl);
//    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);

    // Read the CRC status register to see whether it is OK or not
    tempCtrl = Pga305_ReadRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_CrcStatus);
    sprintf(str, "EE CRC Status = 0x%02X\r\n", tempCtrl);
    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);

    // Read the calculated CRC value
    tempCtrl = Pga305_ReadRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_CrcValue);
    sprintf(str, "EE CRC Value = 0x%02X\r\n", tempCtrl);
    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//
//    
//    // Force CRC calculation
//    
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_CalculateCrc, 0x01);
//
//    do
//    {
//        tempCtrl = Pga305_ReadRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_CrcStatus);
//        sprintf(str, "EE CRC Status = 0x%02X\r\n", tempCtrl);
//        status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//    } while (tempCtrl & 0x01);
//    
//    tempCtrl = Pga305_ReadRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_CrcValue);
//    sprintf(str, "EE CRC Value = 0x%02X\r\n", tempCtrl);
//    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
    
    
    tempCtrl = Pga305_ReadRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_PageAddress);
    sprintf(str, "EE Page = 0x%02X\r\n", tempCtrl);
    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);

    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_PageAddress, 0x0F);

    tempCtrl = Pga305_ReadRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_PageAddress);
    sprintf(str, "EE Page = 0x%02X\r\n", tempCtrl);
    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
    
    
    // Read EEPROM contents
    for (int line = 0; line < 16; line++)
    {
        str[0] = '\0';
        
        // Read each byte in the line
        for (int byteIndex = 0; byteIndex < 8; byteIndex++)
        {
            uint8_t eepromValue;
            eepromValue = Pga305_ReadRegister(PGA305_EEPROM_PAGE, line * 8 + byteIndex);
            eeprom[line * 8 + byteIndex] = eepromValue;
            sprintf(str + strlen(str), "%02X ", eepromValue);
        }
        
        strcat(str, "\r\n");
        status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
    }

//    // Update something in EEPROM
//    // Set to first page
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_PageAddress, 0x00);
//    
//    // Just something to toggle the LSb of the offset coefficient
//    uint8_t value = 1 - Pga305_ReadRegister(PGA305_EEPROM_PAGE, 0x00);
//    
//    // Transfer those 8 bytes into cache
//    // With default values, values are 00 00 20 00 00 00 00 00
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Cache0, value);
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Cache1, 0x00);
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Cache2, 0x20);
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Cache3, 0x00);
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Cache4, 0x00);
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Cache5, 0x00);
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Cache6, 0x00);
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Cache7, 0x00);
//    
//    // Erase and program
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Control, PGA305_EEPROMCTRL_ERASEPROGRAM);
//    
//    // Wait for operations to finish
//    do
//    {
//        tempCtrl = Pga305_ReadRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Status);
//    } while (tempCtrl & (PGA305_EEPROMSTATUS_ERASE_Msk|PGA305_EEPROMSTATUS_PROGRAM_Msk));
//    
//    // Force a CRC calculation
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_CalculateCrc, PGA305_EEPROMCRC_CALCULATE);
//    
//    // Wait for it to finish
//    do
//    {
//        tempCtrl = Pga305_ReadRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_CrcStatus);
//    } while (tempCtrl & PGA305_EEPROMCRCSTATUS_CHECKING_Msk);
//    
//    // Get the calculated CRC value
//    uint8_t crcValue;
//    crcValue = Pga305_ReadRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_CrcValue);
//    
//    // Set to last 8 bytes in EEPROM
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_PageAddress, 0x0F);
//    
//    // Transfer those 8 bytes into cache
//    // With default values, penultimate 7 bytes are 0xFF and last is CRC (0x90)
//    // Put in calculated CRC value so we don't have to calculate it ourselves
//    // However, this relies on the penultimate 7 bytes not changing.
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Cache0, 0xFF);
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Cache1, 0xFF);
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Cache2, 0xFF);
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Cache3, 0xFF);
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Cache4, 0xFF);
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Cache5, 0xFF);
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Cache6, 0xFF);
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Cache7, crcValue);
//    
//    // Erase and program
//    Pga305_WriteRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Control, PGA305_EEPROMCTRL_ERASEPROGRAM);
//    
//    // Wait for operations to finish
//    do
//    {
//        tempCtrl = Pga305_ReadRegister(PGA305_EEPROM_PAGE, Pga305_EepromReg_Status);
//    } while (tempCtrl & (PGA305_EEPROMSTATUS_ERASE_Msk|PGA305_EEPROMSTATUS_PROGRAM_Msk));


    uint8_t crcCalculation;
    
    eeprom[0] = 0x00;
    crcCalculation = Pga305_CalculateCrc(eeprom);
    sprintf(str, "CRC 0x00 = 0x%02X expect 0x90\r\n", crcCalculation);
    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
    
    eeprom[0] = 0x01;
    crcCalculation = Pga305_CalculateCrc(eeprom);
    sprintf(str, "CRC 0x01 = 0x%02X expect 0x91\r\n", crcCalculation);
    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
    
    uint8_t regValue;
    

    
    
    // Temperature mux must be single ended for internal, and external the way PE has configured boards
    //Pga305_WriteRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_TEMP_SE, PGA305_TEMPSE_DIFFERENTIAL);
    regValue = Pga305_ReadRegister(PGA305_EEPROM_PAGE, Pga305_EEAddr_TEMP_SE);
    sprintf(str, "TempSe = 0x%02X 0 for SE, 1 for diff, MUST be 0\r\n", regValue);
    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
    
    // Configure gain for 5V/V - used for both int/ext
    Pga305_WriteRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_T_GAIN_SELECT, PGA305_TGAINSELECT_NOTINVERTED | PGA305_TGAINSELECT_5V);
    regValue = Pga305_ReadRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_T_GAIN_SELECT);
    sprintf(str, "TempGain = 0x%02X\r\n", regValue);
    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
    
    // When you want the device to run normally, set the compensation control
    // register back to 0x00
//    strcpy(str, "Write comp ctrl\r\n");
//    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//    Pga305_WriteRegister(PGA305_DATA_PAGE, Pga305_DataReg_CompensationControl,
//                         PGA305_COMPENSATIONCTRL_COMPENSATION_RUNNING|PGA305_COMPENSATIONCTRL_IFSEL_CALCULATIONENGINE);
    
//    tempCtrl = Pga305_ReadRegister(PGA305_DATA_PAGE, 0x0C);
//    sprintf(str, "COMP_CTRL = 0x%02X\r\n", tempCtrl);
//    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//
//    tempCtrl = Pga305_ReadTempCtrl();
//    sprintf(str, "TEMP_CTRL = 0x%02X\r\n", tempCtrl);
//    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//    
//    tempCtrl = Pga305_ReadRegister(PGA305_CONTROL_PAGE, 0x03B);
//    sprintf(str, "OP_STAGE_CTRL = 0x%02X\r\n", tempCtrl);
//    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);

    
    while (1)
    {
        uint32_t adcValue;
        double voltage;
        double pressurePsi;
        double temperatureDegC;
        
        // Use internal temperature sensor
//        Pga305_WriteRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_TEMP_CTRL, PGA305_TEMPCTRL_CURRENTOFF | PGA305_TEMPCTRL_INTERNAL);
//        regValue = Pga305_ReadRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_TEMP_CTRL);
//        sprintf(str, "TempCtrl = 0x%02X\r\n", regValue);
//        status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
        Pga305_SetTemperatureChannel(PGA305_TEMPERATURE_CHANNEL_INTERNAL);

        /* Delay to allow new sample to be taken */
        Sleep(1);
        
        //adcValue = Pga305_ReadTemperatureRaw();
        adcValue = Pga305_ReadTemperatureAdc();
        temperatureDegC = Pga305_CalculateInternalTemperature(adcValue);
        
        sprintf(str, "Int T ADC 0x%08lX == %lu == %.2f\r\n", adcValue, adcValue, temperatureDegC);
        status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
        if (status != HAL_OK)
        {
            Error_Handler();
        }

        // Use external temperature sensor
//        Pga305_WriteRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_TEMP_CTRL, PGA305_TEMPCTRL_CURRENT50UA | PGA305_TEMPCTRL_EXTERNAL);
//        regValue = Pga305_ReadRegister(PGA305_CONTROL_PAGE, Pga305_ControlReg_TEMP_CTRL);
//        sprintf(str, "TempCtrl = 0x%02X\r\n", regValue);
//        status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);

        Pga305_SetTemperatureChannel(PGA305_TEMPERATURE_CHANNEL_EXTERNAL);
        
        Sleep(1);
        
        //adcValue = Pga305_ReadTemperatureRaw();
        adcValue = Pga305_ReadTemperatureAdc();

        // TODO - External temperature sensor on Pressure Transducer needs it's own conversion coefficients
        //temperatureDegC = lower_m * adcValue + lower_C;
        temperatureDegC = Pga305_CalculateInternalTemperature(adcValue);
        
        sprintf(str, "Ext T ADC 0x%08lX == %lu == %.2f\r\n", adcValue, adcValue, temperatureDegC);
        status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
        if (status != HAL_OK)
        {
            Error_Handler();
        }

//        uint32_t compensated = Pga305_ReadCompensated();
//        sprintf(str, "P Comp 0x%08lX == %lu\r\n", compensated, compensated);
//        status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//        if (status != HAL_OK)
//        {
//            Error_Handler();
//        }

        adcValue = Pga305_ReadPressureAdc();
        
        // Convert ADC counts into voltage
        voltage = adcValue;
        
        // Coefficients from Paine cert 61124
        double x00 = 7.287195624060e1;
        double x10 = 3.847552369950e3;
        pressurePsi = voltage * x10 + x00;
        
        sprintf(str, "P 0x%08lX == %lu counts == %.6fV == %.0fpsi\r\n", adcValue, adcValue, voltage, pressurePsi);
        status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
        if (status != HAL_OK)
        {
            Error_Handler();
        }
        
        
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
        __WFE();
        __NOP();
        Sleep(1000);
    }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Supply configuration update enable
  */
  HAL_PWREx_ConfigSupply(PWR_DIRECT_SMPS_SUPPLY);
  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 75;
  RCC_OscInitStruct.PLL.PLLP = 2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_3;
  RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
  RCC_OscInitStruct.PLL.PLLFRACN = 0;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV2;
  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* MPU Configuration */

void MPU_Config(void)
{

  /* Disables the MPU */
  HAL_MPU_Disable();
  /* Enables the MPU */
  HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);

}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
