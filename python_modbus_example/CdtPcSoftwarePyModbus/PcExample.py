# Simulation of CDT tool modbus interface, as for running example CDT modbus
# code against.
#
# Requires Pyside2 (Qt) for user interface.
# Requires modbus_tk, pyserial for performing the modbus comms.

import sys
from PySide2.QtWidgets import QApplication, QMainWindow, QMessageBox
from PySide2.QtCore import Slot
import uiLoader
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
import serial
import CustomModbusFunctions
from CustomModbusFunctions import BeginWriteUserModbusRequest
from CustomModbusFunctions import WriteDataUserModbusRequest
from CustomModbusFunctions import FinishWriteUserModbusRequest
from CustomModbusFunctions import CancelWriteUserModbusRequest

# The address of the server on the bus.
serverUnitBusAddress = 0x20

#
class MainWindow(QMainWindow):
    #
    def __init__(self, parent=None):
        #
        QMainWindow.__init__(self, parent)
        self._server = None
        self._serverThread = None
        uiLoader.loadUi('untitled.ui', self)

    # Get the serial configuration to use. Firmware will use 115200,8,E,1
    def extract_serial_parameters(self):
        self._port_path = self.portName.text()
        self._baud_rate = int(self.baud.currentText())
        self._data_bits = int(self.dataBits.currentText())
        self._parity = [serial.PARITY_NONE, serial.PARITY_MARK, serial.PARITY_SPACE, serial.PARITY_ODD, serial.PARITY_EVEN][self.parity.currentIndex()]
        self._stop_bits = [serial.STOPBITS_ONE, serial.STOPBITS_ONE_POINT_FIVE, serial.STOPBITS_TWO][self.stopBits.currentIndex()]

    @Slot()
    def on_queryServer_clicked(self):
        self.extract_serial_parameters()
        client = ModbusClient(method='rtu', timeout=5,
                              port=self._port_path,
                              baudrate=self._baud_rate,
                              parity=self._parity)
        if client.connect():
            self.plainTextEdit.appendPlainText('Read input registers\r\n')
            # These register addresses are PDU  addresses, so are 0 based.
            # This is the opposite of what you set up in the server. WTF!
            rr = client.read_input_registers(6, 8, unit=serverUnitBusAddress)
            self.plainTextEdit.appendPlainText(str(rr))
            for rv in rr.registers:
                self.plainTextEdit.appendPlainText('    0x%04X' % rv)

            client.close()
        else:
            QMessageBox.critical(self, 'Error', 'Could not open port \'' + self._port_path + '\'')

    @Slot()
    def on_writeConfiguration_clicked(self):
        # The dummy configuration blob to send
        address = 0
        configuration_blob = bytearray(iter(range(256)))*4

        # Create the client object
        self.extract_serial_parameters()
        client = ModbusClient(method='rtu', timeout=5,
                              port=self._port_path,
                              baudrate=self._baud_rate,
                              parity=self._parity)

        # We need to tell the client about our custom response messages
        client.register(CustomModbusFunctions.BeginWriteUserModbusResponse)
        client.register(CustomModbusFunctions.WriteDataUserModbusResponse)
        client.register(CustomModbusFunctions.FinishWriteUserModbusResponse)
        client.register(CustomModbusFunctions.CancelWriteUserModbusResponse)

        # Tell the client to connect to the server
        if client.connect():
            # Create the request message to send. Note that it must include
            # the server bus address as the unit named parameter.
            request = BeginWriteUserModbusRequest(CustomModbusFunctions.MEMORY_AREA_CONFIGURATION,
                                                  unit=serverUnitBusAddress)
            result = client.execute(request)
            if not result.isError():
                self.plainTextEdit.appendPlainText('BeginWrite successful\r\n')

            # Maximum number of bytes that can be written in one command
            # (as defined by our own custom function code in the firmware)
            #  = Modbus max length (256) - unit address length (1) - 
            #                              function code length (1) -
            #                              function code structure (4 + 1) -
            #                              crc length (2)
            #  = 256 - 1 - 1 - (4 + 1) - 2
            #  = 247
            maximum_bytes = 247

            # Just a little something for information purposes
            write_count = 0

            # Loop to write all the configuration data in blocks
            # of up to maximum_bytes
            while address < len(configuration_blob):
                bytes_to_write = len(configuration_blob) - address
                if (bytes_to_write > maximum_bytes):
                    bytes_to_write = maximum_bytes
                
                request = WriteDataUserModbusRequest(address,
                                                     configuration_blob[address:address+bytes_to_write],
                                                     unit=serverUnitBusAddress)
                result = client.execute(request)
                if not result.isError():
                    self.plainTextEdit.appendPlainText('WriteData ' +
                                                        str(write_count) +
                                                        ' successful\r\n')
                
                address += bytes_to_write
                write_count += 1

            request = FinishWriteUserModbusRequest(CustomModbusFunctions.MEMORY_AREA_CONFIGURATION,
                                                  unit=serverUnitBusAddress)
            result = client.execute(request)
            if not result.isError():
                self.plainTextEdit.appendPlainText('FinishWrite successful\r\n')
            else:
                self.plainTextEdit.appendPlainText('FinishWrite error\r\n')

            # TODO - probably need a short delay in here while the data is written
            #        to non-volatile memory

            # TODO - other things here, such as polling for status, read back
            #        configuration to check that it has programmed correctly

            # Close the client to end communications
            client.close()
        else:
            QMessageBox.critical(self, 'Error', 'Could not open port \'' + self._port_path + '\'')

#
def main():
    app = QApplication(sys.argv)
    mainWindow = MainWindow()
    mainWindow.show()
    sys.exit(app.exec_())

#
if (__name__ == '__main__'):
    main()

