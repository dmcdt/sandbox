/* Copyright(c) 2021 Cereus Downhole Technology Ltd, All rights reserved. */

//#include "CommonDefinitions.h"

#include "RhModel.h"
#include "RhDmaHandler.h"
#include "RhConductor.h"
//#include "deserializer.h"
//#include "FmcManager.h"

#define CRC_TRANSFER_LENGTH (2U)

static Array16_t *pixelBuffer;
static uint32_t *pCRC;

static uint32_t cycleCounter;

void RhCInit(void)
{
    /*TODO:
     * - Initialise Model
     *  = Get config values?
     * 
     */

    RhMInit();
    RhDmaHandlerSetupDataTriggers();
    pixelBuffer = RhMGetCurrentPixelBuffer();
    pCRC = RhMGetCurrentCrcBuffer();
    //Deserializer_PowerOn();
    
    RhMSetState(RH_READY);
    
}

void RhCRun(void)
{
    /* TODO:
     * - Set FMC to Correct values for intercepting the serialiser data
     * - Send MODBUS commands
     *  = Repeat Pixel data
     *  = Change medium
     * 
     */

    switch (RhMGetState())
    {
    case RH_UNINIT:
        /* TODO
         * - Decide if anything needs to happen here
         */
        cycleCounter = 0;
        RhMSetState(RH_READY);
        break;
    case RH_READY:
        cycleCounter++;
        
        /* Setup for Pixel data collection */
        //FmcManagerDeserialiserEnable();
        //Deserializer_Enable();

        RhDmaHandlerSetupDataTransfer((uint32_t)pixelBuffer->pArray, (uint32_t)pixelBuffer->arraySize);
        RhDmaHandlerSetTriggerAdcRd();
        RhMSetState(RH_PIXEL_COLLECTION);
        break;
    case RH_PIXEL_COLLECTION:
        /* TODO
         * - Set FMC to Deserialiser
         */
        
        if (RhDmaHandlerGetDataTrasferFlagState())
        {
            /* Setup for CRC data collection */
            RhDmaHandlerSetupDataTransfer((uint32_t)pCRC, CRC_TRANSFER_LENGTH);
            RhDmaHandlerSetTriggerCrc();
            RhMSetState(RH_CRC_PROCESSING);
            //RhMCalculateCrc();
        }
        break;
    case RH_CRC_PROCESSING:

        if (RhDmaHandlerGetDataTrasferFlagState())
        {
            //RhMCompareCrc();
            //RhMStorePixel();
            pixelBuffer = RhMGetCurrentPixelBuffer();
            RhMSetState(RH_READY);
        }
        
        break;
    case RH_REPEAT_PIXEL:
        /* TODO
         * - Decide if anything needs to happen here
         */
        break;
    default:
        /* TODO
         * - Decide if anything needs to happen here
         */
        break;
    }
}