/* Copyright(c) 2021 Cereus Downhole Technology Ltd, All rights reserved. */

#include <string.h>
#include <stdbool.h>

#include "CommonDefinitions.h"

//#include "RhModBusInterface.h"
//#include "CalculateCrcCM7.h"
//#include "ConfigurationCM7.h"
#include "RhModel.h"
//#include "LogMemory.h"

#define MAXIMUM_PIXEL_ARRAY_SIZE (10000U)

enum operatingModes
{
    normalOperation = 0,
    neverRepeat = 1,
    alwaysRepeat = 2
};

/* Variables used for receiving the Pixel data - Stored in AXI SRAM */
#pragma default_variable_attributes = @ "DMA"
static uint16_t pixelArrayTick[MAXIMUM_PIXEL_ARRAY_SIZE];
static uint16_t pixelArrayTock[MAXIMUM_PIXEL_ARRAY_SIZE];
static uint16_t pixelArrayRepeat[MAXIMUM_PIXEL_ARRAY_SIZE];
static uint32_t transmittedCRC;
static uint32_t originalCRC;
static uint32_t repeatCRC;
#pragma default_variable_attributes = // end of AXI SRAM storage


static Array16_t pixelBufferTick = {.pArray = pixelArrayTick, .arraySize = MAXIMUM_PIXEL_ARRAY_SIZE};
static Array16_t pixelBufferTock = {.pArray = pixelArrayTock, .arraySize = MAXIMUM_PIXEL_ARRAY_SIZE};
static Array16_t pixelBufferRepeat = {.pArray = pixelArrayRepeat, .arraySize = MAXIMUM_PIXEL_ARRAY_SIZE};

static Array16_t *currentPixelBuffer = &pixelBufferTick;
static Array16_t *checkedPixelBuffer;
static Array16_t *originalPixelBuffer;
static uint32_t calculatedCRC;
static bool pixelRepeat;
static bool repeatModeNever = false;
static bool repeatModeAlways = false;

/* Model State and configuration */
static RhModelState_t currentRhModelState;
//static struct RhConfig rhConfigData;

/* Local Functions */
/**********************************************************************************************************************/

void updateOperatingMode(uint16_t newMode)
{
    if (newMode > 2)
    {
        newMode = 0;
    }

    switch ((enum operatingModes)newMode)
    {
    case normalOperation:
        repeatModeNever = false;
        repeatModeAlways = false;
        break;
    case neverRepeat:
        repeatModeNever = true;
        repeatModeAlways = false;
        break;
    case alwaysRepeat:
        repeatModeNever = false;
        repeatModeAlways = true;
        break;
    default:
        repeatModeNever = false;
        repeatModeAlways = false;
        break;
    }

}

/* Public Functions */
/**********************************************************************************************************************/

void RhM_SetNormalMode(void)
{
    repeatModeNever = false;
    repeatModeAlways = false;
}

void RhM_SetNeverRepeatMode(void)
{
    repeatModeNever = true;
    repeatModeAlways = false;
}

void RhM_SetAlwaysRepeatMode(void)
{
    repeatModeNever = false;
    repeatModeAlways = true;
}

void RhMInit(void)
{
    //struct RhConfig *newData;

    //newData = ConfigurationCm7GetRhValues();
    /* TODO: remove the physical copy of data; keep using pointer to CM4 data */
    //memcpy(&rhConfigData, newData, sizeof(struct RhConfig));
    
    pixelBufferTick.arraySize = MAXIMUM_PIXEL_ARRAY_SIZE;//rhConfigData.RH_pixel_samples;
    pixelBufferTock.arraySize = MAXIMUM_PIXEL_ARRAY_SIZE;//rhConfigData.RH_pixel_samples;
    pixelBufferRepeat.arraySize = MAXIMUM_PIXEL_ARRAY_SIZE;//rhConfigData.RH_pixel_samples;

    updateOperatingMode(neverRepeat);//rhConfigData.RH_operating_mode);

    //RhModBusInterfaceInit();
}

RhModelState_t RhMGetState(void)
{
    return currentRhModelState;
}

void RhMSetState(RhModelState_t newState)
{
    currentRhModelState = newState;
}

Array16_t *RhMGetCurrentPixelBuffer(void)
{
    return currentPixelBuffer;
}

uint32_t *RhMGetCurrentCrcBuffer(void)
{
    return &transmittedCRC;
}

void RhMCalculateCrc(void)
{
    if (!pixelRepeat)
    {
        //calculatedCRC = CalculateCrc32bHalfwordCalc(currentPixelBuffer->pArray, currentPixelBuffer->arraySize);
        calculatedCRC = 0xFFFFFFFFUL;
    }
}

void RhMCompareCrc(void)
{
    if (!pixelRepeat)
    {
        originalCRC = transmittedCRC;

        /* Implementing the 3 repeat modes:
         * 1.) Normal operation: Repeat requested if CRC compare are not equal:
               + repeateModeNever = false
               + repeatModeAlways = false
         * 2.) Never Repeat:     Repeat data is never requested
               + repeateModeNever = true
               + repeateModeAlways = false
         * 3.) Always Repeat:    Repeat requested every time
               + repeateModeNever = false
               + repeateModeAlways = true
         */
        if (repeatModeNever || (!repeatModeAlways && calculatedCRC == originalCRC))
        /* 1.) [false       || {true              && calculated                  }] = calculated
         * 2.) [true        || {true              && calculated                  }] = true
         * 3.) [false       || {false             && calculated                  }] = false
         */
        {
            if (currentPixelBuffer == &pixelBufferTick)
            {
                checkedPixelBuffer = &pixelBufferTick;
                currentPixelBuffer = &pixelBufferTock;
            }
            else
            {
                checkedPixelBuffer = &pixelBufferTock;
                currentPixelBuffer = &pixelBufferTick;
            }
            repeatCRC = 0;
        }
        else if (calculatedCRC != originalCRC)
        {
            //RhModBusSendCommand(RH_RetransmitPixelData);

            pixelRepeat = true;
            originalPixelBuffer = currentPixelBuffer;
            currentPixelBuffer = &pixelBufferRepeat;
        }
    }
    else
    {

        repeatCRC = transmittedCRC;

        if(originalCRC == repeatCRC)
        {
            checkedPixelBuffer = &pixelBufferRepeat;
        }
        else if (calculatedCRC == repeatCRC)
        {
            checkedPixelBuffer = originalPixelBuffer;
        }
        else
        {
            /* To get here means that none of the CRC's match: have to default to either original or repeat Pixel data.
             * To keep data provenance known, it is decided to keep the original Pixel data.
             */
            checkedPixelBuffer = originalPixelBuffer;
        }

        /* Now ensure that the Bufferpointer is pointed to either the Tick or Tock arrays. */
        if (originalPixelBuffer == &pixelBufferTick)
        {
            currentPixelBuffer = &pixelBufferTock;
        }
        else
        {
            currentPixelBuffer = &pixelBufferTick;
        }

        pixelRepeat = false;
    }
}

void RhMStorePixel(void)
{
    if (checkedPixelBuffer)
    {
        // TODO DataStore_SetPixelNumber(pixel);
        //LogMemory_AppendPixel(checkedPixelBuffer->pArray, checkedPixelBuffer->arraySize);
    }
}
