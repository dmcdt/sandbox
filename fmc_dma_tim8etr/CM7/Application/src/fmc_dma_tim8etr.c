#include "main.h"
#include "tim.h"
#include "gpio.h"
#include "fmc.h"

#include <stdint.h>
#include <stdbool.h>

#include "RhDmaHandler.h"
#include "RhConductor.h"

#define COUNTOF(x)  (sizeof (x) / sizeof ((x)[0]))

#define Led1Port    GPIOB
#define Led1Pin     0
#define Led1Out     Led1Port->ODR
#define Led1PinMsk  (1UL << Led1Pin)

#define Led2Port    GPIOE
#define Led2Pin     1
#define Led2Out     Led2Port->ODR
#define Led2PinMsk  (1UL << Led2Pin)

#define Led3Port    GPIOB
#define Led3Pin     14
#define Led3Out     Led3Port->ODR
#define Led3PinMsk  (1UL << Led3Pin)

#define MAXIMUM_PIXEL_ARRAY_SIZE    (10000)


// Cube doesn't put this handy definition anywhere even though you need it in
// all calls to the HAL for the DMA. Crap. Putting it in the headers it
// generates would make way more sense, then the name would update if you 
// change settings in Cube.
DMA_HandleTypeDef hDmaTim3Trig;

volatile bool gDmaComplete;
volatile bool gDmaCycleComplete;
volatile bool gDmaSingleTransferComplete;

// Test output DMA sequence, for testing DMA is doing something
#pragma location = 0x30000100 // SRAM1 D2
uint32_t    dmaSequence[] = {Led1PinMsk, Led3PinMsk, Led1PinMsk|Led3PinMsk, 0};




#pragma location = "DMA"
static uint16_t pixelArrayTick[MAXIMUM_PIXEL_ARRAY_SIZE];

#pragma location = "DMA"
static uint32_t pixelChecksumTick;


void Sleep(uint32_t Delay);
void DmaTim3TrigTransferCompleteCallback(DMA_HandleTypeDef *_hdma);

void fmc_dma_tim8etr_init(void)
{
    /* Test: Does the main code fail because it is not running out of ITCM?
     * Result: Not specifically, this test code also works out of flash, but it's possible that with more happening in the real code that it makes a difference.
     */
#if VECT_TAB_ITCM
    /* If we are remapping execution into ITCM then we need to do it ASAP after
     * the system has started, and before any interrupts are used.
     */
    SCB->VTOR = D1_ITCMRAM_BASE; //| VECT_TAB_OFFSET;        /* Vector Table Relocation in ITCM RAM */
#endif

    HAL_EnableDBGSleepMode();
    HAL_EnableDBGStopMode();
    HAL_EnableDBGStandbyMode();
    
    HAL_EnableDomain2DBGSleepMode();
    HAL_EnableDomain2DBGStopMode();
    HAL_EnableDomain2DBGStandbyMode();

    HAL_EnableDomain3DBGStopMode();
    HAL_EnableDomain3DBGStandbyMode();
    
    __HAL_RCC_DMA1_FORCE_RESET();
    __HAL_RCC_DMA1_RELEASE_RESET();

    __HAL_RCC_TIM2_FORCE_RESET();
    __HAL_RCC_TIM2_RELEASE_RESET();

    __HAL_RCC_TIM4_FORCE_RESET();
    __HAL_RCC_TIM4_RELEASE_RESET();

    __HAL_RCC_TIM8_FORCE_RESET();
    __HAL_RCC_TIM8_RELEASE_RESET();
    
//    __HAL_DBGMCU_FREEZE_TIM3();
//    __HAL_DBGMCU_FREEZE_TIM3();
    
    __HAL_RCC_D2SRAM1_CLK_ENABLE();         // Enable access to SRAM1 from this Core
    __HAL_RCC_D2SRAM1_CLK_SLEEP_ENABLE();   // Enable access to SRAM1 from this Core while in Low Power

    __HAL_RCC_D2SRAM2_CLK_ENABLE();         // Enable access to SRAM2 from this Core
    __HAL_RCC_D2SRAM2_CLK_SLEEP_ENABLE();   // Enable access to SRAM2 from this Core while in Low Power
}


/* Function lifted from the HAL, to separate out the enabling of an OC and only
 * starting it when the timer is started.
 *
 * Only used for the test stimulus timers.
 */
HAL_StatusTypeDef My_TIM_OC_Start(TIM_HandleTypeDef *htim, uint32_t Channel)
{
  /* Check the parameters */
  assert_param(IS_TIM_CCX_INSTANCE(htim->Instance, Channel));

  /* Check the TIM channel state */
  if (TIM_CHANNEL_STATE_GET(htim, Channel) != HAL_TIM_CHANNEL_STATE_READY)
  {
    return HAL_ERROR;
  }

  /* Set the TIM channel state */
  TIM_CHANNEL_STATE_SET(htim, Channel, HAL_TIM_CHANNEL_STATE_BUSY);

  /* Enable the Output compare channel */
  TIM_CCxChannelCmd(htim->Instance, Channel, TIM_CCx_ENABLE);

  if (IS_TIM_BREAK_INSTANCE(htim->Instance) != RESET)
  {
    /* Enable the main output */
    __HAL_TIM_MOE_ENABLE(htim);
  }

  /* Return function status */
  return HAL_OK;
}


#define TEST_OWN_CONTROL    (0) /* Set this to 1 to enable own code control over the RhDma. Mutually exclusive with TEST_RHCONDUCTOR. */
#define TEST_RHCONDUCTOR    (1) /* Set this to 1 to enable RhConductor control over the RhDma. Mutually exclusive with TEST_OWN_CONTROL. */

void fmc_dma_tim8etr_run(void)
{
#if TEST_OWN_CONTROL == 1
    bool    dataTransferCrc;
    
    /* Just a little burst of FMC reads to prove that they are detectable irrespective of the DMA. */
    for (int i = 0; i < 10000; i++)
    {
        pixelArrayTick[i] = *(__IO uint16_t *)(0x88000000UL);
    }
    
    RhDmaHandlerSetupDataTriggers();
    RhTimerSetup();
    
    dataTransferCrc = false;
    RhDmaHandlerSetupDataTransfer((uint32_t)pixelArrayTick, MAXIMUM_PIXEL_ARRAY_SIZE);
    
    //RhStartAdcRdDma();

    HAL_Delay(1);
    
    Led3Out &= ~Led3PinMsk;
    ADC_RD_GPIO_Port->ODR &= ~ADC_RD_Pin;
    
    /* Start slave timer used to generate 10 MHz clock. */
    HAL_TIM_OC_Start(&htim4, TIM_CHANNEL_1);

    /* Prepare master timer channels used to generate ADC_RD_N and CRC signals. */
    My_TIM_OC_Start(&htim2, TIM_CHANNEL_1);
    My_TIM_OC_Start(&htim2, TIM_CHANNEL_2);
    My_TIM_OC_Start(&htim2, TIM_CHANNEL_3);
    My_TIM_OC_Start(&htim2, TIM_CHANNEL_4);
    
    /* Start the entire master timer now that all channels are ready. */
    HAL_TIM_Base_Start(&htim2);
    
    while (1)
    {
        /* Check to see whether the data transfer has completed. */
        bool flag = RhDmaHandlerGetDataTrasferFlagState();
        if (flag)
        {
            Led3Out ^= Led3PinMsk;
            
            dataTransferCrc = !dataTransferCrc;
            if (dataTransferCrc)
            {
                RhDmaHandlerSetupDataTransfer((uint32_t)&pixelChecksumTick, sizeof (pixelChecksumTick) / sizeof (uint16_t));
            }
            else
            {
                RhDmaHandlerSetupDataTransfer((uint32_t)pixelArrayTick, MAXIMUM_PIXEL_ARRAY_SIZE);
            }
            
//            uint32_t dmaEvents = RhDmaHandlerGetFlags();
//            
//            if (dmaEvents & 4)
//            {
                /* Data transfer complete with the CRC DMA having completed.
                 * Either a new cycle or a repeat must be performed, depending
                 * on the CRC comparison. Assume no repeats for now.
                 */
                //RhDmaHandlerNewCycle();
                //RhDmaHandlerSetupDataTransfer((uint32_t)pixelArrayTick, MAXIMUM_PIXEL_ARRAY_SIZE);
//            }
//            else if (dmaEvents & 1)
//            {
//                /* Data transfer complete with the ADC DMA having completed, expect CRC data next. */
//                RhDmaHandlerSetupDataTransfer((uint32_t)&pixelChecksumTick, sizeof (pixelChecksumTick) / sizeof (uint16_t));
//            }

            /* Can manually restart the ADC_RD_N CC and DMA with this call. */
            //RhStartAdcRdDma();
            
            /* Test: Set DMA for ADC_RD_N to circular and see if we do not need to call this.
             * Result: works, always triggers along with the ADC_RD_N signal falling edge.
             */
        }
        
//        __WFE();
//        __NOP();
    }
#endif
    
    
#if TEST_RHCONDUCTOR == 1
    /* Set up the rotating head modules. */
    RhCInit();
    
    /* Configure the test stimulus. */
    Led3Out &= ~Led3PinMsk;
    ADC_RD_GPIO_Port->ODR &= ~ADC_RD_Pin;
    
    /* Start slave timer used to generate 10 MHz clock. */
    HAL_TIM_OC_Start(&htim4, TIM_CHANNEL_1);

    /* Prepare master timer channels used to generate ADC_RD_N and CRC signals. */
    My_TIM_OC_Start(&htim2, TIM_CHANNEL_1);
    My_TIM_OC_Start(&htim2, TIM_CHANNEL_2);
    My_TIM_OC_Start(&htim2, TIM_CHANNEL_3);
    
    /* Start the entire master timer now that all channels are ready. */
    HAL_TIM_Base_Start(&htim2);

    RhTimerSetup();
    
    while (1)
    {
        RhCRun();
//        __WFE();
//        __NOP();
    }
#endif
}
