/* Copyright(c) 2021 Cereus Downhole Technology Ltd, All rights reserved. */

/* The way the DMA transfers works is such:
 * 1. All the streams are set up - each allocated its own specific trigger.
 * 2. Initially, the Timer8 Trigger stream is set to transfer data to one of the pixel data arrays, _tick or _tock.
 * 3. The Timer8 Channels are setup to produce a DMA timer burst, which will update the DMA and Interrupt enable
 *    register to enable the Timer8 Trigger DMA.
 * 4. When the Timer8 Channel Burst DMA completes, it will enable the Timer8 Trigger DMA which will conduct the actual
 *    data transfer.
 * 5. When the Data transfer completes, the RhDmaHandlerSetupDataTransfer() function is called with the new destination
 *    for either the CRC or repeat pixel data.
 */

#include "RhDmaHandler.h"

#define NAND_ATT_SPACE (0x08000000UL)
#define DATA_ADDRESS ((NAND_DEVICE | NAND_ATT_SPACE))

extern DMA_HandleTypeDef hdma_tim8_trig;    // Stream used for transferring data
extern DMA_HandleTypeDef hdma_tim8_ch1;     // Stream used to initiate pixel record
//extern DMA_HandleTypeDef hdma_tim8_ch2;     // Stream used to initiate repeat pixel record
extern DMA_HandleTypeDef hdma_tim8_ch3;     // Stream used to initiate CRC record

#pragma location = "DMA"
uint32_t dmaBurstDierValue = (TIM_DIER_TDE); /* On a trigger, this value gets copied to DIER by DMA to enable only the data transfer trigger. */
//uint32_t dmaBurstDierValue = (TIM_DIER_TDE | TIM_DIER_CC3DE | TIM_DIER_CC2DE | TIM_DIER_CC1DE);

static volatile bool dataTransferState = false;

void RhDmaHandlerSetupDataTransfer(uint32_t destAddress, uint32_t dataLength)
{
    if (hdma_tim8_trig.State == HAL_DMA_STATE_BUSY)
    {
        HAL_DMA_Abort_IT(&hdma_tim8_trig);
    }
    
    TIM8->DIER &= ~TIM_DIER_TDE;  /* Removes the Parallel clock trigger */

    HAL_DMA_Start_IT(&hdma_tim8_trig, DATA_ADDRESS, destAddress, dataLength);
}

void RhDmaHandlerSetupDataTriggers(void)
{
    HAL_DMA_Start_IT(&hdma_tim8_ch1, (uint32_t)&dmaBurstDierValue, (uint32_t)&TIM8->DMAR, 1);
    //HAL_DMA_Start_IT(&hdma_tim8_ch2, (uint32_t)&dmaBurstDierValue, (uint32_t)&TIM8->DMAR, 1);
    HAL_DMA_Start_IT(&hdma_tim8_ch3, (uint32_t)&dmaBurstDierValue, (uint32_t)&TIM8->DMAR, 1);

    HAL_DMA_RegisterCallback(&hdma_tim8_trig, HAL_DMA_XFER_CPLT_CB_ID, RhDmaHandlerDataTransferCompleteCallback);
}

void RhTimerSetup(void)
{
    //TIM8->DIER |= (TIM_DIER_CC3DE | TIM_DIER_CC2DE | TIM_DIER_CC1DE);
    RhDmaHandlerSetTriggerAdcRd(); /* Only trigger on ADC_RD_N initially. */
    TIM8->DCR = (TIM8->DCR & ~(TIM_DCR_DBA_Msk | TIM_DCR_DBL_Msk)) | (TIM_DMABURSTLENGTH_1TRANSFER | TIM_DMABASE_DIER);
    TIM8->CCER |= (TIM_CCER_CC3E | TIM_CCER_CC2E | TIM_CCER_CC1E);
}

void RhDmaHandlerSetTriggerAdcRd(void)
{
    TIM8->DIER |= (TIM_DIER_CC1DE);
}

void RhDmaHandlerSetTriggerCrc(void)
{
    TIM8->DIER |= (TIM_DIER_CC3DE);
}

void RhDmaHandlerDataTransferCompleteCallback(DMA_HandleTypeDef *_hdma)
{
    dataTransferState = true;
}

bool RhDmaHandlerGetDataTrasferFlagState(void)
{
    bool returnValue = dataTransferState;

    if (returnValue)
    {
        dataTransferState = false;
    }

    return returnValue;
}
