/* Copyright(c) 2021 Cereus Downhole Technology Ltd, All rights reserved. */

#ifndef RH_DMA_HANDLER_H
#define RH_DMA_HANDLER_H

#include <stdint.h>
#include <stdbool.h>

#include "main.h"

void RhDmaHandlerSetupDataTransfer(uint32_t srcAddress, uint32_t dataLength);
void RhDmaHandlerSetupDataTriggers(void);
void RhTimerSetup(void);
void RhDmaHandlerSetTriggerAdcRd(void);
void RhDmaHandlerSetTriggerCrc(void);
void RhDmaHandlerDataTransferCompleteCallback(DMA_HandleTypeDef *_hdma);
bool RhDmaHandlerGetDataTrasferFlagState(void);

#endif // RH_DMA_HANDLER_H