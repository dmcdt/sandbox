/* Copyright(c) 2021 Cereus Downhole Technology Ltd, All rights reserved. */

#ifndef RH_MODEL_H
#define RH_MODEL_H

#include "CommonDefinitions.h"

typedef enum
{
    RH_UNINIT,
    RH_READY,
    RH_PIXEL_COLLECTION,
    RH_CRC_PROCESSING,
    RH_REPEAT_PIXEL
} RhModelState_t;

void RhMInit(void);
RhModelState_t RhMGetState(void);
void RhMSetState(RhModelState_t newState);
Array16_t *RhMGetCurrentPixelBuffer(void);
uint32_t *RhMGetCurrentCrcBuffer(void);
void RhMCompareCrc(void);
void RhMCalculateCrc(void);
void RhMStorePixel(void);

#endif // RH_MODEL