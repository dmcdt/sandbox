/* Copyright 2020 Cereus Downhole Technology Ltd, All rights reserved. */

#ifndef COMMONDEFINITIONS_H
#define COMMONDEFINITIONS_H

#include <stdint.h>

#define COUNTOF(x) (sizeof (x) / sizeof ((x)[0]))
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

#define sizeof_member(type, member) sizeof(((type *)0)->member)

#define UPTIME_TICK_TO_MINUTES_FACTOR  (600000)
#define UPTIME_TICK_TO_SECONDS_FACTOR  ( 10000)
#define UPTIME_TICK_TO_MILLISECONDS_FACTOR (10)

#define SECONDS_PER_MINUTE (60)

typedef struct
{
    uint16_t *pArray;
    uint16_t arraySize;
} Array16_t;

#endif
