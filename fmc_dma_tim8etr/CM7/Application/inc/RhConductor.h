/* Copyright(c) 2021 Cereus Downhole Technology Ltd, All rights reserved. */

#ifndef RH_CONDUCTOR_H
#define RH_CONDUCTOR_H

void RhCInit(void);
void RhCRun(void);

#endif // RH_CONDUCTOR_H