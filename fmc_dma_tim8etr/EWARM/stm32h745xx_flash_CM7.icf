/*###ICF### Section handled by ICF editor, don't touch! ****/
/*-Editor annotation file-*/
/* IcfEditorFile="$TOOLKIT_DIR$\config\ide\IcfEditor\cortex_v1_0.xml" */
/*-Specials-*/
define symbol __ICFEDIT_intvec_start__ = 0x08000000;
/*-Memory Regions-*/
define symbol __ICFEDIT_region_ROM_start__     = 0x08000000;
define symbol __ICFEDIT_region_ROM_end__       = 0x080FFFFF;
define symbol __ICFEDIT_region_RAM_start__     = 0x20000000;
define symbol __ICFEDIT_region_RAM_end__       = 0x2001FFFF;
define symbol __ICFEDIT_region_ITCMRAM_start__ = 0x00000000;
define symbol __ICFEDIT_region_ITCMRAM_end__   = 0x0000FFFF;
/*-Sizes-*/
define symbol __ICFEDIT_size_cstack__ = 0x400;
define symbol __ICFEDIT_size_heap__ = 0x200;
/**** End of ICF editor section. ###ICF###*/

/* Extra definitions for the different SRAM areas */
define symbol AXI_SRAM_CM4_start	= 0x24000000;
define symbol AXI_SRAM_CM4_end		= 0x2403FFFF;
define symbol AXI_SRAM_CM7_start	= 0x24040000;
define symbol AXI_SRAM_CM7_end		= 0x2407FFFF;
define symbol SRAM1_start		= 0x30000000;
define symbol SRAM1_end			= 0x3001FFFF;
define symbol SRAM2_start		= 0x30020000;
define symbol SRAM2_end			= 0x3003FFFF;
define symbol SRAM3_start		= 0x30040000;
define symbol SRAM3_end			= 0x30047FFF;
define symbol SRAM4_start		= 0x38000000;
define symbol SRAM4_end			= 0x3800FFFF;
define symbol BACKUP_SRAM_start	        = 0x38800000;
define symbol BACKUP_SRAM_end	        = 0x38800FFF;

/* Extra definition to specify the area reserverd for the EEPROM emulations */
define symbol EMULATED_EEPROM_start = 0x081C0000;
define symbol EMULATED_EEPROM_end   = 0x081FFFFF;


define memory mem with size = 4G;
define region ROM_region      = mem:[from __ICFEDIT_region_ROM_start__   to __ICFEDIT_region_ROM_end__];
define region RAM_region      = mem:[from __ICFEDIT_region_RAM_start__   to __ICFEDIT_region_RAM_end__];
define region ITCMRAM_region  = mem:[from __ICFEDIT_region_ITCMRAM_start__ to __ICFEDIT_region_ITCMRAM_end__];
define region AXI_SRAM_CM4_region       = mem:[from AXI_SRAM_CM4_start to AXI_SRAM_CM4_end];
define region AXI_SRAM_CM7_region       = mem:[from AXI_SRAM_CM7_start to AXI_SRAM_CM7_end];
define region SRAM1_region              = mem:[from SRAM1_start to SRAM1_end];
define region SRAM2_region              = mem:[from SRAM2_start to SRAM2_end];
define region SRAM3_region              = mem:[from SRAM3_start to SRAM3_end];
define region SRAM4_region              = mem:[from SRAM4_start to SRAM4_end];
define region BACKUP_SRAM_region        = mem:[from BACKUP_SRAM_start to BACKUP_SRAM_end];
define region EMULATED_EEPROM           = mem:[from EMULATED_EEPROM_start to EMULATED_EEPROM_end];

define block CSTACK    with alignment = 8, size = __ICFEDIT_size_cstack__   { };
define block HEAP      with alignment = 8, size = __ICFEDIT_size_heap__     { };

initialize by copy { readwrite };
do not initialize  { section .noinit };

place at address mem:__ICFEDIT_intvec_start__ { readonly section .intvec };

place in ROM_region   { readonly };
place in RAM_region   { readwrite,
                        block CSTACK, block HEAP };
place in AXI_SRAM_CM4_region { section AXI_SRAM_CM4 };
place in AXI_SRAM_CM7_region { section AXI_SRAM_CM7 };
place in SRAM2_region { section DMA };
place in SRAM3_region { section SHARED };
