#ifndef BMI160TRANSPORT_H
#define BMI160TRANSPORT_H

/*
 *  Declarations for performing the communications to the BMI160.
 */

#include <stdint.h>

void Bmi160Transport_InitialiseSpi(void);
void Bmi160Transport_InitialiseI2C(void);
void Bmi160Transport_ActivateCommunicationsInterface(void);
void Bmi160Transport_ReadRegisters(uint8_t registerAddress, unsigned int length, uint8_t *out);
void Bmi160Transport_WriteRegisters(uint8_t registerAddress, unsigned int length, uint8_t *data);
void Bmi160_Read(uint8_t *data, unsigned int length);
void Bmi160_Write(uint8_t *data, unsigned int length);


#endif
