#ifndef PGA305_H
#define PGA305_H


/*
 *  Declarations for functions that communicate to the PGA305 device.
 */


#include <stdint.h>



/*  
 *  I2C addresses are a combination of the base address plus the page, so for
 *  example to address a register in the data page it would be 0x40 + 2.
 */
#define PGA305_I2C_ADDRESS  (0x40)
#define PGA305_DATA_PAGE    (0)
#define PGA305_CONTROL_PAGE (2)
#define PGA305_EEPROM_PAGE  (5)


/*
 *  Helper macro to transform the base address and page into the value required
 *  to be sent to the HAL / STM transmit register. Needs shifted left by 1
 *  as the R/W bit overwrites the LSb.
 */
#define PGA305_ADDRESS_TRANSMIT(page)  ((PGA305_I2C_ADDRESS + (page)) << 1)


#define PGA305_DATAPAGE_ADDRESS     PGA305_ADDRESS_TRANSMIT(PGA305_DATA_PAGE)
#define PGA305_CONTROLPAGE_ADDRESS  PGA305_ADDRESS_TRANSMIT(PGA305_CONTROL_PAGE)
#define PGA305_EEPROMPAGE_ADDRESS   PGA305_ADDRESS_TRANSMIT(PGA305_EEPROM_PAGE)


/* The registers available in the Pga305_DATA_PAGE */
enum Pga305_Data_Registers
{
    Pga305_DataReg_DataLSB              = 0x04,     /* LSB of requested data */
    Pga305_DataReg_DataMSB              = 0x05,     /* MSB of requested data */
    Pga305_DataReg_Command              = 0x09,     /* Command to execute */
    Pga305_DataReg_CompensationControl  = 0x0C,     /* Controls whether compensation and calculations are running */
};


// These are the commands that can be sent to the Pga305, by writing
// them to the register Pga305_DataReg_Command.
enum Pga305_Data_Commands
{
    Pga305_DataCmd_ReadPAdc         = 0x00,     // Read one 24-bit sample from the Pressure ADC channel
    Pga305_DataCmd_ReadTAdc         = 0x02,     // Read one 24-bit sample from the Temperature ADC channel
    Pga305_DataCmd_ReadCompensated  = 0x04,     // Read compensated output value
    Pga305_DataCmd_ReadDiagnostics  = 0x06,
    Pga305_DataCmd_ReadTrailWord    = 0x70,     // Read the lower 16 bits of any previously executed command
};


/* These registers reside in the Control and Status page, Pga305_CONTROL_PAGE */
enum Pga305_Control_Registers
{
    Pga305_ControlReg_PADC_DATA1    = 0x20,
    Pga305_ControlReg_PADC_DATA2    = 0x21,
    Pga305_ControlReg_PADC_DATA3    = 0x22,
    
    Pga305_ControlReg_TADC_DATA1    = 0x24,
    Pga305_ControlReg_TADC_DATA2    = 0x25,
    Pga305_ControlReg_TADC_DATA3    = 0x26,
    
    Pga305_ControlReg_DAC_REG0_1    = 0x30,
    Pga305_ControlReg_DAC_REG0_2    = 0x31,
    
    Pga305_ControlReg_DAC_CTRL_STATUS   = 0x38,
    Pga305_ControlReg_DAC_CONFIG    = 0x39,
    
    Pga305_ControlReg_OP_STAGE_CTRL = 0x3B,
    
    Pga305_ControlReg_BRDG_CTRL     = 0x46,
    Pga305_ControlReg_P_GAIN_SELECT = 0x47,
    Pga305_ControlReg_T_GAIN_SELECT = 0x48,
    
    Pga305_ControlReg_TEMP_CTRL     = 0x4C
};


/*  
 *  These are the registers that are available in the Pga305_EEPROM_PAGE.
 *  Note that some of these are different from the contents of EEPROM itself,
 *  these are registers you can use to operate on EEPROM.
 */
enum Pga305_Eeprom_Registers
{
    /*  
     *  You can read the EEPROM contents by reading from the following
     *  registers in the Pga305_EEPROM_PAGE, all registers from 0x00 to 0x7F
     *  are valid and represent the EEPROM array, but are not defined here.
     *  See enum Pga305EepromAddresses for the addresses.
     */
    Pga305_EepromReg_ReadEepromContentsBegin = 0x00,
    Pga305_EepromReg_ReadEepromContentsEnd = 0x7F,
    
    /*
     *  The cache registers are used to write to the 8 byte cache memory
     *  prior to writing into the EEPROM array.
     */
    Pga305_EepromReg_Cache0     = 0x80,
    Pga305_EepromReg_Cache1     = 0x81,
    Pga305_EepromReg_Cache2     = 0x82,
    Pga305_EepromReg_Cache3     = 0x83,
    Pga305_EepromReg_Cache4     = 0x84,
    Pga305_EepromReg_Cache5     = 0x85,
    Pga305_EepromReg_Cache6     = 0x86,
    Pga305_EepromReg_Cache7     = 0x87,
    Pga305_EepromReg_CacheBegin = Pga305_EepromReg_Cache0,
    Pga305_EepromReg_CacheEnd   = Pga305_EepromReg_Cache7,
    
    Pga305_EepromReg_PageAddress    = 0x88,
    Pga305_EepromReg_Control        = 0x89,
    Pga305_EepromReg_CalculateCrc   = 0x8A,
    Pga305_EepromReg_Status         = 0x8B,
    Pga305_EepromReg_CrcStatus      = 0x8C,
    Pga305_EepromReg_CrcValue       = 0x8D,
};


/*
 *  These are the EEPROM addresses of various bytes. Note that the address is
 *  just the lower 8 bits from the datasheet, so that it can be used in I2C
 *  transactions.
 */
enum Pga305EepromAddresses
{
    /*
     *  The coefficients stored in EEPROM are used to perform the digital
     *  compensation of pressure with respect to temperature. The coefficients
     *  form a 3rd order polynomial in pressure and temperature:
     *
     *  DATA_OUT = (H0 + H1 * T + H2 * T^2 + H3 * T^3) +
     *             (G0 + G1 * T + G2 * T^2 + G3 * T^3) * P +
     *             (N0 + N1 * T + N2 * T^2 + N3 * T^3) * P^2 +
     *             (M0 + M1 * T + M2 * T^2 + M3 * T^3) * P^3
     *
     * Where the coefficients in the above equation are the values from
     * EEPROM / 2^22.
     */
    Pga305_EEAddr_H0_Low,           /* Bits 7:0 of 24 bit coefficient H0 */
    Pga305_EEAddr_H0_Middle,        /* Bits 15:8 of 24 bit coefficient H0 */
    Pga305_EEAddr_H0_High,          /* Bits 23:16 of 24 bit coefficient H0 */

    Pga305_EEAddr_H1_Low,           /* Bits 7:0 of 24 bit coefficient H1 */
    Pga305_EEAddr_H1_Middle,        /* Bits 15:8 of 24 bit coefficient H1 */
    Pga305_EEAddr_H1_High,          /* Bits 23:16 of 24 bit coefficient H1 */

    Pga305_EEAddr_H2_Low,           /* Bits 7:0 of 24 bit coefficient H2 */
    Pga305_EEAddr_H2_Middle,        /* Bits 15:8 of 24 bit coefficient H2 */
    Pga305_EEAddr_H2_High,          /* Bits 23:16 of 24 bit coefficient H2 */

    Pga305_EEAddr_H3_Low,           /* Bits 7:0 of 24 bit coefficient H3 */
    Pga305_EEAddr_H3_Middle,        /* Bits 15:8 of 24 bit coefficient H3 */
    Pga305_EEAddr_H3_High,          /* Bits 23:16 of 24 bit coefficient H3 */

    Pga305_EEAddr_G0_Low,           /* Bits 7:0 of 24 bit coefficient G0 */
    Pga305_EEAddr_G0_Middle,        /* Bits 15:8 of 24 bit coefficient G0 */
    Pga305_EEAddr_G0_High,          /* Bits 23:16 of 24 bit coefficient G0 */

    Pga305_EEAddr_G1_Low,           /* Bits 7:0 of 24 bit coefficient G1 */
    Pga305_EEAddr_G1_Middle,        /* Bits 15:8 of 24 bit coefficient G1 */
    Pga305_EEAddr_G1_High,          /* Bits 23:16 of 24 bit coefficient G1 */

    Pga305_EEAddr_G2_Low,           /* Bits 7:0 of 24 bit coefficient G2 */
    Pga305_EEAddr_G2_Middle,        /* Bits 15:8 of 24 bit coefficient G2 */
    Pga305_EEAddr_G2_High,          /* Bits 23:16 of 24 bit coefficient G2 */

    Pga305_EEAddr_G3_Low,           /* Bits 7:0 of 24 bit coefficient G3 */
    Pga305_EEAddr_G3_Middle,        /* Bits 15:8 of 24 bit coefficient G3 */
    Pga305_EEAddr_G3_High,          /* Bits 23:16 of 24 bit coefficient G3 */

    Pga305_EEAddr_N0_Low,           /* Bits 7:0 of 24 bit coefficient N0 */
    Pga305_EEAddr_N0_Middle,        /* Bits 15:8 of 24 bit coefficient N0 */
    Pga305_EEAddr_N0_High,          /* Bits 23:16 of 24 bit coefficient N0 */

    Pga305_EEAddr_N1_Low,           /* Bits 7:0 of 24 bit coefficient N1 */
    Pga305_EEAddr_N1_Middle,        /* Bits 15:8 of 24 bit coefficient N1 */
    Pga305_EEAddr_N1_High,          /* Bits 23:16 of 24 bit coefficient N1 */

    Pga305_EEAddr_N2_Low,           /* Bits 7:0 of 24 bit coefficient N2 */
    Pga305_EEAddr_N2_Middle,        /* Bits 15:8 of 24 bit coefficient N2 */
    Pga305_EEAddr_N2_High,          /* Bits 23:16 of 24 bit coefficient N2 */

    Pga305_EEAddr_N3_Low,           /* Bits 7:0 of 24 bit coefficient N3 */
    Pga305_EEAddr_N3_Middle,        /* Bits 15:8 of 24 bit coefficient N3 */
    Pga305_EEAddr_N3_High,          /* Bits 23:16 of 24 bit coefficient N3 */

    Pga305_EEAddr_M0_Low,           /* Bits 7:0 of 24 bit coefficient M0 */
    Pga305_EEAddr_M0_Middle,        /* Bits 15:8 of 24 bit coefficient M0 */
    Pga305_EEAddr_M0_High,          /* Bits 23:16 of 24 bit coefficient M0 */

    Pga305_EEAddr_M1_Low,           /* Bits 7:0 of 24 bit coefficient M1 */
    Pga305_EEAddr_M1_Middle,        /* Bits 15:8 of 24 bit coefficient M1 */
    Pga305_EEAddr_M1_High,          /* Bits 23:16 of 24 bit coefficient M1 */

    Pga305_EEAddr_M2_Low,           /* Bits 7:0 of 24 bit coefficient M2 */
    Pga305_EEAddr_M2_Middle,        /* Bits 15:8 of 24 bit coefficient M2 */
    Pga305_EEAddr_M2_High,          /* Bits 23:16 of 24 bit coefficient M2 */

    Pga305_EEAddr_M3_Low,           /* Bits 7:0 of 24 bit coefficient M3 */
    Pga305_EEAddr_M3_Middle,        /* Bits 15:8 of 24 bit coefficient M3 */
    Pga305_EEAddr_M3_High,          /* Bits 23:16 of 24 bit coefficient M3 */
    
    
    Pga305_EEAddr_DIG_IF_CTRL,
    
    Pga305_EEAddr_DAC_CTRL_STATUS,
    
    Pga305_EEAddr_DAC_CONFIG,
    
    Pga305_EEAddr_OP_STAGE_CTRL,
    
    Pga305_EEAddr_BRDG_CTRL,
    
    Pga305_EEAddr_P_GAIN_SELECT,
    
    Pga305_EEAddr_T_GAIN_SELECT,
    
    Pga305_EEAddr_TEMP_CTRL,
    
    Pga305_EEAddr_Reserved_38,
    Pga305_EEAddr_Reserved_39,
    
    Pga305_EEAddr_TEMP_SE,
    
    Pga305_EEAddr_Reserved_3B,
    
    Pga305_EEAddr_NORMAL_LOW_LSB,
    Pga305_EEAddr_NORMAL_LOW_MSB,
    
    Pga305_EEAddr_NORMAL_HIGH_LSB,
    Pga305_EEAddr_NORMAL_HIGH_MSB,
    
    Pga305_EEAddr_LOW_CLAMP_LSB,
    Pga305_EEAddr_LOW_CLAMP_MSB,
    
    Pga305_EEAddr_HIGH_CLAMP_LSB,
    Pga305_EEAddr_HIGH_CLAMP_MSB,
    
    Pga305_EEAddr_PADC_GAIN_LSB,
    Pga305_EEAddr_PADC_GAIN_MID,
    Pga305_EEAddr_PADC_GAIN_MSB,
    
    Pga305_EEAddr_PADC_OFFSET_LSB,
    Pga305_EEAddr_PADC_OFFSET_MID,
    Pga305_EEAddr_PADC_OFFSET_MSB,
    
    Pga305_EEAddr_A0_LSB,
    Pga305_EEAddr_A0_MSB,
    Pga305_EEAddr_A1_LSB,
    Pga305_EEAddr_A1_MSB,
    Pga305_EEAddr_A2_LSB,
    Pga305_EEAddr_A2_MSB,
    Pga305_EEAddr_B0_LSB,
    Pga305_EEAddr_B0_MSB,
    Pga305_EEAddr_B1_LSB,
    Pga305_EEAddr_B1_MSB,
    Pga305_EEAddr_B2_LSB,
    Pga305_EEAddr_B2_MSB,
    
    Pga305_EEAddr_DIAG_ENABLE,
    
    Pga305_EEAddr_EEPROM_LOCK,
    
    Pga305_EEAddr_AFEDIAG_CFG,
    Pga305_EEAddr_AFEDIAG_MASK,

    Pga305_EEAddr_Reserved_5A,
    Pga305_EEAddr_Reserved_5B,
    
    Pga305_EEAddr_FAULT_LSB,
    Pga305_EEAddr_FAULT_MSB,
    
    Pga305_EEAddr_TADC_GAIN_LSB,
    Pga305_EEAddr_TADC_GAIN_MID,
    Pga305_EEAddr_TADC_GAIN_MSB,
    Pga305_EEAddr_TADC_OFFSET_LSB,
    Pga305_EEAddr_TADC_OFFSET_MID,
    Pga305_EEAddr_TADC_OFFSET_MSB,
    
    Pga305_EEAddr_SERIAL_NUMBER0,
    Pga305_EEAddr_SERIAL_NUMBER1,
    Pga305_EEAddr_SERIAL_NUMBER2,
    Pga305_EEAddr_SERIAL_NUMBER3,
    
    Pga305_EEAddr_ADC_24BIT_ENABLED,
    Pga305_EEAddr_OFFSET_ENABLE,

    Pga305_EEAddr_Reserved_70,
    Pga305_EEAddr_Reserved_71,
    Pga305_EEAddr_Reserved_72,
    Pga305_EEAddr_Reserved_73,
    Pga305_EEAddr_Reserved_74,
    Pga305_EEAddr_Reserved_75,
    Pga305_EEAddr_Reserved_76,
    Pga305_EEAddr_Reserved_77,
    Pga305_EEAddr_Reserved_78,
    Pga305_EEAddr_Reserved_79,
    Pga305_EEAddr_Reserved_7A,
    Pga305_EEAddr_Reserved_7B,
    Pga305_EEAddr_Reserved_7C,
    Pga305_EEAddr_Reserved_7D,
    Pga305_EEAddr_Reserved_7E,
    
    Pga305_EEAddr_EEPROM_CRC_VALUE,
};


#define PGA305_EEPROM_SIZE          (0x80U)
#define PGA305_EEPROM_CACHE_SIZE    (8)


#define PGA305_DACCTRLSTATUS_ENABLE_Pos (0U)
#define PGA305_DACCTRLSTATUS_ENABLE_Msk (0x1UL << PGA305_DACCTRLSTATUS_ENABLE_Pos)
#define PGA305_DACCTRLSTATUS_ENABLE PGA305_DACCTRLSTATUS_ENABLE_Msk
#define PGA305_DACCONFIG_RATIOMETRIC_Pos (0U)
#define PGA305_DACCONFIG_RATIOMETRIC_Msk (0x1UL << PGA305_DACCONFIG_RATIOMETRIC_Pos)
#define PGA305_DACCONFIG_RATIOMETRIC PGA305_DACCONFIG_RATIOMETRIC_Msk
#define PGA305_OPSTAGECTRL_DACGAIN0_Pos (0U)
#define PGA305_OPSTAGECTRL_DACGAIN0_Msk (0x1UL << PGA305_OPSTAGECTRL_DACGAIN0_Pos)
#define PGA305_OPSTAGECTRL_DACGAIN0 PGA305_OPSTAGECTRL_DACGAIN0_Msk
#define PGA305_OPSTAGECTRL_DACGAIN1_Pos (1U)
#define PGA305_OPSTAGECTRL_DACGAIN1_Msk (0x1UL << PGA305_OPSTAGECTRL_DACGAIN1_Pos)
#define PGA305_OPSTAGECTRL_DACGAIN1 PGA305_OPSTAGECTRL_DACGAIN1_Msk
#define PGA305_OPSTAGECTRL_DACGAIN2_Pos (2U)
#define PGA305_OPSTAGECTRL_DACGAIN2_Msk (0x1UL << PGA305_OPSTAGECTRL_DACGAIN2_Pos)
#define PGA305_OPSTAGECTRL_DACGAIN2 PGA305_OPSTAGECTRL_DACGAIN2_Msk
#define PGA305_OPSTAGECTRL_4TO20MAEN_Pos (3U)
#define PGA305_OPSTAGECTRL_4TO20MAEN_Msk (0x1UL << PGA305_OPSTAGECTRL_4TO20MAEN_Pos)
#define PGA305_OPSTAGECTRL_4TO20MAEN PGA305_OPSTAGECTRL_4TO20MAEN_Msk
#define PGA305_OPSTAGECTRL_DACCAPEN_Pos (4U)
#define PGA305_OPSTAGECTRL_DACCAPEN_Msk (0x1UL << PGA305_OPSTAGECTRL_DACCAPEN_Pos)
#define PGA305_OPSTAGECTRL_DACCAPEN PGA305_OPSTAGECTRL_DACCAPEN_Msk
#define PGA305_BRDGCTRL_ENABLE_Pos (0U)
#define PGA305_BRDGCTRL_ENABLE_Msk (0x1UL << PGA305_BRDGCTRL_ENABLE_Pos)
#define PGA305_BRDGCTRL_ENABLE PGA305_BRDGCTRL_ENABLE_Msk
#define PGA305_BRDGCTRL_VBRDGCTRL0_Pos (1U)
#define PGA305_BRDGCTRL_VBRDGCTRL0_Msk (0x1UL << PGA305_BRDGCTRL_VBRDGCTRL0_Pos)
#define PGA305_BRDGCTRL_VBRDGCTRL0 PGA305_BRDGCTRL_VBRDGCTRL0_Msk
#define PGA305_BRDGCTRL_VBRDGCTRL1_Pos (2U)
#define PGA305_BRDGCTRL_VBRDGCTRL1_Msk (0x1UL << PGA305_BRDGCTRL_VBRDGCTRL1_Pos)
#define PGA305_BRDGCTRL_VBRDGCTRL1 PGA305_BRDGCTRL_VBRDGCTRL1_Msk


/***************** Bit definitions for T_GAIN_SELECT register ******************/
#define PGA305_PGAINSELECT_PGAIN0_Pos   (0U)
#define PGA305_PGAINSELECT_PGAIN0_Msk   (0x1UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_PGAIN0       PGA305_PGAINSELECT_PGAIN0_Msk
#define PGA305_PGAINSELECT_PGAIN1_Pos   (1U)
#define PGA305_PGAINSELECT_PGAIN1_Msk   (0x1UL << PGA305_PGAINSELECT_PGAIN1_Pos)
#define PGA305_PGAINSELECT_PGAIN1       PGA305_PGAINSELECT_PGAIN1_Msk
#define PGA305_PGAINSELECT_PGAIN2_Pos   (2U)
#define PGA305_PGAINSELECT_PGAIN2_Msk   (0x1UL << PGA305_PGAINSELECT_PGAIN2_Pos)
#define PGA305_PGAINSELECT_PGAIN2       PGA305_PGAINSELECT_PGAIN2_Msk
#define PGA305_PGAINSELECT_PGAIN3_Pos   (3U)
#define PGA305_PGAINSELECT_PGAIN3_Msk   (0x1UL << PGA305_PGAINSELECT_PGAIN3_Pos)
#define PGA305_PGAINSELECT_PGAIN3       PGA305_PGAINSELECT_PGAIN3_Msk
#define PGA305_PGAINSELECT_PGAIN4_Pos   (4U)
#define PGA305_PGAINSELECT_PGAIN4_Msk   (0x1UL << PGA305_PGAINSELECT_PGAIN4_Pos)
#define PGA305_PGAINSELECT_PGAIN4       PGA305_PGAINSELECT_PGAIN4_Msk
#define PGA305_PGAINSELECT_PINV_Pos     (7U)
#define PGA305_PGAINSELECT_PINV_Msk     (0x1UL << PGA305_PGAINSELECT_PINV_Pos)
#define PGA305_PGAINSELECT_PINV         PGA305_PGAINSELECT_PINV_Msk

/**************** Value definitions for P_GAIN_SELECT register *****************/
#define PGA305_PGAINSELECT_5V           (0UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_5V48         (1UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_5V97         (2UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_6V56         (3UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_7V02         (4UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_8V           (5UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_9V09         (6UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_10V          (7UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_10V53        (8UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_11V11        (9UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_12V5         (10UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_13V33        (11UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_14V29        (12UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_16V          (13UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_17V39        (14UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_18V18        (15UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_19V05        (16UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_20V          (17UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_22V22        (18UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_25V          (19UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_30V77        (20UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_36V36        (21UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_40V          (22UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_44V44        (23UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_50V          (24UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_57V14        (25UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_66V67        (26UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_80V          (27UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_100V         (28UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_133V33       (29UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_200V         (30UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_400V         (31UL << PGA305_PGAINSELECT_PGAIN0_Pos)
#define PGA305_PGAINSELECT_NOTINVERTED  (0)
#define PGA305_PGAINSELECT_INVERTED     PGA305_PGAINSELECT_PINV


/***************** Bit definitions for T_GAIN_SELECT register ******************/
#define PGA305_TGAINSELECT_TGAIN0_Pos   (0U)
#define PGA305_TGAINSELECT_TGAIN0_Msk   (0x1UL << PGA305_TGAINSELECT_TGAIN0_Pos)
#define PGA305_TGAINSELECT_TGAIN0       PGA305_TGAINSELECT_TGAIN0_Msk
#define PGA305_TGAINSELECT_TGAIN1_Pos   (1U)
#define PGA305_TGAINSELECT_TGAIN1_Msk   (0x1UL << PGA305_TGAINSELECT_TGAIN1_Pos)
#define PGA305_TGAINSELECT_TGAIN1       PGA305_TGAINSELECT_TGAIN1_Msk
#define PGA305_TGAINSELECT_TINV_Pos     (7U)
#define PGA305_TGAINSELECT_TINV_Msk     (0x1UL << PGA305_TGAINSELECT_TINV_Pos)
#define PGA305_TGAINSELECT_TINV         PGA305_TGAINSELECT_TINV_Msk

/**************** Value definitions for T_GAIN_SELECT register *****************/
#define PGA305_TGAINSELECT_1V33         (0)
#define PGA305_TGAINSELECT_2V           (PGA305_TGAINSELECT_TGAIN0)
#define PGA305_TGAINSELECT_5V           (PGA305_TGAINSELECT_TGAIN1)
#define PGA305_TGAINSELECT_20V          (PGA305_TGAINSELECT_TGAIN1|PGA305_TGAINSELECT_TGAIN0)
#define PGA305_TGAINSELECT_NOTINVERTED  (0)
#define PGA305_TGAINSELECT_INVERTED     PGA305_TGAINSELECT_TINV


/******************* Bit definitions for TEMP_CTRL register *******************/
#define PGA305_TEMPCTRL_TEMPMUX0_Pos    (0U)
#define PGA305_TEMPCTRL_TEMPMUX0_Msk    (0x1UL << PGA305_TEMPCTRL_TEMPMUX0_Pos)
#define PGA305_TEMPCTRL_TEMPMUX0        PGA305_TEMPCTRL_TEMPMUX0_Msk
#define PGA305_TEMPCTRL_TEMPMUX1_Pos    (1U)
#define PGA305_TEMPCTRL_TEMPMUX1_Msk    (0x1UL << PGA305_TEMPCTRL_TEMPMUX1_Pos)
#define PGA305_TEMPCTRL_TEMPMUX1        PGA305_TEMPCTRL_TEMPMUX1_Msk
#define PGA305_TEMPCTRL_TEMPMUX2_Pos    (2U)
#define PGA305_TEMPCTRL_TEMPMUX2_Msk    (0x1UL << PGA305_TEMPCTRL_TEMPMUX2_Pos)
#define PGA305_TEMPCTRL_TEMPMUX2        PGA305_TEMPCTRL_TEMPMUX2_Msk
#define PGA305_TEMPCTRL_TEMPMUX3_Pos    (3U)
#define PGA305_TEMPCTRL_TEMPMUX3_Msk    (0x1UL << PGA305_TEMPCTRL_TEMPMUX3_Pos)
#define PGA305_TEMPCTRL_TEMPMUX3        PGA305_TEMPCTRL_TEMPMUX3_Msk
#define PGA305_TEMPCTRL_ITEMP0_Pos      (4U)
#define PGA305_TEMPCTRL_ITEMP0_Msk      (0x1UL << PGA305_TEMPCTRL_ITEMP0_Pos)
#define PGA305_TEMPCTRL_ITEMP0          PGA305_TEMPCTRL_ITEMP0_Msk
#define PGA305_TEMPCTRL_ITEMP1_Pos      (5U)
#define PGA305_TEMPCTRL_ITEMP1_Msk      (0x1UL << PGA305_TEMPCTRL_ITEMP1_Pos)
#define PGA305_TEMPCTRL_ITEMP1          PGA305_TEMPCTRL_ITEMP1_Msk
#define PGA305_TEMPCTRL_ITEMP2_Pos      (6U)
#define PGA305_TEMPCTRL_ITEMP2_Msk      (0x1UL << PGA305_TEMPCTRL_ITEMP2_Pos)
#define PGA305_TEMPCTRL_ITEMP2          PGA305_TEMPCTRL_ITEMP2_Msk

/****************** Value definitions for TEMP_CTRL register ******************/
#define PGA305_TEMPCTRL_EXTERNAL        (0)
#define PGA305_TEMPCTRL_INTERNAL        (PGA305_TEMPCTRL_TEMPMUX1 | PGA305_TEMPCTRL_TEMPMUX0)
#define PGA305_TEMPCTRL_CURRENT25UA     (0U << PGA305_TEMPCTRL_ITEMP0_Pos)
#define PGA305_TEMPCTRL_CURRENT50UA     (1U << PGA305_TEMPCTRL_ITEMP0_Pos)
#define PGA305_TEMPCTRL_CURRENT100UA    (2U << PGA305_TEMPCTRL_ITEMP0_Pos)
#define PGA305_TEMPCTRL_CURRENT500UA    (3U << PGA305_TEMPCTRL_ITEMP0_Pos)
#define PGA305_TEMPCTRL_CURRENTOFF      (4U << PGA305_TEMPCTRL_ITEMP0_Pos)


/******************** Bit definitions for TEMP_SE register ********************/
#define PGA305_TEMPSE_TEMPSE_Pos        (0U)
#define PGA305_TEMPSE_TEMPSE_Msk        (0x1UL << PGA305_TEMPSE_TEMPSE_Pos)
#define PGA305_TEMPSE_TEMPSE            PGA305_TEMPSE_TEMPSE_Msk

/******************* Value definitions for TEMP_SE register *******************/
#define PGA305_TEMPSE_SINGLEENDED       (0)
#define PGA305_TEMPSE_DIFFERENTIAL      (PGA305_TEMPSE_TEMPSE)


/****************** Bit definitions for DIAG_ENABLE register ******************/
#define PGA305_DIAGENABLE_EN_Pos (0U)
#define PGA305_DIAGENABLE_EN_Msk (0x1UL << PGA305_DIAGENABLE_EN_Pos)
#define PGA305_DIAGENABLE_EN PGA305_DIAGENABLE_EN_Msk
#define PGA305_EEPROMLOCK_LOCK_Pos (0U)
#define PGA305_EEPROMLOCK_LOCK_Msk (0x1UL << PGA305_EEPROMLOCK_LOCK_Pos)
#define PGA305_EEPROMLOCK_LOCK PGA305_EEPROMLOCK_LOCK_Msk
#define PGA305_AFEDIAGCFG_PD1_Pos (0U)
#define PGA305_AFEDIAGCFG_PD1_Msk (0x1UL << PGA305_AFEDIAGCFG_PD1_Pos)
#define PGA305_AFEDIAGCFG_PD1 PGA305_AFEDIAGCFG_PD1_Msk
#define PGA305_AFEDIAGCFG_PD2_Pos (1U)
#define PGA305_AFEDIAGCFG_PD2_Msk (0x1UL << PGA305_AFEDIAGCFG_PD2_Pos)
#define PGA305_AFEDIAGCFG_PD2 PGA305_AFEDIAGCFG_PD2_Msk
#define PGA305_AFEDIAGCFG_THRS0_Pos (2U)
#define PGA305_AFEDIAGCFG_THRS0_Msk (0x1UL << PGA305_AFEDIAGCFG_THRS0_Pos)
#define PGA305_AFEDIAGCFG_THRS0 PGA305_AFEDIAGCFG_THRS0_Msk
#define PGA305_AFEDIAGCFG_THRS1_Pos (3U)
#define PGA305_AFEDIAGCFG_THRS1_Msk (0x1UL << PGA305_AFEDIAGCFG_THRS1_Pos)
#define PGA305_AFEDIAGCFG_THRS1 PGA305_AFEDIAGCFG_THRS1_Msk
#define PGA305_AFEDIAGCFG_THRS2_Pos (4U)
#define PGA305_AFEDIAGCFG_THRS2_Msk (0x1UL << PGA305_AFEDIAGCFG_THRS2_Pos)
#define PGA305_AFEDIAGCFG_THRS2 PGA305_AFEDIAGCFG_THRS2_Msk
#define PGA305_AFEDIAGCFG_DISRP_Pos (5U)
#define PGA305_AFEDIAGCFG_DISRP_Msk (0x1UL << PGA305_AFEDIAGCFG_DISRP_Pos)
#define PGA305_AFEDIAGCFG_DISRP PGA305_AFEDIAGCFG_DISRP_Msk
#define PGA305_AFEDIAGCFG_DISRT_Pos (6U)
#define PGA305_AFEDIAGCFG_DISRT_Msk (0x1UL << PGA305_AFEDIAGCFG_DISRT_Pos)
#define PGA305_AFEDIAGCFG_DISRT PGA305_AFEDIAGCFG_DISRT_Msk
#define PGA305_AFEDIAGMASK_INPOV_Pos (0U)
#define PGA305_AFEDIAGMASK_INPOV_Msk (0x1UL << PGA305_AFEDIAGMASK_INPOV_Pos)
#define PGA305_AFEDIAGMASK_INPOV PGA305_AFEDIAGMASK_INPOV_Msk
#define PGA305_AFEDIAGMASK_INPUV_Pos (1U)
#define PGA305_AFEDIAGMASK_INPUV_Msk (0x1UL << PGA305_AFEDIAGMASK_INPUV_Pos)
#define PGA305_AFEDIAGMASK_INPUV PGA305_AFEDIAGMASK_INPUV_Msk
#define PGA305_AFEDIAGMASK_INTOV_Pos (2U)
#define PGA305_AFEDIAGMASK_INTOV_Msk (0x1UL << PGA305_AFEDIAGMASK_INTOV_Pos)
#define PGA305_AFEDIAGMASK_INTOV PGA305_AFEDIAGMASK_INTOV_Msk
#define PGA305_AFEDIAGMASK_PGAINOV_Pos (4U)
#define PGA305_AFEDIAGMASK_PGAINOV_Msk (0x1UL << PGA305_AFEDIAGMASK_PGAINOV_Pos)
#define PGA305_AFEDIAGMASK_PGAINOV PGA305_AFEDIAGMASK_PGAINOV_Msk
#define PGA305_AFEDIAGMASK_PGAINUV_Pos (5U)
#define PGA305_AFEDIAGMASK_PGAINUV_Msk (0x1UL << PGA305_AFEDIAGMASK_PGAINUV_Pos)
#define PGA305_AFEDIAGMASK_PGAINUV PGA305_AFEDIAGMASK_PGAINUV_Msk
#define PGA305_AFEDIAGMASK_TGAINOV_Pos (6U)
#define PGA305_AFEDIAGMASK_TGAINOV_Msk (0x1UL << PGA305_AFEDIAGMASK_TGAINOV_Pos)
#define PGA305_AFEDIAGMASK_TGAINOV PGA305_AFEDIAGMASK_TGAINOV_Msk
#define PGA305_AFEDIAGMASK_TGAINUV_Pos (7U)
#define PGA305_AFEDIAGMASK_TGAINUV_Msk (0x1UL << PGA305_AFEDIAGMASK_TGAINUV_Pos)
#define PGA305_AFEDIAGMASK_TGAINUV PGA305_AFEDIAGMASK_TGAINUV_Msk
#define PGA305_ADC24BITENABLE_EN_Pos (0U)
#define PGA305_ADC24BITENABLE_EN_Msk (0x1UL << PGA305_ADC24BITENABLE_EN_Pos)
#define PGA305_ADC24BITENABLE_EN PGA305_ADC24BITENABLE_EN_Msk
#define PGA305_OFFSETENABLE_EN_Pos (0U)
#define PGA305_OFFSETENABLE_EN_Msk (0x1UL << PGA305_OFFSETENABLE_EN_Pos)
#define PGA305_OFFSETENABLE_EN PGA305_OFFSETENABLE_EN_Msk

/************* Bit definitions for COMPENSATION_CONTROL register **************/
#define PGA305_COMPENSATIONCTRL_IFSEL_Pos               (0U)
#define PGA305_COMPENSATIONCTRL_IFSEL_Msk               (0x1UL << PGA305_COMPENSATIONCTRL_IFSEL_Pos)
#define PGA305_COMPENSATIONCTRL_IFSEL                   PGA305_COMPENSATIONCTRL_IFSEL_Msk
#define PGA305_COMPENSATIONCTRL_COMPENSATIONRESET_Pos   (1U)
#define PGA305_COMPENSATIONCTRL_COMPENSATIONRESET_Msk   (0x1UL << PGA305_COMPENSATIONCTRL_COMPENSATIONRESET_Pos)
#define PGA305_COMPENSATIONCTRL_COMPENSATIONRESET       PGA305_COMPENSATIONCTRL_COMPENSATIONRESET_Msk

/************ Value definitions for COMPENSATION_CONTROL register *************/
#define PGA305_COMPENSATIONCTRL_IFSEL_CALCULATIONENGINE (0)
#define PGA305_COMPENSATIONCTRL_IFSEL_DIGITALINTERFACE  PGA305_COMPENSATIONCTRL_IFSEL
#define PGA305_COMPENSATIONCTRL_COMPENSATION_RUNNING    (0)
#define PGA305_COMPENSATIONCTRL_COMPENSATION_RESET      PGA305_COMPENSATIONCTRL_COMPENSATIONRESET

#define PGA305_EEPROMPAGEADDR_ADDR0_Pos             (0U)
#define PGA305_EEPROMPAGEADDR_ADDR0_Msk             (0x1UL << PGA305_EEPROMPAGEADDR_ADDR0_Pos)
#define PGA305_EEPROMPAGEADDR_ADDR0                 PGA305_EEPROMPAGEADDR_ADDR0_Msk
#define PGA305_EEPROMPAGEADDR_ADDR1_Pos             (1U)
#define PGA305_EEPROMPAGEADDR_ADDR1_Msk             (0x1UL << PGA305_EEPROMPAGEADDR_ADDR1_Pos)
#define PGA305_EEPROMPAGEADDR_ADDR1                 PGA305_EEPROMPAGEADDR_ADDR1_Msk
#define PGA305_EEPROMPAGEADDR_ADDR2_Pos             (2U)
#define PGA305_EEPROMPAGEADDR_ADDR2_Msk             (0x1UL << PGA305_EEPROMPAGEADDR_ADDR2_Pos)
#define PGA305_EEPROMPAGEADDR_ADDR2                 PGA305_EEPROMPAGEADDR_ADDR2_Msk

#define PGA305_EEPROMCTRL_PROGRAM_Pos               (0U)
#define PGA305_EEPROMCTRL_PROGRAM_Msk               (0x1UL << PGA305_EEPROMCTRL_PROGRAM_Pos)
#define PGA305_EEPROMCTRL_PROGRAM                   PGA305_EEPROMCTRL_PROGRAM_Msk
#define PGA305_EEPROMCTRL_ERASE_Pos                 (1U)
#define PGA305_EEPROMCTRL_ERASE_Msk                 (0x1UL << PGA305_EEPROMCTRL_ERASE_Pos)
#define PGA305_EEPROMCTRL_ERASE                     PGA305_EEPROMCTRL_ERASE_Msk
#define PGA305_EEPROMCTRL_ERASEPROGRAM_Pos          (2U)
#define PGA305_EEPROMCTRL_ERASEPROGRAM_Msk          (0x1UL << PGA305_EEPROMCTRL_ERASEPROGRAM_Pos)
#define PGA305_EEPROMCTRL_ERASEPROGRAM              PGA305_EEPROMCTRL_ERASEPROGRAM_Msk
#define PGA305_EEPROMCTRL_FIXEDERASEPROGTIME_Pos    (3U)
#define PGA305_EEPROMCTRL_FIXEDERASEPROGTIME_Msk    (0x1UL << PGA305_EEPROMCTRL_FIXEDERASEPROGTIME_Pos)
#define PGA305_EEPROMCTRL_FIXEDERASEPROGTIME        PGA305_EEPROMCTRL_FIXEDERASEPROGTIME_Msk

#define PGA305_EEPROMCRC_CALCULATE_Pos              (0U)
#define PGA305_EEPROMCRC_CALCULATE_Msk              (0x1UL << PGA305_EEPROMCRC_CALCULATE_Pos)
#define PGA305_EEPROMCRC_CALCULATE                  PGA305_EEPROMCRC_CALCULATE_Msk

#define PGA305_EEPROMSTATUS_READ_Pos                (0U)
#define PGA305_EEPROMSTATUS_READ_Msk                (0x1UL << PGA305_EEPROMSTATUS_READ_Pos)
#define PGA305_EEPROMSTATUS_READ                    PGA305_EEPROMSTATUS_READ_Msk
#define PGA305_EEPROMSTATUS_ERASE_Pos               (1U)
#define PGA305_EEPROMSTATUS_ERASE_Msk               (0x1UL << PGA305_EEPROMSTATUS_ERASE_Pos)
#define PGA305_EEPROMSTATUS_ERASE                   PGA305_EEPROMSTATUS_ERASE_Msk
#define PGA305_EEPROMSTATUS_PROGRAM_Pos             (2U)
#define PGA305_EEPROMSTATUS_PROGRAM_Msk             (0x1UL << PGA305_EEPROMSTATUS_PROGRAM_Pos)
#define PGA305_EEPROMSTATUS_PROGRAM                 PGA305_EEPROMSTATUS_PROGRAM_Msk

/*************** Bit definitions for EEPROM_CRC_STATUS register ***************/
#define PGA305_EEPROMCRCSTATUS_CHECKING_Pos         (0U)
#define PGA305_EEPROMCRCSTATUS_CHECKING_Msk         (0x1UL << PGA305_EEPROMCRCSTATUS_CHECKING_Pos)
#define PGA305_EEPROMCRCSTATUS_CHECKING             PGA305_EEPROMCRCSTATUS_CHECKING_Msk
#define PGA305_EEPROMCRCSTATUS_GOOD_Pos             (1U)
#define PGA305_EEPROMCRCSTATUS_GOOD_Msk             (0x1UL << PGA305_EEPROMCRCSTATUS_GOOD_Pos)
#define PGA305_EEPROMCRCSTATUS_GOOD                 PGA305_EEPROMCRCSTATUS_GOOD_Msk



/**************** Function Declarations for PGA305 Operations *****************/
void Pga305_Initialise(void);
uint32_t Pga305_ReadCompensated(void);
uint32_t Pga305_ReadPressureRaw(void);
uint32_t Pga305_ReadPressureAdc(void);

/* Definitions for Pga305_SetTemperatureChannel() */
#define PGA305_TEMPERATURE_CHANNEL_EXTERNAL     (0)
#define PGA305_TEMPERATURE_CHANNEL_INTERNAL     (1)

void Pga305_SetTemperatureChannel(int channel);
uint32_t Pga305_ReadTemperatureRaw(void);
uint32_t Pga305_ReadTemperatureAdc(void);
uint8_t Pga305_ReadTempCtrl(void);

uint8_t Pga305_CalculateCrc(uint8_t const *data);
uint8_t Pga305_CalculateCrcOverEeprom(void);
void Pga305_WriteEepromCrc(uint8_t crc);

double Pga305_CalculateInternalTemperature(uint32_t temperatureAdc);


/************************ Low level access functions **************************/
uint8_t Pga305_ReadRegister(uint8_t page, uint8_t offset);
void Pga305_WriteRegister(uint8_t page, uint8_t offset, uint8_t value);
uint8_t Pga305_ReadEeprom(uint8_t address);
void Pga305_WriteEepromBlock(uint8_t address, uint8_t const data[PGA305_EEPROM_CACHE_SIZE]);


#endif
