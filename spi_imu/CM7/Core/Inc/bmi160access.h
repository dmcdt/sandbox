#ifndef BMI160ACCESS_H
#define BMI160ACCESS_H

/*
 *  Declarations for functions to access registers on the BMI160.
 */

#include <stdint.h>

uint8_t Bmi160_ReadRegister(uint8_t registerAddress);
void Bmi160_ReadRegisters(uint8_t registerAddress, unsigned int length, uint8_t *out);
void Bmi160_WriteRegister(uint8_t registerAddress, uint8_t value);
void Bmi160_WriteRegisters(uint8_t registerAddress, unsigned int length, uint8_t *data);

#endif
