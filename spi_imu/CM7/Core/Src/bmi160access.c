/*
 *  Functions for reading and writing from and to the BMI160 registers.
 *  Very thin convenience layer onto the transport layer.
 */


#include "bmi160access.h"
#include "bmi160transport.h"


uint8_t Bmi160_ReadRegister(uint8_t registerAddress)
{
    uint8_t spi_rx;
    
    Bmi160_ReadRegisters(registerAddress, 1, &spi_rx);
    
    return spi_rx;
}


void Bmi160_ReadRegisters(uint8_t registerAddress, unsigned int length, uint8_t *out)
{
    Bmi160Transport_ReadRegisters(registerAddress, length, out);
}


void Bmi160_WriteRegister(uint8_t registerAddress, uint8_t value)
{
    Bmi160_WriteRegisters(registerAddress, 1, &value);
}


void Bmi160_WriteRegisters(uint8_t registerAddress, unsigned int length, uint8_t *data)
{
    Bmi160Transport_WriteRegisters(registerAddress, length, data);
}
