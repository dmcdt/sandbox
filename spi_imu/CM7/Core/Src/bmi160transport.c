/*
 *  Functions for performing communications to the BMI160 over a peripheral.
 */

#include "bmi160transport.h"
#include "bmi160.h"
#include "spi.h"
#include "i2c.h"
#include "main.h"

#include <stdbool.h>


#define BMI160_ACTIVATE_SPI_PULSEWIDTH_MS   (10)

#define BMI160_ACCEL_POWERUP_DELAY_MS   (10)
#define BMI160_GYRO_POWERUP_DELAY_MS    (100)


/* Communications timeout in milliseconds */
#define BMI160_COMMS_TIMEOUT_MS (100)


static void     *ghBmi160;
static bool     gSpiMode;
static uint8_t  gI2cAddress;


/* Internal function declarations */
void Bmi160Transport_ChipSelect(void);
void Bmi160Transport_ChipDeselect(void);
void Bmi160Transport_ReadRegistersI2C(uint8_t directionAddress, unsigned int length, uint8_t *out);
void Bmi160Transport_ReadRegistersSpi(uint8_t directionAddress, unsigned int length, uint8_t *out);
void Bmi160Transport_WriteRegistersI2C(uint8_t directionAddress, unsigned int length, uint8_t *data);
void Bmi160Transport_WriteRegistersSpi(uint8_t directionAddress, unsigned int length, uint8_t *data);
void Bmi160_ReadI2C(uint8_t *data, unsigned int length);
void Bmi160_WriteI2C(uint8_t *data, unsigned int length);
void Bmi160_TransferSpi(uint8_t *data, unsigned int length);


void Bmi160Transport_InitialiseSpi(void)
{
    // Assumes CubeMX initialisation routines have already been called to
    // perform the actual peripheral initialisation.
    // Take a copy here for working with.
    ghBmi160 = &hspi1;
    gSpiMode = true;
}


void Bmi160Transport_InitialiseI2C(void)
{
    // Assumes CubeMX initialisation routines have already been called to
    // perform the actual peripheral initialisation.
    // Take a copy here for working with.
    ghBmi160 = &hi2c1;
    gSpiMode = false;
    gI2cAddress = BMI160_I2C_DEFAULT_ADDRESS;
}


void Bmi160Transport_ActivateCommunicationsInterface(void)
{
	if(gSpiMode)
    {
        // Pulse CS line to put device in SPI mode
        // If you cannot make a pulse without transmitting some data then
        // recommended to read register address 0x7F, which is non existant
        // Arduino code reads CHIPID 0x00
//          Bmi160_ReadRegister(BMI160_RA_DUMMY);
//          HAL_Delay(BMI160_RESET_DELAY);
        Bmi160Transport_ChipSelect();
        HAL_Delay(BMI160_ACTIVATE_SPI_PULSEWIDTH_MS);
        Bmi160Transport_ChipDeselect();
    }
    else
    {
        /* Pull SDO low or high to use the default I2C address or the
         * alternative I2C address respectively. We pull it low here to use the
         * default.
         */
        HAL_GPIO_WritePin(SPI1_MISO_IMU_GPIO_Port, SPI1_MISO_IMU_Pin, GPIO_PIN_RESET);
    }
}


void Bmi160Transport_ChipSelect(void)
{
    HAL_GPIO_WritePin(SPI1_CE_IMU_GPIO_Port, SPI1_CE_IMU_Pin, GPIO_PIN_RESET);
}


void Bmi160Transport_ChipDeselect(void)
{
    HAL_GPIO_WritePin(SPI1_CE_IMU_GPIO_Port, SPI1_CE_IMU_Pin, GPIO_PIN_SET);
}


void Bmi160Transport_ReadRegisters(uint8_t registerAddress, unsigned int length, uint8_t *out)
{
    registerAddress = BMI160_REG_READ | (registerAddress & BMI160_REG_ADDRESS_Msk);

    if (gSpiMode)
    {
        Bmi160Transport_ReadRegistersSpi(registerAddress, length, out);
    }
    else
    {
        Bmi160Transport_ReadRegistersI2C(registerAddress, length, out);
    }
}


void Bmi160Transport_ReadRegistersI2C(uint8_t directionAddress, unsigned int length, uint8_t *out)
{
    HAL_StatusTypeDef   status;

    status = HAL_I2C_Master_Transmit(ghBmi160, gI2cAddress, &directionAddress, 1, BMI160_COMMS_TIMEOUT_MS);
    if (status == HAL_OK)
    {
        status = HAL_I2C_Master_Receive(ghBmi160, gI2cAddress, out, length, BMI160_COMMS_TIMEOUT_MS);
    }

    // TODO - do something with status
}


void Bmi160Transport_ReadRegistersSpi(uint8_t directionAddress, unsigned int length, uint8_t *out)
{
    HAL_StatusTypeDef   status;

    Bmi160Transport_ChipSelect();
    status = HAL_SPI_Transmit(ghBmi160, &directionAddress, 1, BMI160_COMMS_TIMEOUT_MS);
    if (status == HAL_OK)
    {
        status = HAL_SPI_Receive(ghBmi160, out, length, BMI160_COMMS_TIMEOUT_MS);
    }
    Bmi160Transport_ChipDeselect();

    // TODO - do something with status
}


void Bmi160Transport_WriteRegisters(uint8_t registerAddress, unsigned int length, uint8_t *data)
{
    registerAddress = BMI160_REG_WRITE | (registerAddress & BMI160_REG_ADDRESS_Msk);

    if (gSpiMode)
    {
        Bmi160Transport_WriteRegistersSpi(registerAddress, length, data);
    }
    else
    {
        Bmi160Transport_WriteRegistersI2C(registerAddress, length, data);
    }
}


void Bmi160Transport_WriteRegistersI2C(uint8_t directionAddress, unsigned int length, uint8_t *data)
{
    HAL_StatusTypeDef   status;
    
    status = HAL_I2C_Master_Transmit(ghBmi160, gI2cAddress, &directionAddress, 1, BMI160_COMMS_TIMEOUT_MS);
    if (status == HAL_OK)
    {
        status = HAL_I2C_Master_Transmit(ghBmi160, gI2cAddress, data, length, BMI160_COMMS_TIMEOUT_MS);
    }

    // TODO - do something with status
}


void Bmi160Transport_WriteRegistersSpi(uint8_t directionAddress, unsigned int length, uint8_t *data)
{
    HAL_StatusTypeDef   status;
    
    Bmi160Transport_ChipSelect();
    status = HAL_SPI_Transmit(ghBmi160, &directionAddress, 1, BMI160_COMMS_TIMEOUT_MS);
    if (status == HAL_OK)
    {
        status = HAL_SPI_Transmit(ghBmi160, data, length, BMI160_COMMS_TIMEOUT_MS);
    }
    Bmi160Transport_ChipDeselect();

    // TODO - do something with status
}


void Bmi160_ReadI2C(uint8_t *data, unsigned int length)
{
    HAL_StatusTypeDef   status;

    /* No real choice for I2C, still has to be done as two calls. */
    status = HAL_I2C_Master_Transmit(ghBmi160, gI2cAddress, data, 1, BMI160_COMMS_TIMEOUT_MS);
    if (status == HAL_OK)
    {
        status = HAL_I2C_Master_Receive(ghBmi160, gI2cAddress, &data[1], length - 1, BMI160_COMMS_TIMEOUT_MS);
    }
    // TODO - do something with status
}


void Bmi160_WriteI2C(uint8_t *data, unsigned int length)
{
    HAL_StatusTypeDef   status;

    status = HAL_I2C_Master_Transmit(ghBmi160, gI2cAddress, data, length, BMI160_COMMS_TIMEOUT_MS);

    // TODO - do something with status
}


void Bmi160_TransferSpi(uint8_t *data, unsigned int length)
{
    HAL_StatusTypeDef   status;

    Bmi160Transport_ChipSelect();
    status = HAL_SPI_TransmitReceive(ghBmi160, data, data, length, BMI160_COMMS_TIMEOUT_MS);
    Bmi160Transport_ChipDeselect();

    // TODO - do something with status
}

void Bmi160_Read(uint8_t *data, unsigned int length)
{
    data[0] = BMI160_REG_READ | (data[0] & BMI160_REG_ADDRESS_Msk);
    if (gSpiMode)
    {
        Bmi160_TransferSpi(data, length);
    }
    else
    {
        Bmi160_ReadI2C(data, length);
    }
}


void Bmi160_Write(uint8_t *data, unsigned int length)
{
    data[0] = BMI160_REG_WRITE | (data[0] & BMI160_REG_ADDRESS_Msk);

    if (gSpiMode)
    {
        Bmi160_TransferSpi(data, length);
    }
    else
    {
        Bmi160_WriteI2C(data, length);
    }
}


