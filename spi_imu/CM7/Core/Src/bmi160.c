/*
 *  Implementation of BMI handling.
 */

#include "bmi160.h"
#include "bmi160access.h"
#include "bmi160transport.h"
#include "stm32h7xx_hal.h"
#include "Error_Handler.h"
#include <string.h>


/* Internal functions */     
void Bmi160_DoSetSlowNoMotionDetection(uint8_t threshold, uint8_t duration, uint8_t slowOrNo);


static volatile bool gFifoWatermark;
static volatile bool gImuEvent;


void Bmi160_Initialise(void)
{
    uint8_t pmuStatus;
    uint8_t verify;

    gFifoWatermark = false;
    gImuEvent = false;
    
    Bmi160Transport_InitialiseSpi();

    Bmi160Transport_ActivateCommunicationsInterface();
    HAL_Delay(BMI160_RESET_DELAY);
    
    // Issue a soft reset
    Bmi160_WriteRegister(BMI160_CMD, BMI160_CMD_SOFTRESET);
    HAL_Delay(BMI160_RESET_DELAY);
    
    Bmi160Transport_ActivateCommunicationsInterface();
    HAL_Delay(BMI160_RESET_DELAY);

    /* Quick check to see whether we are communicating with the chip */
    uint8_t chipId = Bmi160_ReadChipId();
    if (chipId != BMI160_CHIP_ID_VALUE)
    {
        Error_Handler();
    }
    
    /* Set the interface mode, should be configured correctly after a reset */
    Bmi160_WriteRegister(BMI160_IFCONF, 0x00);
    HAL_Delay(10);
    
    /* Turn on accelerometer */
    Bmi160_WriteRegister(BMI160_CMD, BMI160_CMD_ACCSETPMUNORMAL);
    /* Max power on time is 3.8 + 0.3 ms */
    HAL_Delay(10);
    /* Check status is good */
    pmuStatus = Bmi160_ReadRegister(BMI160_PMUSTATUS);
    if ((pmuStatus & BMI160_PMUSTATUS_ACC_Msk) != BMI160_PMUSTATUS_ACC_NORMAL)
    {
        // Low poer mode not set
        Error_Handler();
    }
    
    /* Turn on gyroscope */
    Bmi160_WriteRegister(BMI160_CMD, BMI160_CMD_GYRSETPMUNORMAL);
    /* Max power on time is 80 ms from suspend */
    HAL_Delay(100);
    /* Check status is good */
    pmuStatus = Bmi160_ReadRegister(BMI160_PMUSTATUS);
    if ((pmuStatus & BMI160_PMUSTATUS_GYR_Msk) != BMI160_PMUSTATUS_GYR_NORMAL)
    {
        // Low power mode not set
        Error_Handler();
    }
    
    /* Set acclerometer range and rate */
    Bmi160_WriteRegister(BMI160_ACCRANGE, BMI160_ACCRANGE_2G);
    /* "When operated in normal mode there is no significant dependance on settings
     * like ODR, undersampling, and bandwidth.", so just set the output data rate
     * as we will be operating in normal mode. However, datasheet says that is
     * an error, but it also says that operating at ODR=100 Hz and having
     * bandwidth set to 16 (so 1600 base sample rate / 16) or higher means normal
     * mode.
     */
    Bmi160_WriteRegister(BMI160_ACCCONF, BMI160_ACCCONF_BWP_DLPFNORMAL | BMI160_ACCCONF_ODR_100HZ);
    verify = Bmi160_ReadRegister(BMI160_ACCCONF);
    
//    myBMI160.setAccelDLPFMode(BMI160_DLPF_MODE_OSR4);
//    myBMI160.autoCalibrateZAccelOffset(1);
//    HAL_Delay(500);
//    myBMI160.autoCalibrateXAccelOffset(0);
//    HAL_Delay(500);
//    myBMI160.autoCalibrateYAccelOffset(0);
//    HAL_Delay(500);
//    myBMI160.setAccelOffsetEnabled(true);

    /* Set gyroscope range and rate */
    Bmi160_WriteRegister(BMI160_GYRRANGE, BMI160_GYRRANGE_125DPS);
    Bmi160_WriteRegister(BMI160_GYRCONF, BMI160_GYRCONF_BWP_DLPFNORMAL | BMI160_GYRCONF_ODR_100HZ);
    verify = Bmi160_ReadRegister(BMI160_GYRCONF);
}


uint8_t Bmi160_ReadChipId(void)
{
    return Bmi160_ReadRegister(BMI160_CHIPID);
}


uint8_t Bmi160_ReadStatus(void)
{
    return Bmi160_ReadRegister(BMI160_STATUS);
}


void Bmi160_ReadData(int16_t *data)
{
    Bmi160_ReadRegisters(BMI160_GYRXLSB, 6 * 2, (uint8_t *)data);
}


void Bmi160_EnableFifo(void)
{
    uint8_t verify;
    /* Set FIFO to take filtered data (data at the output data rate set above)
     * and no further "downsampling" (actually only decimation, no filtering).
     * (Should be set to this on reset.)
     */
    Bmi160_WriteRegister(BMI160_FIFODOWNS, BMI160_FIFODOWNS_ACCFILT | BMI160_FIFODOWNS_ACCDOWNS_1 |
                         BMI160_FIFODOWNS_GYRFILT | BMI160_FIFODOWNS_GYRDOWNS_1);

    verify = Bmi160_ReadRegister(BMI160_FIFODOWNS);
    
    /* Configure the FIFO. 0.5s of data for watermark,
     * gyro, accel only in data (no mag, time, or header).
     */
//    Bmi160_WriteRegister(BMI160_FIFO_CONFIG_0, 50 * 6 * 2 / 4); // Half a seconds worth = 50 samples @ 12 bytes / 4 bytes per unit in this register
//    Bmi160_WriteRegister(BMI160_FIFO_CONFIG_1, 
//                         (1U << BMI160_FIFO_ACC_EN_BIT) |
//                         (1U << BMI160_FIFO_GYR_EN_BIT));
    
    uint8_t config[2];
    config[0] = 50 * 6 * 2 / 4;
    config[1] = BMI160_FIFOCONFIG1_ACCEN | BMI160_FIFOCONFIG1_GYREN;
    Bmi160_WriteRegisters(BMI160_FIFOCONFIG0, 2, config);
    verify = Bmi160_ReadRegister(BMI160_FIFOCONFIG1);
}


void Bmi160_DisableFifo(void)
{
    uint8_t verify;

    /* Configure the FIFO. watermark at half full, no sensors enabled. */
    uint8_t config;
    config = 0;
    Bmi160_WriteRegister(BMI160_FIFOCONFIG1, config);
    
    verify = Bmi160_ReadRegister(BMI160_FIFOCONFIG1);
}


void Bmi160_ClearFifo(void)
{
    Bmi160_WriteRegister(BMI160_CMD, BMI160_CMD_FIFOFLUSH);
}


uint16_t Bmi160_GetFifoCount(void)
{
    uint16_t count;
    
    Bmi160_ReadRegisters(BMI160_FIFOLENGTH0, 2, (uint8_t *)&count);
    
    return count;
}


void Bmi160_ReadFifo(int16_t *imuSamples, int numberFrames)
{
    Bmi160_ReadRegisters(BMI160_FIFODATA, numberFrames * 6 * 2, (uint8_t *)imuSamples);
}


bool Bmi160_IsFifoDataReady(void)
{
    return gFifoWatermark;
}


void Bmi160_SetMotionDetectionSources(void)
{
    /* Set data source for motion detection to be the filtered data sources,
     * running at the output data rate, rather than the pre-filtered raw
     * 1600Hz/6400Hz data.
     */
    uint8_t intDataSrc[2];
    
    intDataSrc[0] = BMI160_INTDATA0_LOWHIGH_FILTERED | BMI160_INTDATA0_TAP_FILTERED;
    intDataSrc[1] = BMI160_INTDATA1_MOTION_FILTERED;
    Bmi160_WriteRegisters(BMI160_INTDATA0, 2, intDataSrc);
    // TODO - verify
}


void Bmi160_SetLowGDetection(uint8_t threshold, uint8_t duration, uint8_t hysteresis)
{
    uint8_t lowGSettings[3];
    
    lowGSettings[0] = duration;
    lowGSettings[1] = threshold;
    lowGSettings[2] = Bmi160_ReadRegister(BMI160_INTLOWHIGH2);
    lowGSettings[2] = (lowGSettings[2] & ~BMI160_INTLOWHIGH2_LOWHY_Msk) | (hysteresis & BMI160_INTLOWHIGH2_LOWHY_Msk);
    Bmi160_WriteRegisters(BMI160_INTLOWHIGH0, 3, lowGSettings);
    
    /* Verify */
    uint8_t verify;
    verify = Bmi160_ReadRegister(BMI160_INTLOWHIGH0);
    if (verify != lowGSettings[0])
    {
        Error_Handler();
    }

    verify = Bmi160_ReadRegister(BMI160_INTLOWHIGH1);
    if (verify != lowGSettings[1])
    {
        Error_Handler();
    }

    verify = Bmi160_ReadRegister(BMI160_INTLOWHIGH2);
    if (verify != lowGSettings[2])
    {
        Error_Handler();
    }
}


void Bmi160_GetLowGDetection(uint8_t *threshold, uint8_t *duration, uint8_t *hysteresis)
{
    uint8_t lowGSettings[3];

    Bmi160_ReadRegisters(BMI160_INTLOWHIGH0, 3, lowGSettings);
    
    *duration = lowGSettings[0];
    *threshold = lowGSettings[1];
    *hysteresis = (lowGSettings[2] & BMI160_INTLOWHIGH2_LOWHY_Msk);
}


void Bmi160_SetHighGDetection(uint8_t threshold, uint8_t duration, uint8_t hysteresis)
{
    uint8_t highGSettings[3];
    
    highGSettings[1] = duration;
    highGSettings[2] = threshold;
    highGSettings[0] = Bmi160_ReadRegister(BMI160_INTLOWHIGH2);
    highGSettings[0] = (highGSettings[2] & ~BMI160_INTLOWHIGH2_HIGHHY_Msk) | (hysteresis & BMI160_INTLOWHIGH2_HIGHHY_Msk);
    Bmi160_WriteRegisters(BMI160_INTLOWHIGH2, 3, highGSettings);
    // TODO - verify
}


void Bmi160_GetHighGDetection(uint8_t *threshold, uint8_t *duration, uint8_t *hysteresis)
{
    uint8_t highGSettings[3];

    Bmi160_ReadRegisters(BMI160_INTLOWHIGH2, 3, highGSettings);
    
    *duration = highGSettings[1];
    *threshold = highGSettings[2];
    *hysteresis = (highGSettings[0] & BMI160_INTLOWHIGH2_HIGHHY_Msk);
}


void Bmi160_SetNoMotionDetection(uint8_t threshold, uint8_t duration)
{
    Bmi160_DoSetSlowNoMotionDetection(threshold, duration, BMI160_INTMOTION3_NOMOTSEL_NOMOTION);
}


void Bmi160_SetSlowMotionDetection(uint8_t threshold, uint8_t duration)
{
    Bmi160_DoSetSlowNoMotionDetection(threshold, duration, BMI160_INTMOTION3_NOMOTSEL_SLOWMOTION);
}


#define MOTION_NUMBER_REGS (4)
void Bmi160_DoSetSlowNoMotionDetection(uint8_t threshold, uint8_t duration, uint8_t slowOrNo)
{
    uint8_t motion[MOTION_NUMBER_REGS];
    
    Bmi160_ReadRegisters(BMI160_INTMOTION0, MOTION_NUMBER_REGS, motion);
    motion[0] = (duration & BMI160_INTMOTION0_SLONOMOTDUR_Msk) | (motion[0] & BMI160_INTMOTION0_ANYMDUR_Msk);
    /* motion[1] is kept as-is */
    motion[2] = threshold;
    motion[3] = (motion[3] & ~BMI160_INTMOTION3_NOMOTSEL_Msk) | (slowOrNo & BMI160_INTMOTION3_NOMOTSEL_Msk);
    
    Bmi160_WriteRegisters(BMI160_INTMOTION0, MOTION_NUMBER_REGS, motion);
    
    /* verify */
    uint8_t     verify[MOTION_NUMBER_REGS];
    Bmi160_ReadRegisters(BMI160_INTMOTION0, MOTION_NUMBER_REGS, verify);
    for (int i = 0; i < MOTION_NUMBER_REGS; i++)
    {
        if (verify[i] != motion[i])
        {
            Error_Handler();
        }
    }
}


void Bmi160_SetAnyMotionDetection(uint8_t threshold, uint8_t duration)
{
    uint8_t motion[MOTION_NUMBER_REGS];
    
    Bmi160_ReadRegisters(BMI160_INTMOTION0, MOTION_NUMBER_REGS, motion);
    motion[0] = (motion[0] & BMI160_INTMOTION0_SLONOMOTDUR_Msk) | (duration & BMI160_INTMOTION0_ANYMDUR_Msk);
    motion[1] = threshold;
    /* motion[2] is kept as-is */
    motion[3] = (motion[3] & ~BMI160_INTMOTION3_SIGMOTSEL_Msk) | BMI160_INTMOTION3_SIGMOTSEL_ANYMOTION;
    
    Bmi160_WriteRegisters(BMI160_INTMOTION0, MOTION_NUMBER_REGS, motion);
    
    /* verify */
    uint8_t     verify[MOTION_NUMBER_REGS];
    Bmi160_ReadRegisters(BMI160_INTMOTION0, MOTION_NUMBER_REGS, verify);
    for (int i = 0; i < MOTION_NUMBER_REGS; i++)
    {
        if (verify[i] != motion[i])
        {
            Error_Handler();
        }
    }
}


void Bmi160_SetSignificantMotionDetection(uint8_t threshold, uint8_t sleepDuration, uint8_t proofDuration)
{
    uint8_t motion;
    
    motion = Bmi160_ReadRegister(BMI160_INTMOTION3);

    motion = (proofDuration & BMI160_INTMOTION3_SIGMOTPROOF_Msk) |
             (sleepDuration & BMI160_INTMOTION3_SIGMOTSKIP_Msk) |
             (BMI160_INTMOTION3_SIGMOTSEL_SIGNIFICANT) |
             (motion & BMI160_INTMOTION3_NOMOTSEL_Msk);

    Bmi160_WriteRegister(BMI160_INTMOTION1, threshold);
    Bmi160_WriteRegister(BMI160_INTMOTION3, motion);
    
    /* verify */
    uint8_t verify;
    verify = Bmi160_ReadRegister(BMI160_INTMOTION3);
    if (verify != motion)
    {
        Error_Handler();
    }
}


void Bmi160_SetInterruptSignalDuration(uint8_t latchMode)
{
    uint8_t interruptLatch;
    interruptLatch = Bmi160_ReadRegister(BMI160_INTLATCH);
    interruptLatch = (interruptLatch & ~BMI160_INTLATCH_INTLATCH_Msk) | (latchMode & BMI160_INTLATCH_INTLATCH_Msk);
    Bmi160_WriteRegister(BMI160_INTLATCH, interruptLatch);
}


/*
 *  Configure the interrupt pins electrical specification.
 *  
 *  Inspects the configuration passed in here and then sets the registers
 *  to the appropriate values to configure the electrical signalling
 *  specifications for each interrupt pin.
 *
 *  @param int1PinConfig - Configuration for INT1 pin.
 *  @param int2PinConfig - Configuration for INT2 pin.
 *  
 *  @remark Each parameter is a combination of the BMI160_INTPIN_* bit masks
 *  defined below.
 */
void Bmi160_ConfigureInterruptPins(uint8_t int1PinConfig, uint8_t int2PinConfig)
{
    uint8_t pinConfig;
    uint8_t latchValue;
    uint8_t inputEnables;
    
    /* TODO - should ensure cannot be input and output */
    
    /* Extract the bit fields into the relevant variables for writing to
     * separate registers later.
     */
    
    pinConfig = (int1PinConfig & 0x0F) |
                ((int2PinConfig & 0x0F) << 4);
         
    inputEnables = (((int1PinConfig & BMI160_INTPIN_INPUT_EN_Msk) >> BMI160_INTPIN_INPUT_EN_Pos) << BMI160_INTLATCH_INT1INPUTEN_Pos) |
                   (((int2PinConfig & BMI160_INTPIN_INPUT_EN_Msk) >> BMI160_INTPIN_INPUT_EN_Pos) << BMI160_INTLATCH_INT2INPUTEN_Pos);
    
     /* Signalling on pins plus outputs */
    Bmi160_WriteRegister(BMI160_INTOUTCTRL, pinConfig);
    
    /* For setting the input enable flags if specified */
    latchValue = Bmi160_ReadRegister(BMI160_INTLATCH);
    latchValue = (latchValue & BMI160_INTLATCH_INTLATCH_Msk) | inputEnables;
    Bmi160_WriteRegister(BMI160_INTLATCH, latchValue);
}


/**
 *  Enables the specified interrupts.
 *
 *  @param mask - Bitwise combination of the unified BMI160_INT_*_EN definitions
 *  so they can be passed in one parameter.
 */
void Bmi160_EnableInterrupts(uint32_t mask)
{
    // TODO - could assert that the mask has no invalid bits?
    
    uint8_t interruptsEnabled[3];
    
    interruptsEnabled[0] = mask & BMI160_INTEN0_VALID_Msk;
    interruptsEnabled[1] = (mask >> 8) & BMI160_INTEN1_VALID_Msk;
    interruptsEnabled[2] = (mask >> 16) & BMI160_INTEN2_VALID_Msk;
    
    Bmi160_WriteRegisters(BMI160_INTEN0, 3, interruptsEnabled);
}


void Bmi160_DisableInterrupts(void)
{
    uint8_t interruptsEnabled[3];
    memset(interruptsEnabled, 0x00, 3);
    Bmi160_WriteRegisters(BMI160_INTEN0, 3, interruptsEnabled);
}


/**
 *  Maps the specified interrupts to an interrupt pin.
 *
 *
 *  @param int1 Bitwise combination of the interrupts that should cause an
 *  interrupt on INT1.
 */
void Bmi160_MapInterrupts(uint16_t int1, uint16_t int2)
{
    uint8_t intMaps[3];
    
    intMaps[0] = int1 & 0xFF;
    intMaps[1] = ((int1 & 0x0F00) >> 4) | ((int2 & 0x0F00) >> 8);
    intMaps[2] = int2 & 0xFF;
    
    Bmi160_WriteRegisters(BMI160_INTMAP0, 3, intMaps);
}


uint32_t Bmi160_GetInterruptStatus(void)
{
    /* Naive implementation, leaves gaps between register address and response. */
#if 0
    uint32_t    interruptStatus = 0;
    
    Bmi160_ReadRegisters(BMI160_INTSTATUS0, 4, (uint8_t *)&interruptStatus);
    
    return interruptStatus;
#endif
    
    /* The Cortex M7 supports unaligned access of multibyte variables.
     * The Cortex M4 does as well, depending on the state of a processor flag.
     * If you get memory faults at the return line then you may need to
     * use the union method which creates some space to hold the command before
     * the response which the command code can be fiddled into to appear
     * immediately before it, and still requires no extra copy for the return
     * value on the way out.
     *
     * If all else fails, there is always memcpy().
     */
#if 0
    
    /* Create a structure with natural alignment, and then a union so we can
     * fit the command immediately in front of the still aligned member.
     */
    struct GetInterruptStatusFields
    {
        uint8_t     command;      
        uint32_t    interruptStatus;
    };
    union GetInterruptStatusCmdRsp
    {
        struct GetInterruptStatusFields fields;
        uint8_t buffer[sizeof(fields)];
    } transfer;

    transfer.buffer[offsetof(union GetInterruptStatusCmdRsp, fields.interruptStatus) - 1] = BMI160_INTSTATUS0;
    Bmi160_Read(&transfer.buffer[offsetof(union GetInterruptStatusCmdRsp, fields.interruptStatus) - 1], 5);

    return transfer.fields.interruptStatus;

#else

    /* Force single byte alignment in memory, so we can transmit the structure. */
#pragma pack(1)
    struct GetInterruptStatusFields
    {
        uint8_t     command;      
        uint32_t    interruptStatus;
    } transfer;
#pragma pack()

    transfer.command = BMI160_INTSTATUS0;
    Bmi160_Read((uint8_t *)&transfer, sizeof(transfer));

    return transfer.interruptStatus;

#endif
    
}


#define COMMAND_LENGTH (2)
void Bmi160_ClearInterruptStatus(void)
{
    //Bmi160_WriteRegister(BMI160_CMD, BMI160_CMD_INTRESET);
    uint8_t command[COMMAND_LENGTH];
    
    command[0] = BMI160_CMD;
    command[1] = BMI160_CMD_INTRESET;
    Bmi160_Write(command, COMMAND_LENGTH);
}


bool Bmi160_IsImuEvent(void)
{
    return gImuEvent;
}


void Bmi160_SetImuEvent(void)
{
    gImuEvent = true;
}


void Bmi160_ClearImuEvent(void)
{
    gImuEvent = false;
}

