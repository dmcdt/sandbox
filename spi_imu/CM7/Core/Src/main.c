/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include <stdio.h>
#include <string.h>
#include <math.h>

#include "sleep.h"
#include "Error_Handler.h"
#include "pga305.h"
#include "bmi160.h"
#include "bmi160access.h"
#include "bmi160transport.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

//#ifndef HSEM_ID_0
//#define HSEM_ID_0 (0U) /* HW semaphore 0*/
//#endif

#define IMU_NUMBER_FRAMES   (50)
#define IMU_NUMBER_SENSORS  (6)

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
    
int16_t     imuSamples[IMU_NUMBER_FRAMES * IMU_NUMBER_SENSORS];
double      freefall[IMU_NUMBER_FRAMES];

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MPU_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

    HAL_EnableDBGSleepMode();
    HAL_EnableDBGStopMode();
    HAL_EnableDBGStandbyMode();
    
    HAL_EnableDomain2DBGSleepMode();
    HAL_EnableDomain2DBGStopMode();
    HAL_EnableDomain2DBGStandbyMode();

    HAL_EnableDomain3DBGStopMode();
    HAL_EnableDomain3DBGStandbyMode();

    __HAL_RCC_TIM2_FORCE_RESET();
    __HAL_RCC_TIM2_RELEASE_RESET();
    
    __HAL_DBGMCU_UnFreeze_TIM2();
    __HAL_DBGMCU_FREEZE_TIM2();
    
    __HAL_RCC_TIM13_FORCE_RESET();
    __HAL_RCC_TIM13_RELEASE_RESET();
    
    __HAL_DBGMCU_UnFreeze_TIM13();
    __HAL_DBGMCU_FREEZE_TIM13();
    
    __HAL_RCC_USART1_FORCE_RESET();
    __HAL_RCC_USART1_RELEASE_RESET();

    __HAL_RCC_I2C1_FORCE_RESET();
    __HAL_RCC_I2C1_RELEASE_RESET();

    __HAL_RCC_SPI1_FORCE_RESET();
    __HAL_RCC_SPI1_RELEASE_RESET();

    __HAL_RCC_GPIOA_FORCE_RESET();
    __HAL_RCC_GPIOA_RELEASE_RESET();

  /* USER CODE END 1 */
/* USER CODE BEGIN Boot_Mode_Sequence_0 */
//  int32_t timeout;
/* USER CODE END Boot_Mode_Sequence_0 */

  /* MPU Configuration--------------------------------------------------------*/
  MPU_Config();

  /* Enable I-Cache---------------------------------------------------------*/
  SCB_EnableICache();

  /* Enable D-Cache---------------------------------------------------------*/
  SCB_EnableDCache();

/* USER CODE BEGIN Boot_Mode_Sequence_1 */
//  /* Wait until CPU2 boots and enters in stop mode or timeout*/
//  timeout = 0xFFFF;
//  while((__HAL_RCC_GET_FLAG(RCC_FLAG_D2CKRDY) != RESET) && (timeout-- > 0));
//  if ( timeout < 0 )
//  {
//  Error_Handler();
//  }
/* USER CODE END Boot_Mode_Sequence_1 */
  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();
/* USER CODE BEGIN Boot_Mode_Sequence_2 */
///* When system initialization is finished, Cortex-M7 will release Cortex-M4 by means of
//HSEM notification */
///*HW semaphore Clock enable*/
//__HAL_RCC_HSEM_CLK_ENABLE();
///*Take HSEM */
//HAL_HSEM_FastTake(HSEM_ID_0);
///*Release HSEM in order to notify the CPU2(CM4)*/
//HAL_HSEM_Release(HSEM_ID_0,0);
///* wait until CPU2 wakes up from stop mode */
//timeout = 0xFFFF;
//while((__HAL_RCC_GET_FLAG(RCC_FLAG_D2CKRDY) == RESET) && (timeout-- > 0));
//if ( timeout < 0 )
//{
//Error_Handler();
//}
/* USER CODE END Boot_Mode_Sequence_2 */

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_SPI1_Init();
  MX_USART1_UART_Init();
  MX_USART3_UART_Init();
  /* USER CODE BEGIN 2 */
    HAL_StatusTypeDef   status;
    char                str[128];
    uint8_t             spi_buf[16];
    uint8_t             spi_buf_rx[16];
    uint8_t             errorStatus;
    uint8_t             chipId;

    strcpy(str, "BMI160 SPI Test\r\n");
    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);

    Pga305_Initialise();

//    SPI_HandleTypeDef *hBmi160;
//
//    hBmi160 = &hspi1;
//  
//    // Pulse CS line to put device in SPI mode
//    // If you cannot make a pulse without transmitting some data then
//    // recommended to read register address 0x7F, which is non existant
//    // Arduino code reads CHIPID 0x00
//    spi_buf[0] = BMI160_SPI_READ | 0x7F;
//    HAL_GPIO_
//    status = HAL_SPI_Transmit(hBmi160, spi_buf, 1, BMI160_TIMEOUT);
//    HAL_Delay(100);
//
//    // Issue a soft reset
//    spi_buf[0] = BMI160_SPI_WRITE | BMI160_RA_CMD;
//    spi_buf[1] = BMI160_CMD_SOFT_RESET;
//    status = HAL_SPI_Transmit(hBmi160, spi_buf, 2, BMI160_TIMEOUT);
//    HAL_Delay(100);
//
//
//    // Pulse CS line to put device in SPI mode
//    // If you cannot make a pulse without transmitting some data then
//    // recommended to read register address 0x7F, which is non existant
//    // Arduino code reads CHIPID 0x00
//    spi_buf[0] = BMI160_SPI_READ | 0x7F;
//    status = HAL_SPI_Transmit(hBmi160, spi_buf, 1, BMI160_TIMEOUT);
//    HAL_Delay(100);
//
//    // Read ChipID to see if we can communicate
//    spi_buf[0] = BMI160_SPI_READ | BMI160_RA_CHIP_ID;
//    spi_buf[1] = 0x00;
//    status = HAL_SPI_TransmitReceive(hBmi160, spi_buf, spi_buf_rx, 2, BMI160_TIMEOUT);
//    HAL_Delay(100);
    
    Bmi160_Initialise();
    
    chipId = Bmi160_ReadChipId();
    sprintf(str, "ChipId = 0x%02X\r\n", chipId);
    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
    
    errorStatus = Bmi160_ReadRegister(BMI160_ERR);
    sprintf(str, "ERR_REG = 0x%02X\r\n", errorStatus);
    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);

    Bmi160_DisableFifo();
    Bmi160_DisableInterrupts();
    Bmi160_MapInterrupts(0, 0);
    Bmi160_ClearInterruptStatus();

    int16_t     imuData[IMU_NUMBER_SENSORS];
    uint8_t     imuStatus;
    
    double      ax, ay, az, gx, gy, gz;
    
    uint16_t    fifoCount, lastFifoCount;
    int         lineCount;
    uint32_t    startTicks, endTicks;
    int         loopsBeforeImuEvent;
    uint32_t    interruptEnable;
    
    double lastax, lastay, lastaz;
    int nomotioncount = 0;
    int slowmotioncount = 0;
    
    int cumIntLoopCount = 0;
    
    uint32_t    lastFwmTick = 0;
    uint32_t    thisFwmTick;
    int         missedFwmCount = 0;
    
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
    
    fifoCount = 0;
    lastFifoCount = 0;
    
    lineCount = 0;

    /* Configure the NVIC interrupt for EXTI2 but do not enable it until the IMU is configured */
    HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);

    Bmi160_ClearFifo();

    /* Configure motion detection */
    Bmi160_SetMotionDetectionSources();

    // Low G (freefall) detection - 500 mg, 7.5 ms, 125 mg in 2G range
    // why you no work?! Paul E got it to work.
    Bmi160_SetLowGDetection(64, 2, 1);
    
    /* High G (shock) detection - 1500 mg in 2G range, 7.5 ms, 375 mg */
    Bmi160_SetHighGDetection(192, 2, 3);
    
    /* Noise floor of accel seems to be around 5mg. How that is going to be used
     * point to point to detect motion I don't know.
     */
    
    /* No motion and slow motion detection are performed using the same
     * processing and interrupt, and are mutually exclusive from each other,
     * just the timer/counter trigger is inverted.
     */
    /* No motion detection - threshold 15mg, duration in set options */
    /* Motion must be detected first and then stopped, but once it has stopped
     * and left enabled it will re-trigger every duration interval.
     */
    Bmi160_SetNoMotionDetection(4, BMI160_INTMOTION0_NOMOTDUR_10_24S);

    /* Slow motion detection - threshold 100 mg in 2G range, duration in samples+1 equiv to 0.1 s*/
    //Bmi160_SetSlowMotionDetection(26, 9);

    
    /* TODO - Any/significant motion detection */
    /* Any motion detection - threshold 100 mg in 2G range, duration is samples+1 equiv to 40ms */
    //Bmi160_SetAnyMotionDetection(26, 3);
    
    /* The point of significant motion is to check if the user is changing
     * location in a low power way and then wake up the MCU. So walking, cycling,
     * travelling in a train are all given as examples, but how does this work
     * at constant speed? Does it just assume that signifcant motion will
     * have larger fluctuations in acceleration?
     */
    /* threshold 70 mg in 2G range, sleep time from set of options, proof time from set of options. */
    Bmi160_SetSignificantMotionDetection(18, BMI160_INTMOTION3_SIGMOTSKIP_3S,
                                         BMI160_INTMOTION3_SIGMOTPROOF_1S);

    /* TODO - should possibly make the enable, and enable rather than set */
    
    /* One of the other interrupts is blocking the Any Motion in the Z axis.
     * It is either a poor choice of configurations, or one interrupt occurs
     * before the other and then clears the counters.
     * Given the speed that multiple any motion inteerrupts come in, I doubt
     * it is good enough to use for motion detection.
     */
    
    /* Seems to be slow motion and any motion get in the way of each other
     * (they are essentially tracking some motion starting, so you can see why).
     * But other things need to be considered for causing multiple events
     * unnecessarily, such as the HIGHG and LOWG when the motion is aligned
     * with the gravity vector.
     */
    
    /* No motion and Slow motion use the same interrupt enable and map bits */
    Bmi160_SetInterruptSignalDuration(BMI160_INTLATCH_LATCHED);
    
    Bmi160_ConfigureInterruptPins(BMI160_INTPIN_INPUT_DISABLED | BMI160_INTPIN_OUTPUT_DISABLED,
                                  BMI160_INTPIN_INPUT_DISABLED | BMI160_INTPIN_OUTPUT_ENABLED | BMI160_INTPIN_PUSHPULL | BMI160_INTPIN_ACTIVEHIGH | BMI160_INTPIN_EDGE_TRIGGERED);
    
//    Bmi160_MapInterrupts(0, BMI160_INTMAP_FWM | BMI160_INTMAP_FFULL | 
//                         BMI160_INTMAP_LOWGSTEP |
//                         BMI160_INTMAP_HIGHG |
//                         BMI160_INTMAP_NOMOTION |   /* No and Slow motion are mapped using the same bit */
//                         BMI160_INTMAP_ANYMOTION);  /* Any and significant motion are mapped using the same bit */

    /* Only have data interrupts regularly and then check other status at same
     * time. Motion time calculations are in the order of multiple seconds so
     * +/-0.5s is not going to be a problem.
     */
    Bmi160_MapInterrupts(0, BMI160_INTMAP_FWM | BMI160_INTMAP_FFULL);
    
    interruptEnable = BMI160_INTEN_FWMEN | BMI160_INTEN_FFULLEN |
                        BMI160_INTEN_LOWEN |
                        BMI160_INTEN_HIGHXEN | BMI160_INTEN_HIGHYEN | BMI160_INTEN_HIGHZEN |
                        BMI160_INTEN_NOMOXEN | BMI160_INTEN_NOMOYEN | BMI160_INTEN_NOMOZEN |    /* No and Slow motion share the same enable bit and are mutually exclusive */
                        BMI160_INTEN_ANYMOXEN | BMI160_INTEN_ANYMOYEN | BMI160_INTEN_ANYMOZEN;  /* Any and significant motion share the same enable bit and are mutually exclusive */
    Bmi160_EnableInterrupts(interruptEnable);

    /* Clear out any stale interrupt status and then enable the interrupt for the IMU */
    __HAL_GPIO_EXTI_CLEAR_IT(IMU_INT1_Pin);
    HAL_NVIC_EnableIRQ(EXTI2_IRQn);
    
    Bmi160_ClearFifo();
    Bmi160_EnableFifo();
    
    lastFwmTick = thisFwmTick = HAL_GetTick();
    while (1)
    {
//        imuStatus = Bmi160_ReadStatus();
//        sprintf(str, "Status = 0x%02X\r\n", imuStatus);
//        status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//        
//        if ((imuStatus & (BMI160_STATUS_DRDY_ACC_Msk | BMI160_STATUS_DRDY_GYR_Msk)) == (BMI160_STATUS_DRDY_ACC_Msk | BMI160_STATUS_DRDY_GYR_Msk))
//        {
//            startTicks = HAL_GetTick();
//            Bmi160_ReadData(imuData);
//            endTicks = HAL_GetTick();
//
//            ax = (double)imuData[3] / BMI160_ACC_NOMINAL_SCALE2G;
//            ay = (double)imuData[4] / BMI160_ACC_NOMINAL_SCALE2G;
//            az = (double)imuData[5] / BMI160_ACC_NOMINAL_SCALE2G;
//            
//            gx = (double)imuData[0] / BMI160_GYR_NOMINAL_SCALE_125dps;
//            gy = (double)imuData[1] / BMI160_GYR_NOMINAL_SCALE_125dps;
//            gz = (double)imuData[2] / BMI160_GYR_NOMINAL_SCALE_125dps;
//
//            //sprintf(str, "Ax/y/z=0x%04hX 0x%04hX 0x%04hX\r\n", imuData[3], imuData[4], imuData[5]);
//            //status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//
//            sprintf(str, "Ax/y/z=%.6f %.6f %.6f\r\n", ax, ay, az);
//            status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//
//            //sprintf(str, "Gx/y/z=0x%04hX 0x%04hX 0x%04hX\r\n", imuData[0], imuData[1], imuData[2]);
//            //status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//            
//            sprintf(str, "Gx/y/z=%.6f %.6f %.6f\r\n", gx, gy, gz);
//            status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//
//            endTicks = startTicks;
//        }
        
//        fifoCount = Bmi160_GetFifoCount();
//        if (fifoCount != lastFifoCount)
//        {
//            sprintf(str, "%d\r\n", fifoCount);
//            status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//            
//            lastFifoCount = fifoCount;
//        }
//        
//        if (fifoCount >= sizeof imuSamples)
//        {
//            startTicks = HAL_GetTick();
//            Bmi160_ReadFifo(imuSamples, IMU_NUMBER_FRAMES);
//            endTicks = HAL_GetTick();
//
//            ax = (double)imuSamples[3] / BMI160_ACC_NOMINAL_SCALE2G;
//            ay = (double)imuSamples[4] / BMI160_ACC_NOMINAL_SCALE2G;
//            az = (double)imuSamples[5] / BMI160_ACC_NOMINAL_SCALE2G;
//            
//            gx = (double)imuSamples[0] / BMI160_GYR_NOMINAL_SCALE_125dps;
//            gy = (double)imuSamples[1] / BMI160_GYR_NOMINAL_SCALE_125dps;
//            gz = (double)imuSamples[2] / BMI160_GYR_NOMINAL_SCALE_125dps;
//
//            //sprintf(str, "Ax/y/z=0x%04hX 0x%04hX 0x%04hX\r\n", imuData[3], imuData[4], imuData[5]);
//            //status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//
//            //sprintf(str, "Ax/y/z=%.6f %.6f %.6f\r\n", ax, ay, az);
//            //status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//
//            //sprintf(str, "Gx/y/z=0x%04hX 0x%04hX 0x%04hX\r\n", imuData[0], imuData[1], imuData[2]);
//            //status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//            
//            //sprintf(str, "Gx/y/z=%.6f %.6f %.6f\r\n", gx, gy, gz);
//            //status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
//            
//            ++lineCount;
//            if (lineCount >= 16)
//            {
//                lineCount = 0;
//                status = HAL_UART_Transmit(&huart1, ".\r\n", 3, HAL_MAX_DELAY);
//            }
//            else
//            {
//                status = HAL_UART_Transmit(&huart1, ".", 1, HAL_MAX_DELAY);
//            }
//
//            endTicks = startTicks;
//        }
        
        
        /* Assuming we only have the watermark interrupt activated */
        if (Bmi160_IsImuEvent())
        {
            uint32_t    imuInterruptStatus;
            int         irqLoopCount;

            /* Clear internal flag controlling loop */
            Bmi160_ClearImuEvent();
            loopsBeforeImuEvent = 0;
            irqLoopCount = 0;
            
            /* Read interrupt status from IMU to see what has happened */
            imuInterruptStatus = Bmi160_GetInterruptStatus();

            /* Loop while we have interrupts pending, since interrupt conditions
             * that still hold do not cause a new edge to be output. This means
             * we might handle a genuine interrupt here and have a pending
             * Bmi160_IsImuEvent() == true, but rather that and do nothing than
             * miss interrupts.
             */
            while (imuInterruptStatus & BMI160_INTSTATUS_IRQ_Msk)
            {
                /* Clear interrupt status on IMU, so it can generate new interrupts */
                Bmi160_ClearInterruptStatus();
            
                sprintf(str, "Int=0x%08lX LC=%d/%d\r\n", imuInterruptStatus, irqLoopCount, cumIntLoopCount);
                status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);

                //uint8_t err;
                //err = Bmi160_ReadRegister(BMI160_ERR);
                
                /* Now process the interrupts that happened */
                if (imuInterruptStatus & BMI160_INTSTATUS_FWMINT)
                {
                    thisFwmTick = startTicks = HAL_GetTick();
                    
                    /* Need to disable the FIFO watermak interrupt around reading from
                     * the FIFO. Reason stated in datasheet is that you could read
                     * a frame, which clears the interrupt on the IMU, it then stores
                     * new data, which causes a new interrupt, even though you are
                     * servicing it already.
                     */
                    Bmi160_EnableInterrupts(interruptEnable & ~BMI160_INTEN_FWMEN);
                    Bmi160_ReadFifo(imuSamples, IMU_NUMBER_FRAMES);
                    Bmi160_EnableInterrupts(interruptEnable);
                    endTicks = HAL_GetTick();

#if 1
                    ax = (double)imuSamples[3] / BMI160_ACC_NOMINAL_SCALE2G;
                    ay = (double)imuSamples[4] / BMI160_ACC_NOMINAL_SCALE2G;
                    az = (double)imuSamples[5] / BMI160_ACC_NOMINAL_SCALE2G;
                    
                    gx = (double)imuSamples[0] / BMI160_GYR_NOMINAL_SCALE_125dps;
                    gy = (double)imuSamples[1] / BMI160_GYR_NOMINAL_SCALE_125dps;
                    gz = (double)imuSamples[2] / BMI160_GYR_NOMINAL_SCALE_125dps;

                    //sprintf(str, "Ax/y/z=0x%04hX 0x%04hX 0x%04hX\r\n", imuData[3], imuData[4], imuData[5]);
                    //status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);

                    //sprintf(str, "Ax/y/z=%.6f %.6f %.6f\r\n", ax, ay, az);
                    //status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);

                    //sprintf(str, "Gx/y/z=0x%04hX 0x%04hX 0x%04hX\r\n", imuData[0], imuData[1], imuData[2]);
                    //status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
                    
                    //sprintf(str, "Gx/y/z=%.6f %.6f %.6f\r\n", gx, gy, gz);
                    //status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);

                    double minx, maxx, avgx;
                    double miny, maxy, avgy;
                    double minz, maxz, avgz;
                    int freefallcount;
                    int ffcountmax;
                    
                    double maxdiffx;
                    double maxdiffy;
                    double maxdiffz;
                                    
                    minx = miny = minz = 2;
                    maxx = maxy = maxz = -2;
                    avgx = avgy = avgz = 0;
                    freefallcount = 0;
                    ffcountmax = 0;
                    
                    maxdiffx = maxdiffy = maxdiffz = 0;

                    for (int i = 0; i < IMU_NUMBER_FRAMES; i++)
                    {
                        ax = (double)imuSamples[i * IMU_NUMBER_SENSORS + 3] / BMI160_ACC_NOMINAL_SCALE2G;
                        ay = (double)imuSamples[i * IMU_NUMBER_SENSORS + 4] / BMI160_ACC_NOMINAL_SCALE2G;
                        az = (double)imuSamples[i * IMU_NUMBER_SENSORS + 5] / BMI160_ACC_NOMINAL_SCALE2G;
                        
                        if (ax < minx)
                        {
                            minx = ax;
                        }
                        
                        if (ax > maxx)
                        {
                            maxx = ax;
                        }

                        if (ay < miny)
                        {
                            miny = ay;
                        }
                        
                        if (ay > maxy)
                        {
                            maxy = ay;
                        }

                        if (az < minz)
                        {
                            minz = az;
                        }
                        
                        if (az > maxz)
                        {
                            maxz = az;
                        }

                        avgx += ax;
                        avgy += ay;
                        avgz += az;
                        
                        freefall[i] = fabs(ax) + fabs(ay) + fabs(az);
                        if (freefall[i] < 0.9)
                        {
                            freefallcount++;
                        }
                        else if (freefall[i] > 0.95)
                        {
                            if (ffcountmax < freefallcount)
                            {
                                ffcountmax = freefallcount;
                            }
                            
                            freefallcount = 0;
                        }
                        
                        if (i > 0)
                        {
                            if (maxdiffx < fabs(ax - lastax))
                            {
                                maxdiffx = fabs(ax - lastax);
                            }

                            if (maxdiffy < fabs(ay - lastay))
                            {
                                maxdiffy = fabs(ay - lastay);
                            }

                            if (maxdiffz < fabs(az - lastaz))
                            {
                                maxdiffz = fabs(az - lastaz);
                            }
                            
                            if ((maxdiffx < 0.1) && (maxdiffy < 0.1) && (maxdiffz < 0.1))
                            {
                                nomotioncount++;
                            }
                            else
                            {
                                nomotioncount = 0;
                            }
                            
                            if ((maxdiffx >= 0.1) || (maxdiffy >= 0.1) || (maxdiffz >= 0.1))
                            {
                                slowmotioncount++;
                            }
                            else
                            {
                                slowmotioncount = 0;
                            }
                        }
                        
                        lastax = ax;
                        lastay = ay;
                        lastaz = az;
                    }
                    
                    avgx /= IMU_NUMBER_FRAMES;
                    avgy /= IMU_NUMBER_FRAMES;
                    avgz /= IMU_NUMBER_FRAMES;
                    
                    //sprintf(str, "Ax/y/z=%.3f %.3f %.3f / %.3f %.3f %.3f / %.3f %.3f %.3f\r\n", minx, maxx, avgx, miny, maxy, avgy, minz, maxz, avgz);
                    //status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);

                    //sprintf(str, "FF %d %d\r\n", freefallcount, ffcountmax);
                    //status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);

                    sprintf(str, "MaxDiffAx/y/z=%.3f %.3f %.3f   Slow/No=%d %d\r\n", maxdiffx, maxdiffy, maxdiffz, slowmotioncount, nomotioncount);
                    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
#else
                    ++lineCount;
                    if (lineCount >= 16)
                    {
                        lineCount = 0;
                        status = HAL_UART_Transmit(&huart1, ".\r\n", 3, HAL_MAX_DELAY);
                    }
                    else
                    {
                        status = HAL_UART_Transmit(&huart1, ".", 1, HAL_MAX_DELAY);
                    }
#endif
                    endTicks = startTicks;
                    
                    uint32_t fwmInterval = (thisFwmTick - lastFwmTick);
                    if (fwmInterval > 750)
                    {
                        missedFwmCount++;
                        sprintf(str, "MsWm=%d %lu\r\n", missedFwmCount, fwmInterval);
                        status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
                    }
                    lastFwmTick = thisFwmTick;
                }
                
                if (imuInterruptStatus & BMI160_INTSTATUS_FFULLINT)
                {
                    /* FIFO full, we've dropped samples */
                    strcpy(str, "FFULL\r\n");
                    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
                }

                if (imuInterruptStatus & BMI160_INTSTATUS_LOWGINT)
                {
                    strcpy(str, "LOWG\r\n");
                    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
                }

                if (imuInterruptStatus & BMI160_INTSTATUS_HIGHGINT)
                {
                    char direction;
                    char axis;
                    
                    if (imuInterruptStatus & BMI160_INTSTATUS_HIGHSIGN)
                    {
                        direction = '-';
                    }
                    else
                    {
                        direction = '+';
                    }
                    
                    switch ((imuInterruptStatus & (BMI160_INTSTATUS_HIGHFIRSTX | BMI160_INTSTATUS_HIGHFIRSTY | BMI160_INTSTATUS_HIGHFIRSTZ)) >> 24)
                    {
                        case 1:
                            axis = 'X';
                            break;
                        case 2:
                            axis = 'Y';
                            break;
                        case 4:
                            axis = 'Z';
                            break;
                        default:
                            axis = '#';
                            break;
                    }
                    
                    sprintf(str, "HIGH %c%c\r\n", direction, axis);
                    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
                }
                
                /* No motion and slow motion use the same interrupt */
                if (imuInterruptStatus & BMI160_INTSTATUS_NOMOINT)
                {
                    strcpy(str, "No motion\r\n");
                    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);

                    //strcpy(str, "Slow motion\r\n");
                    //status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
                }
                
                
                /* Any motion and significant motion are mutually exclusive */
                if (imuInterruptStatus & BMI160_INTSTATUS_ANYMINT)
                {
                    char direction;
                    char axis;
                    
                    if (imuInterruptStatus & BMI160_INTSTATUS_ANYMSIGN)
                    {
                        direction = '-';
                    }
                    else
                    {
                        direction = '+';
                    }
                    
                    switch ((imuInterruptStatus & (BMI160_INTSTATUS_ANYMFIRSTX | BMI160_INTSTATUS_ANYMFIRSTY | BMI160_INTSTATUS_ANYMFIRSTZ)))
                    {
                        case BMI160_INTSTATUS_ANYMFIRSTX:
                            axis = 'X';
                            break;
                        case BMI160_INTSTATUS_ANYMFIRSTY:
                            axis = 'Y';
                            break;
                        case BMI160_INTSTATUS_ANYMFIRSTZ:
                            axis = 'Z';
                            break;
                        default: /* Combination - surely not valid? */
                            axis = '#';
                            break;
                    }
                    
                    sprintf(str, "AnyMo %c%c\r\n", direction, axis);
                    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
                }
                
                /* Significant motion shares the any motion threshold register,
                 * but has its own flag bit, which just indicates that motion
                 * happened, not how.
                 */
                if (imuInterruptStatus & BMI160_INTSTATUS_SIGMOTINT)
                {
                    sprintf(str, "SigMo\r\n");
                    status = HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
                }
                
                irqLoopCount++;
                //imuInterruptStatus = Bmi160_GetInterruptStatus();
                imuInterruptStatus = 0;
            }
            
            cumIntLoopCount += (irqLoopCount - 1);
        }
        
        /* Must wake twice in quick succession as there are two bursts of SPI
         * activity, 8 clocks then 16 clocks, as two blocks of CE, every 1ms.
         * This would match Bmi160_GetFifoCount() getting called.
         */
        __WFE();
        __NOP();
        
        loopsBeforeImuEvent++;

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
        //__WFE();
        //__NOP();
        //Sleep(1000);
    }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Supply configuration update enable
  */
  HAL_PWREx_ConfigSupply(PWR_DIRECT_SMPS_SUPPLY);
  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 75;
  RCC_OscInitStruct.PLL.PLLP = 2;
  RCC_OscInitStruct.PLL.PLLQ = 20;
  RCC_OscInitStruct.PLL.PLLR = 6;
  RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_3;
  RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
  RCC_OscInitStruct.PLL.PLLFRACN = 0;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV2;
  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* Put the ISR here so that CubeMX settings cannot remove it by simply trying to
 * not have it active on startup.
 */
void EXTI2_IRQHandler(void)
{
    static int count = 0;
    
    /* USER CODE BEGIN EXTI2_IRQn 0 */
    /* This is the HAL function called by the handler by default. It combines
     * any EXTI interrupt into one callback, so that your code then needs to
     * check what value and dispatch instead of just doing it from the interrupt.
     * Note that it does handle the dual core and CM4 as the registers are
     * different.
     */
#if 0
#if defined(DUAL_CORE) && defined(CORE_CM4)
  if (__HAL_GPIO_EXTID2_GET_IT(GPIO_Pin) != 0x00U)
  {
    __HAL_GPIO_EXTID2_CLEAR_IT(GPIO_Pin);
    HAL_GPIO_EXTI_Callback(GPIO_Pin);
  }
#else
  /* EXTI line interrupt detected */
  if (__HAL_GPIO_EXTI_GET_IT(GPIO_Pin) != 0x00U)
  {
    __HAL_GPIO_EXTI_CLEAR_IT(GPIO_Pin);
    HAL_GPIO_EXTI_Callback(GPIO_Pin);
  }
#endif
#endif
    /* Original generated code was as below, see above for HAL function that has
     * been supplanted into the ISR here.
     */
#if 0
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
#endif
    
    /* USER CODE END EXTI2_IRQn 0 */
    /* USER CODE BEGIN EXTI2_IRQn 1 */

    /* Note that we can use the user label for the pin because each pin number
     * is connected to the same EXTI line. So for example, PA2, PB2, PC2 etc
     * all connect to EXTI2 and the user pin labels would all be GPIO_PIN_2.
     */
#if defined(DUAL_CORE) && defined(CORE_CM4)
    if (__HAL_GPIO_EXTID2_GET_IT(IMU_INT1_Pin) != 0x00U)
    {
        __HAL_GPIO_EXTID2_CLEAR_IT(IMU_INT1_Pin);
#else
    if (__HAL_GPIO_EXTI_GET_IT(IMU_INT1_Pin) != 0x00U)
    {
        __HAL_GPIO_EXTI_CLEAR_IT(IMU_INT1_Pin);
#endif

        count = 1 - count;
        if (count == 0)
        {
            count = 0;
        }
        else
        {
            count = 1;
        }
        
        HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
        
        /* Bring in local definition of flag setter and call it, rather
         * than expose the entire module definitions.
         */
        void Bmi160_SetImuEvent(void);
        Bmi160_SetImuEvent();
    }
    
    /* USER CODE END EXTI2_IRQn 1 */
}


/* Copy here so Cube doesn't wreck the function when it is enabled or disabled. */
void EXTI9_5_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI9_5_IRQn 0 */
    /* Original generated code was as below, see above for HAL function that has
     * been supplanted into the ISR here.
     */
#if 0
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_5);
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_7);
#endif
    
  /* USER CODE END EXTI9_5_IRQn 0 */
  /* USER CODE BEGIN EXTI9_5_IRQn 1 */

  /* USER CODE END EXTI9_5_IRQn 1 */
}

/* USER CODE END 4 */

/* MPU Configuration */

void MPU_Config(void)
{

  /* Disables the MPU */
  HAL_MPU_Disable();
  /* Enables the MPU */
  HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);

}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
