#ifndef EEPROM_EMUL_MEMORYINTERFACE_H
#define EEPROM_EMUL_MEMORYINTERFACE_H


#include "stm32h7xx_hal.h"


#define FLASHWORD_SIZE                  (FLASH_NB_32BITWORD_IN_FLASHWORD * 32 / 8)
#define NUMBER_FLASHWORDS               (FLASH_SECTOR_SIZE / FLASHWORD_SIZE)


int EEPROM_NumberSectors(void);
uint32_t EEPROM_BaseValue(void);
uint32_t EEPROM_Size(void);


#endif
