#ifndef INTERNALFLASHERASE_H
#define INTERNALFLASHERASE_H

/*
 *  Declarations for erasing MCU internal flash memory.
 */

#include "stm32h7xx_hal.h"
#include <stdint.h>
#include <stdbool.h>

HAL_StatusTypeDef FlashEraseSectorByAddress(uint32_t flashAddress);
bool FlashSectorBlankCheck(uint32_t flashAddress);

#endif
