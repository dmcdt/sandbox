#ifndef CALCULATECRC_H
#define CALCULATECRC_H


#include <stdint.h>

/* Initialise and calculate Modbus CRC */
uint16_t CRCCalculate_ModbusRtu(uint8_t const *data, uint16_t length);

/* Initialise for performing the Modbus CRC */
uint16_t CRCInitialise_ModbusRtu(void);

/* Update the CRC calculation for a single byte */
uint16_t CRCUpdate_ModbusRtu(uint8_t data);


#endif
