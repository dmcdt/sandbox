#ifndef EEPROM_EMUL_H
#define EEPROM_EMUL_H


/*
 *  Own implementation of an emulated EEPROM using MCU internal flash.
 * 
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 */


#include <stdint.h>


/* Maximum number of bytes that can be stored per variable */
#define EEPROM_VARIABLE_SIZE            (28)


/* The range that variable IDs can be selected from. */
#define EEPROM_VARIABLE_ID_MINIMUM      (0x0001)
#define EEPROM_VARIABLE_ID_MAXIMUM      (0x03FE)


/* Return codes from the EEPROM emulation functions. */
typedef enum EEPROMStatus
{
    EE_OK,              /* Operation completed successfully */
    EE_LockError,       /* Locking or unlocking internal flash failed, MCU probably needs reset to operate correctly. */
    
    EE_EraseError,      /* Error erasing */
    EE_ProgramError,    /* Error programming, further details from HAL_FLASH_GetError() */
    EE_VeryifyError,    /* Contents of memory do not match what was written */
    EE_NotFound,        /* Variable not found */
    EE_BadChecksum,     /* Calculated checksum does not match stored checksum */

    EE_BadParameter,    /* At least one parameter to the function is incorrect */
    EE_BadStorage,      /* No more storage space (e.g. all sectors bad) */
} EEPROMStatus;


/* Public function declarations */
EEPROMStatus EEPROM_Init(void);
EEPROMStatus EEPROM_WriteVariable(uint16_t variable, uint8_t *data, uint16_t length);
EEPROMStatus EEPROM_ReadVariable(uint16_t variable, uint8_t *data, uint16_t length);


#endif
