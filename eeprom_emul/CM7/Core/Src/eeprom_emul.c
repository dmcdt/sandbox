/*
 *  EEPROM emulation functionality in MCU internal flash.
 *
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 * 
 *  Provides a way to emulate EEPROM storage of variables in internal flash.
 *  Based on the concept of storing "variables" within successive flash
 *  locations, each location measured in units of a flashword (smallest writable
 *  area of internal flash).
 *
 *  Variables are written to successive locations, with the latest value being
 *  the latest write for that variable in the memory area. For example:
 *      Location 0: Variable => Value
 *      Location 1: Variable => Value 2     <- this is the value of "Variable"
 *
 *  The flashwords in memory contain the variable state, identifier, value, and
 *  checksum. The checksum is an additional protection, since the MCU flash has
 *  built in SECDED ECC. The state allows some limited recovery/handling from
 *  interruptions (reset, power failure, etc) when swqapping sectors.
 *
 *  When the flash sector fills up, the next flash sector is erased and the current
 *  variable values are copied across.
 *
 *  This scheme has the following benefits:
 *      Multiple writes to a variable are wear levelled, giving endurance more like real EEPROM
 *      The flash memory sector is written sequentially (minimise program disturb)
 *      Each flashword is written only once (minimise program disturb)
 *
 *  For example, given a sector size of 128KiB and flashword size of 32B (the
 *  values for STM32H745), you can write 4096 times before erasing the flash
 *  sector (although really 4096 * number of sectors used for emulated EEPROM).
 *
 *  Each flash sector has 10k cycles of endurance (from the device datasheet).
 *  Depending on the number of variables you want to store you can calculate the
 *  endurance of the emulated EEPROM. If it were 1 variable, it would be
 *  4096 * number sectors * 10k. In a case where you want equal endurance over all
 *  variables then it would be:
 *      4096 * number sectors * 10k / number variables^2
 *  
 *  However, not all variables are going to be written as often as each other
 *  so the endurance will be linear in how many time any variable is update.
 *
 *  CAUTION: Do not write to variables at high temperatures, you run the risk
 *  of worse data retention or erasing a page at high temperature which is
 *  not recommended.
 */

#include "eeprom_emul.h"

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdalign.h>
#include <stdlib.h>

#include "stm32h7xx_hal.h"
#include "CommonDefinitions.h"
#include "CompilerSpecific.h"
#include "eeprom_emul_memoryinterface.h"
#include "InternalFlashErase.h"
#include "CalculateCRC.h"


#define EEPROM_VARIABLE_STATEID_SIZE    (2)
#define EEPROM_VARIABLE_CRC_SIZE        (2)
#if ((EEPROM_VARIABLE_STATEID_SIZE + EEPROM_VARIABLE_SIZE + EEPROM_VARIABLE_CRC_SIZE) > FLASHWORD_SIZE)
#error "All EEPROM variable data must fit with one flashword."
#endif

/* The first 16 bits of the flashword contain the state and ID of the variable.
 * A quick full value check can see whether the flashword (sector?) is bad or erased.
 */
#define EEPROM_FLASHWORD_BAD            (0x0000)
#define EEPROM_FLASHWORD_ERASED         (0xFFFF)

/*  The size of the state and ID have been chosen to have reasonable number of
 *  variables that *could* all fit within a single sector. In the case where
 *  only two sectors are used for storage, it would not be good to get into a
 *  state where a variable's current value could be erased (to start writing to
 *  the next sector) and then something happens to prevent the copyback from
 *  happening. So idea is that all variables should be able to fit into a single
 *  sector, probably at least twice (once for initial value, once for an update).
 *
 *  For instance, there are 4096 flashwords per sector. Assume no more than a
 *  quarter as variables (gives initial value + three updates), so 1024 variables.
 */
#define EEPROM_VARIABLE_STATE_Msk       (0xFC00)
#define EEPROM_VARIABLE_ID_Msk          (0x03FF)

#define EEPROM_VARIABLE_STATE_ERASED    (0xFC00)    /* Variable state is erased, do not use in code */
#define EEPROM_VARIABLE_STATE_WRITTEN   (0xA800)
#define EEPROM_VARIABLE_STATE_COPYBACK  (0x5400)
#define EEPROM_VARIABLE_STATE_BAD       (0x0000)    /* Variable state is bad, do not use in code */

#define EEPROM_VARIABLE_ID_BAD          (0x0000)    /* Variable ID is bad, do not use in code */
#define EEPROM_VARIABLE_ID_ERASED       (0x03FF)    /* Variable ID is erased, do not use in code */


/* Structure for the internal variables rquired by the EEPROM emulation layer
 * (except the flashword buffer).
 */
typedef struct EEPROMContext
{
    int         sector;     /* Currently active sector */
    uint16_t    nextIndex;  /* Next flashword index within the sector to write to */
} EEPROMContext;


/* Internal functions */
int EEPROM_FindCurrentSector(void);
uint16_t EEPROM_FindNextIndexWithinSector(int sector);
void EEPROM_ScanForVariableIndices(int sector, uint16_t endIndex);
EEPROMStatus EEPROM_CopyVariables(int toSector, uint16_t exceptVariableId, uint16_t *nextIndex);
void EEPROM_PrepareBuffer(uint16_t stateId, uint8_t const *data, uint16_t length);
uint32_t EEPROM_CalculateFlashAddress(int sector, uint16_t flashwordIndex);
EEPROMStatus EEPROM_Erase(uint32_t flashAddress);
EEPROMStatus EEPROM_Write(uint32_t flashAddress, uint32_t dataAddress);
EEPROMStatus EEPROM_Read(uint32_t flashAddress, uint8_t *dataAddress);
bool EEPROM_IsDataLengthValid(uint16_t length);
bool EEPROM_IsVariableIdValid(uint16_t variable);


/*  This buffer is used to write to a single flashword to flash memory, so it
 *  must be 32-bit aligned.
 */
NOINIT alignas(4) uint8_t gFlashWordBuffer[FLASHWORD_SIZE];

/* The context (all the internal variables) for managed the EEPROM emulation. */
EEPROMContext   gEepromContext;

/* Index into the EEPROM are (in flashwords) for each variable */
NOINIT uint16_t    gVariableFlashIndex[EEPROM_VARIABLE_ID_MAXIMUM - EEPROM_VARIABLE_ID_MINIMUM + 1];


EEPROMStatus EEPROM_Init(void)
{
    EEPROMStatus   status = EE_OK;
    
    /* TODO - find actual next sector and flashword index */
    gEepromContext.sector = EEPROM_FindCurrentSector();
    if (gEepromContext.sector != -1)
    {
        gEepromContext.nextIndex = EEPROM_FindNextIndexWithinSector(gEepromContext.sector);
        
        /* Clear all variables. Index of 0xFFFF is invalid. */
        memset(gVariableFlashIndex, 0xFF, sizeof gVariableFlashIndex);
        
        /* TODO - Find all the indexes to the latest version of the supported variables? */
        /* Is that worth spending the RAM on? Would only help speed up read access,
         * but might make life easier when copying back a sector.
         * 1024 * 2 (uint16 - as long as 4096 * NUMBER_PAGES < 65536, which it would have to be there are only 16 pages in flash)?
         */
        if (gEepromContext.nextIndex > 0)
        {
            EEPROM_ScanForVariableIndices(gEepromContext.sector, gEepromContext.nextIndex);
        }
    }
    else
    {
        status = EE_BadStorage;
    }
    
    return status;
}


int EEPROM_FindCurrentSector(void)
{
    /* TODO - how to find when a sector is full on a fresh restart? */
    /* For the sake of simplicity, leave the last flashword in each page erased
     * except when need to show that page is full.
     */
    
    int     sector = -1;
    
    uint16_t *sectorStartIdState;
    uint16_t *sectorEndIdState;
    
    /* TODO - need more advanced checking here to handle the case of errors where copyback is interrupted */
    /* For example, when the sector loops round and it has copied a few of the
     * old values to the new sector, but not all. What to do at that point?
     * Should mostly use the old page, as they will all be OK except the value
     * that caused the write.
     */
    for (int sectorIndex = 0; (sectorIndex < EEPROM_NumberSectors()) && (sector == -1); sectorIndex++)
    {
        sectorStartIdState = (uint16_t *)EEPROM_CalculateFlashAddress(sectorIndex, 0);
        sectorEndIdState = (uint16_t *)EEPROM_CalculateFlashAddress(sectorIndex, NUMBER_FLASHWORDS - 1);

        if ((*sectorStartIdState != EEPROM_FLASHWORD_ERASED) &&
            (*sectorEndIdState == EEPROM_FLASHWORD_ERASED))
        {
            sector = sectorIndex;
        }
    }

    /* If not found, check whether first page completely empty */
    if (sector == -1)
    {
        sectorStartIdState = (uint16_t *)EEPROM_CalculateFlashAddress(0, 0);
        sectorEndIdState = (uint16_t *)EEPROM_CalculateFlashAddress(0, NUMBER_FLASHWORDS - 1);

        if ((*sectorStartIdState == EEPROM_FLASHWORD_ERASED) &&
            (*sectorEndIdState == EEPROM_FLASHWORD_ERASED))
        {
            sector = 0;
        }
    }
    
    return sector;
}


uint16_t EEPROM_FindNextIndexWithinSector(int sector)
{
    uint16_t    nextIndex = 0xFFFFU;
    
    uint16_t *flashwordIdState;
    
    for (uint16_t index = 0; (index < NUMBER_FLASHWORDS) && (nextIndex == 0xFFFFU); index++)
    {
        flashwordIdState = (uint16_t *)EEPROM_CalculateFlashAddress(sector, index);
        if (*flashwordIdState == EEPROM_FLASHWORD_ERASED)
        {
            nextIndex = index;
        }
    }
    
    return nextIndex;
}


void EEPROM_ScanForVariableIndices(int sector, uint16_t endIndex)
{
    uint32_t    flashAddress;
    uint16_t    crcCalculated;
    uint16_t    crcStored;
    
    for (uint16_t flashwordIndex = 0; flashwordIndex < endIndex; flashwordIndex++)
    {
        flashAddress = EEPROM_CalculateFlashAddress(sector, flashwordIndex);
        crcCalculated = CRCCalculate_ModbusRtu((uint8_t *)flashAddress, FLASHWORD_SIZE - EEPROM_VARIABLE_CRC_SIZE);
        crcStored = *(uint16_t *)(flashAddress + FLASHWORD_SIZE - EEPROM_VARIABLE_CRC_SIZE);
        if (crcCalculated == crcStored)
        {
            uint16_t stateId;
            uint16_t variableState;
            uint16_t variableId;
            int variableIndex;

            stateId = *((uint16_t *)flashAddress);
            variableState = stateId & EEPROM_VARIABLE_STATE_Msk;
            variableId = stateId & EEPROM_VARIABLE_ID_Msk;

            if ((variableState == EEPROM_VARIABLE_STATE_WRITTEN) ||
                (variableState == EEPROM_VARIABLE_STATE_COPYBACK))
            {
                variableIndex = variableId - EEPROM_VARIABLE_ID_MINIMUM;
                gVariableFlashIndex[variableIndex] = sector * NUMBER_FLASHWORDS + flashwordIndex;
            }
        }
    }
}


EEPROMStatus EEPROM_WriteVariable(uint16_t variableId, uint8_t *data, uint16_t length)
{
    EEPROMStatus    eeStatus;

    if ((EEPROM_IsVariableIdValid(variableId)) &&
        (data) && EEPROM_IsDataLengthValid(length))
    {
        if (gEepromContext.nextIndex == (NUMBER_FLASHWORDS - 1))
        {
            int                 nextSector;
            uint16_t            nextIndex;
            uint32_t            flashAddress;
            uint16_t            stateId;

            nextSector = (gEepromContext.sector + 1) % EEPROM_NumberSectors();

            /* Erase next sector. */
            flashAddress = EEPROM_CalculateFlashAddress(nextSector, 0);
            eeStatus = EEPROM_Erase(flashAddress);
            if (eeStatus == EE_OK)
            {
                /* Copy all variables other than this one to the new sector. */
                eeStatus = EEPROM_CopyVariables(nextSector, variableId, &nextIndex);
                if (eeStatus == EE_OK)
                {
                    /* Write the updated variable that caused this function call. */
                    stateId = EEPROM_VARIABLE_STATE_WRITTEN | (variableId & EEPROM_VARIABLE_ID_Msk);
                    EEPROM_PrepareBuffer(stateId, data, length);
                    flashAddress = EEPROM_CalculateFlashAddress(nextSector, nextIndex);
                    eeStatus = EEPROM_Write(flashAddress, (uint32_t)gFlashWordBuffer);
                    nextIndex++;
                    if (eeStatus == EE_OK)
                    {
                        /* Mark old page as full. */
                        uint16_t pageFullmarker;
                        uint8_t erasedData;
                        pageFullmarker = EEPROM_VARIABLE_STATE_WRITTEN | EEPROM_VARIABLE_ID_ERASED;
                        erasedData = 0xFF;
                        EEPROM_PrepareBuffer(pageFullmarker, &erasedData, 1);
                        flashAddress = EEPROM_CalculateFlashAddress(gEepromContext.sector, gEepromContext.nextIndex);
                        eeStatus = EEPROM_Write(flashAddress, (uint32_t)gFlashWordBuffer);
                        if (eeStatus == EE_OK)
                        {
                            /* Update all variable indices to the new ones. Could possibly have
                            * done it when copying across but a failure in the middle of that
                            * may have meant inconsistency in the indices.
                            */
                            EEPROM_ScanForVariableIndices(nextSector, nextIndex);

                            gEepromContext.sector = nextSector;
                            gEepromContext.nextIndex = nextIndex;
                        } /* Page full mark written OK */
                    }
                }
            }
        }
        else
        {
            /* Copy the details into the flashword buffer to ensure it is aligned for writing */
            uint16_t stateid;
            stateid = EEPROM_VARIABLE_STATE_WRITTEN | (variableId & EEPROM_VARIABLE_ID_Msk);
            EEPROM_PrepareBuffer(stateid, data, length);

            uint32_t flashAddress;
            flashAddress = EEPROM_CalculateFlashAddress(gEepromContext.sector, gEepromContext.nextIndex);
            eeStatus = EEPROM_Write(flashAddress, (uint32_t)gFlashWordBuffer);

            /* TODO - Verify */
            
            /* Update indices and move onto next flashword */
            int variableIndex;
            variableIndex = variableId - EEPROM_VARIABLE_ID_MINIMUM;
            gVariableFlashIndex[variableIndex] = gEepromContext.sector * NUMBER_FLASHWORDS + gEepromContext.nextIndex;
            gEepromContext.nextIndex++;
        }
    }
    else
    {
        eeStatus = EE_BadParameter;
    }
    
    return eeStatus;
}


EEPROMStatus EEPROM_CopyVariables(int toSector, uint16_t exceptVariableId, uint16_t *nextIndex)
{
    EEPROMStatus    eeStatus = EE_OK;
    uint16_t        newIndexInSector;

    newIndexInSector = 0;

    for (int variableId = EEPROM_VARIABLE_ID_MINIMUM; (variableId <= EEPROM_VARIABLE_ID_MAXIMUM) && (eeStatus == EE_OK); variableId++)
    {
        if (variableId != exceptVariableId)
        {
            int         variableIndex;
            uint16_t    oldFlashwordIndex;

            variableIndex = variableId - EEPROM_VARIABLE_ID_MINIMUM;
            oldFlashwordIndex = gVariableFlashIndex[variableIndex];
            if (oldFlashwordIndex != 0xFFFFU)
            {
                uint32_t    oldFlashAddress;
                uint32_t    newFlashAddress;
                uint16_t    copyStateId;
                uint16_t    crc;
                
                oldFlashAddress =  EEPROM_BaseValue() + oldFlashwordIndex * FLASHWORD_SIZE;

                /* Updated state and variable ID */
                copyStateId = EEPROM_VARIABLE_STATE_COPYBACK | variableId;
                memcpy(&gFlashWordBuffer[0], &copyStateId, EEPROM_VARIABLE_STATEID_SIZE);

                /* Get the data into the buffer */
                memcpy(&gFlashWordBuffer[2], (void *)(oldFlashAddress + EEPROM_VARIABLE_STATEID_SIZE), EEPROM_VARIABLE_SIZE);

                /* New CRC for the updated state */
                crc = CRCCalculate_ModbusRtu(gFlashWordBuffer, FLASHWORD_SIZE - EEPROM_VARIABLE_CRC_SIZE);
                memcpy(&gFlashWordBuffer[FLASHWORD_SIZE - EEPROM_VARIABLE_CRC_SIZE], &crc, EEPROM_VARIABLE_CRC_SIZE);

                /* Calculate new location */
                newFlashAddress = EEPROM_CalculateFlashAddress(toSector, newIndexInSector);

                /* Write it to new location */
                eeStatus = EEPROM_Write(newFlashAddress, (uint32_t)gFlashWordBuffer);

                newIndexInSector++;
            }
        }
    }

    *nextIndex = newIndexInSector;

    return eeStatus;
}


void EEPROM_PrepareBuffer(uint16_t stateId, uint8_t const *data, uint16_t length)
{
    uint16_t    crc;

    memcpy(&gFlashWordBuffer[0], &stateId, EEPROM_VARIABLE_STATEID_SIZE);
    memcpy(&gFlashWordBuffer[EEPROM_VARIABLE_STATEID_SIZE], data, length);
    if (length < EEPROM_VARIABLE_SIZE)
    {
        memset(&gFlashWordBuffer[EEPROM_VARIABLE_STATEID_SIZE + length], 0xFF, EEPROM_VARIABLE_SIZE - length);
    }
    crc = CRCCalculate_ModbusRtu(gFlashWordBuffer, FLASHWORD_SIZE - EEPROM_VARIABLE_CRC_SIZE);
    memcpy(&gFlashWordBuffer[FLASHWORD_SIZE - EEPROM_VARIABLE_CRC_SIZE], &crc, EEPROM_VARIABLE_CRC_SIZE);
}


EEPROMStatus EEPROM_Erase(uint32_t flashAddress)
{
    EEPROMStatus        eeStatus = EE_OK;
    HAL_StatusTypeDef   halStatus;

    halStatus = HAL_FLASH_Unlock();
    if (halStatus == HAL_OK)
    {
        halStatus = FlashEraseSectorByAddress(flashAddress);
        if (halStatus != HAL_OK)
        {
            eeStatus = EE_EraseError;
        }

        /* Always want to re-lock the flash regardless of whether the previous
         * operation failed.
         */
        halStatus = HAL_FLASH_Lock();
        if (halStatus != HAL_OK)
        {
            /* Bank still locked, only use the first error code. Can (should) always
             * check whether the lock failed by inspecting the bits if you
             * get a program error returned from this function.
             */
            if (eeStatus == EE_OK)
            {
                eeStatus = EE_LockError;
            }
        }
    }
    else
    {
        /* Bank still locked */
        eeStatus = EE_LockError;
    }
    
    return eeStatus;
}

EEPROMStatus EEPROM_Write(uint32_t flashAddress, uint32_t dataAddress)
{
    EEPROMStatus        eeStatus = EE_OK;
    HAL_StatusTypeDef   halStatus;

    halStatus = HAL_FLASH_Unlock();
    if (halStatus == HAL_OK)
    {
        halStatus = HAL_FLASH_Program(FLASH_TYPEPROGRAM_FLASHWORD, flashAddress, dataAddress);
        if (halStatus != HAL_OK)
        {
            eeStatus = EE_ProgramError;
        }

        /* Always want to re-lock the flash regardless of whether the previous
         * failed.
         */
        halStatus = HAL_FLASH_Lock();
        if (halStatus != HAL_OK)
        {
            /* Bank still locked, only use the first error code. Can (should) always
             * check whether the lock failed by inspecting the bits if you
             * get a program error returned from this function.
             */
            if (eeStatus == EE_OK)
            {
                eeStatus = EE_LockError;
            }
        }
    }
    else
    {
        /* Bank still locked */
        eeStatus = EE_LockError;
    }
    
    return eeStatus;
}


EEPROMStatus EEPROM_ReadVariable(uint16_t variableId, uint8_t *data, uint16_t length)
{
    EEPROMStatus    eeStatus = EE_OK;

    if ((EEPROM_IsVariableIdValid(variableId)) &&
        (data) && EEPROM_IsDataLengthValid(length))
    {
        int variableIndex;
        
        variableIndex = variableId - EEPROM_VARIABLE_ID_MINIMUM;
        if (gVariableFlashIndex[variableIndex] != 0xFFFFU)
        {
            uint32_t    flashAddress;
            uint16_t    crcCalculated;
            uint16_t    crcStored;
            
            flashAddress = EEPROM_BaseValue() + gVariableFlashIndex[variableIndex] * FLASHWORD_SIZE;

            /* Always copy the data, then the caller can see the bad data and the fact the checksum is wrong */
            memcpy(data, (void *)(flashAddress + EEPROM_VARIABLE_STATEID_SIZE), length);

            crcCalculated = CRCCalculate_ModbusRtu((uint8_t *)flashAddress, FLASHWORD_SIZE - EEPROM_VARIABLE_CRC_SIZE);
            crcStored = *(uint16_t *)(flashAddress + FLASHWORD_SIZE - EEPROM_VARIABLE_CRC_SIZE);
            if (crcCalculated != crcStored)
            {
                eeStatus = EE_BadChecksum;
            }
        }
        else
        {
            eeStatus = EE_NotFound;
        }
    }
    else
    {
        eeStatus = EE_BadParameter;
    }
    
    return eeStatus;
}


bool EEPROM_IsDataLengthValid(uint16_t length)
{
    return ((length > 0) && (length <= EEPROM_VARIABLE_SIZE));
}


bool EEPROM_IsVariableIdValid(uint16_t variable)
{
    return ((variable >= EEPROM_VARIABLE_ID_MINIMUM) &&
            (variable <= EEPROM_VARIABLE_ID_MAXIMUM));
}


uint32_t EEPROM_CalculateFlashAddress(int sectorNumber, uint16_t sectorFlashwordIndex)
{
    return EEPROM_BaseValue() + sectorNumber * FLASH_SECTOR_SIZE + sectorFlashwordIndex * FLASHWORD_SIZE;
}


