/*
 *  Functionality to encapsulate erasing MCU internal flash memory.
 * 
 *  This is required because the HAL provides an inconsistent API when
 *  programming and erasing is compared. Programming is OK, the caller passes
 *  in the address to write to, and the data. The erase is cumbersome, requiring
 *  the application to be aware of whether the MCU is dual code, whether there
 *  are dual banks, which bank the sector to erase is in, and whether the banks
 *  have been swapped.
 */

#include "InternalFlashErase.h"

#include <stdint.h>
#include <stdbool.h>

#include "stm32h7xx.h"
#include "stm32h7xx_hal_flash.h"
#include "stm32h7xx_hal_flash_ex.h"

#include "IntrinsicsWrapper.h"


uint32_t GetBankNumber(uint32_t flashAddress);
bool IsSwappedBanks(void);
uint32_t GetSectorNumber(uint32_t flashAddress);


HAL_StatusTypeDef FlashEraseSectorByAddress(uint32_t flashAddress)
{
    HAL_StatusTypeDef       halStatus;
    FLASH_EraseInitTypeDef  eraseParams;
    uint32_t                sectorError;    /* Sector number that failed to erase */

    eraseParams.TypeErase = FLASH_TYPEERASE_SECTORS;
    eraseParams.Banks = GetBankNumber(flashAddress); /* Bank MASK! We only deal with a single bank, which depends on address and bank swap bit */
    eraseParams.Sector = GetSectorNumber(flashAddress); /* Sector number WITHIN BANK */
    eraseParams.NbSectors = 1;
    eraseParams.VoltageRange = FLASH_VOLTAGE_RANGE_4;   /* This could be reduced to save power at expense of runtime */

    halStatus = HAL_FLASHEx_Erase(&eraseParams, &sectorError);
    /* We are only erasing a single sector here, so if the return code indicates
     * an error then we know which sector failed the erase.
     */

    /* Ensure the caches will not have stale data after the erase. */
    SCB_InvalidateICache();
    SCB_InvalidateDCache();

    if (!FlashSectorBlankCheck(flashAddress))
    {
        halStatus = HAL_ERROR;
    }
    
    return halStatus;
}


uint32_t GetBankNumber(uint32_t flashAddress)
{
    uint32_t    bank;

    if (IsSwappedBanks())
    {
        if (flashAddress < FLASH_BANK2_BASE)
        {
            bank = FLASH_BANK_2;
        }
        else
        {
            bank = FLASH_BANK_1;
        }
    }
    else
    {
        if (flashAddress < FLASH_BANK2_BASE)
        {
            bank = FLASH_BANK_1;
        }
        else
        {
            bank = FLASH_BANK_2;
        }
    }

    return bank;
}


bool IsSwappedBanks(void)
{
    /* Contrary to what it says in the reference manual, setting the option
     * byte to swap the banks does not seem to make a difference for the
     * erase of memory? I swapped the banks, erased the memory, and it did not
     * erase at the address I thought. Taking out the check below and it always
     * erased at the correct address.
     *
     * Code below is left in, just in case it needs restored.
     */
    //return !!(READ_BIT(FLASH->OPTCR, FLASH_OPTCR_SWAP_BANK));
    return false;
}


uint32_t GetSectorNumber(uint32_t flashAddress)
{
    uint32_t    sector;

    sector = ((flashAddress - FLASH_BASE) / FLASH_SECTOR_SIZE) % FLASH_SECTOR_TOTAL;

    return sector;
}


bool FlashSectorBlankCheck(uint32_t flashAddress)
{
    bool        isBlank = true;
    uint32_t    *check;
    
    /* Round down to the sector start address. */
    flashAddress = ((flashAddress - FLASH_BASE) / FLASH_SECTOR_SIZE) * FLASH_SECTOR_SIZE + FLASH_BASE;
    
    check = (uint32_t *)flashAddress;
    
    while ((check < (uint32_t *)(flashAddress + FLASH_SECTOR_SIZE)) &&
           (isBlank))
    {
        if (*check++ != 0xFFFFFFFFUL)
        {
            isBlank = false;
        }
    }
    
    return isBlank;
}

