/*
 *  CRC Calculation functions.
 *
 *  These functions calculate various CRCs and use the CRC peripheral.
 */
 
#include "CalculateCRC.h"

#include <stm32h7xx.h>


/* Modbus RTU CRC:
 *      Poly:               0x8005 (Modbus std lists it as 0xA001 but calculation only seems to work with 0x8005, which is 0xA001 bit reversed.
 *      Initial value:      0xFFFF
 *      Input reflection:   Yes (per byte)
 *      Output reflection:  Yes
 *      Output XOR:         0x0000
 */
#define CRC_MODBUS_IV   (0xFFFFU)
#define CRC_MODBUS_POLY (0x8005U)


uint16_t CRCCalculate_ModbusRtu(uint8_t const *data, uint16_t length)
{
    CRCInitialise_ModbusRtu();

    /* Define own pointer to the CRC data register because calculation depends
     * on write data width - we need to force bytes here, not words */
    __IO uint8_t *dr = (__IO uint8_t *)&CRC->DR;
    
    /* Push all data into the CRC data register */
    while (length--)
    {
        *dr = *data++;
    }
    
    /* and return the result */
    return CRC->DR;
}


uint16_t CRCInitialise_ModbusRtu(void)
{
    CRC->INIT = CRC_MODBUS_IV;
    CRC->CR = CRC_CR_REV_OUT | CRC_CR_REV_IN_0 | CRC_CR_POLYSIZE_0 | CRC_CR_RESET;
    CRC->POL = CRC_MODBUS_POLY;
    
    return CRC->DR;
}


uint16_t CRCUpdate_ModbusRtu(uint8_t data)
{
    /* Define own pointer to the CRC data register because calculation depends
     * on write data width - we need to force bytes here, not words */
    __IO uint8_t *dr = (__IO uint8_t *)&CRC->DR;
    
    /* Update CRC */
    *dr = data;

    /* CRC result so far */
    return CRC->DR;
}

