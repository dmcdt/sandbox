#include "eeprom_emul_memoryinterface.h"

#include <stm32h7xx.h>


/* Number of sectors to dedicate to EEPROM emulation. Must be >1 */
#define EEPROM_NUMBER_SECTORS   (2)


/*  Base address at which to store emulated EEPROM. This must be within a single
 *  bank, for the core that is going to be the custodian of the emulated EEPROM.
 *  Sharing is not considered here.
 */
#define EEPROM_BASE_VALUE   (0x080C0000U)
#define EEPROM_BASE         ((void *)EEPROM_BASE_VALUE)


/*  Total size of emulated EEPROM storage */
#define EEPROM_SIZE (EEPROM_NUMBER_SECTORS * FLASH_SECTOR_SIZE)


int EEPROM_NumberSectors(void)
{
    return EEPROM_NUMBER_SECTORS;
}


uint32_t EEPROM_BaseValue(void)
{
    return EEPROM_BASE_VALUE;
}


uint32_t EEPROM_Size(void)
{
    return EEPROM_SIZE;
}

