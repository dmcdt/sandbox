/* Example started from Ceedling sample project */

#include "IntrinsicsWrapper.h"

#ifdef __ICCARM__
#include <intrinsics.h>
#endif

void Interrupt_Enable(void)
{
#ifdef __ICCARM__
    __enable_interrupt();
#endif
}

void Interrupt_Disable(void)
{
#ifdef __ICCARM__
    __disable_interrupt();
#endif
}


void WaitForEvent(void)
{
#ifdef __ICCARM__
    __WFE();
#endif
}

void NoOperation(void)
{
#ifdef __ICCARM__
    __no_operation();
#endif
}


void FlushCpuIDCache(void)
{
#ifdef __ICCARM__
    __ISB();
    __DSB();
#endif
}

