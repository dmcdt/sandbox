#ifndef ERRORHANDLER_H
#define ERRORHANDLER_H

/* Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved. */

void Error_Handler(void);

#endif
