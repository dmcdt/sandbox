#ifndef COMMONDEFINITIONS_H
#define COMMONDEFINITIONS_H

/* Copyright 2020 Cereus Downhole Technology Ltd, All rights reserved. */

#define COUNTOF(x) (sizeof (x) / sizeof ((x)[0]))

#endif
