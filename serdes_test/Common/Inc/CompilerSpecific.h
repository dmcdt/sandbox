#ifndef COMPILERSPECIFIC_H
#define COMPILERSPECIFIC_H


/*
 *  Compiler specific macros in order to make the code compile regardless of the
 *  compiler.
 * 
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 */


#if defined(__ICCARM__)

#   define NOINIT __no_init

#else

#   define NOINIT

#endif


#endif
