#ifndef UNIONS_H
#define UNIONS_H

/**
 * @brief Definitions for accessing the underlying bytes of variables.
 * 
 * @copyright Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 */


union FloatBytes
{
    float   f;
    char    bytes[sizeof(float)];
};


#endif
