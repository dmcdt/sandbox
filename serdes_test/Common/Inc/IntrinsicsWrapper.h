#ifndef INTRINSICSWRAPPER_H
#define INTRINSICSWRAPPER_H

void Interrupt_Enable(void);
void Interrupt_Disable(void);

void WaitForEvent(void);

void NoOperation(void);

void FlushCpuIDCache(void);

#endif
