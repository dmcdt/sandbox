/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "crc.h"
#include "rtc.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"
#include "fmc.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "deserializer.h"
#include "peripherals.h"
#include "Power.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#ifndef HSEM_ID_0
#define HSEM_ID_0 (0U) /* HW semaphore 0*/
#endif


// Define this to 1 so that the CM7 and CM4 use the above hardware semaphore
// to start up together. Better to put it in the project definitions for
// both configurations.
//#define WAIT_FOR_CM4    (0)
#ifndef WAIT_FOR_CM4
#define WAIT_FOR_CM4    (0)
#endif

#if 1
// For running on Dimo's Cereus board
#define PLL1_DIVM   (2)
#define PLL1_DIVN   (60)
#else
// For running on the Nucleo board
#define PLL1_DIVM   (1)
#define PLL1_DIVN   (75)
#endif

//#define DES_UART_TX_TIMEOUT HAL_MAX_DELAY
#define DES_UART_TX_TIMEOUT (200)
#define DES_UART_RX_TIMEOUT (200)

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void PeriphCommonClock_Config(void);
static void MPU_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
    uint8_t             request[64];
    uint8_t             response[64];
    HAL_StatusTypeDef   halStatus;
    char const          *str;
    uint8_t             deserializerAddress = 0x90;
    uint8_t             serializerAddress = 0x80;
    
    HAL_EnableDBGSleepMode();
    HAL_EnableDBGStopMode();
    HAL_EnableDBGStandbyMode();
    
    HAL_EnableDomain2DBGSleepMode();
    HAL_EnableDomain2DBGStopMode();
    HAL_EnableDomain2DBGStandbyMode();

    HAL_EnableDomain3DBGStopMode();
    HAL_EnableDomain3DBGStandbyMode();

    __HAL_RCC_UART8_FORCE_RESET();
    __HAL_RCC_UART8_RELEASE_RESET();
    
    __HAL_RCC_GPIOC_FORCE_RESET();
    __HAL_RCC_GPIOC_RELEASE_RESET();

    __HAL_RCC_GPIOG_FORCE_RESET();
    __HAL_RCC_GPIOG_RELEASE_RESET();

  /* USER CODE END 1 */
/* USER CODE BEGIN Boot_Mode_Sequence_0 */
#if WAIT_FOR_CM4==1
    int32_t timeout;
#endif
/* USER CODE END Boot_Mode_Sequence_0 */

  /* MPU Configuration--------------------------------------------------------*/
  MPU_Config();

  /* Enable I-Cache---------------------------------------------------------*/
  SCB_EnableICache();

  /* Enable D-Cache---------------------------------------------------------*/
  SCB_EnableDCache();

/* USER CODE BEGIN Boot_Mode_Sequence_1 */
#if WAIT_FOR_CM4==1
    /* Wait until CPU2 boots and enters in stop mode or timeout*/
    timeout = 0xFFFF;
    while((__HAL_RCC_GET_FLAG(RCC_FLAG_D2CKRDY) != RESET) && (timeout-- > 0));
    if ( timeout < 0 )
    {
        Error_Handler();
    }
#endif
/* USER CODE END Boot_Mode_Sequence_1 */
  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

/* Configure the peripherals common clocks */
  PeriphCommonClock_Config();
/* USER CODE BEGIN Boot_Mode_Sequence_2 */
  
#if WAIT_FOR_CM4==1
    /* When system initialization is finished, Cortex-M7 will release Cortex-M4 by means of
    HSEM notification */
    /*HW semaphore Clock enable*/
    __HAL_RCC_HSEM_CLK_ENABLE();
    
    /*Take HSEM */
    HAL_HSEM_FastTake(HSEM_ID_0);
    
    /*Release HSEM in order to notify the CPU2(CM4)*/
    HAL_HSEM_Release(HSEM_ID_0,0);
    
    /* wait until CPU2 wakes up from stop mode */
    timeout = 0xFFFF;
    while((__HAL_RCC_GET_FLAG(RCC_FLAG_D2CKRDY) == RESET) && (timeout-- > 0));
    if ( timeout < 0 )
    {
        Error_Handler();
    }
#endif
/* USER CODE END Boot_Mode_Sequence_2 */

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_FMC_Init();
  MX_CRC_Init();
  MX_TIM1_Init();
  MX_TIM8_Init();
  MX_TIM3_Init();
  MX_UART8_Init();
  MX_RTC_Init();
  MX_TIM5_Init();
  MX_TIM12_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
//    char const * const hw = "DEM: Start\r\n";
//    HAL_StatusTypeDef status;
//
//    status = HAL_UART_Transmit(&huart3, (uint8_t *)hw, strlen(hw), HAL_MAX_DELAY);
//    if (status != HAL_OK)
//    {
//      Error_Handler();
//    }
    
    Power_EnableRotating();
    HAL_Delay(10);
  
    Deserializer_PowerOn();
    Deserializer_Enable();
    
    HAL_Delay(100);

    /*
        Deserialiser address = CX/TP = low, ADD2:0 open => low = 0x90 | R/W
        Serialiser address = 0x80 until rewritten
     */
    
    Peripherals_Enable();
    Peripherals_Select(Peripheral_Deserializer);

    /***************************************************************************
     *
     * Read the first couple of registers to make sure the addresses are OK.
     */

    HAL_Delay(100);

    request[0] = DESERIALIZER_SYNC;
    request[1] = deserializerAddress | DESERIALIZER_READ;
    request[2] = SerializerId; // Register address
    request[3] = 2; //number bytes
    
    halStatus = HAL_UART_Transmit(&huart8, request, 4, DES_UART_TX_TIMEOUT);
    if (halStatus != HAL_OK)
    {
        Error_Handler();
    }
    
    halStatus = HAL_UART_Receive(&huart8, response, 3, DES_UART_RX_TIMEOUT);
    if (halStatus != HAL_OK)
    {
        Error_Handler();
    }
    
    // Response format should be
    if ((response[0] == DESERIALIZER_ACK) &&
        (response[1] == serializerAddress) &&
        (response[2] == deserializerAddress))
    {
        str = "Got defaults\r\n";
        //printf(str);
    }
    
    /***************************************************************************
     *
     * Read the device ID, make sure we can read something sensible
     */

    HAL_Delay(10);
    
    request[0] = DESERIALIZER_SYNC;
    request[1] = deserializerAddress | DESERIALIZER_READ;
    request[2] = DeviceId; // Register address
    request[3] = 1; //number bytes
    
    halStatus = HAL_UART_Transmit(&huart8, request, 4, DES_UART_TX_TIMEOUT);
    if (halStatus != HAL_OK)
    {
        Error_Handler();
    }
    
    halStatus = HAL_UART_Receive(&huart8, response, 2, DES_UART_RX_TIMEOUT);
    if (halStatus != HAL_OK)
    {
        Error_Handler();
    }
    
    // Response format should be
    if ((response[0] == DESERIALIZER_ACK) &&
        (response[1] == MAX9276_Deserializer))
    {
        str = "Got correct device\r\n";
        //printf(str);
    }

    
    /***************************************************************************
     *
     * Try reading from the SERIALIZER
     */

    HAL_Delay(10);

    request[0] = DESERIALIZER_SYNC;
    request[1] = serializerAddress | DESERIALIZER_READ;
    request[2] = SerializerId; // Register address
    request[3] = 2; //number bytes
    
    halStatus = HAL_UART_Transmit(&huart8, request, 4, DES_UART_TX_TIMEOUT);
    if (halStatus != HAL_OK)
    {
        Error_Handler();
    }
    
    halStatus = HAL_UART_Receive(&huart8, response, 3, DES_UART_RX_TIMEOUT);
    if (halStatus == HAL_OK)
    {
        // Response format should be
        if ((response[0] == DESERIALIZER_ACK) &&
            (response[1] == serializerAddress) &&
            (response[2] == deserializerAddress))
        {
            str = "Got device ids from serialiser\r\n";
            //printf(str);
        }
    }
    else
    {
        // Maybe not connected?
    }
    

    /* Arm the one pulse timer that creates the sync pulse */
    halStatus = HAL_TIM_OnePulse_Start(&htim12, TIM_CHANNEL_2);
    if (halStatus != HAL_OK)
    {
        Error_Handler();
    }
    
    /* Start main timer to trigger the pulse output every period */
    halStatus = HAL_TIM_OC_Start(&htim5, TIM_CHANNEL_1);
    if (halStatus != HAL_OK)
    {
        Error_Handler();
    }
    
    while (1)
    {
        request[0] = DESERIALIZER_SYNC;
        request[1] = deserializerAddress | DESERIALIZER_READ;
        request[2] = 0x00; // Register address
        request[3] = 0x20; //number bytes
        
        halStatus = HAL_UART_Transmit(&huart8, request, 4, DES_UART_TX_TIMEOUT);
        if (halStatus != HAL_OK)
        {
            // Transmit really should work
            //Error_Handler();
        }
        
        halStatus = HAL_UART_Receive(&huart8, response, 0x21, DES_UART_RX_TIMEOUT);
        if (halStatus != HAL_OK)
        {
            // Receive - why would it fail?
            //Error_Handler();
        }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
        HAL_Delay(1000);
        __NOP();
        __NOP();
    }

    Deserializer_Disable();
    Deserializer_PowerOff();

    Power_DisableRotating();
    
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Supply configuration update enable
  */
  HAL_PWREx_ConfigSupply(PWR_DIRECT_SMPS_SUPPLY);
  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}
  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_HIGH);
  /** Macro to configure the PLL clock source
  */
  __HAL_RCC_PLL_PLLSOURCE_CONFIG(RCC_PLLSOURCE_HSE);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 2;
  RCC_OscInitStruct.PLL.PLLN = 60;
  RCC_OscInitStruct.PLL.PLLP = 2;
  RCC_OscInitStruct.PLL.PLLQ = 3;
  RCC_OscInitStruct.PLL.PLLR = 4;
  RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_3;
  RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
  RCC_OscInitStruct.PLL.PLLFRACN = 0;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV2;
  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief Peripherals Common Clock Configuration
  * @retval None
  */
void PeriphCommonClock_Config(void)
{
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Initializes the peripherals clock
  */
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_ADC|RCC_PERIPHCLK_SPI1;
  PeriphClkInitStruct.PLL2.PLL2M = 2;
  PeriphClkInitStruct.PLL2.PLL2N = 60;
  PeriphClkInitStruct.PLL2.PLL2P = 20;
  PeriphClkInitStruct.PLL2.PLL2Q = 2;
  PeriphClkInitStruct.PLL2.PLL2R = 2;
  PeriphClkInitStruct.PLL2.PLL2RGE = RCC_PLL2VCIRANGE_3;
  PeriphClkInitStruct.PLL2.PLL2VCOSEL = RCC_PLL2VCOWIDE;
  PeriphClkInitStruct.PLL2.PLL2FRACN = 0;
  PeriphClkInitStruct.Spi123ClockSelection = RCC_SPI123CLKSOURCE_PLL2;
  PeriphClkInitStruct.AdcClockSelection = RCC_ADCCLKSOURCE_PLL2;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* MPU Configuration */

void MPU_Config(void)
{
  MPU_Region_InitTypeDef MPU_InitStruct = {0};

  /* Disables the MPU */
  HAL_MPU_Disable();
  /** Initializes and configures the Region and the memory to be protected
  */
  MPU_InitStruct.Enable = MPU_REGION_ENABLE;
  MPU_InitStruct.Number = MPU_REGION_NUMBER0;
  MPU_InitStruct.BaseAddress = EEPROM_MEMORY_BASE;
  MPU_InitStruct.Size = MPU_REGION_SIZE_256KB;
  MPU_InitStruct.SubRegionDisable = 0x0;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL1;
  MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
  MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_DISABLE;
  MPU_InitStruct.IsShareable = MPU_ACCESS_NOT_SHAREABLE;
  MPU_InitStruct.IsCacheable = MPU_ACCESS_NOT_CACHEABLE;
  MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);
  /** Initializes and configures the Region and the memory to be protected
  */
  MPU_InitStruct.Number = MPU_REGION_NUMBER1;
  MPU_InitStruct.BaseAddress = EXTERNAL_MEMORY_BASE;
  MPU_InitStruct.Size = MPU_REGION_SIZE_256MB;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL2;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);
  /** Initializes and configures the Region and the memory to be protected
  */
  MPU_InitStruct.Number = MPU_REGION_NUMBER2;
  MPU_InitStruct.BaseAddress = 0x0;
  MPU_InitStruct.Size = MPU_REGION_SIZE_32B;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
  MPU_InitStruct.AccessPermission = MPU_REGION_NO_ACCESS;
  MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);
  /* Enables the MPU */
  HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);

}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
