/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

#define GPIO_WRITE(line, state) HAL_GPIO_WritePin(line##_GPIO_Port, line##_Pin, state)
#define GPIO_READ(line) HAL_GPIO_ReadPin(line##_GPIO_Port, line##_Pin)

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define EXTERNAL_MEMORY_BASE 0x80000000
#define EEPROM_MEMORY_BASE 0x081C0000
#define RS0_Pin GPIO_PIN_2
#define RS0_GPIO_Port GPIOE
#define RS1_Pin GPIO_PIN_3
#define RS1_GPIO_Port GPIOE
#define RS2_Pin GPIO_PIN_4
#define RS2_GPIO_Port GPIOE
#define MCU_NEW_PAGE_Pin GPIO_PIN_5
#define MCU_NEW_PAGE_GPIO_Port GPIOE
#define MCU_FTDI_WR_Pin GPIO_PIN_6
#define MCU_FTDI_WR_GPIO_Port GPIOE
#define DES_PWRDWN_N_Pin GPIO_PIN_13
#define DES_PWRDWN_N_GPIO_Port GPIOC
#define USB_PWR_SEN_Pin GPIO_PIN_9
#define USB_PWR_SEN_GPIO_Port GPIOF
#define MCU_HS_OSC_Pin GPIO_PIN_0
#define MCU_HS_OSC_GPIO_Port GPIOH
#define BATT_FAIL_Pin GPIO_PIN_1
#define BATT_FAIL_GPIO_Port GPIOH
#define RS_DAC_WR__Pin GPIO_PIN_0
#define RS_DAC_WR__GPIO_Port GPIOA
#define MCU_PEND_INT_N_Pin GPIO_PIN_1
#define MCU_PEND_INT_N_GPIO_Port GPIOA
#define IMU_INT1_Pin GPIO_PIN_2
#define IMU_INT1_GPIO_Port GPIOA
#define TIM5C4_BLDC_INDEX_Pin GPIO_PIN_3
#define TIM5C4_BLDC_INDEX_GPIO_Port GPIOA
#define ADC_SHDWN_Pin GPIO_PIN_4
#define ADC_SHDWN_GPIO_Port GPIOC
#define ANLG_CLK_EN_Pin GPIO_PIN_5
#define ANLG_CLK_EN_GPIO_Port GPIOC
#define MCU_NAND_CE0_Pin GPIO_PIN_0
#define MCU_NAND_CE0_GPIO_Port GPIOB
#define MCU_NAND_CE1_Pin GPIO_PIN_1
#define MCU_NAND_CE1_GPIO_Port GPIOB
#define MCU_COLSEL_Pin GPIO_PIN_2
#define MCU_COLSEL_GPIO_Port GPIOB
#define MCU_TXE_INT_N_Pin GPIO_PIN_11
#define MCU_TXE_INT_N_GPIO_Port GPIOF
#define MCU_ALL_RE_EN_Pin GPIO_PIN_14
#define MCU_ALL_RE_EN_GPIO_Port GPIOF
#define FT600_RXF_N_Pin GPIO_PIN_14
#define FT600_RXF_N_GPIO_Port GPIOB
#define TIM12C2_SYNC_OUT_Pin GPIO_PIN_15
#define TIM12C2_SYNC_OUT_GPIO_Port GPIOB
#define DAC_SLEEP_Pin GPIO_PIN_13
#define DAC_SLEEP_GPIO_Port GPIOD
#define MCU_CPLD_SPARE0_Pin GPIO_PIN_6
#define MCU_CPLD_SPARE0_GPIO_Port GPIOG
#define SSI_PA_PWR_SHUTDOWN_Pin GPIO_PIN_7
#define SSI_PA_PWR_SHUTDOWN_GPIO_Port GPIOG
#define RS_ADC_CLK_PCLK_Pin GPIO_PIN_8
#define RS_ADC_CLK_PCLK_GPIO_Port GPIOG
#define RS_ADC_RD__Pin GPIO_PIN_6
#define RS_ADC_RD__GPIO_Port GPIOC
#define RS_DATA_REPEAT_Pin GPIO_PIN_7
#define RS_DATA_REPEAT_GPIO_Port GPIOC
#define RS_DATA_CRC_Pin GPIO_PIN_8
#define RS_DATA_CRC_GPIO_Port GPIOC
#define MASTER_CLK_OUT_Pin GPIO_PIN_9
#define MASTER_CLK_OUT_GPIO_Port GPIOC
#define TIM1C1_PA_RX_GAIN_Pin GPIO_PIN_8
#define TIM1C1_PA_RX_GAIN_GPIO_Port GPIOA
#define TIM1C2_PA_RX_EN_Pin GPIO_PIN_9
#define TIM1C2_PA_RX_EN_GPIO_Port GPIOA
#define TIM1C3_PA_TX_EN_Pin GPIO_PIN_10
#define TIM1C3_PA_TX_EN_GPIO_Port GPIOA
#define TIM1C4_PA_EN_Pin GPIO_PIN_11
#define TIM1C4_PA_EN_GPIO_Port GPIOA
#define TIM3ETR_ADC_CLK_Pin GPIO_PIN_2
#define TIM3ETR_ADC_CLK_GPIO_Port GPIOD
#define RB_CLEAR_Pin GPIO_PIN_3
#define RB_CLEAR_GPIO_Port GPIOD
#define MCU_FMC_RE_N_Pin GPIO_PIN_4
#define MCU_FMC_RE_N_GPIO_Port GPIOD
#define MCU_FMC_WE_N_Pin GPIO_PIN_5
#define MCU_FMC_WE_N_GPIO_Port GPIOD
#define MCU_ALL_RB_INT_N_Pin GPIO_PIN_6
#define MCU_ALL_RB_INT_N_GPIO_Port GPIOD
#define MCU_LATCH_RB_Pin GPIO_PIN_7
#define MCU_LATCH_RB_GPIO_Port GPIOD
#define MCU_FMC_CE_N_Pin GPIO_PIN_9
#define MCU_FMC_CE_N_GPIO_Port GPIOG
#define MS0_Pin GPIO_PIN_10
#define MS0_GPIO_Port GPIOG
#define MS1_Pin GPIO_PIN_11
#define MS1_GPIO_Port GPIOG
#define MS2_Pin GPIO_PIN_12
#define MS2_GPIO_Port GPIOG
#define PEN_Pin GPIO_PIN_13
#define PEN_GPIO_Port GPIOG
#define RS_PWR_EN_Pin GPIO_PIN_14
#define RS_PWR_EN_GPIO_Port GPIOG
#define DES_LOCK_Pin GPIO_PIN_6
#define DES_LOCK_GPIO_Port GPIOB
#define DES_ERR_Pin GPIO_PIN_7
#define DES_ERR_GPIO_Port GPIOB
#define DES_TX_MCU_RX_Pin GPIO_PIN_0
#define DES_TX_MCU_RX_GPIO_Port GPIOE
#define DES_RX_MCU_TX_Pin GPIO_PIN_1
#define DES_RX_MCU_TX_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
