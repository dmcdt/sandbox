#ifndef POWER_H
#define POWER_H


/*
 *  Declarations for controlling power state of system components.
 */


#include <stdbool.h>


void Power_EnableRotating(void);
void Power_DisableRotating(void);
bool Power_GetRotating(void);


#endif
