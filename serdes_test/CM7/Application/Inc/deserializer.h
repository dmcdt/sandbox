#ifndef DESERIALIZER_H
#define DESERIALIZER_H

/*
 *  Functionality to handle the deserializer, MAX9276A.
 */

     
#define DESERIALIZER_WRITE  (0x00)
#define DESERIALIZER_READ   (0x01)

#define DESERIALIZER_SYNC   (0x79)
#define DESERIALIZER_ACK    (0xC3)


typedef enum RegisterAddress_t
{
    SerializerId,
    DeserializerId,
    DeviceId = 0x1E
} RegisterAddress_t;


typedef enum DeviceIdentifier_t
{
    MAX9275_Serializer      = 0x21,
    MAX9276_Deserializer    = 0x22,
    MAX9279_Serializer      = 0x25,
    MAX9280_Deserializer    = 0x26
} DeviceIdentifier_t;


void Deserializer_PowerOff(void);
void Deserializer_PowerOn(void);
void Deserializer_Enable(void);
void Deserializer_Disable(void);

#endif
