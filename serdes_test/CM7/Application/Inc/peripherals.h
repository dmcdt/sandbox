#ifndef PERIPHERALS_H
#define PERIPHERALS_H


typedef enum PeripheralSelect_t
{
    Peripheral_ADC_DAC,
    Peripheral_NAND_to_FTDI,
    Peripheral_MCU_to_NAND,
    Peripheral_NAND_to_MCU,
    Peripheral_CPLD_to_MCU,    // for reading the R/B status register
    Peripheral_FTDI_to_MCU,
    Peripheral_MCU_to_FTDI,
    Peripheral_Deserializer
} PeripheralSelect_t;


void Peripherals_Enable(void);
void Peripherals_Disable(void);
void Peripherals_Select(PeripheralSelect_t peripheral);

#endif
