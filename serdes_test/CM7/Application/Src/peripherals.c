#include "peripherals.h"

#include "main.h"
#include <stm32h7xx_hal.h>


void Peripherals_Enable(void)
{
    //HAL_GPIO_WritePin(PEN_GPIO_Port, PEN_Pin, GPIO_PIN_SET);
    GPIO_WRITE(PEN, GPIO_PIN_SET);
}


void Peripherals_Disable(void)
{
    GPIO_WRITE(PEN, GPIO_PIN_RESET);
}


void Peripherals_Select(PeripheralSelect_t peripheral)
{
    /* This works because the 3 bits that encode the peripheral selection
     * are in consecutive bits. So we can just mask off the unused bits and
     * shift it into place (shift by multiplying, as it's the only definition
     * we have that's generated).
     */
    uint16_t    pinSet = ((peripheral & 0x07) * GPIO_PIN_10);
    uint16_t    pinClear = ((~peripheral & 0x07) * GPIO_PIN_10);
    uint32_t    pinStates = (pinClear << 16) | pinSet;
    
    PEN_GPIO_Port->BSRR = pinStates;
}

