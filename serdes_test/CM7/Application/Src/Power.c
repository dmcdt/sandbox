#include "power.h"

#include "main.h"
#include <stm32h7xx_hal.h>


void Power_EnableRotating(void)
{
    HAL_GPIO_WritePin(RS_PWR_EN_GPIO_Port, RS_PWR_EN_Pin, GPIO_PIN_SET);
    /* TODO - other stuff after power up
     *  Enable deserialiser
     *  Enable RS485 output
     *  Enable sync output (although this should be low anyway)
     */
}


void Power_DisableRotating(void)
{
    /* TODO - other stuff preparing for power down
     *  Shutdown deserialiser
     *  Shutdown RS485 output
     *  Shutdown sync output
     */
    HAL_GPIO_WritePin(RS_PWR_EN_GPIO_Port, RS_PWR_EN_Pin, GPIO_PIN_RESET);
}


bool Power_GetRotating(void)
{
    GPIO_PinState pinState;
    pinState = HAL_GPIO_ReadPin(RS_PWR_EN_GPIO_Port, RS_PWR_EN_Pin);
    return pinState == GPIO_PIN_SET;
}
