#include "deserializer.h"


#include "main.h"
#include <stm32h7xx_hal.h>

#include "peripherals.h"


void Deserializer_PowerOff(void)
{
    HAL_GPIO_WritePin(DES_PWRDWN_N_GPIO_Port, DES_PWRDWN_N_Pin, GPIO_PIN_RESET);
}


void Deserializer_PowerOn(void)
{
    HAL_GPIO_WritePin(DES_PWRDWN_N_GPIO_Port, DES_PWRDWN_N_Pin, GPIO_PIN_SET);
}


void Deserializer_Enable(void)
{
    // Output enable comes from the small CPLD, based on the peripheral that the FMC reads from.
    //HAL_GPIO_WritePin(DES_OE_N_GPIO_Port, DES_OE_N_Pin, GPIO_PIN_RESET);
    Peripherals_Enable();
    Peripherals_Select(Peripheral_Deserializer);
}


void Deserializer_Disable(void)
{
    // Output enable comes from the small CPLD, based on the peripheral that the FMC reads from.
    //HAL_GPIO_WritePin(DES_OE_N_GPIO_Port, DES_OE_N_Pin, GPIO_PIN_SET);
    Peripherals_Select(Peripheral_ADC_DAC);
    Peripherals_Disable();
}
