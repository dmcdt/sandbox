/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define EXTERNAL_MEMORY_BASE 0x80000000
#define EEPROM_MEMORY_BASE 0x081C0000
#define UART7_RX_RS485_RS_Pin GPIO_PIN_6
#define UART7_RX_RS485_RS_GPIO_Port GPIOF
#define UART7_TX_RS485_RS_Pin GPIO_PIN_7
#define UART7_TX_RS485_RS_GPIO_Port GPIOF
#define UART7_DE_RS485_RS_Pin GPIO_PIN_8
#define UART7_DE_RS485_RS_GPIO_Port GPIOF
#define ADC3_INP6_18V_Pin GPIO_PIN_10
#define ADC3_INP6_18V_GPIO_Port GPIOF
#define MCU_HS_OSC_Pin GPIO_PIN_0
#define MCU_HS_OSC_GPIO_Port GPIOH
#define ADC3_INP10_32V_Pin GPIO_PIN_0
#define ADC3_INP10_32V_GPIO_Port GPIOC
#define ADC3_INP11_12V_Pin GPIO_PIN_1
#define ADC3_INP11_12V_GPIO_Port GPIOC
#define ADC3_INP0_BATT_Pin GPIO_PIN_2
#define ADC3_INP0_BATT_GPIO_Port GPIOC
#define ADC3_INP1_Pt1000_Pin GPIO_PIN_3
#define ADC3_INP1_Pt1000_GPIO_Port GPIOC
#define RS_DAC_WR__Pin GPIO_PIN_0
#define RS_DAC_WR__GPIO_Port GPIOA
#define IMU_INT1_Pin GPIO_PIN_2
#define IMU_INT1_GPIO_Port GPIOA
#define SPI1_CE_IMU_Pin GPIO_PIN_4
#define SPI1_CE_IMU_GPIO_Port GPIOA
#define SPI1_SCK_IMU_Pin GPIO_PIN_5
#define SPI1_SCK_IMU_GPIO_Port GPIOA
#define SPI1_MISO_IMU_Pin GPIO_PIN_6
#define SPI1_MISO_IMU_GPIO_Port GPIOA
#define SPI1_MOSI_IMU_Pin GPIO_PIN_7
#define SPI1_MOSI_IMU_GPIO_Port GPIOA
#define UART3_TX_BOOT_Pin GPIO_PIN_10
#define UART3_TX_BOOT_GPIO_Port GPIOB
#define UART3_RX_BOOT_Pin GPIO_PIN_11
#define UART3_RX_BOOT_GPIO_Port GPIOB
#define UART5_RX_BLDC_Pin GPIO_PIN_12
#define UART5_RX_BLDC_GPIO_Port GPIOB
#define UART5_TX_BLDC_Pin GPIO_PIN_13
#define UART5_TX_BLDC_GPIO_Port GPIOB
#define SSI_PA_PWRGD_Pin GPIO_PIN_12
#define SSI_PA_PWRGD_GPIO_Port GPIOA
#define UART4_TX_RS485_PC_Pin GPIO_PIN_10
#define UART4_TX_RS485_PC_GPIO_Port GPIOC
#define UART4_RX_RS485_PC_Pin GPIO_PIN_11
#define UART4_RX_RS485_PC_GPIO_Port GPIOC
#define UART4_DE_RS485_PC_Pin GPIO_PIN_12
#define UART4_DE_RS485_PC_GPIO_Port GPIOC
#define I2C1_SCL_PG_Pin GPIO_PIN_8
#define I2C1_SCL_PG_GPIO_Port GPIOB
#define I2C1_SDA_PG_Pin GPIO_PIN_9
#define I2C1_SDA_PG_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
