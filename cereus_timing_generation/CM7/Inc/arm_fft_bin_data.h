#ifndef ARM_FFT_BIN_DATA_H
#define ARM_FFT_BIN_DATA_H

#include <arm_math.h>

#define TEST_LENGTH_SAMPLES (2048)

extern float32_t testInput_f32_10khz[TEST_LENGTH_SAMPLES];

#endif
