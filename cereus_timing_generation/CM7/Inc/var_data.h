#ifndef VAR_DATA_H
#define VAR_DATA_H

#include "arm_math.h"

#define VARIANCE_TEST_DATA_SIZE (2000)

extern float32_t const varianceTestData[VARIANCE_TEST_DATA_SIZE];



#endif
