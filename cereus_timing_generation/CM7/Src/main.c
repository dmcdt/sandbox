/**
  ******************************************************************************
  * @file    Templates/BootCM4_CM7/CM7/Src/main.c
  * @author  MCD Application Team
  * @brief   Main program body for Cortex-M7.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stm32h7xx.h>
#include "main.h"
#include "stm32h7xx_it.h"
#include "utilities.h"

#include <stdio.h>
#include <math.h>

#include <arm_math.h>
#include <arm_const_structs.h>

#include <arm_fft_bin_data.h>
#include <var_data.h>


/** @addtogroup STM32H7xx_HAL_Examples
  * @{
  */

/** @addtogroup Templates
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define HSEM_ID_0 (0U) /* HW semaphore 0*/

#ifndef M_PI
#define M_PI (3.14159265358979323846264338327950288)
#endif /* M_PI */


/* Private macro -------------------------------------------------------------*/

#define COUNTOF(x) (sizeof (x) / sizeof (x)[0])

/* Private variables ---------------------------------------------------------*/

static float32_t inputData[TEST_LENGTH_SAMPLES];
static float32_t outputData[TEST_LENGTH_SAMPLES / 2];

/* Private function prototypes -----------------------------------------------*/
static void CPU_CACHE_Enable(void);
void Sleep(uint32_t Delay);
static void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void Error_Handler(void);


/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
    int32_t timeout;
    uint32_t    startTime, endTime, setupTime;

    uint32_t cycles = 0;
    uint32_t cycles_systick_interrupt = 0;
    __IO uint32_t cycles_result = 0; /* with uint32_t type, the algo execution 
	                                    time should not exceed 4,2 ms */
    
    /* System Init, System clock, voltage scaling and L1-Cache configuration are done by CPU1 (Cortex-M7)
       in the meantime Domain D2 is put in STOP mode(Cortex-M4 in deep-sleep)
    */

    HAL_EnableDBGSleepMode();
    HAL_EnableDBGStopMode();
    HAL_EnableDBGStandbyMode();
    
    HAL_EnableDomain2DBGSleepMode();
    HAL_EnableDomain2DBGStopMode();
    HAL_EnableDomain2DBGStandbyMode();

    HAL_EnableDomain3DBGStopMode();
    HAL_EnableDomain3DBGStandbyMode();

    /* Enable the CPU Cache */
    CPU_CACHE_Enable();

    // Code compiled out for CM4 boot gated
#if 0
    /* Wait until CPU2 boots and enters in stop mode or timeout*/
    timeout = 0xFFFFFFFL;
    while((__HAL_RCC_GET_FLAG(RCC_FLAG_D2CKRDY) != RESET) && (timeout-- > 0));
    if ( timeout < 0 )
    {
        Error_Handler();
    }
#endif
    
    /* STM32H7xx HAL library initialization:
       - Systick timer is configured by default as source of time base, but user
         can eventually implement his proper time base source (a general purpose
         timer for example or other time source), keeping in mind that Time base
         duration should be kept 1ms since PPP_TIMEOUT_VALUEs are defined and
         handled in milliseconds basis.
       - Set NVIC Group Priority to 4
       - Low Level Initialization
     */
    HAL_Init();

    /* Configure the system clock */
    SystemClock_Config();

    // Code compiled out for CM4 boot gated
#if 0
    /* When system initialization is finished, Cortex-M7 will release Cortex-M4  by means of
       HSEM notification */

    /*HW semaphore Clock enable*/
    __HAL_RCC_HSEM_CLK_ENABLE();

    /*Take HSEM */
    HAL_HSEM_FastTake(HSEM_ID_0);
    /*Release HSEM in order to notify the CPU2(CM4)*/
    HAL_HSEM_Release(HSEM_ID_0,0);

    /* wait until CPU2 wakes up from stop mode */
    timeout = 0xFFFF;
    while((__HAL_RCC_GET_FLAG(RCC_FLAG_D2CKRDY) == RESET) && (timeout-- > 0));
    if ( timeout < 0 )
    {
        Error_Handler();
    }
#endif
    
    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    BSP_LED_Init(LED1);
    BSP_LED_Init(LED2);
    BSP_LED_Init(LED3);
    

    /* Count the cycle number consumed by systick handler (it's counted only
     * when a systick overflow occurs). Need to stop, start the timer, run the
     * handler and get the duration. Calling the handler should be similar to
     * the interrupt getting called and then it calling the handler.
     */
    TimerCount_DeInit();	
    TimerCount_Start(); 
    SysTick_Handler();
    TimerCount_Stop(&cycles_systick_interrupt);

    /* Re-initialise the tick timer for 1ms intervals if you need to do any
     * semi-accurate delays after using systick as a cycle counter.
     */
    if(HAL_InitTick(TICK_INT_PRIORITY) != HAL_OK)
    {
        Error_Handler();
    }
    
    /* Add Cortex-M7 user application code here */
    BSP_LED_Off(LED1);
    
    uint32_t    fftSize = 1024;
    uint32_t    performIFFT = 0;
    uint32_t    doBitReverse = 1;
    
    float32_t   maxValue;
    uint32_t    maxIndex;
    
    uint32_t    expectedIndex = 213;    // Expected bin with peak value, taken from example code */
    
    float32_t   sampleRate = 48000; /* Sample rate estimated from graphs at https://www.keil.com/pack/doc/CMSIS/DSP/html/group__FrequencyBin.html
                                     * and that 48 kHz is fairly standard for audio.
                                     * All very well and good knowing the expected index
                                     * and that it is 10kHz input signal but if there is
                                     * no way to associated the two then how do you know?
                                     */
    
    /* Plotting the FFT output magnitudes show that it is mirrored with the
     * lower half of the array corresponding to bins 0 ... fftSize/2
     * (0 ... sampleFrequency/2).
     */
    
    int maxIterations = 1000;    // Number of times to repeat the loop
    
    // Some background info to exclude from calculation times
    startTime = HAL_GetTick();
    for (int i = 0; i < maxIterations; ++i)
    {
        // Make copy of test data as FFT calculated in place, want to be able to reuse it
        memcpy(&inputData[0], &testInput_f32_10khz[0], sizeof(testInput_f32_10khz));
    }
    endTime = HAL_GetTick();
    setupTime = endTime - startTime;
    
    //FILE *logFile;
    //logFile = fopen("out.log", "w+");
    //fprintf(logFile, "System clock is %lu Hz\r\n", HAL_RCC_GetSysClockFreq());
    printf("System clock is %lu Hz\r\n", HAL_RCC_GetSysClockFreq());
    
    startTime = HAL_GetTick();
    
    for (int i = 0; i < maxIterations; ++i)
    {
        // Make copy of test data as FFT calculated in place, want to be able to reuse it
        memcpy(&inputData[0], &testInput_f32_10khz[0], sizeof(testInput_f32_10khz));
        
        // Calculate FFT, result stored in place
        arm_cfft_f32(&arm_cfft_sR_f32_len1024, inputData, performIFFT, doBitReverse);
        
        // Calculate magnitude of complex results into output array
        arm_cmplx_mag_f32(inputData, outputData, fftSize);
        
        // Calculate maximum magnitude and bin number
        arm_max_f32(outputData, fftSize, &maxValue, &maxIndex);
    }
    
    endTime = HAL_GetTick();
    
    //fprintf(logFile, "Peak expected=%lu index=%lu value=%f freq=%f took %.3f ms per iteration\r\n", expectedIndex, maxIndex, maxValue, maxIndex * (sampleRate / fftSize), (double)(endTime - startTime - setupTime) / maxIterations);
    printf("Peak expected=%lu index=%lu value=%f freq=%f took %.3f ms per iteration\r\n", expectedIndex, maxIndex, maxValue, maxIndex * (sampleRate / fftSize), (double)(endTime - startTime - setupTime) / maxIterations);

    // Test out windowed variance, assume window length of N
#define ROLLING_WINDOW_SIZE     (16)
#define NUMBER_WINDOWS          (VARIANCE_TEST_DATA_SIZE - ROLLING_WINDOW_SIZE)
        
#if 1
    float32_t   sampleData[ROLLING_WINDOW_SIZE];
    float32_t   variance;
    float32_t   mean;
    
    //fprintf(logFile, "\r\nFull buffer recalculation using CMSIS DSP (f32 only)\r\n");
    printf("\r\nFull buffer recalculation using CMSIS DSP (f32 only)\r\n");

    // Copy the samples from the test input data, ignoring the all-zero imaginary values. Could have calculated the magnitude but seems a bit overkill.
    memset(&sampleData[0], 0, sizeof sampleData);
    int dataIndex = 0;
    
    /* Re-start the benchmark. De-initialise timer, zero counter and than start */
    TimerCount_DeInit();
    BenchmarkTick = 0;
    TimerCount_Start();

    for (int position = 0; position < VARIANCE_TEST_DATA_SIZE; ++position)
    {
//        memset(&sampleData[0], 0, sizeof sampleData);
//        int copy_start = 0;
//        int copy_size = position + 1;
//        if (position >= ROLLING_WINDOW_SIZE)
//        {
//            copy_start = position - ROLLING_WINDOW_SIZE + 1;
//            copy_size = ROLLING_WINDOW_SIZE;
//        }
//        
//        for (int i = 0; i < copy_size; ++i)
//        {
//            //sampleData[i] = testInput_f32_10khz[(i + position) * 2];
//            sampleData[i] = varianceTestData[copy_start + i];
//        }
        
        sampleData[dataIndex] = varianceTestData[position];
        dataIndex = (dataIndex + 1) % ROLLING_WINDOW_SIZE;
        
        int data_size = ROLLING_WINDOW_SIZE;
        if (data_size > position)
        {
            data_size = position + 1;
        }
        
        //arm_mean_f32(sampleData, copy_size, &mean);
        arm_var_f32(sampleData, data_size, &variance);
        //fprintf(logFile, "%4d: mean=%.17e var=%.17e\r\n", position, mean, variance);
        //printf("%4d: mean=%.17e var=%.17e\r\n", position, mean, variance);
    }

    /* Stop cycle counter time and restore 1ms timer */
    TimerCount_Stop(&cycles);
    if(HAL_InitTick(TICK_INT_PRIORITY) != HAL_OK)
    {
        Error_Handler();
    }

    /* Compute the number of cycles taken. 0x1000000 -> systick is 24-bit counter */
    cycles_result = (uint64_t)(0x1000000 - cycles_systick_interrupt) * BenchmarkTick + (uint64_t)cycles;   

    printf("Final var=%.17e took %.6f ms\r\n", variance, ((double)cycles_result * 1000.0) / HAL_RCC_GetSysClockFreq());
#endif
        
#if 1
        typedef float32_t   float_type;
        typedef uint32_t    int_type;
#define BITS_FMT "%08lX"
#else
        typedef float64_t   float_type;
        typedef uint64_t    int_type;
#define BITS_FMT "%016llX"
#endif
        
#if 0
        union float_int
        {
            float_type  f;
            int_type    i;
        };
#endif
        
        // Now test out a rolling variance implementation
        // Fill up to the point where we start being able to calculate variance for the outputs
        float32_t   sample;
        
        static float32_t varianceTestData_dtcm[VARIANCE_TEST_DATA_SIZE];
        memcpy(&varianceTestData_dtcm[0], &varianceTestData[0], sizeof(varianceTestData));
        float32_t   *sampleBuffer = &varianceTestData_dtcm[0];

        float32_t       meanBuffer[ROLLING_WINDOW_SIZE] = {0};
        int             meanIndex = 0;
        float_type      meanMoving;
        
        meanMoving = 0;

        float_type      meanMovingPrev = 0;
        float_type      varianceSum = 0;
        float_type      varianceSumPrev = 0;
        float_type      varianceMoving;
        float_type      varianceDivisor = 1.0f / (ROLLING_WINDOW_SIZE - 1);
        
        varianceMoving = 0;
        
        //fprintf(logFile, "\r\nOnline algorithm\r\n");
        printf("\r\nOnline algorithm\r\n");
        
        // 1ms timer not resolute enough, use cycle counter benchmark
        //startTime = HAL_GetTick();

        /* Re-start the benchmark. De-initialise timer, zero counter and than start */
        TimerCount_DeInit();
        BenchmarkTick = 0;
        TimerCount_Start();

        // Variances are calculated from index (ROLLING_WINDOW_SIZE-1) onwards
        for (int position = 0; position < VARIANCE_TEST_DATA_SIZE; ++position)
        {
            //sample = testInput_f32_10khz[position * 2];
            //sample = varianceTestData[position];
            sample = *sampleBuffer++;
            //sample = varianceTestData_dtcm[position];
            
            // Don't need a combined Welford and windowed method if done correctly
            // and happy to ignore the first WINDOW_SIZE points
#if 0
            if (position < ROLLING_WINDOW_SIZE)
            {
                // Pre-fill up to window size using Welford
                meanMoving = meanMoving + (sample - meanMoving) / (meanIndex + 1);
                varianceSum = varianceSumPrev + (sample - meanMovingPrev) * (sample - meanMoving);
            }
            else
            {
                // Update rolling mean and variance for window
                meanMoving = meanMoving + (sample - meanBuffer[meanIndex]) / ROLLING_WINDOW_SIZE;
                
                // DanS equation from https://stackoverflow.com/a/6664212
                // varSum = var_sum + (x_new - mean) * (x_new - new_mean) - (xs[next_index] - mean) * (xs[next_index] - new_mean);
                //varianceSum = varianceSumPrev + (sample - meanMovingPrev) * (sample - meanMoving.f) - (meanBuffer[meanIndex] - meanMovingPrev) * (meanBuffer[meanIndex] - meanMoving.f);
                
                // Jamie modified version of DanS equation (in comments)
                // varSum += (x_new + x_old - mean - new_mean) * (x_new - x_old), where x_old = xs[next_index]
                varianceSum = varianceSumPrev + (sample + meanBuffer[meanIndex] - meanMovingPrev - meanMoving.f) * (sample - meanBuffer[meanIndex]);
            }
#else
            // Always calculate 
            meanMoving = meanMoving + (sample - meanBuffer[meanIndex]) / ROLLING_WINDOW_SIZE;
            // Jaime modified equation
            varianceSum = varianceSumPrev + (sample + meanBuffer[meanIndex] - meanMovingPrev - meanMoving) * (sample - meanBuffer[meanIndex]);
#endif
            
            meanBuffer[meanIndex] = sample;
            meanIndex = (meanIndex + 1) % COUNTOF(meanBuffer);
            
#if 0
            if (position >= ROLLING_WINDOW_SIZE)
            {
                varianceMoving = varianceSum / (ROLLING_WINDOW_SIZE - 1);
            }
            else if (position > 0)
            {
                varianceMoving = varianceSum / position;
            }
#else
            //varianceMoving.f = varianceSum / (ROLLING_WINDOW_SIZE - 1);
            //varianceMoving = varianceSum * varianceDivisor;
#endif

            meanMovingPrev = meanMoving;
            varianceSumPrev = varianceSum;
            
            //fprintf(logFile, "%4d: mean=%.17e var=%.17e\r\n", position, meanMoving, varianceMoving);
            //printf("%4d: mean=%.17e var=%.17e\r\n", position, meanMoving, varianceMoving);
            //printf("%4d: mean=%.17e var=%.17e mean_bits=" BITS_FMT " var_bits=" BITS_FMT "\r\n", position, meanMoving.f, varianceMoving.f, meanMoving.i, varianceMoving.i);
        }
        
        // 1ms timer not resolute enough, using cycle counter instead
        //endTime = HAL_GetTick();
        //printf("Final mean=%.7f var=%.7f took %lu ms\r\n", meanMoving, varianceMoving, endTime - startTime);
        
        /* Stop cycle counter time and restore 1ms timer */
        TimerCount_Stop(&cycles);
        if(HAL_InitTick(TICK_INT_PRIORITY) != HAL_OK)
        {
            Error_Handler();
        }

        /* Compute the number of cycles taken. 0x1000000 -> systick is 24-bit counter */
        cycles_result = (uint64_t)(0x1000000 - cycles_systick_interrupt) * BenchmarkTick + (uint64_t)cycles;   

        //printf("Final mean=%.7f var=%.7f took %lu cycles\r\n", meanMoving, varianceMoving, cycles_result);
        //fprintf(logFile, "Final mean=%.17e var=%.17e took %.6f ms\r\n", meanMoving, varianceMoving, ((double)cycles_result * 1000.0) / HAL_RCC_GetSysClockFreq());
        //printf("Final mean=%.17e var=%.17e mean_bits=" BITS_FMT " var_bits=" BITS_FMT " took %.6f ms\r\n", meanMoving.f, varianceMoving.f, meanMoving.i, varianceMoving.i, ((double)cycles_result * 1000.0) / HAL_RCC_GetSysClockFreq());
        //printf("Final mean=%.17e var=%.17e took %.6f ms\r\n", meanMoving, varianceMoving, ((double)cycles_result * 1000.0) / HAL_RCC_GetSysClockFreq());
        printf("Final mean=%.17e var=%.17e took %.6f ms\r\n", meanMoving, varianceSum * varianceDivisor, ((double)cycles_result * 1000.0) / HAL_RCC_GetSysClockFreq());
        
        //fclose(logFile);
        //logFile = NULL;
        
    /* Infinite loop */
    while (1)
    {
        BSP_LED_Toggle(LED1);
        
        //HAL_Delay(1000);
        Sleep(1000);
    }
}


/* CPU low power version of HAL_Delay */
void Sleep(uint32_t Delay)
{
    uint32_t tickstart = HAL_GetTick();
    uint32_t wait = Delay;

    /* Add a freq to guarantee minimum wait */
    if (wait < HAL_MAX_DELAY)
    {
        wait += (uint32_t)(uwTickFreq);
    }

    while ((HAL_GetTick() - tickstart) < wait)
    {
        __WFE();
        __NOP();
    }

    __NOP();
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follows, which is the fastest
  *         allowed CPU1 clock downhole:
  *            System Clock source            = PLL (HSE BYPASS)
  *            SYSCLK(Hz)                     = 300000000 (CPU Clock)
  *            HCLK(Hz)                       = 150000000 (Cortex-M4 CPU, Bus matrix Clocks)
  *            AHB Prescaler                  = 2
  *            D1 APB3 Prescaler              = 2 (APB3 Clock 75MHz)
  *            D2 APB1 Prescaler              = 2 (APB1 Clock 75MHz)
  *            D2 APB2 Prescaler              = 2 (APB2 Clock 75MHz)
  *            D3 APB4 Prescaler              = 2 (APB4 Clock 75MHz)
  *            HSE Frequency(Hz)              = 8000000
  *            PLL_M                          = 2
  *            PLL_N                          = 150
  *            PLL_P                          = 2
  *            PLL_Q                          = 2
  *            PLL_R                          = 2
  *            VDD(V)                         = 3.3
  *            Flash Latency(WS)              = 2
  *
  *         As an alternative you could run the following to maximise the slow peripheral
  *         clocks, at the expense of only achieving 200MHz CPU1 clock and 100MHz
  *         fast peripheral clocks:
  *            System Clock source            = PLL (HSE BYPASS)
  *            SYSCLK(Hz)                     = 200000000 (CPU Clock)
  *            HCLK(Hz)                       = 100000000 (Cortex-M4 CPU, Bus matrix Clocks)
  *            AHB Prescaler                  = 2
  *            D1 APB3 Prescaler              = 1 (APB3 Clock 100MHz)
  *            D2 APB1 Prescaler              = 1 (APB1 Clock 100MHz)
  *            D2 APB2 Prescaler              = 1 (APB2 Clock 100MHz)
  *            D3 APB4 Prescaler              = 1 (APB4 Clock 100MHz)
  *            HSE Frequency(Hz)              = 8000000
  *            PLL_M                          = 2
  *            PLL_N                          = 100
  *            PLL_P                          = 2
  *            PLL_Q                          = 2
  *            PLL_R                          = 2
  *            VDD(V)                         = 3.3
  *            Flash Latency(WS)              = 1
  *
  *         In both cases VOS2 is used since the maximum peripheral speeds in
  *         VOS3 is 100MHz and if the frequency is slightly off it may be on the edge.
  *
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_OscInitTypeDef RCC_OscInitStruct;
    HAL_StatusTypeDef ret = HAL_OK;

    /*!< Supply configuration update enable */
    HAL_PWREx_ConfigSupply(PWR_DIRECT_SMPS_SUPPLY);

    /* The voltage scaling allows optimizing the power consumption when the device is
     * clocked below the maximum system frequency, to update the voltage scaling value
     * regarding system frequency refer to product datasheet.
     */
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

    while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}

    /* Enable HSE Oscillator and activate PLL with HSE as source */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
    RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
    RCC_OscInitStruct.CSIState = RCC_CSI_OFF;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;

    RCC_OscInitStruct.PLL.PLLM = 2;
    RCC_OscInitStruct.PLL.PLLN = 150;
    RCC_OscInitStruct.PLL.PLLFRACN = 0;
    RCC_OscInitStruct.PLL.PLLP = 2;
    RCC_OscInitStruct.PLL.PLLR = 2;
    RCC_OscInitStruct.PLL.PLLQ = 2;

    RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
    RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_2;
    ret = HAL_RCC_OscConfig(&RCC_OscInitStruct);
    if(ret != HAL_OK)
    {
        Error_Handler();
    }

    /* Select PLL as system clock source and configure  bus clocks dividers */
    RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_D1PCLK1 | RCC_CLOCKTYPE_PCLK1 | \
                                 RCC_CLOCKTYPE_PCLK2  | RCC_CLOCKTYPE_D3PCLK1);

    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV2;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV2;
    RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV2;
    ret = HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);
    if(ret != HAL_OK)
    {
        Error_Handler();
    }

    /*
     *  Note : The activation of the I/O Compensation Cell is recommended with communication  interfaces
     *         (GPIO, SPI, FMC, QSPI ...)  when  operating at  high frequencies(please refer to product datasheet)
     *         The I/O Compensation Cell activation  procedure requires :
     *          - The activation of the CSI clock
     *          - The activation of the SYSCFG clock
     *          - Enabling the I/O Compensation Cell : setting bit[0] of register SYSCFG_CCCSR
     *
     *    To do this please uncomment the following code
     */

    /*
    __HAL_RCC_CSI_ENABLE() ;

    __HAL_RCC_SYSCFG_CLK_ENABLE() ;

    HAL_EnableCompensationCell();
    */
  
    /* Enable the system clock output to MCO2 */
    HAL_RCC_MCOConfig(RCC_MCO2, RCC_MCO2SOURCE_SYSCLK, RCC_MCODIV_1);
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  //__HAL_RCC_GPIOH_CLK_ENABLE();
  //__HAL_RCC_GPIOA_CLK_ENABLE();
  //__HAL_RCC_GPIOB_CLK_ENABLE();
  //__HAL_RCC_GPIOD_CLK_ENABLE();
  //__HAL_RCC_GPIOG_CLK_ENABLE();

  /*Configure GPIO pin : PC9 */
  GPIO_InitStruct.Pin = GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF0_MCO;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
  /* User may add here some code to deal with this error */
  while(1)
  {
  }
}

/**
  * @brief  CPU L1-Cache enable.
  * @param  None
  * @retval None
  */
static void CPU_CACHE_Enable(void)
{
  /* Enable I-Cache */
  SCB_EnableICache();

  /* Enable D-Cache */
  SCB_EnableDCache();
}


#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
