#include <stm32h7xx.h>

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>

//#include <arm_math.h>
//#include <arm_const_structs.h>

//#include "var_data.h"
#include "main.h"
#include "stm32h7xx_it.h"
#include "utilities.h"

#include <stm32h7xx_hal_nand.h>

#define COUNTOF(x) (sizeof (x) / sizeof (x)[0])


#define BUFFERSIZE (4096)
#define SCRATCHSIZE (10)

#define CH3_PERIOD (99999)

// Jitter priod of 350us from a 25MHz/25 clock
#define EDGE_JITTER_LOCKOUT_PERIOD (350)

// Jitter priod of 350us from a 150MHz/1 clock
#define EDGE_JITTER_LOCKOUT_PERIOD (52500)

#define NUMBER_TIMES  (10000L)

/* Global private variables, usually device instances */

TIM_HandleTypeDef htim2;


static void Sleep(uint32_t Delay);
static void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM2_Init(void);
static void Error_Handler(void);


int main(void)
{
    /* System Init, System clock, voltage scaling and L1-Cache configuration are done by CPU1 (Cortex-M7)
       in the meantime Domain D2 is put in STOP mode(Cortex-M4 in deep-sleep)
    */

    HAL_EnableDBGSleepMode();
    HAL_EnableDBGStopMode();
    HAL_EnableDBGStandbyMode();
    
    HAL_EnableDomain2DBGSleepMode();
    HAL_EnableDomain2DBGStopMode();
    HAL_EnableDomain2DBGStandbyMode();

    HAL_EnableDomain3DBGStopMode();
    HAL_EnableDomain3DBGStandbyMode();

    /* Stop TIM2 in debug from CM7 */
    __HAL_DBGMCU_FREEZE_TIM2();
    
    /* STM32H7xx HAL library initialization:
       - Systick timer is configured by default as source of time base, but user
         can eventually implement his proper time base source (a general purpose
         timer for example or other time source), keeping in mind that Time base
         duration should be kept 1ms since PPP_TIMEOUT_VALUEs are defined and
         handled in milliseconds basis.
       - Set NVIC Group Priority to 4
       - Low Level Initialization
     */
    HAL_Init();

    /* Configure the system clock */
    SystemClock_Config();

    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_TIM2_Init();
    
    /* Re-initialise the tick timer for 1ms intervals if you need to do any
     * semi-accurate delays after using systick as a cycle counter.
     */
    if(HAL_InitTick(TICK_INT_PRIORITY) != HAL_OK)
    {
        Error_Handler();
    }
    
    //HAL_TIM_Base_Start_IT(&htim2);
    HAL_TIM_Base_Start(&htim2);
    
    // Start input compares for rising and falling edge, need 2 inputs as
    // want different processing on each (without having to manually check
    // state of input).
    HAL_TIM_IC_Start_IT(&htim2, TIM_CHANNEL_3);
    HAL_TIM_IC_Start_IT(&htim2, TIM_CHANNEL_4); // Do I even need to do this? Yes, because all I do later is enable/disable interrupts so I have to start the timer
    htim2.Instance->DIER &= ~TIM_IT_CC4; // Ignore falling edges for now
    
    /* Infinite loop */
    while (1)
    {
        Sleep(1000);
    }
    
    return 0;
}


bool     nextEdge = 0;


void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
    HAL_GPIO_WritePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin, GPIO_PIN_SET);
    
    if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_3)
    {
        // Channel 3 configured for rising edge on TI3 input
        uint32_t capture = htim->Instance->CCR3;

        lastCapture = capture;
        HAL_GPIO_WritePin(YELLOW_LED_GPIO_Port, YELLOW_LED_Pin, GPIO_PIN_SET);
        //HAL_GPIO_TogglePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin);

        // Disable further input captures (don't want to stop the timer)
        htim->Instance->DIER &= ~TIM_IT_CC3;
        //HAL_TIM_IC_Stop_IT(&htim2, TIM_CHANNEL_2);
        // Or maybe should disable capture
        //htim->Instance->CCER &= ~TIM_CCER_CC1E;
        
        // Start lockout timer
        htim2.Instance->CCR2 = capture + EDGE_JITTER_LOCKOUT_PERIOD;
        HAL_TIM_OC_Start_IT(&htim2, TIM_CHANNEL_2);
        
        // Set which edge will be enabled
        nextEdge = false;
    }
    else if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_4)
    {
        uint32_t capture = htim->Instance->CCR4;

        // Channel 4 configured for falling edge on TI3 (yes, 3) input
        HAL_GPIO_WritePin(YELLOW_LED_GPIO_Port, YELLOW_LED_Pin, GPIO_PIN_RESET);
        //HAL_GPIO_TogglePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin);
        
        // Disable further input captures
        htim->Instance->DIER &= ~TIM_IT_CC4;
        //HAL_TIM_IC_Stop_IT(&htim2, TIM_CHANNEL_4);
        
        // Start lockout timer for 200us in future (200us @ 1MHz == 200), must be
        // less than half fastest pulse from 1/rev encoder
        htim2.Instance->CCR2 = capture + EDGE_JITTER_LOCKOUT_PERIOD;
        HAL_TIM_OC_Start_IT(&htim2, TIM_CHANNEL_2);

        // Set which edge will be enabled
        nextEdge = true;
    }

    HAL_GPIO_WritePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin, GPIO_PIN_RESET);
}


/* CPU low power version of HAL_Delay */
void Sleep(uint32_t Delay)
{
    uint32_t tickstart = HAL_GetTick();
    uint32_t wait = Delay;

    /* Add a freq to guarantee minimum wait */
    if (wait < HAL_MAX_DELAY)
    {
        wait += (uint32_t)(uwTickFreq);
    }

    while ((HAL_GetTick() - tickstart) < wait)
    {
        __WFE();
        __NOP();
    }

    __NOP();
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follows, which is the fastest
  *         allowed CPU1 clock downhole:
  *            System Clock source            = PLL (HSE BYPASS)
  *            SYSCLK(Hz)                     = 300000000 (CPU Clock)
  *            HCLK(Hz)                       = 150000000 (Cortex-M4 CPU, Bus matrix Clocks)
  *            AHB Prescaler                  = 2
  *            D1 APB3 Prescaler              = 2 (APB3 Clock 75MHz)
  *            D2 APB1 Prescaler              = 2 (APB1 Clock 75MHz)
  *            D2 APB2 Prescaler              = 2 (APB2 Clock 75MHz)
  *            D3 APB4 Prescaler              = 2 (APB4 Clock 75MHz)
  *            HSE Frequency(Hz)              = 8000000
  *            PLL_M                          = 2
  *            PLL_N                          = 150
  *            PLL_P                          = 2
  *            PLL_Q                          = 2
  *            PLL_R                          = 2
  *            VDD(V)                         = 3.3
  *            Flash Latency(WS)              = 2
  *
  *         As an alternative you could run the following to maximise the slow peripheral
  *         clocks, at the expense of only achieving 200MHz CPU1 clock and 100MHz
  *         fast peripheral clocks:
  *            System Clock source            = PLL (HSE BYPASS)
  *            SYSCLK(Hz)                     = 200000000 (CPU Clock)
  *            HCLK(Hz)                       = 100000000 (Cortex-M4 CPU, Bus matrix Clocks)
  *            AHB Prescaler                  = 2
  *            D1 APB3 Prescaler              = 1 (APB3 Clock 100MHz)
  *            D2 APB1 Prescaler              = 1 (APB1 Clock 100MHz)
  *            D2 APB2 Prescaler              = 1 (APB2 Clock 100MHz)
  *            D3 APB4 Prescaler              = 1 (APB4 Clock 100MHz)
  *            HSE Frequency(Hz)              = 8000000
  *            PLL_M                          = 2
  *            PLL_N                          = 100
  *            PLL_P                          = 2
  *            PLL_Q                          = 2
  *            PLL_R                          = 2
  *            VDD(V)                         = 3.3
  *            Flash Latency(WS)              = 1
  *
  *         In both cases VOS2 is used since the maximum peripheral speeds in
  *         VOS3 is 100MHz and if the frequency is slightly off it may be on the edge.
  *
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
    RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
    RCC_OscInitTypeDef RCC_OscInitStruct = {0};
    RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};
    HAL_StatusTypeDef ret = HAL_OK;

    /*!< Supply configuration update enable */
    HAL_PWREx_ConfigSupply(PWR_DIRECT_SMPS_SUPPLY);

    /* The voltage scaling allows optimizing the power consumption when the device is
     * clocked below the maximum system frequency, to update the voltage scaling value
     * regarding system frequency refer to product datasheet.
     */
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

    while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}

    /* Enable HSE Oscillator and activate PLL with HSE as source */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
    RCC_OscInitStruct.CSIState = RCC_CSI_OFF;
    
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    
    // For running at 25MHz (50MHz CPU7) while developing
//    RCC_OscInitStruct.PLL.PLLM = 5;
//    RCC_OscInitStruct.PLL.PLLN = 100;
//    RCC_OscInitStruct.PLL.PLLFRACN = 0;
//    RCC_OscInitStruct.PLL.PLLP = 10;
//    RCC_OscInitStruct.PLL.PLLR = 10;
//    RCC_OscInitStruct.PLL.PLLQ = 10;

    // For running at 300MHz (300MHz CPU7) for highest downhole speed
    RCC_OscInitStruct.PLL.PLLM = 2;
    RCC_OscInitStruct.PLL.PLLN = 48;
    RCC_OscInitStruct.PLL.PLLFRACN = 0;
    RCC_OscInitStruct.PLL.PLLP = 2;
    RCC_OscInitStruct.PLL.PLLR = 6;
    RCC_OscInitStruct.PLL.PLLQ = 6;

    RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
    RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_2;
    ret = HAL_RCC_OscConfig(&RCC_OscInitStruct);
    if(ret != HAL_OK)
    {
        Error_Handler();
    }

    /* Select PLL as system clock source and configure  bus clocks dividers */
    RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_D1PCLK1 | RCC_CLOCKTYPE_PCLK1 | \
                                 RCC_CLOCKTYPE_PCLK2  | RCC_CLOCKTYPE_D3PCLK1);

    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV2;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV2;
    RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV2;
    
    ret = HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);
    if(ret != HAL_OK)
    {
        Error_Handler();
    }

    /*
     *  Note : The activation of the I/O Compensation Cell is recommended with communication  interfaces
     *         (GPIO, SPI, FMC, QSPI ...)  when  operating at  high frequencies(please refer to product datasheet)
     *         The I/O Compensation Cell activation  procedure requires :
     *          - The activation of the CSI clock
     *          - The activation of the SYSCFG clock
     *          - Enabling the I/O Compensation Cell : setting bit[0] of register SYSCFG_CCCSR
     *
     *    To do this please uncomment the following code
     */

    /*
    __HAL_RCC_CSI_ENABLE() ;

    __HAL_RCC_SYSCFG_CLK_ENABLE() ;

    HAL_EnableCompensationCell();
    */
  
    /* Configure clock to FMC peripheral */
    PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_FMC;
    PeriphClkInitStruct.FmcClockSelection = RCC_FMCCLKSOURCE_D1HCLK;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
    {
        Error_Handler();
    }

    
    /* Enable the system clock output to MCO2 */
    HAL_RCC_MCOConfig(RCC_MCO2, RCC_MCO2SOURCE_SYSCLK, RCC_MCODIV_1);
}


/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* GPIO Ports Clock Enable - generated by STM32CubeMx, in order of port usage starting from pin 1 and working round */
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOH_CLK_ENABLE();
    __HAL_RCC_GPIOE_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();

    // Configure output levels prior to configuring IO
    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOB, GREEN_LED_Pin|GPIO_PIN_14|NAND_CE2__Pin, GPIO_PIN_RESET);

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(YELLOW_LED_GPIO_Port, YELLOW_LED_Pin, GPIO_PIN_RESET);

    /* Configure GPIO pin : PC9 */
    GPIO_InitStruct.Pin = GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF0_MCO;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    
    /* Configure extra GPIO pins for NAND flash : PB14 - WP#, PB15 - CE2# */
    GPIO_InitStruct.Pin = GREEN_LED_Pin | GPIO_PIN_14 | GPIO_PIN_15;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

//    GPIO_InitStruct.Pin = GPIO_PIN_15;
//    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//    GPIO_InitStruct.Pull = GPIO_NOPULL;
//    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
//    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
    
    /*Configure GPIO pin : YELLOW_LED_Pin */
    GPIO_InitStruct.Pin = YELLOW_LED_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(YELLOW_LED_GPIO_Port, &GPIO_InitStruct);

    // Why not the other GPIO pins for the peripherals?
    // Are they done in the peripheral drivers?
    // A: they are done in the "MSP" functions in stm32h7xx_hal_msp.c
}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{
    /* USER CODE BEGIN TIM2_Init 0 */

    /* USER CODE END TIM2_Init 0 */

    TIM_ClockConfigTypeDef sClockSourceConfig = {0};
    TIM_MasterConfigTypeDef sMasterConfig = {0};
    TIM_IC_InitTypeDef sConfigIC = {0};
    TIM_OC_InitTypeDef sConfigOC = {0};

    /* USER CODE BEGIN TIM2_Init 1 */

    /* USER CODE END TIM2_Init 1 */
    htim2.Instance = TIM2;
    htim2.Init.Prescaler = 0;
    htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim2.Init.Period = 4294967295;
    //htim2.Init.Period = 999999;
    htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
    {
        Error_Handler();
    }

    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
    {
        Error_Handler();
    }

    if (HAL_TIM_IC_Init(&htim2) != HAL_OK)
    {
        Error_Handler();
    }

    if (HAL_TIM_OC_Init(&htim2) != HAL_OK)
    {
        Error_Handler();
    }

    sConfigOC.OCMode = TIM_
    sConfigOC.Pulse = 0;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    if (HAL_TIM_OC_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
    {
        Error_Handler();
    }

    sConfigOC.OCMode = TIM_OCMODE_TOGGLE;
    sConfigOC.Pulse = 0;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    if (HAL_TIM_OC_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
    {
        Error_Handler();
    }

    sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
    sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
    sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
    sConfigIC.ICFilter = 0;
    if (HAL_TIM_IC_ConfigChannel(&htim2, &sConfigIC, TIM_CHANNEL_3) != HAL_OK)
    {
        Error_Handler();
    }

    sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_FALLING;
    sConfigIC.ICSelection = TIM_ICSELECTION_INDIRECTTI;
    sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
    sConfigIC.ICFilter = 0;
    if (HAL_TIM_IC_ConfigChannel(&htim2, &sConfigIC, TIM_CHANNEL_4) != HAL_OK)
    {
        Error_Handler();
    }
    /* USER CODE BEGIN TIM2_Init 2 */

    /* USER CODE END TIM2_Init 2 */
    HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{
    /* USER CODE BEGIN TIM3_Init 0 */

    /* USER CODE END TIM3_Init 0 */

    TIM_ClockConfigTypeDef sClockSourceConfig = {0};
    TIM_SlaveConfigTypeDef sSlaveConfig = {0};
    TIM_MasterConfigTypeDef sMasterConfig = {0};
    //TIM_OC_InitTypeDef sConfigOC = {0};
    TIM_OnePulse_InitTypeDef sConfig = {0};

    /* USER CODE BEGIN TIM3_Init 1 */

    /* USER CODE END TIM3_Init 1 */
    htim3.Instance = TIM3;
    htim3.Init.Prescaler = 149;
    htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim3.Init.Period = 833;
    htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
    {
        Error_Handler();
    }
    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
    {
        Error_Handler();
    }

//    if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
//    {
//        Error_Handler();
//    }

    if (HAL_TIM_OnePulse_Init(&htim3, TIM_OPMODE_SINGLE) != HAL_OK)
    {
        Error_Handler();
    }
    
    sSlaveConfig.SlaveMode = TIM_SLAVEMODE_COMBINED_RESETTRIGGER;
    sSlaveConfig.InputTrigger = TIM_TS_ITR1;
    if (HAL_TIM_SlaveConfigSynchro(&htim3, &sSlaveConfig) != HAL_OK)
    {
        Error_Handler();
    }
    
    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
    {
        Error_Handler();
    }
    
//    sConfigOC.OCMode = TIM_OCMODE_PWM2;
//    sConfigOC.Pulse = 1;
//    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
//    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
//    if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
    
    
    sConfig.OCMode       = TIM_OCMODE_PWM2;
    sConfig.OCPolarity   = TIM_OCPOLARITY_HIGH;
    sConfig.Pulse        = 1;
    sConfig.ICPolarity   = TIM_ICPOLARITY_RISING;
    sConfig.ICSelection  = TIM_ICSELECTION_DIRECTTI;
    sConfig.ICFilter     = 0;
    sConfig.OCNPolarity  = TIM_OCNPOLARITY_HIGH;
    sConfig.OCIdleState  = TIM_OCIDLESTATE_RESET;
    sConfig.OCNIdleState = TIM_OCNIDLESTATE_RESET;    
    if (HAL_TIM_OnePulse_ConfigChannelOutput(&htim3, &sConfig, TIM_CHANNEL_1) != HAL_OK)
    {
        Error_Handler();
    }
    
    __HAL_TIM_DISABLE_OCxPRELOAD(&htim3, TIM_CHANNEL_1);
    /* USER CODE BEGIN TIM3_Init 2 */

    /* USER CODE END TIM3_Init 2 */
    HAL_TIM_MspPostInit(&htim3);
}


/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
    /* User may add here some code to deal with this error */
    while(1)
    {
    }
}
