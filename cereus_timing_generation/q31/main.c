#include <stm32h7xx.h>

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>

//#include <arm_math.h>
//#include <arm_const_structs.h>

//#include "var_data.h"
#include "main.h"
#include "stm32h7xx_it.h"
#include "utilities.h"

#include <stm32h7xx_hal_nand.h>

#define COUNTOF(x) (sizeof (x) / sizeof (x)[0])


#define BUFFERSIZE (4096)
#define SCRATCHSIZE (10)

#define CH3_PERIOD (99999)

// Jitter priod of 350us from a 25MHz/25 clock
#define EDGE_JITTER_LOCKOUT_PERIOD (400)

// Jitter priod of 350us from a 150MHz/1 clock
//#define EDGE_JITTER_LOCKOUT_PERIOD (52500)

// Minimum expected time allowed between motor index pulses in microseconds
#define MINIMUM_CAPTURE_PERIOD (2000)


#define NUMBER_TIMES  (2000L)


#define NCO_MODULUS (4294967296.0)


// Number of seconds after first motor edge before considering motor is up to speed
#define MOTOR_STARTUP_DELAY_SECONDS (12)

// Number of seconds after first motor edge before starting to generate output pulses
#define MOTOR_OUTPUT_DELAY_SECONDS (15)


/* Global private variables, usually device instances */
NAND_HandleTypeDef hnand1;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
//TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim12;


static void CPU_CACHE_Enable(void);
void Sleep(uint32_t Delay);
static void SystemClock_Config(void);
static void MPU_Config(void);
static void MX_FMC_Init(void);
static void MX_GPIO_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
//static void MX_TIM4_Init(void);
static void MX_TIM12_Init(void);
static void Error_Handler(void);


static volatile uint32_t frequencyBufferRegister;

// Motor gearbox is 3375/64 : 1
volatile int outputSamplesPerRotation = 60;
volatile int gearboxNumerator = 3375;
volatile int gearboxDenominator = 64;
//double A = 1.1377777777777777777777777777778;
volatile double A;


//    // Timer algorithm variables
//    uint32_t secondPreviousMotorCapture;
//    uint32_t previousMotorCapture;
//    uint32_t motorCapture;
//    //uint32_t deltaMotorCapture;
//
//    uint32_t outputPeriodNumerator;
//    uint32_t outputPeriodDenominator;
//    uint32_t outputPeriodRemainder;
//
//    //uint32_t previousMotorPeriod;
//    uint32_t motorPeriod;
//
//    uint32_t previousOutputPeriod;
//    uint32_t outputPeriod;
//
//    int32_t compareAdjustment;
//    int32_t compareAdjustmentRemainder = 0;
//    uint32_t newCompareRegister;

volatile int32_t delayedCompareAdjustment = 0; // If the timer is too close we just fire on existing time and adjust afterwards


// Second timer attempt based on the NCO calculations used to drive timer compare points
volatile uint32_t ncoBasedTimerCounter = 0;
volatile uint32_t lastTimerCounterValue = 0;
volatile uint32_t previousCaptureRegister = 0;


// Used for self-testing the timing
//volatile uint32_t   motorIndexTimes[NUMBER_TIMES];
//volatile uint32_t   motorIndexPosition = 0;
//
//volatile uint32_t   takeSampleTimes[NUMBER_TIMES];
//volatile uint32_t   takeSamplePosition = 0;


enum TimerTraceType
{
    TTT_MOTORINDEX,
    TTT_SAMPLESYNC
};

struct TimerTrace
{
    enum TimerTraceType type;
    uint32_t            counter;
    uint32_t            ccr;
    uint32_t            newNcoCount;
    uint32_t            nextCcr1;
};
volatile struct TimerTrace  timerTraces[NUMBER_TIMES];
volatile uint32_t           nextTimerTracesIndex = 0;

volatile uint32_t           lastCapture = 0x80000000UL;
volatile bool               nextEdge = true;
volatile static uint32_t    lastCompare = 0;


volatile bool       motorStarted = false;
volatile uint32_t   timingStartupCounter = 0;
volatile bool       generateTiming = false;


volatile uint32_t   missedSamples = 0;

int main(void)
{
//    uint32_t cycles = 0;
//    uint32_t cycles_systick_interrupt = 0;
//    __IO uint32_t cycles_result = 0; /* with uint32_t type, the algo execution 
//	                                    time should not exceed 4,2 ms */
    
//    __IO uint16_t data;
//    __IO uint32_t data32;
//    uint32_t deviceAddress = NAND_DEVICE;
//    uint16_t id[4];
//    static uint8_t buffer[BUFFERSIZE];

    A = ((double)outputSamplesPerRotation * gearboxDenominator) / gearboxNumerator;
    
    /* System Init, System clock, voltage scaling and L1-Cache configuration are done by CPU1 (Cortex-M7)
       in the meantime Domain D2 is put in STOP mode(Cortex-M4 in deep-sleep)
    */

    HAL_EnableDBGSleepMode();
    HAL_EnableDBGStopMode();
    HAL_EnableDBGStandbyMode();
    
    HAL_EnableDomain2DBGSleepMode();
    HAL_EnableDomain2DBGStopMode();
    HAL_EnableDomain2DBGStandbyMode();

    HAL_EnableDomain3DBGStopMode();
    HAL_EnableDomain3DBGStandbyMode();

    /* Stop TIM2 in debug from CM7 */
    __HAL_DBGMCU_FREEZE_TIM2();
    __HAL_DBGMCU_FREEZE_TIM3();
    __HAL_DBGMCU_FREEZE_TIM4();
    __HAL_DBGMCU_FREEZE_TIM12();
    
    __HAL_RCC_TIM2_FORCE_RESET();
    __HAL_RCC_TIM2_RELEASE_RESET();
    
    __HAL_RCC_TIM3_FORCE_RESET();
    __HAL_RCC_TIM3_RELEASE_RESET();

    __HAL_RCC_TIM12_FORCE_RESET();
    __HAL_RCC_TIM12_RELEASE_RESET();

    /* Enable the CPU Cache */
    CPU_CACHE_Enable();

    /* STM32H7xx HAL library initialization:
       - Systick timer is configured by default as source of time base, but user
         can eventually implement his proper time base source (a general purpose
         timer for example or other time source), keeping in mind that Time base
         duration should be kept 1ms since PPP_TIMEOUT_VALUEs are defined and
         handled in milliseconds basis.
       - Set NVIC Group Priority to 4
       - Low Level Initialization
     */
    HAL_Init();

    /* Configure the system clock - MUST come after HAL_Init() as it calls some
     * HAL function that fails if it has not previously been set up.
     * Not sure why HAL_Init() then uses the wrong clock...
     */
    SystemClock_Config();

    MPU_Config();
    
    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_FMC_Init();
    MX_TIM2_Init();
    MX_TIM3_Init();
    //MX_TIM4_Init();
    MX_TIM12_Init();
    
    // Don't want an update event thank you very much HAL code, nothing is preloaded in this timer
    //htim12.Instance->SR &= ~TIM_SR_UIF;
    
    //BSP_LED_Init(LED1);
    //BSP_LED_Init(LED2);
    //BSP_LED_Init(LED3);
    
    /* Count the cycle number consumed by systick handler (it's counted only
     * when a systick overflow occurs). Need to stop, start the timer, run the
     * handler and get the duration. Calling the handler should be similar to
     * the interrupt getting called and then it calling the handler.
     */
//    TimerCount_DeInit();
//    TimerCount_Start();
//    SysTick_Handler();
//    TimerCount_Stop(&cycles_systick_interrupt);

    /* Re-initialise the tick timer for 1ms intervals if you need to do any
     * semi-accurate delays after using systick as a cycle counter.
     * Might also need to be done after setting up the system clock.
     */
    if(HAL_InitTick(TICK_INT_PRIORITY) != HAL_OK)
    {
        Error_Handler();
    }
    
    /* Add Cortex-M7 user application code here */
    

    // Set second Chip select high while testing
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_SET);
    
    // Write protect
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_RESET);

    Sleep(100);
    
    // Does FMC manage it's own chip select? - Yes
//    HAL_StatusTypeDef fmcStatus;
//    fmcStatus = HAL_NAND_Reset(&hnand1);
//    if (fmcStatus != HAL_OK)
//    {
//        Error_Handler();
//    }


    //HAL_TIM_Base_Start_IT(&htim2);
    HAL_TIM_Base_Start(&htim2);
    
    // Start input compares for rising and falling edge, need 2 inputs as
    // want different processing on each (without having to manually check
    // state of input).
    HAL_TIM_IC_Start_IT(&htim2, TIM_CHANNEL_3);
//    HAL_TIM_IC_Start_IT(&htim2, TIM_CHANNEL_4); // Do I even need to do this? Yes, because all I do later is enable/disable interrupts so I have to start the timer
//    htim2.Instance->DIER &= ~TIM_DIER_CC4IE_Msk; // Ignore falling edges for now
    
    // Don't start the output compare for the sampling timing generator - will be done later when up to speed
    //HAL_TIM_OC_Start_IT(&htim2, TIM_CHANNEL_1);
    
    
    // After the CCR is configured using the HAL function (CCR set by the "Pulse" duration), just write to register to update it.
    //htim2.Instance->CCR3 = htim2.Instance->CNT + CH3_PERIOD;
    //htim2.Instance->CCR3 = htim2.Instance->CNT + CH3_PERIOD;
    //HAL_TIM_OC_Start_IT(&htim2, TIM_CHANNEL_3);

    //htim2.Instance->CCR4 = htim2.Instance->CNT + 999;
    //HAL_TIM_OC_Start_IT(&htim2, TIM_CHANNEL_4);

    // Need to use own modified functions for OnePulse mode since the HAL clears all the slave/trigger settings
//    HAL_TIM_Base_Start(&htim3);
//    HAL_TIM_OnePulse_Start(&htim3, TIM_CHANNEL_1);

    HAL_TIM_Base_Start(&htim3);
    HAL_TIM_OnePulse_Start(&htim3, TIM_CHANNEL_1);
    TIM3->CCMR1 = (TIM3->CCMR1 & ~TIM_CCMR1_OC1M_Msk) | (0 | TIM_CCMR1_OC1M_2 | 0 | 0); // Force pulse channel output inactive for now

//    HAL_TIM_Base_Start(&htim4);
//    HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);

    // Only start htim12 when we start the timing as it is essentially a WDT
    
    /* Infinite loop */
    
    while (1)
    {
        //HAL_Delay(1000);
        Sleep(1000);

        if (motorStarted)
        {
            if (timingStartupCounter < MOTOR_OUTPUT_DELAY_SECONDS)
            {
                ++timingStartupCounter;
                if (timingStartupCounter == MOTOR_STARTUP_DELAY_SECONDS)
                {
                    generateTiming = true;
                    
                    HAL_TIM_OC_Start_IT(&htim2, TIM_CHANNEL_1);
                }
                else if (timingStartupCounter == MOTOR_OUTPUT_DELAY_SECONDS)
                {
                    // Change pulse channel output to generate
                    TIM3->CCMR1 = (TIM3->CCMR1 & ~TIM_CCMR1_OC1M_Msk) | (0 | TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_0);

                    // Start timing WDT
                    TIM12->SR &= ~TIM_SR_UIF_Msk;
                    HAL_TIM_Base_Start_IT(&htim12);
                }
            }
        }
    }
}


#if 1
// Called when timer overflow occurs
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if (htim == &htim12)
    {
        // 8 ms since the last sample point - too long
        // Force an update?
        uint32_t counter = htim2.Instance->CNT;
        uint32_t motorIdxCapture = htim2.Instance->CCR3;
        uint32_t sampleCompare = htim2.Instance->CCR1;
        
        ++missedSamples;
    }
}
#endif


void Timer2_OC1_OutputSample(void)
{
    HAL_GPIO_WritePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin, GPIO_PIN_SET);

    // Kick the sample timer WDT
    TIM12->CNT = 0;
    
    // Sample timing generation period
    //__HAL_TIM_GetCounter(&htim2);
    uint32_t counter = htim2.Instance->CNT;
    uint32_t compareRegister = htim2.Instance->CCR1;
    
//    if (takeSamplePosition < NUMBER_TIMES)
//    {
//        takeSampleTimes[takeSamplePosition++] = compareRegister;
//    }

    if ((compareRegister - lastCompare) < 100)
    {
        // Should never happen? Something out of sync
        lastCompare = lastCompare;
    }
    
    // After the CCR is configured using the HAL function (CCR set by the "Pulse" duration), just write to register to update it.
    //htim2.Instance->CCR1 += CH1_PERIOD;
    
    //HAL_GPIO_TogglePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin);

    //HAL_TIM_OC_Start(&htim3, TIM_CHANNEL_1);

    // Timer algorithmn based on NCO calculations
    // Only change if it has not already been done at the same instant
    uint32_t deltaCompare = compareRegister - lastTimerCounterValue;
    uint32_t lastNcoCount = ncoBasedTimerCounter;
    ncoBasedTimerCounter = lastNcoCount + deltaCompare * frequencyBufferRegister;

    uint32_t freqBufferHold = frequencyBufferRegister;
    uint32_t newNcoCount = ncoBasedTimerCounter;
    //compareRegister += (NCO_MODULUS - ncoBasedTimerCounter + frequencyBufferRegister - 1) / frequencyBufferRegister + delayedCompareAdjustment;
    //uint32_t adjustNumerator = (NCO_MODULUS - newNcoCount + freqBufferHold - 1);
    //uint32_t ncoOverflowPeriod = (NCO_MODULUS - newNcoCount + freqBufferHold - 1) / freqBufferHold;
    uint32_t ncoOverflowPeriod = lround(ceil((NCO_MODULUS - newNcoCount) / freqBufferHold));
    //uint32_t ncoOverflowPeriod = (uint32_t)((NCO_MODULUS - newNcoCount + freqBufferHold - 1) / freqBufferHold);
    uint32_t finalAdjust = delayedCompareAdjustment;
    uint32_t comparePeriod = ncoOverflowPeriod + finalAdjust;
    uint32_t maxPeriod = lround(ceil(NCO_MODULUS / frequencyBufferRegister));
    
//    if (comparePeriod < 100)
//    {
//        comparePeriod = maxPeriod;
//    }
//    
    uint32_t newCompare = compareRegister + comparePeriod;

    timerTraces[nextTimerTracesIndex].type = TTT_SAMPLESYNC;
    timerTraces[nextTimerTracesIndex].counter = counter;
    timerTraces[nextTimerTracesIndex].ccr = compareRegister;
    timerTraces[nextTimerTracesIndex].newNcoCount = ncoBasedTimerCounter;
    timerTraces[nextTimerTracesIndex].nextCcr1 = newCompare;
    ++nextTimerTracesIndex;
    if (nextTimerTracesIndex == NUMBER_TIMES)
    {
        nextTimerTracesIndex = 0;
    }

    ////            if ((newCompare - lastCompare) > 6000)
////            {
////                newCompare = lastCompare;
////            }
//
//    // Check if compare register is being set way out in future compared to current counter value
////            if (newCompare > (counter + maxPeriod + 1000))
////            {
////                // Already adjusted?
////                newCompare = compareRegister;
////            }
    htim2.Instance->CCR1 = newCompare;
//    delayedCompareAdjustment = 0;
    
    lastCompare = compareRegister;
    lastTimerCounterValue = compareRegister;

    HAL_GPIO_WritePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin, GPIO_PIN_RESET);

#if 0
    // Only toggle the pin temporarily while not using slave timer for output pulse
    HAL_GPIO_TogglePin(SAMPLE_SYNC_Port, SAMPLE_SYNC_Pin);
#endif
}


// Edge detection lockout timer
void Timer2_OC2_MotorIndexLockout(void)
{
#if 0
    // Stop lockout timer
    HAL_TIM_OC_Stop_IT(&htim2, TIM_CHANNEL_2); /* Is this going to stop the entire timer? */

    // Enable 1/rev input capture again for new edges, depending on
    // which edge we expect next.
    if (nextEdge)
    {
        //HAL_TIM_IC_Start_IT(&htim2, TIM_CHANNEL_3);
        htim2.Instance->SR &= ~(TIM_SR_CC3OF | TIM_SR_CC3IF);
        htim2.Instance->DIER |= (TIM_DIER_CC3IE);
    }
    else
    {
        //HAL_TIM_IC_Start_IT(&htim2, TIM_CHANNEL_4);
        htim2.Instance->SR &= ~(TIM_SR_CC4OF | TIM_SR_CC4IF);
        htim2.Instance->DIER |= (TIM_DIER_CC4IE);
    }
#endif
}


void Timer2_IC3_MotorIndexRisingEdge(void)
{
    HAL_GPIO_WritePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin, GPIO_PIN_SET);
    HAL_GPIO_TogglePin(YELLOW_LED_GPIO_Port, YELLOW_LED_Pin);
    
    motorStarted = true;
    
//    // Start lockout timer
//    //HAL_TIM_OC_Start_IT(&htim2, TIM_CHANNEL_2);
//    //htim2.Instance->CCR2 = capture + EDGE_JITTER_LOCKOUT_PERIOD;
//    htim2.Instance->CCR2 = htim2.Instance->CNT + EDGE_JITTER_LOCKOUT_PERIOD;
//    htim2.Instance->CCER |= TIM_CCER_CC2E;
//    htim2.Instance->SR &= ~TIM_SR_CC2IF_Msk;
//    htim2.Instance->DIER |= (TIM_DIER_CC2IE);
//    
//    // Set which edge will be enabled
//    nextEdge = false;

    if (generateTiming)
    {
        // Channel 3 configured for rising edge on TI3 input
        uint32_t counter = htim2.Instance->CNT;
        uint32_t capture = htim2.Instance->CCR3;

        if ((capture - lastCapture) >= MINIMUM_CAPTURE_PERIOD)
        {

//            if (motorIndexPosition < NUMBER_TIMES)
//            {
//                motorIndexTimes[motorIndexPosition++] = capture;
//            }
            
            // Perform input capture rising edge processing for 1/rev
            // Firmware simulation variables - Max's NCO
            static uint32_t previousK = 0;
            static uint32_t previousN = 0;
            static double remainder = 0.0;

            uint32_t currentK = 0;
            uint32_t currentN = 0;
            int32_t deltaK = 0;
            double numerator = 0;

            // Timer based NCO routine
            uint32_t previousNcoCount;
            uint32_t newNcoCount;
            uint32_t deltaCapture;
            
            previousNcoCount = ncoBasedTimerCounter;
            deltaCapture = capture - lastTimerCounterValue;
            newNcoCount = previousNcoCount + deltaCapture * frequencyBufferRegister;
            ncoBasedTimerCounter = newNcoCount;
            
            // NCO routine
            currentK = capture - previousCaptureRegister;
            deltaK = currentK - previousK;
            numerator = A * NCO_MODULUS - (double)deltaK * (double)previousN + remainder;
            remainder = fmod(numerator, (double)currentK);
            previousK = currentK;
            currentN = (uint32_t)floor(numerator / (double)currentK);
            frequencyBufferRegister = currentN;
            previousN = currentN;

            // Timer update method based on NCO calculations
            int32_t compareAdjustment;
            uint32_t newCompareRegister;
            
            uint32_t freqBufferHold = frequencyBufferRegister;
            //compareAdjustment = (NCO_MODULUS - ncoBasedTimerCounter + freqBufferHold - 1) / freqBufferHold;
            //compareAdjustment = lround(ceil((NCO_MODULUS - ncoBasedTimerCounter) / freqBufferHold));
            compareAdjustment = (int32_t)((NCO_MODULUS - ncoBasedTimerCounter + freqBufferHold - 1) / freqBufferHold);
            
    //        if (compareAdjustment < 100)
    //        {
    //            //compareAdjustment = 5000;
    //        }
        newCompareRegister = capture + compareAdjustment;

        timerTraces[nextTimerTracesIndex].type = TTT_MOTORINDEX;
        timerTraces[nextTimerTracesIndex].counter = counter;
        timerTraces[nextTimerTracesIndex].ccr = capture;
        timerTraces[nextTimerTracesIndex].newNcoCount = ncoBasedTimerCounter;
        timerTraces[nextTimerTracesIndex].nextCcr1 = newCompareRegister;
        ++nextTimerTracesIndex;
        if (nextTimerTracesIndex == NUMBER_TIMES)
        {
            nextTimerTracesIndex = 0;
        }

    //    if (!generateTiming)
    //    {
    //        // Just a little thing to simulate not running until motor
    //        // is up to speed. Saves false triggers at low speed.
    //        timingStartupCounter++;
    //        if (timingStartupCounter >= 2400)
    //        {
    //            generateTiming = true;
    //            
    //            // Start sample generation timer
    //            htim2.Instance->CCR1 = newCompareRegister;
    //            HAL_TIM_OC_Start_IT(&htim2, TIM_CHANNEL_1);
    //
    //            // Start timing WDT
    //            //HAL_TIM_Base_Start_IT(&htim12);
    //            
    //            // Just for error detection's sake, really should be
    //            // something like calculated from the NCO modulus, frequency
    //            // buffer register and the current NCO counter value etc.
    //            lastCompare = capture;
    //        }
    //    }
    //    else
        
    //        if ((newCompareRegister - lastCompare) > 6000)
    //        {
    //            newCompareRegister = lastCompare;
    //        }
            
    //            if (htim2.Instance->CNT < (newCompareRegister - 20))
    //            {
    //                if ((htim2.Instance->SR & TIM_SR_CC1IF) == 0)
    //                {
    //                    htim2.Instance->CCR1 = newCompareRegister;
    //                }
    //            }
    //            else
    //            {
    //                delayedCompareAdjustment = compareAdjustment;
    //            }
            
            // Only actually update the register if it isn't already going to do
            // it, i.e. if an ncoOverflow hasn't happened that we're waiting to)
            // process.
            //if (ncoBasedTimerCounter > previousNcoCount)
            if ((TIM2->SR & TIM_SR_CC1IF_Msk) == 0)
            {
                htim2.Instance->CCR1 = newCompareRegister;
                // TODO - this needs to be "if CNT has passed newCompare - even in case of overflow which it does not just now.
                //if (htim2.Instance->CNT >= (newCompareRegister-1))
                if (IsEarlierThan(newCompareRegister, htim2.Instance->CNT))
                {
                    htim2.Instance->EGR |= TIM_EGR_CC1G;
                }
            }

            previousCaptureRegister = capture;
            lastTimerCounterValue = capture;
            lastCapture = capture;
        }
    }

    // Clear out any overcapture, just in case
    TIM2->SR &= ~TIM_SR_CC3OF_Msk;
    
    HAL_GPIO_WritePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin, GPIO_PIN_RESET);
}


bool IsEarlierThan(uint32_t a, uint32_t b)
{
	bool aBeforeB;
	uint32_t min_ab;
	uint32_t max_ab;
	
	if (a <= b)
	{
		min_ab = a;
		max_ab = b;
	}
	else
	{
		min_ab = b;
		max_ab = a;
	}
	
	uint32_t delta = max_ab - min_ab;
	if (delta >= 2147483648UL)
	{
		aBeforeB = (a >= b);
	}
	else
	{
		aBeforeB = (a <= b);
	}
	
	return aBeforeB;
}


void Timer2_IC4_MotorIndexFallingEdge(void)
{
#if 0 
    HAL_GPIO_WritePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin, GPIO_PIN_SET);

    uint32_t capture = htim2.Instance->CCR4;

    // Channel 4 configured for falling edge on TI3 (yes, 3) input
    HAL_GPIO_WritePin(YELLOW_LED_GPIO_Port, YELLOW_LED_Pin, GPIO_PIN_RESET);
    //HAL_GPIO_TogglePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin);
    
    // Disable further input captures
    htim2.Instance->DIER &= ~TIM_DIER_CC4IE_Msk;
    //HAL_TIM_IC_Stop_IT(&htim2, TIM_CHANNEL_4);
    
    // Start lockout timer for 200us in future (200us @ 1MHz == 200), must be
    // less than half fastest pulse from 1/rev encoder
    //htim2.Instance->CCR2 = capture + EDGE_JITTER_LOCKOUT_PERIOD;
    htim2.Instance->CCR2 = htim2.Instance->CNT + EDGE_JITTER_LOCKOUT_PERIOD;
    htim2.Instance->CCER |= TIM_CCER_CC2E;
    htim2.Instance->SR &= ~TIM_SR_CC2IF_Msk;
    htim2.Instance->DIER |= (TIM_DIER_CC2IE);

    // Set which edge will be enabled
    nextEdge = true;

    HAL_GPIO_WritePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin, GPIO_PIN_RESET);
#endif
}


/* CPU low power version of HAL_Delay */
void Sleep(uint32_t Delay)
{
    uint32_t tickstart = HAL_GetTick();
    uint32_t wait = Delay;

    /* Add a freq to guarantee minimum wait */
    if (wait < HAL_MAX_DELAY)
    {
        wait += (uint32_t)(uwTickFreq);
    }

    while ((HAL_GetTick() - tickstart) < wait)
    {
        __WFE();
        __NOP();
    }

    __NOP();
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follows, which is the fastest
  *         allowed CPU1 clock downhole:
  *            System Clock source            = PLL (HSE BYPASS)
  *            SYSCLK(Hz)                     = 300000000 (CPU Clock)
  *            HCLK(Hz)                       = 150000000 (Cortex-M4 CPU, Bus matrix Clocks)
  *            AHB Prescaler                  = 2
  *            D1 APB3 Prescaler              = 2 (APB3 Clock 75MHz)
  *            D2 APB1 Prescaler              = 2 (APB1 Clock 75MHz)
  *            D2 APB2 Prescaler              = 2 (APB2 Clock 75MHz)
  *            D3 APB4 Prescaler              = 2 (APB4 Clock 75MHz)
  *            HSE Frequency(Hz)              = 8000000
  *            PLL_M                          = 2
  *            PLL_N                          = 150
  *            PLL_P                          = 2
  *            PLL_Q                          = 2
  *            PLL_R                          = 2
  *            VDD(V)                         = 3.3
  *            Flash Latency(WS)              = 2
  *
  *         As an alternative you could run the following to maximise the slow peripheral
  *         clocks, at the expense of only achieving 200MHz CPU1 clock and 100MHz
  *         fast peripheral clocks:
  *            System Clock source            = PLL (HSE BYPASS)
  *            SYSCLK(Hz)                     = 200000000 (CPU Clock)
  *            HCLK(Hz)                       = 100000000 (Cortex-M4 CPU, Bus matrix Clocks)
  *            AHB Prescaler                  = 2
  *            D1 APB3 Prescaler              = 1 (APB3 Clock 100MHz)
  *            D2 APB1 Prescaler              = 1 (APB1 Clock 100MHz)
  *            D2 APB2 Prescaler              = 1 (APB2 Clock 100MHz)
  *            D3 APB4 Prescaler              = 1 (APB4 Clock 100MHz)
  *            HSE Frequency(Hz)              = 8000000
  *            PLL_M                          = 2
  *            PLL_N                          = 100
  *            PLL_P                          = 2
  *            PLL_Q                          = 2
  *            PLL_R                          = 2
  *            VDD(V)                         = 3.3
  *            Flash Latency(WS)              = 1
  *
  *         In both cases VOS2 is used since the maximum peripheral speeds in
  *         VOS3 is 100MHz and if the frequency is slightly off it may be on the edge.
  *
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
    RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
    RCC_OscInitTypeDef RCC_OscInitStruct = {0};
    RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};
    HAL_StatusTypeDef ret = HAL_OK;

    /*!< Supply configuration update enable */
    HAL_PWREx_ConfigSupply(PWR_DIRECT_SMPS_SUPPLY);

    /* The voltage scaling allows optimizing the power consumption when the device is
     * clocked below the maximum system frequency, to update the voltage scaling value
     * regarding system frequency refer to product datasheet.
     */
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

    while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}

    /* Enable HSE Oscillator and activate PLL with HSE as source */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
    RCC_OscInitStruct.CSIState = RCC_CSI_OFF;
    
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    
    // For running at 25MHz (50MHz CPU7) while developing
    RCC_OscInitStruct.PLL.PLLM = 5;
    RCC_OscInitStruct.PLL.PLLN = 100;
    RCC_OscInitStruct.PLL.PLLFRACN = 0;
    RCC_OscInitStruct.PLL.PLLP = 10;
    RCC_OscInitStruct.PLL.PLLR = 10;
    RCC_OscInitStruct.PLL.PLLQ = 10;

    // For running at 300MHz (300MHz CPU7) for highest downhole speed
//    RCC_OscInitStruct.PLL.PLLM = 2;
//    RCC_OscInitStruct.PLL.PLLN = 48;
//    RCC_OscInitStruct.PLL.PLLFRACN = 0;
//    RCC_OscInitStruct.PLL.PLLP = 2;
//    RCC_OscInitStruct.PLL.PLLR = 6;
//    RCC_OscInitStruct.PLL.PLLQ = 6;

    RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
    RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_2;
    ret = HAL_RCC_OscConfig(&RCC_OscInitStruct);
    if(ret != HAL_OK)
    {
        Error_Handler();
    }

    /* Select PLL as system clock source and configure  bus clocks dividers */
    RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_D1PCLK1 | RCC_CLOCKTYPE_PCLK1 | \
                                 RCC_CLOCKTYPE_PCLK2  | RCC_CLOCKTYPE_D3PCLK1);

    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV2;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV2;
    RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV2;
    
    ret = HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);
    if(ret != HAL_OK)
    {
        Error_Handler();
    }

    /*
     *  Note : The activation of the I/O Compensation Cell is recommended with communication  interfaces
     *         (GPIO, SPI, FMC, QSPI ...)  when  operating at  high frequencies(please refer to product datasheet)
     *         The I/O Compensation Cell activation  procedure requires :
     *          - The activation of the CSI clock
     *          - The activation of the SYSCFG clock
     *          - Enabling the I/O Compensation Cell : setting bit[0] of register SYSCFG_CCCSR
     *
     *    To do this please uncomment the following code
     */

    /*
    __HAL_RCC_CSI_ENABLE() ;

    __HAL_RCC_SYSCFG_CLK_ENABLE() ;

    HAL_EnableCompensationCell();
    */
  
    /* Configure clock to FMC peripheral */
    PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_FMC;
    PeriphClkInitStruct.FmcClockSelection = RCC_FMCCLKSOURCE_D1HCLK;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
    {
        Error_Handler();
    }

    
    /* Enable the system clock output to MCO2 */
    HAL_RCC_MCOConfig(RCC_MCO2, RCC_MCO2SOURCE_SYSCLK, RCC_MCODIV_1);
}


static void MPU_Config(void)
{
    MPU_Region_InitTypeDef MPU_InitStruct;

    /* Disable the MPU */
    HAL_MPU_Disable();

    /* Configure the MPU as non-shared "Device" memory - accesses in order, non-cacheable, non-bufferable, noexec for NAND flash through FMC */
    MPU_InitStruct.Enable = MPU_REGION_ENABLE;
    MPU_InitStruct.BaseAddress = 0x80000000;
    MPU_InitStruct.Size = MPU_REGION_SIZE_256MB;
    MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
    MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;
    MPU_InitStruct.IsCacheable = MPU_ACCESS_NOT_CACHEABLE;
    MPU_InitStruct.IsShareable = MPU_ACCESS_NOT_SHAREABLE;
    MPU_InitStruct.Number = MPU_REGION_NUMBER0;
    MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL2;   // The magic sauce of this line is.... that is simply gets stuffed into the TEX field, so values are just from any of the "Region attribute control" TEX values in the MPU table, as long as it matches the other fields too
    MPU_InitStruct.SubRegionDisable = 0x00;
    MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_DISABLE;

    HAL_MPU_ConfigRegion(&MPU_InitStruct);

    /* Enable the MPU */
    HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}


/* FMC initialization function */
void MX_FMC_Init(void)
{
    /* USER CODE BEGIN FMC_Init 0 */

    /* USER CODE END FMC_Init 0 */

    FMC_NAND_PCC_TimingTypeDef ComSpaceTiming = {0};
    FMC_NAND_PCC_TimingTypeDef AttSpaceTiming = {0};

    /* USER CODE BEGIN FMC_Init 1 */

    /* USER CODE END FMC_Init 1 */

    /** Perform the NAND1 memory initialization sequence
    */
    hnand1.Instance = FMC_NAND_DEVICE;
    /* hnand1.Init */
    hnand1.Init.NandBank = FMC_NAND_BANK3;
    hnand1.Init.Waitfeature = FMC_NAND_WAIT_FEATURE_ENABLE;
    hnand1.Init.MemoryDataWidth = FMC_NAND_MEM_BUS_WIDTH_16;
    hnand1.Init.EccComputation = FMC_NAND_ECC_DISABLE;
    hnand1.Init.ECCPageSize = FMC_NAND_ECC_PAGE_SIZE_256BYTE;
    hnand1.Init.TCLRSetupTime = 0;
    hnand1.Init.TARSetupTime = 0;
    
    /* hnand1.Config - WTF, CubeMX doesn't save these or export them properly? TBF most applications would probably set these at runtime */
    hnand1.Config.PageSize = 4096;
    hnand1.Config.SpareAreaSize = 224;
    hnand1.Config.BlockSize = 256;
    hnand1.Config.BlockNbr = 16384;
    hnand1.Config.PlaneNbr = 2;
    hnand1.Config.PlaneSize = 2048;
    hnand1.Config.ExtraCommandEnable = DISABLE;
    
    /* ComSpaceTiming */
    ComSpaceTiming.SetupTime = 252;
    ComSpaceTiming.WaitSetupTime = 252;
    ComSpaceTiming.HoldSetupTime = 252;
    ComSpaceTiming.HiZSetupTime = 252;
    /* AttSpaceTiming */
    AttSpaceTiming.SetupTime = 252;
    AttSpaceTiming.WaitSetupTime = 252;
    AttSpaceTiming.HoldSetupTime = 252;
    AttSpaceTiming.HiZSetupTime = 252;

    if (HAL_NAND_Init(&hnand1, &ComSpaceTiming, &AttSpaceTiming) != HAL_OK)
    {
        Error_Handler( );
    }

    /* USER CODE BEGIN FMC_Init 2 */

    /* USER CODE END FMC_Init 2 */
}


/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* GPIO Ports Clock Enable - generated by STM32CubeMx, in order of port usage starting from pin 1 and working round */
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOH_CLK_ENABLE();
    __HAL_RCC_GPIOE_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();

    // Configure output levels prior to configuring IO
    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOB, GREEN_LED_Pin|GPIO_PIN_14|NAND_CE2__Pin, GPIO_PIN_RESET);

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(YELLOW_LED_GPIO_Port, YELLOW_LED_Pin, GPIO_PIN_RESET);

    /* Configure GPIO pin : PC9 */
    GPIO_InitStruct.Pin = GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF0_MCO;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    
    /* Configure extra GPIO pins for NAND flash : PB14 - WP#, PB15 - CE2# */
    GPIO_InitStruct.Pin = GREEN_LED_Pin | GPIO_PIN_14 | GPIO_PIN_15;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

//    GPIO_InitStruct.Pin = GPIO_PIN_15;
//    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//    GPIO_InitStruct.Pull = GPIO_NOPULL;
//    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
//    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
    
    /*Configure GPIO pin : YELLOW_LED_Pin */
    GPIO_InitStruct.Pin = YELLOW_LED_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(YELLOW_LED_GPIO_Port, &GPIO_InitStruct);

    // Why not the other GPIO pins for the peripherals?
    // Are they done in the peripheral drivers?
    // A: they are done in the "MSP" functions in stm32h7xx_hal_msp.c
    
    // Temporary while not using timer to control output
#if 0
    HAL_GPIO_WritePin(SAMPLE_SYNC_Port, SAMPLE_SYNC_Pin, GPIO_PIN_RESET);
    
    GPIO_InitStruct.Pin = SAMPLE_SYNC_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(SAMPLE_SYNC_Port, &GPIO_InitStruct);
#endif
}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{
    /* USER CODE BEGIN TIM2_Init 0 */

    /* USER CODE END TIM2_Init 0 */

    TIM_ClockConfigTypeDef sClockSourceConfig = {0};
    TIM_MasterConfigTypeDef sMasterConfig = {0};
    TIM_IC_InitTypeDef sConfigIC = {0};
    TIM_OC_InitTypeDef sConfigOC = {0};

    /* USER CODE BEGIN TIM2_Init 1 */

    /* USER CODE END TIM2_Init 1 */
    htim2.Instance = TIM2;
    htim2.Init.Prescaler = 24;
    htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim2.Init.Period = 4294967295;
    htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
    {
        Error_Handler();
    }

    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
    {
        Error_Handler();
    }

    if (HAL_TIM_IC_Init(&htim2) != HAL_OK)
    {
        Error_Handler();
    }

    if (HAL_TIM_OC_Init(&htim2) != HAL_OK)
    {
        Error_Handler();
    }

    sMasterConfig.MasterOutputTrigger = TIM_TRGO_OC1;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_ENABLE;
//    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
//    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
    {
        Error_Handler();
    }

    sConfigOC.OCMode = TIM_OCMODE_TIMING;
    sConfigOC.Pulse = 0;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    if (HAL_TIM_OC_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
    {
        Error_Handler();
    }

    sConfigOC.OCMode = TIM_OCMODE_TOGGLE;
    sConfigOC.Pulse = 0;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    if (HAL_TIM_OC_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
    {
        Error_Handler();
    }

    sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
    sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
    sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
    sConfigIC.ICFilter = 0;
    if (HAL_TIM_IC_ConfigChannel(&htim2, &sConfigIC, TIM_CHANNEL_3) != HAL_OK)
    {
        Error_Handler();
    }

    sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_FALLING;
    sConfigIC.ICSelection = TIM_ICSELECTION_INDIRECTTI;
    sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
    sConfigIC.ICFilter = 0;
    if (HAL_TIM_IC_ConfigChannel(&htim2, &sConfigIC, TIM_CHANNEL_4) != HAL_OK)
    {
        Error_Handler();
    }
    /* USER CODE BEGIN TIM2_Init 2 */

    /* USER CODE END TIM2_Init 2 */
    HAL_TIM_MspPostInit(&htim2);

}


#if 1
/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{
    /* USER CODE BEGIN TIM3_Init 0 */

    /* USER CODE END TIM3_Init 0 */

    TIM_ClockConfigTypeDef sClockSourceConfig = {0};
    TIM_SlaveConfigTypeDef sSlaveConfig = {0};
    TIM_MasterConfigTypeDef sMasterConfig = {0};
    //TIM_OC_InitTypeDef sConfigOC = {0};
    TIM_OnePulse_InitTypeDef sConfig = {0};

    /* USER CODE BEGIN TIM3_Init 1 */

    /* USER CODE END TIM3_Init 1 */
    htim3.Instance = TIM3;
    htim3.Init.Prescaler = 24;
    htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim3.Init.Period = 833;
    htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
    {
        Error_Handler();
    }
    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
    {
        Error_Handler();
    }

//    if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
//    {
//        Error_Handler();
//    }

    if (HAL_TIM_OnePulse_Init(&htim3, TIM_OPMODE_SINGLE) != HAL_OK)
    {
        Error_Handler();
    }
    
    sSlaveConfig.SlaveMode = TIM_SLAVEMODE_COMBINED_RESETTRIGGER;
    sSlaveConfig.InputTrigger = TIM_TS_ITR1;
    if (HAL_TIM_SlaveConfigSynchro(&htim3, &sSlaveConfig) != HAL_OK)
    {
        Error_Handler();
    }
    
    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
    {
        Error_Handler();
    }
    
//    sConfigOC.OCMode = TIM_OCMODE_PWM2;
//    sConfigOC.Pulse = 1;
//    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
//    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
//    if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
    
    
    sConfig.OCMode       = TIM_OCMODE_PWM2;
    sConfig.OCPolarity   = TIM_OCPOLARITY_HIGH;
    sConfig.Pulse        = 1;
    sConfig.ICPolarity   = TIM_ICPOLARITY_RISING;
    sConfig.ICSelection  = TIM_ICSELECTION_DIRECTTI;
    sConfig.ICFilter     = 0;
    sConfig.OCNPolarity  = TIM_OCNPOLARITY_HIGH;
    sConfig.OCIdleState  = TIM_OCIDLESTATE_RESET;
    sConfig.OCNIdleState = TIM_OCNIDLESTATE_RESET;    
    if (HAL_TIM_OnePulse_ConfigChannelOutput(&htim3, &sConfig, TIM_CHANNEL_1) != HAL_OK)
    {
        Error_Handler();
    }
    
    __HAL_TIM_DISABLE_OCxPRELOAD(&htim3, TIM_CHANNEL_1);
    /* USER CODE BEGIN TIM3_Init 2 */

    /* USER CODE END TIM3_Init 2 */
    HAL_TIM_MspPostInit(&htim3);
}
#endif


#if 0
/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{
  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 24;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 5688;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 500;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */
  HAL_TIM_MspPostInit(&htim4);
}
#endif

#if 1
/**
  * @brief TIM12 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM12_Init(void)
{

  /* USER CODE BEGIN TIM12_Init 0 */

  /* USER CODE END TIM12_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};

  /* USER CODE BEGIN TIM12_Init 1 */

  /* USER CODE END TIM12_Init 1 */
  htim12.Instance = TIM12;
  htim12.Init.Prescaler = 24;
  htim12.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim12.Init.Period = 8000;
  htim12.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim12.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim12) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim12, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM12_Init 2 */

  /* USER CODE END TIM12_Init 2 */
}
#endif


/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
    /* User may add here some code to deal with this error */
    while(1)
    {
    }
}

/**
  * @brief  CPU L1-Cache enable.
  * @param  None
  * @retval None
  */
static void CPU_CACHE_Enable(void)
{
    /* Enable I-Cache */
    SCB_EnableICache();

    /* Enable D-Cache */
    SCB_EnableDCache();
}
