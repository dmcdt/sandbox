/**
  ******************************************************************************
  * @file    stm32h7x3_cpu_perf/Src/utilities.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    16-June-2017  
  * @brief   This file contains the cycles measurement routines.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2017 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "utilities.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  De-init the systick for benchmark operations
  * @param  None
  * @retval None
  */
  
void TimerCount_DeInit(void) 	
{
    SysTick->LOAD = (uint32_t)0x0;  /* Set reload register */
    SysTick->VAL = (uint32_t)0x0;   /* Clear Counter */		 
    SysTick->CTRL = (uint32_t)0x0;  /* Disable Counting*/	 
}


/**
  * @brief  Configure and start systick for benchmark operations
  * @param  None
  * @retval None
  */
void TimerCount_Start(void)
{
    SysTick->LOAD = (uint32_t)0xFFFFFF; /* Set reload register */
    SysTick->VAL = (uint32_t)0x0;       /* Clear Counter */		 
    SysTick->CTRL = (uint32_t)0x7;      /* Enable Counting*/	
}


/**
  * @brief  Get the cycles number
  * @param  [OUT] cycles_nb: pointer to a variable that will contain the cysles number
  * @retval None
  */
void TimerCount_Stop (uint32_t * cycles_nb)
{
    *cycles_nb = 0;                     /* Initialize the variable */
    SysTick->CTRL = (uint32_t)0x0;	    /* Disable Counting */
    *cycles_nb = SysTick->VAL;          /* Load the SysTick Counter Value */
    *cycles_nb = 0xFFFFFF - *cycles_nb; /* Capture Counts in CPU Cycles*/
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
