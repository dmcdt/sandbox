/**
  ******************************************************************************
  * @file    Templates/BootCM4_CM7/CM7/Inc/main.h
  * @author  MCD Application Team
  * @brief   Header for main.c module for Cortex-M7.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
      
#include <stm32h7xx_hal.h>
//#include <stm32h7xx_nucleo_144.h>

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions ------------------------------------------------------- */

void Timer2_OC1_OutputSample(void);
void Timer2_OC2_MotorIndexLockout(void);
void Timer2_IC3_MotorIndexRisingEdge(void);
void Timer2_IC4_MotorIndexFallingEdge(void);

bool IsEarlierThan(uint32_t a, uint32_t b);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define GREEN_LED_Pin GPIO_PIN_0
#define GREEN_LED_GPIO_Port GPIOB
#define NAND_CE2__Pin GPIO_PIN_15
#define NAND_CE2__GPIO_Port GPIOB
#define YELLOW_LED_Pin GPIO_PIN_1
#define YELLOW_LED_GPIO_Port GPIOE
//#define SAMPLE_SYNC_Pin GPIO_PIN_6
//#define SAMPLE_SYNC_Port GPIOA
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
