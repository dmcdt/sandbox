/**
  ******************************************************************************
  * @file    Templates/BootCM4_CM7/CM7/Src/stm32h7xx_it.c
  * @author  MCD Application Team
  * @brief   Main Interrupt Service Routines for Cortex-M7.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_it.h"
#include "main.h"
#include <stdint.h>
#include <stdbool.h>

/** @addtogroup STM32H7xx_HAL_Examples
  * @{
  */

/** @addtogroup Templates
  * @{
  */

__IO uint32_t BenchmarkTick = 0;

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
bool IsEarlierThan(uint32_t a, uint32_t b);

/* Private functions ---------------------------------------------------------*/
/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim12;

/* USER CODE BEGIN EV */

/* USER CODE END EV */

/******************************************************************************/
/*            Cortex-M7 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
    /* Go to infinite loop when Hard Fault exception occurs */
    while (1)
    {
    }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
    /* Go to infinite loop when Memory Manage exception occurs */
    while (1)
    {
    }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
    /* Go to infinite loop when Bus Fault exception occurs */
    while (1)
    {
    }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
    /* Go to infinite loop when Usage Fault exception occurs */
    while (1)
    {
    }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
    ++BenchmarkTick;
    HAL_IncTick();
}



/******************************************************************************/
/*                 STM32H7xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32h7xx.s).                                               */
/******************************************************************************/

/**
  * @brief This function handles TIM2 global interrupt.
  */
void TIM2_IRQHandler(void)
{
    // Standard behaviour would be to call the HAL interrupt handler, like below
    // and then override weak functions it provides to perform your own handling
    // of each type of interrupt (counter overflow, IC, OC).
//    HAL_TIM_IRQHandler(&htim2);
//    return;
    
    // However, the HAL handling processes interrupts in order of CC sequence
    // and we might have two set at the same time but the capture instant is
    // in a different order.
    
    //TIM_HandleTypeDef   *htim = &htim2;
    TIM_TypeDef         *tim2 = htim2.Instance;

    // Process interrupts that are pending just now. Any that are left will
    // cause execution of ISR again.
    uint32_t sr = tim2->SR;
    uint32_t dier = tim2->DIER;
    
    // CC2 - output, toggle on match, lockout timer for motor index edge detection debounce
    if (((dier & TIM_DIER_CC2IE_Msk) != 0) &&
        ((sr & TIM_SR_CC2IF_Msk) != 0))
    {
        Timer2_OC2_MotorIndexLockout();
        tim2->SR &= ~TIM_SR_CC2IF_Msk;
    }
    
  
    // CC4 - input, falling edge of motor index pulse
    if (((dier & TIM_DIER_CC4IE_Msk) != 0) &&
        ((sr & TIM_SR_CC4IF_Msk) != 0))
    {
        Timer2_IC4_MotorIndexFallingEdge();
        tim2->SR &= ~TIM_SR_CC4IF_Msk;
    }

    // CC1 - output for pulse timing, plus trigger output to slave timer in one pulse mode for actual output signal
    // CC3 - input, rising edge of motor index pulse
  
    // CC1 and CC3 need to be considered together. If they are both set then need
    // to process them in increasing order of their capture registers, and coping
    // with counter wraparound.
    uint32_t combined;

    // DIER and SR bit positions for Capture/Compare interrupt enables and flags
    // are the same, so we can just mask these together.
    combined = dier & sr & (TIM_SR_CC3IF_Msk | TIM_SR_CC1IF_Msk);
    
    if (combined == (TIM_SR_CC3IF | TIM_SR_CC1IF))
    {
        // Check which one happened first
        if (IsEarlierThan(tim2->CCR3, tim2->CCR1))
        {
            Timer2_IC3_MotorIndexRisingEdge();
            Timer2_OC1_OutputSample();
        }
        else
        {
            Timer2_OC1_OutputSample();
            Timer2_IC3_MotorIndexRisingEdge();
        }

        tim2->SR &= ~(TIM_SR_CC3IF_Msk | TIM_SR_CC1IF_Msk);
    }
    else if (combined == TIM_SR_CC3IF)
    {
        Timer2_IC3_MotorIndexRisingEdge();
        tim2->SR &= ~TIM_SR_CC3IF_Msk;
    }
    else if (combined == TIM_SR_CC1IF)
    {
        Timer2_OC1_OutputSample();
        tim2->SR &= ~TIM_SR_CC1IF_Msk;
    }
    // Don't need a case for 0, we don't do anything
  
    // Timer update - not used, just clear and disable interrupt
    if (((dier & TIM_DIER_UIE_Msk) != 0) &&
        ((sr & TIM_SR_UIF_Msk) != 0))
    {
        tim2->DIER &= ~TIM_DIER_UIE_Msk;
        tim2->SR &= ~TIM_SR_UIF_Msk;
    }

    // Break input event - not present on TIM2
    // Break2 input event - not present on TIM2

    // Trigger detection event - not used, just clear and disable interrupt
    if (((dier & TIM_DIER_TIE_Msk) != 0) &&
        ((sr & TIM_SR_TIF_Msk) != 0))
    {
        tim2->DIER &= ~TIM_DIER_TIE_Msk;
        tim2->SR &= ~TIM_SR_TIF_Msk;
    }

    // commutation event - not present on TIM2
}


#if 1
/**
  * @brief This function handles TIM8 break interrupt and TIM12 global interrupt.
  */
void TIM8_BRK_TIM12_IRQHandler(void)
{
  /* USER CODE BEGIN TIM8_BRK_TIM12_IRQn 0 */

  /* USER CODE END TIM8_BRK_TIM12_IRQn 0 */
  HAL_TIM_IRQHandler(&htim12);
  /* USER CODE BEGIN TIM8_BRK_TIM12_IRQn 1 */

  /* USER CODE END TIM8_BRK_TIM12_IRQn 1 */
}
#endif

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
