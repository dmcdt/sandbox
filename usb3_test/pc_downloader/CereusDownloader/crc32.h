#ifndef CRC32_H
#define CRC32_H


#include <cstdint>

uint32_t crc32(const void *data, size_t n_bytes);


#endif
