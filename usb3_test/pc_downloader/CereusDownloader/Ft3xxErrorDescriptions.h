#ifndef FT3XXERRORDESCRIPTIONS_H
#define FT3XXERRORDESCRIPTIONS_H

/*
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 */


#include <map>
#include <string>

#include <FTD3XX.h>


class Ft3xxErrorDescriptions
{
private:
    typedef std::map<FT_STATUS, std::string> StatusDescriptionLookup_t;
    StatusDescriptionLookup_t statusDescription;

    void registerErrorDescriptions(void);

public:
    Ft3xxErrorDescriptions();

    std::string Format(FT_STATUS status, std::string const& msg);
};


#endif
