/*
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 */


#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <fstream>

#include <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>
#include <QThread>
#include <QTextStream>
#include <QFuture>
#include <QFutureWatcher>
#include <QtConcurrent/QtConcurrentRun>

#include <FTD3XX.h>

#include "FT3XXHelper.h"
#include "Ft3xxErrorDescriptions.h"
#include "Ft3xxEnumerator.h"
#include "Ft3xxDeviceInfo.h"
#include "UsbCommands.h"
#include "crc16_modbus.h"
#include "UsbFile.h"
#include "crc32.h"
#include "Identity.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , timeFormat("hh:mm:ss.zzz ") // Keep the space so you don't have to add it to the formatted messages
{
    ui->setupUi(this);

    connect(ui->refreshDeviceList, &QPushButton::clicked, this, &MainWindow::refreshDevicesClicked);
    connect(ui->deviceList, &QListWidget::itemSelectionChanged, this, &MainWindow::selectedDeviceChanged);
    connect(ui->openUsb, &QPushButton::clicked, this, &MainWindow::openUsbClicked);
    connect(ui->closeUsb, &QPushButton::clicked, this, &MainWindow::closeUsbClicked);
    connect(ui->abortWrite, &QPushButton::clicked, this, &MainWindow::abortWriteClicked);
    connect(ui->abortRead, &QPushButton::clicked, this, &MainWindow::abortReadClicked);
    connect(ui->cyclePortButton, &QPushButton::clicked, this, &MainWindow::cyclePortClicked);
    connect(ui->identify, &QPushButton::clicked, this, &MainWindow::identifyClicked);
    connect(ui->writeEcho, &QPushButton::clicked, this, &MainWindow::writeEchoClicked);
    connect(ui->readEcho, &QPushButton::clicked, this, &MainWindow::readEchoClicked);
    connect(ui->autoEcho, &QPushButton::clicked, this, &MainWindow::autoEchoClicked);
    connect(ui->readCountersButton, &QPushButton::clicked, this, &MainWindow::readCountersClicked);
    connect(ui->readBbtButton, &QPushButton::clicked, this, &MainWindow::readBbtClicked);
    connect(ui->markBadBlockButton, &QPushButton::clicked, this, &MainWindow::markBadClicked);
    connect(ui->commitBbtButton, &QPushButton::clicked, this, &MainWindow::commitBbtClicked);
    connect(ui->eraseNandButton, &QPushButton::clicked, this, &MainWindow::eraseNandClicked);
    connect(ui->appendDummyJobButton, &QPushButton::clicked, this, &MainWindow::appendDummyJobClicked);
    // Download button connected by onld way using name
    connect(ui->autoDownloadButton, &QPushButton::clicked, this, &MainWindow::autoDownloadButtonClicked);
    connect(ui->pythonDownloadButton, &QPushButton::clicked, this, &MainWindow::pythonDownloadButtonClicked);

    this->setUiForClosed();

    populateDeviceList();

    //uint32_t crc = crc32("123456789", 9);

    ui->statusbar->showMessage("Not connected to any USB device.");
}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::setUiEnabledStates(bool isOpen)
{
    ui->refreshDeviceList->setEnabled(!isOpen);
    ui->deviceList->setEnabled(!isOpen);
    ui->openUsb->setEnabled(!isOpen && !ui->deviceList->selectedItems().empty());
    ui->closeUsb->setEnabled(isOpen);
    ui->abortWrite->setEnabled(isOpen);
    ui->abortRead->setEnabled(isOpen);
    ui->cyclePortButton->setEnabled(isOpen);
    ui->identify->setEnabled(isOpen);
    ui->writeEcho->setEnabled(isOpen);
    ui->readEcho->setEnabled(isOpen);
    ui->autoEcho->setEnabled(isOpen);
    ui->readCountersButton->setEnabled(isOpen);
    ui->readBbtButton->setEnabled(isOpen);
    ui->markBadBlockButton->setEnabled(isOpen);
    ui->commitBbtButton->setEnabled(isOpen);
    ui->eraseNandButton->setEnabled(isOpen);
    ui->appendDummyJobButton->setEnabled(isOpen);
    ui->downloadButton->setEnabled(isOpen);
    //ui->cancelButton->setEnabled(isOpen);
    ui->autoDownloadButton->setEnabled(isOpen);
    ui->pythonDownloadButton->setEnabled(isOpen);
}


void MainWindow::setUiForOpened(void)
{
    setUiEnabledStates(true);
}

void MainWindow::setUiForClosed(void)
{
    setUiEnabledStates(false);
}


void MainWindow::setUiForIdle()
{
    this->ui->downloadButton->setEnabled(true);
    this->ui->autoDownloadButton->setEnabled(true);
    this->ui->pythonDownloadButton->setEnabled(true);
    //this->ui->cancelButton->setEnabled(false);
}


void MainWindow::setUiForRunning()
{
    this->ui->downloadButton->setEnabled(false);
    this->ui->autoDownloadButton->setEnabled(false);
    this->ui->pythonDownloadButton->setEnabled(false);
    //this->ui->cancelButton->setEnabled(true);
}


void MainWindow::populateDeviceList()
{
    Ft3xxEnumerator ftenum;
    int index = 0;

    for (Ft3xxEnumerator::const_iterator it = ftenum.cbegin(); it != ftenum.cend(); it++)
    {
        Ft3DeviceInfo   di(*it);
        QString         display = QString("%1. S/N: %2 - \"%3\"")
                                    .arg(index)
                                    .arg(QString::fromStdString(di.SerialNumber()))
                                    .arg(QString::fromStdString(di.Description()));

        if (di.Flags() & FT_FLAGS_OPENED)
        {
            display += " (Device in use by another application.)";
        }

        QListWidgetItem *item = new QListWidgetItem();
        item->setText(display);
        item->setData(Qt::UserRole, index++);
        ui->deviceList->addItem(item);
    }

}

void MainWindow::CloseUsb()
{
    ui->log->appendPlainText(QDateTime::currentDateTime().toString(timeFormat) + "closing USB");
    usb.Close();
    setUiForClosed();
}


void MainWindow::AbortWrite()
{
    if (usb.IsOpen())
    {
        FT_STATUS               ftStatus;
        ftStatus = FT_AbortPipe(usb.Handle(), FT_CHANNEL1_WRITE);
        if (ftStatus != FT_OK)
        {
            QMessageBox::critical(this, "Error", QString::fromStdString(ftStrings.Format(ftStatus, "abort write")));
        }
    }
}


void MainWindow::AbortRead()
{
    if (usb.IsOpen())
    {
        FT_STATUS               ftStatus;
        ftStatus = FT_AbortPipe(usb.Handle(), FT_CHANNEL1_READ);
        if (ftStatus != FT_OK)
        {
            QMessageBox::critical(this, "Error", QString::fromStdString(ftStrings.Format(ftStatus, "abort read")));
        }
    }
}

void MainWindow::CyclePort()
{
    if (usb.IsOpen())
    {
        FT_STATUS   ftStatus;
        ftStatus = FT_CycleDevicePort(usb.Handle());
        if (ftStatus == FT_OK)
        {
            CloseUsb();
        }
        else
        {
            QMessageBox::critical(this, "Error", QString::fromStdString(ftStrings.Format(ftStatus, "cycling port")));
        }
    }
}


void MainWindow::Identify()
{
    FT_STATUS               ftStatus;
    ExpandedBuffer_t        writeBuffer;
    std::vector<uint8_t>    command = UsbCommand_Identify();

    CreateWriteBuffer(command, writeBuffer);

    ULONG ulBytesToWrite = writeBuffer.size() * sizeof(writeBuffer[0]) / sizeof(UCHAR);
    ULONG ulBytesWritten;
    ftStatus = FT_WritePipe(usb.Handle(), FT_CHANNEL1_WRITE, reinterpret_cast<PUCHAR>(&writeBuffer[0]), ulBytesToWrite, &ulBytesWritten, NULL);
    if (ftStatus != FT_OK)
    {
        QMessageBox::critical(this, "Error", QString::fromStdString(ftStrings.Format(ftStatus, "write")));
    }

    ExpandedBuffer_t        readBuffer;
    ULONG                   ulElementsRead;
    ULONG                   ulBytesRead;
    std::vector<uint8_t>    response;
    QString                 received;


    QThread::usleep(1000);


    readBuffer.resize(OVERSAMPLE_RATIO * USB_IDENTIFY_RESPONSE_LENGTH + 10);
    ftStatus = FT_ReadPipe(usb.Handle(), FT_CHANNEL1_READ, reinterpret_cast<PUCHAR>(&readBuffer[0]), readBuffer.size() * sizeof(readBuffer[0]) / sizeof(UCHAR), &ulBytesRead, NULL);

    ulElementsRead = ulBytesRead / sizeof(readBuffer[0]);
    received = QString("Received %1 bytes, %2 elements:").arg(ulBytesRead).arg(ulElementsRead);

    for (DWORD i = 0; i < ulElementsRead; i++)
    {
        if ((i % 16) == 0)
        {
            received += "\r\n";
        }
        received += QString("%1 ").arg((uint)readBuffer[i], 4, 16, QChar('0'));
    }
    received += "\r\n";
    ui->log->appendPlainText(received);

    if (ftStatus == FT_OK)
    {
        readBuffer.resize(ulElementsRead);
        ExtractResponse(readBuffer, response);
        if (response.size() > 2)
        {
            uint16_t    crcCalculated = CRC16Modbus_Calculate(&response[0], response.size() - 2);
            uint16_t    crcReceived = (response[response.size() - 1] << 8) | response[response.size() - 2];
            if (crcCalculated == crcReceived)
            {
                QString extracted;
                extracted = "Extracted: ";
                for (size_t index = 0; index < response.size(); index++)
                {
                    extracted += QString("0x%1 ").arg((uint)response[index], 2, 16, QChar('0'));
                }
                extracted += "\r\n";
                ui->log->appendPlainText(extracted);

                // Parse identity from extracted bytes
                if (response.size() == USB_IDENTIFY_RESPONSE_LENGTH)
                {
                    if (response[1] == UsbResponseStatus_Ok)
                    {
                        uint32_t companyId = response[2] | (response[3] << 8) | (response[4] << 16) | (response[5] << 24);
                        uint16_t deviceId = response[6] | (response[7] << 8);
                        uint32_t firmwareChecksum = response[11] | (response[12] << 8) | (response[13] << 16) | (response[14] << 24);
                        QString serialNumber = QString::fromStdString(std::string(reinterpret_cast<char *>(&response[15]), USB_IDENTIFY_RESPONSE_SERIAL_LENGTH));
                        serialNumber.remove(QChar(QChar::Null));
                        serialNumber.remove(QChar(0xFF));
                        QString identity;

                        identity = QString::fromStdString(CompanyDescription(companyId)) % " " %
                                   QString::fromStdString(DeviceTypeDescription(deviceId)) % " v" %
                                   QString("%1.%2.%3").arg(response[8]).arg(response[9]).arg(response[10]) %
                                   "-0x" % QString("%1").arg(firmwareChecksum, 8, 16, QChar('0')) %
                                   " S/N " % serialNumber;

                        ui->log->appendPlainText(identity);
                    }
                    else
                    {
                        ui->log->appendPlainText(QString("Response status 0x%1 is not OK").arg(response[1], 16));
                    }
                }
                else
                {
                    ui->log->appendPlainText("Response length does not match expected.");
                }
            }
            else
            {
                QString message;
                message = QString("CRC mismatch calc %1 != recv %2\r\n").arg((uint)crcCalculated, 4, 16, QChar('0')).arg((uint)crcReceived, 4, 16, QChar('0'));
                ui->log->appendPlainText(message);
            }
        }
        else
        {
            QString message;
            message = QString("Packet too short for valid response %1 bytes\r\n").arg(response.size());
            ui->log->appendPlainText(message);
        }
    }
    else
    {
        QMessageBox::critical(this, "Error", QString::fromStdString(ftStrings.Format(ftStatus, "reading")));
    }
}


void MainWindow::WriteEchoCommand()
{
    FT_STATUS               ftStatus;
    ExpandedBuffer_t        writeBuffer;
    std::vector<uint8_t>    data = { 0xDE, 0xAD, 0xBE, 0xEF };
    std::vector<uint8_t>    command = UsbCommand_Echo(data);

    CreateWriteBuffer(command, writeBuffer);

    ULONG ulBytesToWrite = writeBuffer.size() * sizeof(writeBuffer[0]) / sizeof(UCHAR);
    ULONG ulBytesWritten;
    ftStatus = FT_WritePipe(usb.Handle(), FT_CHANNEL1_WRITE, reinterpret_cast<PUCHAR>(&writeBuffer[0]), ulBytesToWrite, &ulBytesWritten, NULL);
    if (ftStatus != FT_OK)
    {
        QMessageBox::critical(this, "Error", QString::fromStdString(ftStrings.Format(ftStatus, "write")));
    }
}


void MainWindow::ReadEchoResponse()
{
    FT_STATUS               ftStatus;
    ExpandedBuffer_t        readBuffer;
    ULONG                   ulElementsRead;
    ULONG                   ulBytesRead;
    std::vector<uint8_t>    response;
    QString                 received;

    readBuffer.resize(1024);
    ftStatus = FT_ReadPipe(usb.Handle(), FT_CHANNEL1_READ, reinterpret_cast<PUCHAR>(&readBuffer[0]), readBuffer.size() * sizeof(readBuffer[0]) / sizeof(UCHAR), &ulBytesRead, NULL);

    ulElementsRead = ulBytesRead / sizeof(readBuffer[0]);
    received = QString("Received %1 bytes, %2 elements:").arg(ulBytesRead).arg(ulElementsRead);

    for (DWORD i = 0; i < ulElementsRead; i++)
    {
        if ((i % 16) == 0)
        {
            received += "\r\n";
        }
        received += QString("%1 ").arg((uint)readBuffer[i], 4, 16, QChar('0'));
    }
    received += "\r\n";
    ui->log->appendPlainText(received);

    if (ftStatus == FT_OK)
    {
        ExtractResponse(readBuffer, response);
        if (response.size() > 2)
        {
            uint16_t    crcCalculated = CRC16Modbus_Calculate(&response[0], response.size() - 2);
            uint16_t    crcReceived = (response[response.size() - 1] << 8) | response[response.size() - 2];
            if (crcCalculated == crcReceived)
            {
                QString extracted;
                extracted = "Extracted: ";
                for (size_t index = 0; index < response.size(); index++)
                {
                    extracted += QString("0x%1 ").arg((uint)response[index], 2, 16, QChar('0'));
                }
                extracted += "\r\n";
                ui->log->appendPlainText(extracted);
            }
            else
            {
                QString message;
                message = QString("CRC mismatch calc %1 != recv %2\r\n").arg((uint)crcCalculated, 4, 16, QChar('0')).arg((uint)crcReceived, 4, 16, QChar('0'));
                ui->log->appendPlainText(message);
            }
        }
        else
        {
            QString message;
            message = QString("Packet too short for valid response %1 bytes\r\n").arg(response.size());
            ui->log->appendPlainText(message);
        }
    }
    else
    {
        QMessageBox::critical(this, "Error", QString::fromStdString(ftStrings.Format(ftStatus, "reading")));
    }
}


void MainWindow::WriteGetPageRowCountsCommand()
{
    FT_STATUS               ftStatus;
    ExpandedBuffer_t        writeBuffer;
    std::vector<uint8_t>    command = UsbCommand_GetPagerowCounts();

    CreateWriteBuffer(command, writeBuffer);

    ULONG ulBytesToWrite = writeBuffer.size() * sizeof(writeBuffer[0]) / sizeof(UCHAR);
    ULONG ulBytesWritten;
    ftStatus = FT_WritePipe(usb.Handle(), FT_CHANNEL1_WRITE, reinterpret_cast<PUCHAR>(&writeBuffer[0]), ulBytesToWrite, &ulBytesWritten, NULL);
    if (ftStatus != FT_OK)
    {
        QMessageBox::critical(this, "Error", QString::fromStdString(ftStrings.Format(ftStatus, "Send GetPageRowCounts")));
    }
}


void MainWindow::ReadGetPageRowCountsResponse()
{
    FT_STATUS                       ftStatus;
    ExpandedBuffer_t                readBuffer;
    ULONG                           ulBytesRead;
    std::vector<uint8_t>            responsePacket;
    UsbParseResult                  parseResult;
    UsbResponseGetPagerowCounts_t   response;

    readBuffer.resize(1024);
    ftStatus = FT_ReadPipe(usb.Handle(), FT_CHANNEL1_READ, reinterpret_cast<PUCHAR>(&readBuffer[0]), readBuffer.size() * sizeof(readBuffer[0]) / sizeof(UCHAR), &ulBytesRead, NULL);
    if (ftStatus == FT_OK)
    {
        ExtractResponse(readBuffer, responsePacket);
        parseResult = UsbParseResponse_GetPagerowCounts(responsePacket, response);
        if (parseResult == UsbParseResult::OkGoodResponse)
        {
            QString message;
            message = QString("GetPageRowCounts: installed = %1 (pagerows equivalent)\r\n                  capacity = %2 (pagerows equivalent)\r\n                  used = %3\r\n").
                    arg(response.pagerowsInstalled).
                    arg(response.pagerowsCapacity).
                    arg(response.pagerowsUsed);
            ui->log->appendPlainText(message);
        }
        else
        {
            QString message;
            message = QString("Error in GetPageRowCounts parse result=%1 response status=0x%2\r\n").
                    arg((int)parseResult).
                    arg(response.basicResponse.responseStatus, 2, 16, QChar('0'));
            ui->log->appendPlainText(message);
        }
    }
    else
    {
        QMessageBox::critical(this, "Error", QString::fromStdString(ftStrings.Format(ftStatus, "reading")));
    }
}


void MainWindow::ReadBadBlockTablePart()
{
    Sleep(1);
    if (SendReadBadBlockTablePart())
    {
        Sleep(1);
        if (ReceiveReadBadBlockTablePart())
        {
        }
    }
}


bool MainWindow::SendReadBadBlockTablePart()
{
    bool    sentOk = false;

    FT_STATUS               ftStatus;
    ExpandedBuffer_t        writeBuffer;
    std::vector<uint8_t>    command = UsbCommand_ReadBadBlocksTable(0);

    CreateWriteBuffer(command, writeBuffer);

    ULONG ulBytesToWrite = writeBuffer.size() * sizeof(writeBuffer[0]) / sizeof(UCHAR);
    ULONG ulBytesWritten;
    ftStatus = FT_WritePipe(usb.Handle(), FT_CHANNEL1_WRITE, reinterpret_cast<PUCHAR>(&writeBuffer[0]), ulBytesToWrite, &ulBytesWritten, NULL);
    if (ftStatus == FT_OK)
    {
        sentOk = true;
    }
    else
    {
        QMessageBox::critical(this, "Error", QString::fromStdString(ftStrings.Format(ftStatus, "Send ReadBadBlockTablePart")));
    }

    return sentOk;
}


bool MainWindow::ReceiveReadBadBlockTablePart()
{
    bool    receiveOk = false;

    FT_STATUS                           ftStatus;
    ExpandedBuffer_t                    readBuffer;
    ULONG                               ulBytesRead;
    std::vector<uint8_t>                responsePacket;
    UsbParseResult                      parseResult;
    UsbResponse_ReadBadBlocksTable_t    response;
    ULONG                               ulElementsRead;
    QString                             received;

    readBuffer.resize(1024);
    ftStatus = FT_ReadPipe(usb.Handle(), FT_CHANNEL1_READ, reinterpret_cast<PUCHAR>(&readBuffer[0]), readBuffer.size() * sizeof(readBuffer[0]) / sizeof(UCHAR), &ulBytesRead, NULL);

    ulElementsRead = ulBytesRead / sizeof(ExpandedBuffer_t::value_type);
    received = QString("Received %1 bytes, %2 elements:").arg(ulBytesRead).arg(ulElementsRead);
    for (DWORD i = 0; i < ulElementsRead; i++)
    {
        if ((i % 16) == 0)
        {
            received += "\r\n";
        }
        received += QString("%1 ").arg((uint)readBuffer[i], 4, 16, QChar('0'));
    }
    received += "\r\n";
    ui->log->appendPlainText(received);

    if (ftStatus == FT_OK)
    {
        ExtractResponse(readBuffer, responsePacket);
        parseResult = UsbParseResponse_ReadBadBlocksTable(responsePacket, response);
        if (parseResult == UsbParseResult::OkGoodResponse)
        {
            receiveOk = true;

            QString extracted;
            extracted = "BBT Part Content:\r\n";
            for (size_t index = 0; index < response.bbtPartCount; index++)
            {
                extracted += QString("0x%1 ").arg((uint)response.bbtPart[index] & 0xFF, 2, 16, QChar('0'));
                if (((index + 1) % 16) == 0)
                {
                    extracted += "\r\n";
                }
            }
            extracted += "\r\n";
            ui->log->appendPlainText(extracted);
        }
        else
        {
            QString message;
            message = QString("Error in ReadBadBlockTable parse result=%1 response status=0x%2\r\n").
                    arg((int)parseResult).
                    arg(response.basicResponse.responseStatus, 2, 16, QChar('0'));
            ui->log->appendPlainText(message);
        }
    }
    else
    {
        QMessageBox::critical(this, "Error", QString::fromStdString(ftStrings.Format(ftStatus, "reading")));
    }

    return receiveOk;
}


void MainWindow::WriteGetBadBlockCountsCommand()
{
}


void MainWindow::ReadGetBadBlockCountsResponse()
{
//    FT_STATUS                       ftStatus;
//    ExpandedBuffer_t                readBuffer;
//    ULONG                           ulBytesRead;
//    std::vector<uint8_t>            responsePacket;
//    UsbParseResult                  parseResult;
//    UsbResponseGetPagerowCounts_t   response;

//    readBuffer.resize(1024);
//    ftStatus = FT_ReadPipe(usb.Handle(), FT_CHANNEL1_READ, reinterpret_cast<PUCHAR>(&readBuffer[0]), readBuffer.size() * sizeof(readBuffer[0]) / sizeof(UCHAR), &ulBytesRead, NULL);
//    if (ftStatus == FT_OK)
//    {
//        ExtractResponse(readBuffer, responsePacket);
//        parseResult = UsbParseResponse_GetPagerowCounts(responsePacket, response);
//        if (parseResult == UsbParseResult::OkGoodResponse)
//        {
//            QString message;
//            message = QString("GetPageRowCounts: installed = %1 (pagerows equivalent)\r\n                  capacity = %2 (pagerows equivalent)\r\n                  used = %3\r\n").
//                    arg(response.pagerowsInstalled).
//                    arg(response.pagerowsCapacity).
//                    arg(response.pagerowsUsed);
//            ui->log->appendPlainText(message);
//        }
//        else
//        {
//            QString message;
//            message = QString("Error in GetPageRowCounts parse result=%1 response status=0x%2\r\n").
//                    arg((int)parseResult).
//                    arg(response.basicResponse.responseStatus, 2, 16, QChar('0'));
//            ui->log->appendPlainText(message);
//        }
//    }
//    else
//    {
//        QMessageBox::critical(this, "Error", QString::fromStdString(ftStrings.Format(ftStatus, "reading")));
//    }
}


void MainWindow::appendDummyJob(uint32_t numberRevolutions)
{
    FT_STATUS               ftStatus;
    ExpandedBuffer_t        writeBuffer;
    std::vector<uint8_t>    command = UsbCommand_AppendDummyJob(numberRevolutions);

    CreateWriteBuffer(command, writeBuffer);

    ULONG ulBytesToWrite = writeBuffer.size() * sizeof(writeBuffer[0]) / sizeof(UCHAR);
    ULONG ulBytesWritten;
    ftStatus = FT_WritePipe(usb.Handle(), FT_CHANNEL1_WRITE, reinterpret_cast<PUCHAR>(&writeBuffer[0]), ulBytesToWrite, &ulBytesWritten, NULL);
    if (ftStatus != FT_OK)
    {
        QMessageBox::critical(this, "Error", QString::fromStdString(ftStrings.Format(ftStatus, "Send AppendDummyJob")));
    }
}


void MainWindow::performErase()
{
    FT_STATUS               ftStatus;
    ExpandedBuffer_t        writeBuffer;
    std::vector<uint8_t>    command = UsbCommand_EraseNandArray();

    CreateWriteBuffer(command, writeBuffer);

    ULONG ulBytesToWrite = writeBuffer.size() * sizeof(writeBuffer[0]) / sizeof(UCHAR);
    ULONG ulBytesWritten;
    ftStatus = FT_WritePipe(usb.Handle(), FT_CHANNEL1_WRITE, reinterpret_cast<PUCHAR>(&writeBuffer[0]), ulBytesToWrite, &ulBytesWritten, NULL);
    if (ftStatus != FT_OK)
    {
        QMessageBox::critical(this, "Error", QString::fromStdString(ftStrings.Format(ftStatus, "Send EraseNandArray")));
    }
}


void MainWindow::performDownload(int startPageRow, int numberPageRows, QString filename)
{
    std::ofstream           fstr;
    std::fstream            fstrRaw;
    FT_STATUS               ftStatus;
    ExpandedBuffer_t        writeBuffer;
    // 8640 bytes per page
    // 2 NAND devices per pair = 2 * 8640 = 17,280 bytes per page
    // 8 NAND pairs per row = 17,280 * 8 = 138,240 bytes per row
    // Read 2 bytes for each read so Number of NAND reads = 138,240 / 2 = 69,120
    // 8 FTCLKS per NAND read = 69,120 * 8 = 552,960 FTCLKs, so number of elements in target array must match
    // True read length is xxxx bytes, we want to read extra for MCU <-> NAND switching times
    const size_t            pageRowSize = 552960; //
    const size_t            pageRowExtraSize = 100; // Some additional buffer to catch slow to start transfers
    const size_t            pageRowBufferSize = pageRowSize + pageRowExtraSize;
    const size_t            maximumPageRowsToRead = 1; // Maximum number of pageRows to read in one read operation
    ExpandedBuffer_t        readBuffer(maximumPageRowsToRead * pageRowBufferSize);
    std::vector<uint16_t>   downloaded;

    fstr.open(filename.toStdString(), std::ios::out | std::ios::binary);
    if (fstr.is_open())
    {
        QString rawFilename(filename);
        QFileInfo fi(rawFilename);
        QString ext = fi.completeSuffix();
        rawFilename.chop(ext.length());
        rawFilename += "usb";

        fstrRaw.open(rawFilename.toStdString(), std::ios::out | std::ios::binary);
        if (fstrRaw.is_open())
        {
            TlvcFile    usbFile(fstrRaw);

            UsbFile_Header  usbFileHeader;
            usbFileHeader.fileFormatVersion = UsbFile_Format_Version;
            usbFile.Append(UsbFile::Header, reinterpret_cast<char *>(&usbFileHeader), sizeof usbFileHeader);

            QThread::usleep(10000);

            int pageRowsToRead;
            int pageRowsSuccessfullyRead;
            for (int pageRowIndex = 0; pageRowIndex < numberPageRows; pageRowIndex += pageRowsSuccessfullyRead)
            {
                uint32_t currentPageRow = startPageRow + pageRowIndex;

                pageRowsSuccessfullyRead = 0;
                pageRowsToRead = maximumPageRowsToRead;
                if (pageRowsToRead > (numberPageRows - pageRowIndex))
                {
                    pageRowsToRead = numberPageRows - pageRowIndex;
                }

                std::vector<uint8_t> command = UsbCommand_ReadPageRows(currentPageRow, pageRowsToRead);
                CreateWriteBuffer(command, writeBuffer);

                ULONG ulBytesToWrite = writeBuffer.size() * sizeof(writeBuffer[0]) / sizeof(UCHAR);
                ULONG ulBytesWritten;
                ftStatus = FT_WritePipe(usb.Handle(), FT_CHANNEL1_WRITE, reinterpret_cast<PUCHAR>(&writeBuffer[0]), ulBytesToWrite, &ulBytesWritten, NULL);
                if (ftStatus == FT_OK)
                {
                    usbFile.Append(UsbFile::DataOut, reinterpret_cast<char *>(&writeBuffer[0]), writeBuffer.size() * sizeof(writeBuffer[0]));
                }
                else
                {
                    QMessageBox::critical(this, "Error", QString::fromStdString(ftStrings.Format(ftStatus, "download write")));
                }

                QThread::usleep(10000);

                readBuffer.resize(pageRowsToRead * pageRowBufferSize);

                ULONG ulBytesRead;
                ftStatus = FT_ReadPipe(usb.Handle(), FT_CHANNEL1_READ, reinterpret_cast<PUCHAR>(&readBuffer[0]), readBuffer.size() * sizeof(readBuffer[0]) / sizeof(UCHAR), &ulBytesRead, NULL);

                // The read size seems to come through as 552690 elements, which is less than 552960+100. But it's repeatable, and probably ties
                // in with the number of bytes dropped at each TXE_N since it works out to 69086.25 bytes which is 34 bytes smaller than the page size of 69120.
                // 34 is a familiar number to be short from the

                ULONG ulElementsRead = ulBytesRead / sizeof(readBuffer[0]);
                //QMessageBox::information(this, "Information", QString("Received %1 bytes, %2 elements").arg(ulBytesRead).arg(ulElementsRead));
                if (ftStatus == FT_OK)
                {
                    if (ulElementsRead == 552691)
                    {
                        readBuffer.erase(readBuffer.begin());
                        ulElementsRead--;
                    }

                    if (ulElementsRead == 552690)
                    {
                        readBuffer.resize(ulElementsRead);
                        //fstrRaw.write(reinterpret_cast<char *>(&readBuffer[0]), readBuffer.size() * sizeof(readBuffer[0]));
                        usbFile.Append(UsbFile::DataIn, reinterpret_cast<char *>(&readBuffer[0]), readBuffer.size() * sizeof(readBuffer[0]));

                        UsbCommand_ExtractDownloadData(readBuffer, downloaded);
                        fstr.write(reinterpret_cast<char *>(&downloaded[0]), downloaded.size() * sizeof(downloaded[0]));

                        pageRowsSuccessfullyRead = pageRowsToRead;
                    }
                    else
                    {
                        QString message;
                        message = QString("Pagerow %1 response incorrect length (got %2 elements, expected %3), retrying...\r\n").arg(pageRowIndex).arg(ulElementsRead).arg(552690);
                        ui->log->appendPlainText(message);
                    }
                }
                else
                {
                    QString ftStatusDescription = QString::fromStdString(ftStrings.Format(ftStatus, "download read"));

                    QMessageBox::critical(this, "Error", ftStatusDescription);

                    QString message;
                    message = QString("Pagerow %1 error \"%2\", retrying...\r\n").arg(pageRowIndex).arg(ftStatusDescription);
                    ui->log->appendPlainText(message);

                    FT_STATUS               ftStatus;
                    ftStatus = FT_AbortPipe(usb.Handle(), FT_CHANNEL1_READ);
                    if (ftStatus != FT_OK)
                    {
                        QString ftStatusDescription = QString::fromStdString(ftStrings.Format(ftStatus, "aborting read"));
                        ui->log->appendPlainText(ftStatusDescription);
                    }
                }

                ui->progressBar->setValue(100 * (pageRowIndex + pageRowsToRead) / numberPageRows);

                QThread::usleep(10000);
            }
        }
        else
        {
            QMessageBox::critical(this, "Error", "Could not open file to write raw USB data to");
        }
    }
    else
    {
        QMessageBox::critical(this, "Error", "Could not open file to write extracted data to");
    }
}


void MainWindow::performAutoDownload(QString filename)
{
    /*
     *  Sequence for automatic download:
     *      Identify tool.
     *      Get number of page rows to download.
     *      Download the pagerows one at a time (h/w limitation for now, timing between FT600 and data).
     *      Deinterleave the data.
     *      Store data to file.
     */
#if 0
    do
    {
        switch (state)
        {
            case identify:
                GetToolIdentity();
                if (commandsucceeded)
                {
                    // no action
                    state = getpagerows;
                }
                else if (!commandsucceeded && retries==0)
                {
                    displayerror();
                    state = finalState;
                }
                else
                {
                    abort(); retries--; // no other action, will get performed "in state"
                    // stay in same state
                }
                break;

            case getpagerows:
                break;
        }
    } while (state != finalState);

#endif
    // or...

#if 0
    bool runSequence = true;
    extern bool GetToolIdentity(void);
    extern bool GetPagerowsToDownload(void);
    extern bool DownloadPagerows(void);

    if(runSequence) runSequence &= GetToolIdentity(); // Assumes only a single failure path
    if(runSequence) runSequence &= GetPagerowsToDownload(); // Assumes only a single failure path
    if(runSequence) runSequence &= DownloadPagerows(); // Assumes only a single failure path and not good for looping
    // And where to store state in all this?
#endif
}


void MainWindow::performPythonDownload()
{
    /*
     *  Sequence from an external interface point of view.
     *      Connect
     *      BeginRead
     *      ReadAllBlocks*N
     *      EndRead
     *      Disconnect
     */
}


void MainWindow::autoDownloadFinished()
{
    setUiForIdle();
    ui->log->appendPlainText(QDateTime::currentDateTime().toString(timeFormat) + "Download finished");
}


void MainWindow::refreshDevicesClicked()
{
    ui->deviceList->clear();
    populateDeviceList();
}


void MainWindow::selectedDeviceChanged()
{
    // Selection can only change when the port is not open
    setUiForClosed();
}


void MainWindow::on_browseButton_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Save File", "", "All files (*)");
    if (!fileName.isEmpty())
    {
        this->ui->filename->setText(fileName);
    }
}


void MainWindow::openUsbClicked()
{
    QList<QListWidgetItem *> selected = ui->deviceList->selectedItems();
    if (!selected.empty())
    {
        unsigned long deviceIndex = selected[0]->data(Qt::UserRole).toUInt();
        QString message;
        message = QDateTime::currentDateTime().toString(timeFormat);
        message += QString("Opening USB device %1... ").arg(deviceIndex);
        if (usb.Open(deviceIndex))
        {
            message += "Opened successfully.\n";
            setUiForOpened();
            ui->statusbar->showMessage("Connected to .");
        }
        else
        {
            message += "Could not open USB device.\n";
            QMessageBox::critical(this, "Error", "Could not open USB device");
        }

        ui->log->appendPlainText(message);
    }
}


void MainWindow::closeUsbClicked()
{
    CloseUsb();
}


void MainWindow::abortWriteClicked()
{
    ui->log->appendPlainText(QDateTime::currentDateTime().toString(timeFormat) + "Abort write");
    AbortWrite();
}


void MainWindow::abortReadClicked()
{
    ui->log->appendPlainText(QDateTime::currentDateTime().toString(timeFormat) + "Abort read");
    AbortRead();
}


void MainWindow::cyclePortClicked()
{
    ui->log->appendPlainText(QDateTime::currentDateTime().toString(timeFormat) + "Cycle port");
    CyclePort();
}

void MainWindow::identifyClicked()
{
    ui->log->appendPlainText(QDateTime::currentDateTime().toString(timeFormat) + "Identify tool");
    Identify();
}

void MainWindow::writeEchoClicked()
{
    ui->log->appendPlainText(QDateTime::currentDateTime().toString(timeFormat) + "writing to USB");
    WriteEchoCommand();
}


void MainWindow::readEchoClicked()
{
    ui->log->appendPlainText(QDateTime::currentDateTime().toString(timeFormat) + "reading from USB");
    ReadEchoResponse();
}


void MainWindow::autoEchoClicked()
{
    ui->log->appendPlainText(QDateTime::currentDateTime().toString(timeFormat) + "sending and receiving Echo command");
    WriteEchoCommand();
    QThread::msleep(1);
    ReadEchoResponse();
}

void MainWindow::readCountersClicked()
{
    ui->log->appendPlainText(QDateTime::currentDateTime().toString(timeFormat) + "Reading memory size counters");

    WriteGetPageRowCountsCommand();
    QThread::msleep(1);
    ReadGetPageRowCountsResponse();

    QThread::msleep(1);

    WriteGetBadBlockCountsCommand();
    QThread::msleep(1);
    ReadGetBadBlockCountsResponse();
}


void MainWindow::readBbtClicked()
{
    ui->log->appendPlainText(QDateTime::currentDateTime().toString(timeFormat) + "Reading BBT");
    ReadBadBlockTablePart();
}


void MainWindow::markBadClicked()
{
}


void MainWindow::commitBbtClicked()
{
}


void MainWindow::eraseNandClicked()
{
    ui->log->appendPlainText(QDateTime::currentDateTime().toString(timeFormat) + "Start erase");
    setUiForRunning();
    ui->progressBar->setValue(0);

    performErase();

    ui->progressBar->setValue(100);
    setUiForIdle();
    ui->log->appendPlainText(QDateTime::currentDateTime().toString(timeFormat) + "Erase finished");
}


void MainWindow::appendDummyJobClicked()
{
    bool    ok;
    int numberRevolutions = QInputDialog::getInt(this, "Data entry", "Please enter number of revolutions worth of dummy data (RIH and POOH):", 100, 10, 1000000, 1, &ok);
    if (ok)
    {
        ui->log->appendPlainText(QDateTime::currentDateTime().toString(timeFormat) + QString("Append dummy data for %1 revolutions").arg(numberRevolutions));
        appendDummyJob(numberRevolutions);
    }
}


void MainWindow::on_downloadButton_clicked()
{
    ui->log->appendPlainText(QDateTime::currentDateTime().toString(timeFormat) + "Start manual raw download");
    setUiForRunning();
    ui->progressBar->setValue(0);
    performDownload(ui->startPageRow->value(), ui->numberPageRows->value(), ui->filename->text());
    setUiForIdle();
    ui->log->appendPlainText(QDateTime::currentDateTime().toString(timeFormat) + "Download finished");
}


void MainWindow::autoDownloadButtonClicked()
{
    ui->log->appendPlainText(QDateTime::currentDateTime().toString(timeFormat) + "Start automatic download");
    setUiForRunning();
    ui->progressBar->setValue(0);
    performAutoDownload(ui->filename->text());
    autoDownloadFinished();
}


void MainWindow::pythonDownloadButtonClicked()
{
    ui->log->appendPlainText(QDateTime::currentDateTime().toString(timeFormat) + "Start automatic download");
    setUiForRunning();
    ui->progressBar->setValue(0);
    performPythonDownload();
    autoDownloadFinished();
}

