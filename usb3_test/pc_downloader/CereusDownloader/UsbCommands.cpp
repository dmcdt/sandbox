/*
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 */


#include "UsbCommands.h"

#include <algorithm>

#include <cstring>

#include "crc16_modbus.h"


const int oneNandReadCycleCount = 8;        // Number of FTDI clock cycles / transfers that one NAND chip is enabled for
const int oneNandReadIndex = 4;             // Index within the number of FTDI clock cycles that we use as the valid data
const int ftdiTransferBufferSize = 4096;    // Byte length of the FTDI FIFO transfer buffer
const int transferCountPerBuffer = ftdiTransferBufferSize / sizeof(ExpandedBuffer_t::value_type);
const int oneNandTransferCountPerBuffer = transferCountPerBuffer / oneNandReadCycleCount;

const int nandDeviceCount = 8;
const int pageSize = (8192 + 448);          // Number of elements in a page of NAND (either bytes in a single device or 16 bits in a pair)


void CreateWriteBuffer(std::vector<uint8_t> const& data, ExpandedBuffer_t& writeBuffer)
{
    writeBuffer.resize(data.size() * OVERSAMPLE_RATIO);

    for (size_t index = 0; index < data.size(); index++)
    {
        for (size_t repeatIndex = 0; repeatIndex < OVERSAMPLE_RATIO; repeatIndex++)
        {
            writeBuffer[index * OVERSAMPLE_RATIO + repeatIndex] = (index << 8) | data[index];
            //writeBuffer[index * OVERSAMPLE_RATIO + repeatIndex] = (index & 1 ? 0xFF00 : 0x0000) | data[index];
        }
    }
}


void ExtractResponse(ExpandedBuffer_t const& readBuffer, std::vector<uint8_t>& response)
{
    response.clear();
    if (!readBuffer.empty())
    {
        response.reserve(readBuffer.size());
        //uint8_t lastMessageIndex;

        //lastMessageIndex = readBuffer[0] >> 8;
        //response.push_back(readBuffer[0] & 0xFFU);

        //for (size_t readIndex = 1; readIndex < readBuffer.size(); readIndex++)
        //{
        //    uint8_t messageIndex = readBuffer[readIndex] >> 8;
        //    if (messageIndex == lastMessageIndex)
        //    {
        //        // Same as last byte, ignore
        //    }
        //    else if (messageIndex == (lastMessageIndex + 1))
        //    {
        //        // Next message byte
        //        lastMessageIndex = messageIndex;
        //        response.push_back(readBuffer[readIndex] & 0xFFU);
        //    }
        //    else
        //    {
        //        // Error - missed byte in message. Probably shouldn't happen in the PC side as it'll be fast enough.
        //        // CRC will end up mismatch anyway, so don't need to do anything here
        //    }
        //}

        // PC processing needs to be a bit more rigid since it can have more spurious data.
        // Only start when the index byte is 0. Only accept bytes when move onto the next
        // expected index byte. Take a majority vote on the data bytes for a sequence of
        // the same index bytes.

        //  While data to process
        //      Find expected index
        //      Find last expected index
        //      Find "mode" of data values -OR- just the midpoint as the data should be stable
        //      Move onto next message index

        uint8_t expectedIndex = 0;
        for (ExpandedBuffer_t::const_iterator it = readBuffer.cbegin(); it != readBuffer.cend(); /* Incremented in loop */)
        {
            ExpandedBuffer_t::const_iterator    expectedIt;
            expectedIt = std::find_if(it, readBuffer.cend(), [expectedIndex](ExpandedBuffer_t::value_type value) { return (value >> 8) == expectedIndex; } );
            if (expectedIt != readBuffer.cend())
            {
                ExpandedBuffer_t::const_iterator    nextIndexIt;
                nextIndexIt = std::find_if_not(expectedIt + 1, readBuffer.cend(), [expectedIndex](ExpandedBuffer_t::value_type value) { return (value >> 8) == expectedIndex; });

                expectedIt += std::distance(expectedIt, nextIndexIt) / 2;
                response.push_back(*expectedIt & 0xFFU);

                ++expectedIndex;
                it = nextIndexIt;
            }
            else
            {
                it = readBuffer.cend();
            }
        }

        //for (size_t readBufferIndex = 0; readBufferIndex < readBuffer.size(); /* Increased within loop */)
        //{

        //}
    }
}


// Extracts the download data from the data read over the USB bus.
// Currently (depending on CPLD implementation) it extracts the
// 4th element from every 8 in the data read over the USB.
void UsbCommand_ExtractDownloadData(ExpandedBuffer_t const& readBuffer, std::vector<uint16_t>& download)
{
    // Reduce extracted data to an exact page size.
    size_t pageCount;

    pageCount = readBuffer.size() / oneNandReadCycleCount;                                  // Number of elements fully read using CPLD
    pageCount = ((pageCount + (nandDeviceCount * pageSize) - 1) / (nandDeviceCount * pageSize)) * (nandDeviceCount * pageSize);  // Reduced to multiple of whole pages (rounded up as pages always come in short)

    download.resize(pageCount);
    for (size_t downloadIndex = 0; downloadIndex < download.size(); ++downloadIndex)
    {
        // Original unworking indexing
        //download[index] = readBuffer[8 * index + 4 - (index % 4096)];

        // Figured out from inspecting raw binary dump.
        // BD35/256 is the interval at which we drop a byte due to the FIFO buffer becoming full
        // 1280 is the position of the indexed value.
        // 2049 is the period over which the indexed item is inside the TXE_N deasserted period.
        // =8*BD35+4-ROUNDDOWN((BD35-ROUNDDOWN((BD35+(2049-1280))/2049,0))/256,0)

        size_t  readIndex;
        size_t  indexAdjust;

        indexAdjust = (downloadIndex - (downloadIndex + ((transferCountPerBuffer + 1) - ((oneNandReadIndex + 1) * oneNandTransferCountPerBuffer))) / (transferCountPerBuffer + 1)) / oneNandTransferCountPerBuffer;
        readIndex = oneNandReadCycleCount * downloadIndex + oneNandReadIndex - indexAdjust;

        download[downloadIndex] = readBuffer[readIndex];
    }
}


void UsbCommand_DeinterleaveExtractedData(std::vector<uint16_t> const& extracted, std::vector<uint16_t>& flattened)
{
    // Reduce extracted data to an exact page size otherwise there is a risk
    // that the spurious reads at the end will overrun the valid data at the
    // start of the next page.
    size_t sizeAlignToPage;

    sizeAlignToPage = (extracted.size() / (nandDeviceCount * pageSize)) * (nandDeviceCount * pageSize);

    flattened.resize(sizeAlignToPage);

    for (size_t extractedIndex = 0; extractedIndex < sizeAlignToPage; ++extractedIndex)
    {
        size_t flattenedIndex;
        flattenedIndex = (extractedIndex % nandDeviceCount) * pageSize + (extractedIndex / nandDeviceCount);
        flattened[flattenedIndex] = extracted[extractedIndex];
    }
}


std::vector<uint8_t> UsbCommand_CompletePacket(std::vector<uint8_t>& packet)
{
    uint16_t                crc;

    crc = CRC16Modbus_Calculate(&packet[0], packet.size() - USB_PROTOCOL_CRC_LENGTH);

    packet[packet.size() - USB_PROTOCOL_CRC_LSB_FROM_END] = crc & 0xFF;
    packet[packet.size() - USB_PROTOCOL_CRC_MSB_FROM_END] = (crc >> 8) & 0xFF;

    return packet;
}


std::vector<uint8_t> UsbCommand_Echo(std::vector<uint8_t> data)
{
    std::vector<uint8_t>    command(USB_PROTOCOL_COMMAND_OVERHEAD + data.size());

    command[0] = UsbCommandCode_Echo;
    std::copy(data.begin(), data.end(), command.begin() + 1);

    return UsbCommand_CompletePacket(command);
}


std::vector<uint8_t> UsbCommand_Identify()
{
    constexpr size_t        commandLength = USB_IDENTIFY_COMMAND_LENGTH;
    std::vector<uint8_t>    command(commandLength);

    command[0] = UsbCommandCode_Identify;

    return UsbCommand_CompletePacket(command);
}


std::vector<uint8_t> UsbCommand_GetPagerowCounts()
{
    constexpr size_t        commandLength = USB_GETPAGEROWCOUNTS_COMMAND_LENGTH;
    std::vector<uint8_t>    command(commandLength);

    command[0] = UsbCommandCode_GetPageRowCounts;

    return UsbCommand_CompletePacket(command);
}


UsbParseResult UsbParseResponse_BasicChecks(std::vector<uint8_t> const& packet, UsbCommandCode expectedCommandCode, UsbResponse_Common_t& response)
{
    if (packet.size() >= USB_PROTOCOL_RESPONSE_OVERHEAD)
    {
        response.commandCode = static_cast<UsbCommandCode>(packet[0]);
        response.responseStatus = static_cast<UsbResponseStatus>(packet[1]);
        response.crcReceived =  (packet[packet.size() - USB_PROTOCOL_CRC_LSB_FROM_END]) |
                                (packet[packet.size() - USB_PROTOCOL_CRC_MSB_FROM_END] << 8);

        response.crcCalculated = CRC16Modbus_Calculate(&packet[0], packet.size() - USB_PROTOCOL_CRC_LENGTH);
        if (response.crcCalculated == response.crcReceived)
        {
            if (packet[0] == expectedCommandCode)
            {
                if (packet[1] & UsbResponseStatus_ErrorFlag)
                {
                    if (packet.size() == USB_PROTOCOL_RESPONSE_OVERHEAD)
                    {
                        // TODO check command code is recognised
                        // TODO check the response status is actually recognised?
                        return UsbParseResult::OkErrorResponse;
                    }
                    else
                    {
                        return UsbParseResult::IncorrectLength;
                    }
                }
                else if (packet[1] == UsbResponseStatus_InProgress)
                {
                    if (packet.size() == USB_PROTOCOL_RESPONSE_OVERHEAD)
                    {
                        return UsbParseResult::OkGoodResponse;
                    }
                    else
                    {
                        return UsbParseResult::IncorrectLength;
                    }
                }
                else
                {
                    /* Somethign else, length specific to command code. */
                    // TODO check command code is recognised
                    return UsbParseResult::OkContinueParsing;
                }
            }
            else
            {
                return UsbParseResult::CommandCodeMismatch;
            }
        }
        else
        {
            return UsbParseResult::ChecksumMismatch;
        }
    }
    else
    {
        return UsbParseResult::IncorrectLength;
    }
}


UsbParseResult UsbParseResponse_GetPagerowCounts(std::vector<uint8_t> const& packet, UsbResponseGetPagerowCounts_t& response)
{
    UsbParseResult parseResult = UsbParseResponse_BasicChecks(packet, UsbCommandCode_GetPageRowCounts, response.basicResponse);
    if (parseResult == UsbParseResult::OkContinueParsing)
    {
        if (packet.size() == USB_GETPAGEROWCOUNTS_RESPONSE_LENGTH)
        {
            memcpy(&response.pagerowsUsed, &packet[2], sizeof(uint32_t));
            memcpy(&response.pagerowsInstalled, &packet[6], sizeof(uint32_t));
            memcpy(&response.pagerowsCapacity, &packet[10], sizeof(uint32_t));

            parseResult = UsbParseResult::OkGoodResponse;
        }
        else
        {
            parseResult = UsbParseResult::IncorrectLength;
        }
    }

    return parseResult;
}


std::vector<uint8_t> UsbCommand_ReadPageRows(uint32_t startPageRow, uint32_t numberPageRows)
{
    constexpr size_t        commandLength = USB_READPAGEROWS_COMMAND_LENGTH;
    std::vector<uint8_t>    command(commandLength);

    command[0] = UsbCommandCode_ReadPageRows;
    command[1] = startPageRow & 0xFF;
    command[2] = (startPageRow >> 8) & 0xFF;
    command[3] = (startPageRow >> 16) & 0xFF;
    command[4] = (startPageRow >> 24) & 0xFF;
    command[5] = numberPageRows & 0xFF;
    command[6] = (numberPageRows >> 8) & 0xFF;
    command[7] = (numberPageRows >> 16) & 0xFF;
    command[8] = (numberPageRows >> 24) & 0xFF;

    return UsbCommand_CompletePacket(command);
}


std::vector<uint8_t> UsbCommand_GetBadBlockCount(uint32_t deviceIndex)
{
    constexpr size_t        commandLength = USB_GETBADBLOCKCOUNT_COMMAND_LENGTH;
    std::vector<uint8_t>    command(commandLength);

    command[0] = UsbCommandCode_GetBadBlockCount;
    memcpy(&command[1], &deviceIndex, sizeof deviceIndex);

    return UsbCommand_CompletePacket(command);
}


std::vector<uint8_t> UsbCommand_ReadBadBlocksTable(uint32_t bbtIndex)
{
    constexpr size_t        commandLength = USB_READBADBLOCKS_COMMAND_LENGTH;
    std::vector<uint8_t>    command(commandLength);

    command[0] = UsbCommandCode_ReadBadBlocksTable;
    memcpy(&command[1], &bbtIndex, sizeof bbtIndex);

    return UsbCommand_CompletePacket(command);
}


UsbParseResult UsbParseResponse_ReadBadBlocksTable(std::vector<uint8_t> const& packet, UsbResponse_ReadBadBlocksTable_t& response)
{
    UsbParseResult parseResult = UsbParseResponse_BasicChecks(packet, UsbCommandCode_ReadBadBlocksTable, response.basicResponse);
    if (parseResult == UsbParseResult::OkContinueParsing)
    {
        if (packet.size() == USB_READBADBLOCKS_RESPONSE_LENGTH)
        {
            memcpy(&response.bbtPartCount, &packet[2], sizeof(uint16_t));
            memcpy(&response.startIndex, &packet[4], sizeof(uint32_t));
            memcpy(&response.nextIndex, &packet[8], sizeof(uint32_t));
            memcpy(&response.bbtPart, &packet[12], USB_READBADBLOCKS_NUMBERELEMENTS * sizeof(uint32_t));

            parseResult = UsbParseResult::OkGoodResponse;
        }
        else
        {
            parseResult = UsbParseResult::IncorrectLength;
        }
    }

    return parseResult;
}


std::vector<uint8_t> UsbCommand_EraseNandArray()
{
    constexpr size_t        commandLength = USB_ERASENANDARRAY_COMMAND_LENGTH;
    std::vector<uint8_t>    command(commandLength);

    uint32_t confirmationCode = USB_ERASENANDARRAY_CONFIRMATIONCODE;

    command[0] = UsbCommandCode_EraseNandArray;
    memcpy(&command[1], &confirmationCode, sizeof confirmationCode);

    return UsbCommand_CompletePacket(command);
}


std::vector<uint8_t> UsbCommand_AppendDummyJob(uint32_t numberRevolutions)
{
    constexpr size_t        commandLength = USB_APPENDDUMMY_COMMAND_LENGTH;
    std::vector<uint8_t>    command(commandLength);

    command[0] = UsbCommandCode_AppendDummyJob;
    memcpy(&command[1], &numberRevolutions, sizeof numberRevolutions);

    return UsbCommand_CompletePacket(command);
}

