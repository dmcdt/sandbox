#ifndef FT3XXENUMERATOR_H
#define FT3XXENUMERATOR_H

/*
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 *
 *  Enumerator class for FT3XX devices.
 */


#include <vector>

#include "FTD3XX.h"


class Ft3xxEnumerator
{
private:
    DWORD                                   numberDevices;
    std::vector<FT_DEVICE_LIST_INFO_NODE>   deviceList;

public:
    typedef std::vector<FT_DEVICE_LIST_INFO_NODE>::const_iterator const_iterator;

    Ft3xxEnumerator();

    const_iterator   cbegin();
    const_iterator   cend();
};

#endif
