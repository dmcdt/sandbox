#ifndef USBFILE_H
#define USBFILE_H

/*
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 *
 *  Declarations for the ".usb" raw download file format, which is a TLV
 *  style of container file format.
 */


#include "TlvcFile.h"


// Does not use scoped enum because cannot convert from a scoped enum even to the underlying type.
// https://stackoverflow.com/a/63707358
class UsbFile
{
public:
    enum Tag : uint32_t
    {
        Invalid = 0x00000000UL,
        Header  = 0x00425355UL, // "USB\0" when written Little Endian. See struct UsbFile_Header.
        DataIn  = 0x004E4944UL, // "DIN\0" when written Little Endian. Just a binary dump of what has been read in from the USB device.
        DataOut = 0x54554F44UL, // "DOUT" when written Little Endian. Just a binary dump of what has been written out to the USB device.
    };
};

struct UsbFile_Header
{
    uint32_t    fileFormatVersion;  // Currently this is only version 1 (little endian).
};


const uint32_t UsbFile_Format_Version = 1;


#endif
