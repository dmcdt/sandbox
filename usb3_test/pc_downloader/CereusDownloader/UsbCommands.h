#ifndef USBCOMMANDS_H
#define USBCOMMANDS_H

/*
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 */


#include <vector>
#include <cstdint>

// This needs to be defined to match the FTDI on the target board, either zero for the Mainboard (FT600) or 1 for the UMFT601 eval (FT601)
#define FT_TYPE FT_DEVICE_600

constexpr size_t OVERSAMPLE_RATIO = 16;

#if FT_TYPE == FT_DEVICE_600
// For FT600 on the High Speed board, since it is a 2 byte parallel FIFO bus
typedef std::vector<uint16_t> ExpandedBuffer_t;
#elif FT_TYPE == FT_DEVICE_601
// For FT601 on the FTDI eval board, since it is a 4 byte parallel FIFO bus
typedef std::vector<uint32_t> ExpandedBuffer_t;
#else
#pragma error('Unknown FTDI device type')
#endif


enum UsbCommandCode
{
    UsbCommandCode_Invalid              = 0x00,
    UsbCommandCode_Echo                 = 0x01,
    UsbCommandCode_Identify             = 0x02,
    UsbCommandCode_GetPageRowCounts     = 0x03, /* Get number of pagerows required to read all the logged job data, as well as other Pagerow count statistics (maximum addressable, maximum usable, available) */
    UsbCommandCode_ReadPageRows         = 0x04,
    UsbCommandCode_GetBadBlockCount     = 0x06, /* Gets number of bad blocks across entire array or from a single device. */
    UsbCommandCode_ReadBadBlocksTable   = 0x07, /* Reads whatever bad blocks the tool thinks there currently is (from teh RAM BBT). */
    UsbCommandCode_StoreBadBlockTable   = 0x08, /* Stores the current RAM bad block table to the NAND Array. */
    UsbCommandCode_MarkBlockAsWorn      = 0x09, /* Marks a block as worn out (bad) in RAM but DOES NOT store the updated table (in NAND). */
    UsbCommandCode_EraseNandArray       = 0x0A, /* Erases entire contents of NAND array (except bad blocks and bad block tables), and then updates the RAM table but does not store it to NAND. */
    UsbCommandCode_GetCommandResult     = 0x0B, /* Gets the most recent command result / response. Must be used to get the result of long running commands. */

    UsbCommandCode_AppendDummyJob       = 0x7F, /* Appends dummy job information to NAND flash */

    UsbCommandCode_MaximumCommand       = 0xFF  /* Maximum possible command code (given one byte in the protocol). */
};


enum UsbResponseStatus
{
    UsbResponseStatus_Ok                        = 0x00,     /* Command successful. */
    UsbResponseStatus_InProgress                = 0x01,     /* Command accepted and in progress. For long running commands. */
    UsbResponseStatus_ErrorFlag                 = 0x80,     /* Generic unspecified error code, should only be used if a more specific one is not more appropriate. */
    UsbResponseStatus_ErrorNoData               = 0x81,     /* No data specified for the command */
    UsbResponseStatus_ErrorLength               = 0x82,     /* Error in the length of the command, or any length parameter of the command. */
    UsbResponseStatus_ErrorAddress              = 0x83,     /* Start address or Start address + length of out bounds. */
    UsbResponseStatus_ErrorState                = 0x84,     /* Bad state specified in command parameter. */
    UsbResponseStatus_ErrorUid                  = 0x85,     /* Error in a unique identifier passed in the command, e.g. confirmation code, password, etc. */
    UsbResponseStatus_ErrorUnrecognisedCommand  = 0x86,     /* Command packet structured OK but command code not recognised / supported. */

    UsbResponseStatus_MaximumResponse           = 0xFF      /* Maximum possible response status (given one byte in the protocol). */
};


#define USB_PROTOCOL_COMMANDCODE_LENGTH     (1)
#define USB_PROTOCOL_RESPONSESTATUS_LENGTH  (1)
#define USB_PROTOCOL_CRC_LENGTH             (2)

#define USB_PROTOCOL_COMMAND_OVERHEAD       (USB_PROTOCOL_COMMANDCODE_LENGTH + USB_PROTOCOL_CRC_LENGTH)
#define USB_PROTOCOL_RESPONSE_OVERHEAD      (USB_PROTOCOL_COMMANDCODE_LENGTH + USB_PROTOCOL_RESPONSESTATUS_LENGTH + USB_PROTOCOL_CRC_LENGTH)

/* These are the index from the start of the CRC field. */
#define USB_PROTOCOL_CRC_LSB_INDEX          (0)
#define USB_PROTOCOL_CRC_MSB_INDEX          (1)

/*  These are the offsets from the length of the buffer content (so must be
 *  expressed as "length - XXXX").
 */
#define USB_PROTOCOL_CRC_LSB_FROM_END       (USB_PROTOCOL_CRC_LENGTH - USB_PROTOCOL_CRC_LSB_INDEX)
#define USB_PROTOCOL_CRC_MSB_FROM_END       (USB_PROTOCOL_CRC_LENGTH - USB_PROTOCOL_CRC_MSB_INDEX)


/*
 *  Echo: reports back the same data that was sent, with the first data byte inverted.
 *
 *  Command structure:
 *      uint8_t     command code
 *      uint8_t[]   data
 *      uint16_t    CRC
 *
 *  Response structure:
 *      uint8_t     command code
 *      uint8_t     response status
 *      uint8_t[]   data from command, with first byte inverted
 *      uint16_t    CRC
 *
 *  Lengths are indeterminate as it depends on the data sent from the PC.
 *  The maximum length rather than the exact length is defined below.
 */
#define USB_ECHO_COMMAND_LENGTH                 (32)
#define USB_ECHO_RESPONSE_LENGTH                (USB_ECHO_COMMAND_LENGTH  + 1)


/*
 *  Identify: Returns identification information about the tool / firmware.
 *
 *  Command structure:
 *      uint8_t     command code
 *      uint16_t    CRC
 *
 *  Response structure:
 *      uint8_t     command code
 *      uint8_t     response status
 *      uint32_t    company identifier
 *      uint16_t    device identifier
 *      uint8_t     firmware version major
 *      uint8_t     firmware version minor
 *      uint8_t     firmware version patch
 *      uint32_t    firmware checksum
 *      uint8_t[16] serial number
 *      uint16_t    CRC
 */
#define USB_IDENTIFY_COMMAND_LENGTH             (USB_PROTOCOL_COMMAND_OVERHEAD)
#define USB_IDENTIFY_RESPONSE_LENGTH            (USB_PROTOCOL_RESPONSE_OVERHEAD + 29)
#define USB_IDENTIFY_RESPONSE_SERIAL_LENGTH     (16)


/*
 *  Get Pagerow Counts: Get number of pagerows required to read all the logged job data.
 *
 *  Command structure:
 *      uint8_t     command code
 *      uint16_t    CRC
 *
 *  Response structure:
 *      uint8_t     command code
 *      uint8_t     response status
 *      uint32_t    number of pagerows used in log memory.
 *      uint32_t    maximum number of pagerows in installed memory
 *      uint32_t    maximum number of pagerows usable for storage [(maximum installed memory - bad/reserved block capacity) expressed as pagerows]
 *      uint16_t    CRC
 */
#define USB_GETPAGEROWCOUNTS_COMMAND_LENGTH     (USB_PROTOCOL_COMMAND_OVERHEAD)
#define USB_GETPAGEROWCOUNTS_RESPONSE_LENGTH    (USB_PROTOCOL_RESPONSE_OVERHEAD + 12)



/*
 *  Read Page Rows: reads a number of rows of pages.
 *
 *  Command structure:
 *      uint8_t     command code
 *      uint32_t    Start address as pagerow number
 *      uint32_t    Length, number of pagerows to read
 *      uint16_t    CRC
 *
 *  Response structure (for error):
 *      uint8_t     command code
 *      uint8_t     response status
 *      uint16_t    CRC
 *
 *  Response structure (for good):
 *      uint8_t[N]  NAND array data
 *                  where N is the number of bytes in a pagerow (8192 + 448) * 2 * 8
 *                  multiplied by the number of requested pagerows
 *
 *  NB: the good response does not fit the normal response format as it is
 *  generated by the CPLD reading straight out of the NAND array. The caller
 *  should use the length of the response as the determining factor for whether
 *  it is good or bad. The length of the good response is indeterminate as
 *  it depends on the requested length.
 */
#define USB_READPAGEROWS_COMMAND_LENGTH         (USB_PROTOCOL_COMMAND_OVERHEAD + 8)
#define USB_READPAGEROWS_BAD_RESPONSE_LENGTH    (USB_PROTOCOL_RESPONSE_OVERHEAD)


/*
 *  Get Bad Block Count: returns the number of bad blocks from either the entire array or a single device.
 *
 *  Command structure:
 *      uint8_t     command code
 *      uint32_t    Device index, reserved, must always be 0xFFFFFFFFUL for overall count.
 *      uint16_t    CRC
 *
 *  Response structure:
 *      uint8_t     command code
 *      uint8_t     response status
 *      uint32_t    number of bad blocks
 *      uint16_t    CRC
 *
 *  The device index in the command can be calculated as:
 *      (Nand Array Column * Number of Nand Rows + Nand Array Row) * 2 + 0 for low device in pair
 *      (Nand Array Column * Number of Nand Rows + Nand Array Row) * 2 + 1 for high device in pair
 */
#define USB_GETBADBLOCKCOUNT_COMMAND_LENGTH     (USB_PROTOCOL_COMMAND_OVERHEAD + 4)
#define USB_GETBADBLOCKCOUNT_ALLDEVICES         (0xFFFFFFFFUL)
#define USB_GETBADBLOCKCOUNT_RESPONSE_LENGTH    (USB_PROTOCOL_RESPONSE_OVERHEAD + 4)


/*
 *  Read bad blocks table: Read the low level bad block table.
 *
 *  Command structure:
 *      uint8_t     command code
 *      uint32_t    Bad block table index. Start using 0 and then on subsequent calls pass the value in the response.
 *      uint16_t    CRC
 *
 *  Response structure:
 *      uint8_t         command code
 *      uint8_t         response status
 *      uint16_t        Number elements valid (number of elements in the response table that have been filled in), plus aligns the following members.
 *      uint32_t        Start index (copy of command index or 0xFFFFFFFFUL for invalid index)
 *      uint32_t        Next bad block table index or 0xFFFFFFFFUL for no more data.
 *      uint32_t[32]    Bad block table raw data.
 *      uint16_t        CRC
 */
#define USB_READBADBLOCKS_COMMAND_LENGTH        (USB_PROTOCOL_COMMAND_OVERHEAD + 4)
#define USB_READBADBLOCKS_INVALIDINDEX          (0xFFFFFFFFUL)
#define USB_READBADBLOCKS_NUMBERELEMENTS        (32)
#define USB_READBADBLOCKS_RESPONSE_LENGTH       (USB_PROTOCOL_RESPONSE_OVERHEAD + 138)


/*
 *  Store Bad Block Table: Commits the current RAM based bad block table to flash for quicker startup.
 *
 *  Command structure:
 *      uint8_t     command code
 *      uint32_t    confirmation code   (0x53615633 "SaV3", just for command differentiation)
 *      uint16_t    CRC
 *
 *  Response structure:
 *      uint8_t     command code
 *      uint8_t     response status
 *      uint16_t    CRC
 */
#define USB_STOREBBT_COMMAND_LENGTH             (USB_PROTOCOL_COMMAND_OVERHEAD + 4)
#define USB_STOREBBT_CONFIRMATIONCODE           (0x53615633UL)
#define USB_STOREBBT_RESPONSE_LENGTH            (USB_PROTOCOL_RESPONSE_OVERHEAD)


/*
 *  Mark NAND block: Marks a block of NAND in the array as worn, etc.
 *
 *  Command structure:
 *      uint8_t     command code
 *      uint32_t    array location
 *      uint32_t    block index (within selected device)
 *      uint32_t    new state
 *      uint16_t    CRC
 *
 *  Response structure:
 *      uint8_t     command code
 *      uint8_t     response status
 *      uint16_t    CRC
 *
 *  Remarks:
 *  This command must be used to set the state of all NAND_ARRAY_SIMULTANEOUSDEVICES
 *  devices within the array location simultaneously (otherwise hardware
 *  inconsistencies may develop). The states and format of the new state must
 *  match the format of the BBT in "NandBadBlocks.h", with the value right justified
 *  (aligned to the LSb).
 *
 *  The array location in the command can be calculated as:
 *      (Nand Array Column * Number of Nand Rows + Nand Array Row)
 *      (Nand Array Column * Number of Nand Rows + Nand Array Row)
 */
#define USB_MARKBLOCK_COMMAND_LENGTH            (USB_PROTOCOL_COMMAND_OVERHEAD + 12)
#define USB_MARKBLOCK_RESPONSE_LENGTH           (USB_PROTOCOL_RESPONSE_OVERHEAD)


/*
 *  Erase NAND array: Erase all good blocks in the NAND array.
 *
 *  Command structure:
 *      uint8_t     command code
 *      uint32_t    confirmation code   (0x426C4E6B "BlNk", for additional length of command differentiation)
 *      uint16_t    CRC
 *
 *  Initial Response structure:
 *      uint8_t     command code
 *      uint8_t     response status - either pending or an error
 *      uint16_t    CRC
 *
 *  Get Command Result Response structure:
 *      uint8_t     command code
 *      uint8_t     response status
 *      uint32_t    progress - counter for how much progress has been made. In this case number of blocks erased.
 *      uint32_t    total - total count for operation complete. In this case, number of blocks to be erased.
 *      uint16_t    CRC
 *
 *  Completed Response structure:
 *      uint8_t     command code
 *      uint8_t     response status - either OK or an error
 *      uint16_t    CRC
 */
#define USB_ERASENANDARRAY_COMMAND_LENGTH       (USB_PROTOCOL_COMMAND_OVERHEAD + 4)
#define USB_ERASENANDARRAY_CONFIRMATIONCODE     (0x426C4E6BUL)
#define USB_ERASENANDARRAY_RESPONSE_LENGTH      (USB_PROTOCOL_RESPONSE_OVERHEAD)


//    UsbCommandCode_GetCommandResult     = 0x0B, /* Gets the most recent command result / response. Must be used to get the result of long running commands. */


/*
 *  Append dummy data: Appends dummy job information to NAND flash, used internally to test storage and download routines.
 *
 *  Command structure:
 *      uint8_t     command code
 *      uint32_t    Number of revolutions to fill dummy data for.
 *      uint16_t    CRC
 *
 *  Response structure:
 *      uint8_t     command code
 *      uint8_t     response status
 *      uint16_t    CRC
 */
#define USB_APPENDDUMMY_COMMAND_LENGTH      (USB_PROTOCOL_COMMAND_OVERHEAD + 4)
#define USB_APPENDDUMMY_RESPONSE_LENGTH     (USB_PROTOCOL_RESPONSE_OVERHEAD)


enum class UsbParseResult
{
    OkGoodResponse,             // Parsed successfully with a "good" response status
    OkErrorResponse,            // Parsed successfully with an "error" response status
    OkContinueParsing,          // Basic checks passed, further command specific parsing required (internal value, should never be returned to caller).
    IncorrectLength,            // Packet has incorrect length
    ChecksumMismatch,           // Packet checksum does not match checksum calculated over packet
    CommandCodeUnrecognised,    // Command code in packet is not recognised TODO consider removing if comparing to an expected value
    CommandCodeMismatch,        // Command code in packet is not the one that was expected
    ResponseStatusUnrecognised, // Response status in packet is not recognised
};


void CreateWriteBuffer(std::vector<uint8_t> const& data, ExpandedBuffer_t& writeBuffer);
void ExtractResponse(ExpandedBuffer_t const& readBuffer, std::vector<uint8_t>& response);
void UsbCommand_ExtractDownloadData(ExpandedBuffer_t const& readBuffer, std::vector<uint16_t>& download);
void UsbCommand_DeinterleaveExtractedData(std::vector<uint16_t> const& extracted, std::vector<uint16_t>& flattened);


/******************************************************************************
 *
 * Command and Response packet creation.
 *
 */


/* Note: this structure is not packed, and the types do not necessarily match the protocol, so do not assume it maps directly onto the packet bytes. */
struct UsbResponse_Common_t
{
    UsbCommandCode      commandCode;        // Basic response fields that all fields must contain
    UsbResponseStatus   responseStatus;
    uint16_t            crcReceived;

    // Generic information fields, comparing what was recived to what was expected
    uint16_t            crcCalculated;      // Calculated CRC
};


std::vector<uint8_t> UsbCommand_Echo(std::vector<uint8_t> data);
std::vector<uint8_t> UsbCommand_Identify();

std::vector<uint8_t> UsbCommand_GetPagerowCounts();

/* Note: this structure is not packed, and the types do not necessarily match the protocol, so do not assume it maps directly onto the packet bytes. */
struct UsbResponseGetPagerowCounts_t
{
    UsbResponse_Common_t    basicResponse;      // Fields common to all responses

    // Command specific fields
    uint32_t            pagerowsUsed;       // number of pagerows used in log memory.
    uint32_t            pagerowsInstalled;  // maximum number of pagerows in installed memory
    uint32_t            pagerowsCapacity;   // maximum number of pagerows usable for storage [(maximum installed memory - bad/reserved block capacity) expressed as pagerows]
};

UsbParseResult UsbParseResponse_GetPagerowCounts(std::vector<uint8_t> const& packet, UsbResponseGetPagerowCounts_t& response);


std::vector<uint8_t> UsbCommand_ReadPageRows(uint32_t startPageRow, uint32_t numberPageRows);


/**
 * @brief Creates the command to get the number of NAND flash bad blocks in the tool.
 * @param deviceIndex - Reserved, must be USB_GETBADBLOCKCOUNT_ALLDEVICES for now
 * @return The command packet to send to the tool.
 */
std::vector<uint8_t> UsbCommand_GetBadBlockCount(uint32_t deviceIndex);


/******************************************************************************
 *
 *  Read BBT.
 *
 */

std::vector<uint8_t> UsbCommand_ReadBadBlocksTable(uint32_t bbtIndex);

struct UsbResponse_ReadBadBlocksTable_t
{
    UsbResponse_Common_t    basicResponse;      // Fields common to all responses

    // Command specific fields
    uint16_t    bbtPartCount;
    uint32_t    startIndex;
    uint32_t    nextIndex;
    uint32_t    bbtPart[USB_READBADBLOCKS_NUMBERELEMENTS];
};

UsbParseResult UsbParseResponse_ReadBadBlocksTable(std::vector<uint8_t> const& packet, UsbResponse_ReadBadBlocksTable_t& response);


std::vector<uint8_t> UsbCommand_EraseNandArray();

std::vector<uint8_t> UsbCommand_AppendDummyJob(uint32_t numberRevolutions);


#endif

