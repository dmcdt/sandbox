#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/*
 *  Copyright (c) 2021 Cereus Downhole Technology Ltd, All rights reserved.
 */


#include <QMainWindow>
// Future, FutureWatcher, and Concurrent seem no use for performing a sequence in order in another thread, that we can get progress from and cancel to.

#include "Ft3xxDevice.h"
#include "Ft3xxErrorDescriptions.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void refreshDevicesClicked();
    void selectedDeviceChanged();
    void openUsbClicked();
    void closeUsbClicked();
    void on_browseButton_clicked();
    void abortWriteClicked();
    void abortReadClicked();
    void cyclePortClicked();
    void identifyClicked();
    void writeEchoClicked();
    void readEchoClicked();
    void autoEchoClicked();
    void readCountersClicked();
    void readBbtClicked();
    void markBadClicked();
    void commitBbtClicked();
    void eraseNandClicked();
    void appendDummyJobClicked();
    void on_downloadButton_clicked();
    void autoDownloadButtonClicked();
    void pythonDownloadButtonClicked();

    void autoDownloadFinished();

private:
    Ui::MainWindow          *ui;
    Ft3xxErrorDescriptions  ftStrings;
    Ft3xxDevice             usb;
    QString                 timeFormat;

    void setUiEnabledStates(bool isOpen);
    void setUiForIdle();
    void setUiForRunning();
    void setUiForClosed();
    void setUiForOpened();
    void populateDeviceList();
    void CloseUsb();
    void performErase();
    void performDownload(int startPageRow, int numberPageRows, QString filename);
    void performAutoDownload(QString filename);
    void performPythonDownload();
    void AbortWrite();
    void AbortRead();
    void CyclePort();
    void Identify();
    void WriteEchoCommand();
    void ReadEchoResponse();
    void WriteGetPageRowCountsCommand();
    void ReadGetPageRowCountsResponse();
    void WriteGetBadBlockCountsCommand();
    void ReadGetBadBlockCountsResponse();
    void appendDummyJob(uint32_t numberRevolutions);
    void ReadBadBlockTablePart();
    bool SendReadBadBlockTablePart();
    bool ReceiveReadBadBlockTablePart();
};
#endif // MAINWINDOW_H
