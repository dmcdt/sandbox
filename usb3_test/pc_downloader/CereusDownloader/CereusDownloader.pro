QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
DEFINES *= QT_USE_QSTRINGBUILDER

SOURCES += \
    FT3XXHelper.cpp \
    Ft3xxDevice.cpp \
    Ft3xxDeviceInfo.cpp \
    Ft3xxEnumerator.cpp \
    Ft3xxErrorDescriptions.cpp \
    Identity.cpp \
    TlvcFile.cpp \
    UsbCommands.cpp \
    crc16_modbus.cpp \
    crc32.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    FT3XXHelper.h \
    Ft3xxDevice.h \
    Ft3xxDeviceInfo.h \
    Ft3xxEnumerator.h \
    Ft3xxErrorDescriptions.h \
    Identity.h \
    TlvcFile.h \
    UsbCommands.h \
    UsbFile.h \
    crc16_modbus.h \
    crc32.h \
    mainwindow.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32: LIBS += -L$$PWD/../FTD3XX/v1.3.0.4/x64/ -lFTD3XX

INCLUDEPATH += $$PWD/../FTD3XX/v1.3.0.4
DEPENDPATH += $$PWD/../FTD3XX/v1.3.0.4
