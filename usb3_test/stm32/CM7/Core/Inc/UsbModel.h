#ifndef USBMODEL_H
#define USBMODEL_H


#include <stdbool.h>


#include "MemoryBuffer.h"


void UsbModel_Init(void);
void Usb_SetConnected(bool powerSenseState);
bool Usb_IsConnected(void);
void Usb_SetReadyToReadFromUsb(bool receiveFlagState);
bool Usb_IsReadyToReadFromUsb(void);
void Usb_SetTransmit(bool transmitEnableState);
bool Usb_IsReadyToTransmitToUsb(void);
MemoryBuffer_t *Usb_GetReadBuffer(void);
MemoryBuffer_t *Usb_GetWriteBuffer(void);
void Usb_ProcessReadBuffer(MemoryBuffer_t *buffer);
bool Usb_TransmissionPending(void);
void Usb_TransmissionSent(void);


#endif
