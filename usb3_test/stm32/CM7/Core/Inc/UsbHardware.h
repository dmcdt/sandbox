#ifndef USBHARDWARE_H
#define USBHARDWARE_H


#include <stdbool.h>
#include <stdint.h>

#include "MemoryBuffer.h"


void Usb_ReconfigureFmc(void);
bool Usb_GetPowerSenseState(void);
bool Usb_GetReceiveFlagState(void);
bool Usb_GetTransmitEnableState(void);
void Usb_Read(MemoryBuffer_t *buffer);
bool Usb_Write(MemoryBuffer_t *buffer);


#endif


