#ifndef GPIOHELPERS_H
#define GPIOHELPERS_H

/*
 *  Helper macro declarations to make GPIO access less error prone.
 */

#include "stm32h7xx_hal.h"


/* A couple of convenience macros so you only need to change the pin name once. */
#define GPIO_WRITE(line, state) HAL_GPIO_WritePin(line##_GPIO_Port, line##_Pin, state)
#define GPIO_READ(line) HAL_GPIO_ReadPin(line##_GPIO_Port, line##_Pin)
#define GPIO_RESET(line) line##_GPIO_Port->BSRR = ((line##_Pin) << 16)
#define GPIO_SET(line) line##_GPIO_Port->BSRR = (line##_Pin)


/*  Calculate the value to write into a BSRR register in order to set the value
 *  into multiple bits. Value and mask are assumed to be right justified (aligned
 *  to bit 0), offset is the value to multiply by (i.e. 1 << bit position) to
 *  get the value into the correct final position.
 *
 *  For example, given a set of 3 bits (mask b111) occupying GPIO pins 4:2, and
 *  setting their combined state/value to 5 (b101):
 *      GPIO_CALCULATE_BSRR(5, 0x07, GPIO_PIN_2)
 */
#define GPIO_CALCULATE_BSRR(value, mask, offset) (((~value << 16) | value) * (offset)) & ((mask) * 0x010001 * (offset))


/* Old, unused, longer definition. */
#if 0
#define GPIO_WRITEMULTIPLE(port, value, mask, offset) (port)->BSRR = GPIO_CALCULATE_BSRR(value, mask, offset)
#endif


/*  @param base - the base/stem of the lowest bit in the set to write without the _GPIO_Port or _Pin parts, e.g. RS0.
 *  @param value - the value to write to the bits, right justified (as if the bits were aligned to bit 0).
 *  @param mask - the mask to apply over the value bits, right justified (as if the bits were aligned to bit 0).
 *
 *  For example, when setting the row select bits RS0_GPIO_Port/RS0_Pin, also 1 and 2:
 *      GPIO_WRITEMULTIPLE(RS0, 0, 0x07);
 */
#define GPIO_WRITEMULTIPLE(base, value, mask) (base##_GPIO_Port)->BSRR = GPIO_CALCULATE_BSRR(value, mask, base##_Pin)


#endif
