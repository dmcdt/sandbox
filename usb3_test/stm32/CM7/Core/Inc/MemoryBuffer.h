#ifndef MEMORYBUFFER_H
#define MEMORYBUFFER_H


/*
 *  Generic implementation of a memory buffer, allowing an address and length
 *  to be passed around together.
 */


#include <stdint.h>


typedef struct MemoryBuffer
{
    uint8_t     *address;       // Address of the start of the buffer.
    uint16_t    length;         // Length of the buffer in bytes, NOT how much is contained in it.
    uint16_t    contentSize;    // Length of the data contained within the buffer, in bytes.
} MemoryBuffer_t;


#endif
