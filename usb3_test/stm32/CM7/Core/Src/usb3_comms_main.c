#include "main.h"

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "gpio.h"
#include "fmc.h"

#include "GpioHelpers.h"
#include "FmcNandMemory.h"
#include "UsbModel.h"
#include "UsbHardware.h"


#if 0
static uint16_t gUsbReadBuffer[USBREADBUFFER_SIZE];     /* Buffer for reading the data on the USB bus */
static uint8_t  gUsbCommand[USBCOMMAND_SIZE];           /* Unique bytes from data read from USB */
#endif


void usb3_comms_main(void)
{
//    HAL_StatusTypeDef   halStatus;
//    char const          *str;

#if 0
    /*  Reinitialise some FMC pins so that they are an output under manual
     *  control as the FMC signals do not match the requirements of the FT600.
     */
    GPIO_InitTypeDef GPIO_InitStruct;
    
    /* NCE -> maps onto FT600_OE_N */
    /* Set default NCE high so device not enabled */
    GPIO_WRITE(MCU_FMC_CE_N, GPIO_PIN_SET);
    /* Reinitialise the FMC NCE pin so that it is an output under manual control */
    memset(&GPIO_InitStruct, 0x00, sizeof GPIO_InitStruct);
    GPIO_InitStruct.Pin = MCU_FMC_CE_N_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(MCU_FMC_CE_N_GPIO_Port, &GPIO_InitStruct);

    /* NOE -> maps onto FT600_RD_N */
    /* Set default output high so outputs are not enabled */
    GPIO_WRITE(MCU_FMC_RE_N, GPIO_PIN_SET);
    memset(&GPIO_InitStruct, 0x00, sizeof GPIO_InitStruct);
    GPIO_InitStruct.Pin = MCU_FMC_RE_N_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(MCU_FMC_RE_N_GPIO_Port, &GPIO_InitStruct);
    
    /* NWE -> maps onto FT600_WR_N */
    /* Set default output high so outputs are not enabled */
    GPIO_WRITE(MCU_FMC_WE_N, GPIO_PIN_SET);
//    /* Set default output low to simulate potential error in CPLD */
//    GPIO_WRITE(MCU_FMC_WE_N, GPIO_PIN_RESET);
    memset(&GPIO_InitStruct, 0x00, sizeof GPIO_InitStruct);
    GPIO_InitStruct.Pin = MCU_FMC_WE_N_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(MCU_FMC_WE_N_GPIO_Port, &GPIO_InitStruct);
#endif

    UsbModel_Init();
    Usb_ReconfigureFmc();

#if 0    
    // USB connected
    //GPIO_PinState   ft600RxfN;
    uint32_t        ft600RxfN;
    //GPIO_PinState   ft600TxeN;
    uint32_t        ft600TxeN;
    //uint32_t        *external = (uint16_t *)NAND_DEVICE;

    volatile uint16_t   *readUsb = (volatile uint16_t *)NAND_COMMON_DATA;
    volatile uint16_t   *writeUsb = (volatile uint16_t *)NAND_ATTRIB_DATA;
    
    int             usbReadPos = 0;

    int             usbCommandLength;
    bool            usbCommandOk = false;
    
    while (1)
    {
        ft600RxfN = GPIO_READ(FT600_RXF_N);
        if (ft600RxfN == GPIO_PIN_RESET)
        {
            GPIO_WRITE(MCU_FMC_CE_N, GPIO_PIN_RESET);
            GPIO_WRITE(MCU_FMC_RE_N, GPIO_PIN_RESET);
            do
            {
                /* USB has data for reading */
                /* Read from "NAND" */
                gUsbReadBuffer[usbReadPos++] = *readUsb;
                ft600RxfN = GPIO_READ(FT600_RXF_N);
            } while ((ft600RxfN == GPIO_PIN_RESET) && (usbReadPos < USBREADBUFFER_SIZE));
            GPIO_WRITE(MCU_FMC_RE_N, GPIO_PIN_SET);
            GPIO_WRITE(MCU_FMC_CE_N, GPIO_PIN_SET);
        }
        
        /* Now extract the different bytes */
        if (usbReadPos > 0)
        {
            usbCommandLength = 1;
            uint8_t lastIndex = (gUsbReadBuffer[0] >> 8);
            gUsbCommand[0] = gUsbReadBuffer[0] & 0xFFU;
            bool    usbReceiveError = false;
            
            /*  Check data is formed correctly while extracting as not
             *  stored - must start at zero and increments until the end.
             *  To be fair, missed bytes can be ignored if we have a CRC
             *  as that would not match at the end.
             */
            //if (lastIndex == 0)
            //{
                for (int usbExtractPos = 1; (usbExtractPos < usbReadPos) && !usbReceiveError; usbExtractPos++)
                {
                    uint8_t index = (gUsbReadBuffer[usbExtractPos] >> 8);
                    if (index == (lastIndex + 1))
                    {
                        gUsbCommand[usbCommandLength++] = gUsbReadBuffer[usbExtractPos] & 0xFFU;
                        index = lastIndex;
                    }
//                        else if (index == lastIndex)
//                        {
//                            // Same, ignore
//                        }
//                        else
//                        {
//                            // Bigger gap than + 1, missed a byte
//                            usbReceiveError = true;
//                        }
                }
            
                if ((!usbReceiveError) &&
                    (usbCommandLength > 2))
                {
                    /* Check packet is formed correctly - check the CRC
                     * against the received data minus the potentially
                     * spurious byte.
                     */
                    uint16_t crcReceived = (gUsbCommand[usbCommandLength - 2] << 8) | gUsbCommand[usbCommandLength - 3];
                    uint16_t crcCalculated = crcReceived;//CRC16Modbus_Calculate(gUsbCommand, usbCommandLength - 3);
                    
                    if (crcCalculated == crcReceived)
                    {
                        --usbCommandLength;
                    }
                    else
                    {
                        /* Try again with full packet */
                        crcCalculated = crcReceived;//CRC16Modbus_Update(crcCalculated, gUsbCommand[usbCommandLength - 3]);
                        crcReceived = (gUsbCommand[usbCommandLength - 1] << 8) | gUsbCommand[usbCommandLength - 2]; // Could also shift down existing value by 8, might only save a clock cycle
                    }
                    
                    if (crcCalculated == crcReceived)
                    {
                        usbCommandOk = true;
                    }
                    else
                    {
                        // USB packet corrupt
                        usbReceiveError = true;
                    }
                }
            //}
            //else
            //{
            //    // USB packet overrun / dropped bytes
            //    usbReceiveError = true;
            //}
        }
        else
        {
            // USB nothing received
        }
        
        // Check if space in USB to transmit
        ft600TxeN = GPIO_READ(MCU_TXE_INT_N);
        if (ft600TxeN == GPIO_PIN_RESET)
        {
            if (usbCommandOk)
            {
                usbCommandOk = false;
                
                uint16_t crcCalculated;
                
                gUsbCommand[0] = ~gUsbCommand[0];
                crcCalculated = 0xC0DE;//CRC16Modbus_Calculate(gUsbCommand, usbCommandLength - 2);
                gUsbCommand[usbCommandLength - 2] = crcCalculated & 0xFFU;
                gUsbCommand[usbCommandLength - 1] = (crcCalculated >> 8);
                
                //memset(gUsbCommand, 0x00, usbCommandLength);
                
                // Echo it back to PC, with a couple of changes
                //Peripherals_Select(Peripheral_MCU_to_FTDI);

                // Don't need to assert FMC_NCE as that controls the FTDI OE_N and is not wanted for MCU writes
                GPIO_WRITE(MCU_FMC_WE_N, GPIO_PIN_RESET);
                for (int i = 0; i < usbCommandLength; i++)
                {
                    // Send index and byte in one expression instead of converting and storing elsewhere
                    // TODO - evaluate whether this is fast enough, may need to store it and stream it
                    *writeUsb = (i << 8) | gUsbCommand[i];
                }
                
                // Wait for FMC FIFO to complete writes
                while ((FMC_Bank3_R->SR & FMC_SR_FEMPT_Msk) == 0)
                {
                }
                
                // Now can de-assert FT600 WR_N
                GPIO_WRITE(MCU_FMC_WE_N, GPIO_PIN_SET);

                // Restore peripheral selection
                //Peripherals_Select(Peripheral_FTDI_to_MCU);
                __NOP();
                __NOP();
            }
        }
        else
        {
            // No space to transmit
        }
    }
#endif
        
    while (1)
    {
        Usb_SetReadyToReadFromUsb(Usb_GetReceiveFlagState());
        if (Usb_IsReadyToReadFromUsb())
        {
            MemoryBuffer_t *buffer;
            buffer = Usb_GetReadBuffer();
            Usb_Read(buffer);
            Usb_ProcessReadBuffer(buffer);
            
            GPIO_RESET(LD3);
        }
        
        Usb_SetTransmit(Usb_GetTransmitEnableState());
        if (Usb_IsReadyToTransmitToUsb())
        {
            if (Usb_TransmissionPending())
            {
                if (Usb_Write(Usb_GetWriteBuffer()))
                {
                    Usb_TransmissionSent();
                }
                else
                {
                    GPIO_SET(LD3);
                }
            }
        }
        else
        {
            /* No space to transmit in the FTDI USB bridge */
        }
    }
}
