#include "UsbModel.h"

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "MemoryBuffer.h"
#include "UsbCommands.h"


#define USBCOMMAND_SIZE     (32)
#define USBOVERSAMPLE       (16)
#define USBREADBUFFER_SIZE  (USBCOMMAND_SIZE * USBOVERSAMPLE)


typedef struct UsbContext
{
    bool    connected;
    bool    readAvailable;          // Whether read from USB is available at the hardware level
    bool    transmitAvailable;      // Whether transmit to USB is available at the hardware level
    
    bool    transmissionPending;    // Whether there is data waiting to be transmitted
} UsbContext_t;


void Usb_HandleCommand(void);
void Usb_HandleCommand_Echo(void);
void UsbModel_PrepareStatusResponse(UsbCommandCode_t commandCode, UsbResponseStatus_t responseStatus, MemoryBuffer_t *buffer);
void Usb_PrepareTransmission(MemoryBuffer_t *transmission);


static UsbContext_t     sUsbContext;
static uint16_t         sUsbReadBufferStorage[USBREADBUFFER_SIZE];  /* Buffer for reading the data on the USB bus */
static uint8_t          sUsbCommandBufferStorage[USBCOMMAND_SIZE];  /* Unique bytes from data read from USB */


static MemoryBuffer_t   sUsbReadBuffer;     /* For managing storage and size of data read over USB */
static MemoryBuffer_t   sUsbCommandBuffer;  /* For managing storage and size of command extracted from data read over USB */


void UsbModel_Init(void)
{
    sUsbContext.connected = false;
    sUsbContext.readAvailable = false;
    sUsbContext.transmitAvailable = false;
    sUsbContext.transmissionPending = false;
    
    sUsbReadBuffer.address = (uint8_t *)sUsbReadBufferStorage;
    sUsbReadBuffer.length = sizeof sUsbReadBufferStorage;
    
    sUsbCommandBuffer.address = (uint8_t *)sUsbCommandBufferStorage;
    sUsbCommandBuffer.length = sizeof sUsbCommandBufferStorage;
}


void Usb_SetConnected(bool powerSenseState)
{
    sUsbContext.connected = powerSenseState;
}


bool Usb_IsConnected(void)
{
    return sUsbContext.connected;
}


void Usb_SetReadyToReadFromUsb(bool receiveFlagState)
{
    // RXF is active low
    sUsbContext.readAvailable = !receiveFlagState;
}


bool Usb_IsReadyToReadFromUsb(void)
{
    return sUsbContext.readAvailable;
}


void Usb_SetTransmit(bool transmitEnableState)
{
    // TXE is active low
    sUsbContext.transmitAvailable = !transmitEnableState;
}


bool Usb_IsReadyToTransmitToUsb(void)
{
    return sUsbContext.transmitAvailable;
}


MemoryBuffer_t *Usb_GetReadBuffer(void)
{
    return &sUsbReadBuffer;
}


MemoryBuffer_t *Usb_GetWriteBuffer(void)
{
    return &sUsbReadBuffer;
}


static void Usb_ExtractCommand(MemoryBuffer_t *receivedBuffer, MemoryBuffer_t *extractedBuffer)
{
    uint8_t         lastMessageIndex;
    uint16_t const  *received;
    int             receivedLength;

    received = (uint16_t *)receivedBuffer->address;
    receivedLength = receivedBuffer->contentSize / sizeof *received;
    extractedBuffer->contentSize = 1;
    lastMessageIndex = (received[0] >> 8);
    extractedBuffer->address[0] = received[0] & 0xFFU;

    /*  Check data is formed correctly while extracting as not
     *  stored - must start at zero and increments until the end.
     *  To be fair, missed bytes can be ignored if we have a CRC
     *  as that would not match at the end.
     *
     *  Commented out the index checking rules as they were causing problems if
     *  spurious bytes at the start, or repeated bytes in the middle.
     */
    for (int extractPos = 1; extractPos < receivedLength; extractPos++)
    {
        uint8_t messageIndex = (received[extractPos] >> 8);
        if (messageIndex == (lastMessageIndex + 1))
        {
            extractedBuffer->address[extractedBuffer->contentSize++] = received[extractPos] & 0xFFU;
            lastMessageIndex = messageIndex;
        }
    }
}


void Usb_PrepareTransmission(MemoryBuffer_t *transmission)
{
    MemoryBuffer_t  *writeBuffer = Usb_GetWriteBuffer();
    uint16_t        *writeData = (uint16_t *)writeBuffer->address;
    
    for (uint16_t contentIndex = 0; contentIndex < transmission->contentSize; ++contentIndex)
    {
        *writeData++ = (contentIndex << 8) | transmission->address[contentIndex];
    }
    
    writeBuffer->contentSize = transmission->contentSize * sizeof(uint16_t);
}


void Usb_ProcessReadBuffer(MemoryBuffer_t *buffer)
{
    /* Now extract the different bytes */
    if (buffer->contentSize > 0)
    {
        Usb_ExtractCommand(buffer, &sUsbCommandBuffer);
        
        if (sUsbCommandBuffer.contentSize > 2)
        {
            /* Check packet is formed correctly - check the CRC
             * against the received data minus the potentially
             * spurious byte.
             */
            uint16_t crcReceived = 0;//(sUsbCommandBuffer.address[sUsbCommandBuffer.contentSize - 2] << 8) | sUsbCommandBuffer.address[sUsbCommandBuffer.contentSize - 3];
            uint16_t crcCalculated = 1;//CRC16Modbus_Calculate(sUsbCommandBuffer.address, sUsbCommandBuffer.contentSize - 3);
            
            if (crcCalculated == crcReceived)
            {
                --sUsbCommandBuffer.contentSize;
            }
            else
            {
                /* Try again with full packet */
                crcReceived = 0;//(sUsbCommandBuffer.address[sUsbCommandBuffer.contentSize - 1] << 8) | sUsbCommandBuffer.address[sUsbCommandBuffer.contentSize - 2]; // Could also shift down existing value by 8, might only save a clock cycle
                crcCalculated = 0;//CRC16Modbus_Update(crcCalculated, sUsbCommandBuffer.address[sUsbCommandBuffer.contentSize - 3]);
            }
            
            if (crcCalculated == crcReceived)
            {
                Usb_HandleCommand();
            }
            else
            {
                // USB packet corrupt
                //usbReceiveError = true;
            }
        }
    }
}


void Usb_HandleCommand(void)
{
    switch (sUsbCommandBuffer.address[0])
    {
        case UsbCommandCode_Echo:
            Usb_HandleCommand_Echo();
            break;
            
        default:
            // Unrecognised command, should perhaps increase a counter or something
            break;
    }
}


void Usb_HandleCommand_Echo(void)
{
    UsbResponseStatus_t responseStatus;
    uint16_t            crcCalculated;
    
    /* Command code will still be valid in element [0]. */
    
    if (sUsbCommandBuffer.contentSize > USB_PROTOCOL_COMMAND_OVERHEAD)
    {
        responseStatus = UsbResponseStatus_Ok;

        /* Move the echo data to leave room for the response status and invert
         * the first byte.
         */
        uint16_t    insertSize;
        uint16_t    moveSize;
        
        insertSize = 1;
        moveSize = sUsbCommandBuffer.contentSize - USB_PROTOCOL_COMMAND_OVERHEAD;
        if ((moveSize + insertSize) >= (sUsbCommandBuffer.length - USB_PROTOCOL_COMMAND_OVERHEAD))
        {
            moveSize = sUsbCommandBuffer.length - USB_PROTOCOL_COMMAND_OVERHEAD - insertSize;
        }
        
        memmove(&sUsbCommandBuffer.address[1 + insertSize], &sUsbCommandBuffer.address[1], moveSize);
        sUsbCommandBuffer.contentSize = moveSize + USB_PROTOCOL_COMMAND_OVERHEAD + insertSize;
        
        sUsbCommandBuffer.address[2] = ~sUsbCommandBuffer.address[2];
    }
    else
    {
        responseStatus = UsbResponseStatus_ErrorNoData;
    }
    
    /* Populate the response status */
    sUsbCommandBuffer.address[1] = responseStatus;
    
    /* Calculate new CRC and store at end of data in buffer */
    crcCalculated = 0xC0D3;//CRC16Modbus_Calculate(sUsbCommandBuffer.address, sUsbCommandBuffer.contentSize - USB_PROTOCOL_CRC_LENGTH);
    sUsbCommandBuffer.address[sUsbCommandBuffer.contentSize - USB_PROTOCOL_CRC_LENGTH] = crcCalculated & 0xFFU;
    sUsbCommandBuffer.address[sUsbCommandBuffer.contentSize - USB_PROTOCOL_CRC_LENGTH + 1] = (crcCalculated >> 8);
    
    Usb_PrepareTransmission(&sUsbCommandBuffer);
    
    sUsbContext.transmissionPending = true;
}


bool Usb_TransmissionPending(void)
{
    return sUsbContext.transmissionPending;
}


void Usb_TransmissionSent(void)
{
    sUsbContext.transmissionPending = false;
}
