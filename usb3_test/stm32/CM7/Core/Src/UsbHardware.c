#include "UsbHardware.h"

#include <string.h>
#include <stdbool.h>

#include "main.h"
#include "fmc.h"
#include "gpio.h"
#include "GpioHelpers.h"
#include "FmcNandMemory.h"


void Usb_ReconfigureFmc(void)
{
    /* Disable the FMC while reconfiguring registers */
    __FMC_DISABLE();
    __FMC_NAND_DISABLE(hnand1.Instance, hnand1.Init.NandBank);
    
    FMC_NAND_PCC_TimingTypeDef  ComSpaceTiming = {0};
    FMC_NAND_PCC_TimingTypeDef  AttSpaceTiming = {0};

    /*  Reduce ALE/CLE to RE times to minimum for fastest reads, even though
     *  there are no ALE/CLE to RE transitions.
     */
    hnand1.Init.TCLRSetupTime = 0;
    hnand1.Init.TARSetupTime = 0;
    FMC_NAND_Init(hnand1.Instance, &hnand1.Init);
    
    /* ComSpaceTiming - used for reading at the fastest possible rate. */
    ComSpaceTiming.SetupTime = 0;
    ComSpaceTiming.WaitSetupTime = 1;
    ComSpaceTiming.HoldSetupTime = 1;
    ComSpaceTiming.HiZSetupTime = 0;
    FMC_NAND_CommonSpace_Timing_Init(hnand1.Instance, &ComSpaceTiming, hnand1.Init.NandBank);

    /* AttSpaceTiming - used to write a long block of the same value. */
    AttSpaceTiming.SetupTime = 0;
    AttSpaceTiming.WaitSetupTime = 34;
    AttSpaceTiming.HoldSetupTime = 1;
    AttSpaceTiming.HiZSetupTime = 0;
    FMC_NAND_AttributeSpace_Timing_Init(hnand1.Instance, &AttSpaceTiming, hnand1.Init.NandBank);
    
    /* Re-enable NAND bank and FMC now it has been reconfigured. */
    __FMC_NAND_ENABLE(hnand1.Instance);
    __FMC_ENABLE();
    
    /*  Reinitialise some FMC pins so that they are an output under manual
     *  control as the FMC signals do not match the requirements of the FT600.
     */
    GPIO_InitTypeDef GPIO_InitStruct;

    /*  NCE -> maps onto FT600_OE_N.
     *  Set default NCE high so device not enabled.
     */
    GPIO_WRITE(MCU_FMC_CE_N, GPIO_PIN_SET);
    memset(&GPIO_InitStruct, 0x00, sizeof GPIO_InitStruct);
    GPIO_InitStruct.Pin = MCU_FMC_CE_N_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(MCU_FMC_CE_N_GPIO_Port, &GPIO_InitStruct);

    /*  NOE -> maps onto FT600_RD_N.
     *  Set default output high so outputs are not enabled.
     */
    GPIO_WRITE(MCU_FMC_RE_N, GPIO_PIN_SET);
    memset(&GPIO_InitStruct, 0x00, sizeof GPIO_InitStruct);
    GPIO_InitStruct.Pin = MCU_FMC_RE_N_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(MCU_FMC_RE_N_GPIO_Port, &GPIO_InitStruct);
    
    /*  NWE -> maps onto FT600_WR_N.
     *  Set default output high so outputs are not enabled
     */
    GPIO_WRITE(MCU_FMC_WE_N, GPIO_PIN_SET);
    memset(&GPIO_InitStruct, 0x00, sizeof GPIO_InitStruct);
    GPIO_InitStruct.Pin = MCU_FMC_WE_N_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(MCU_FMC_WE_N_GPIO_Port, &GPIO_InitStruct);
}


bool Usb_GetReceiveFlagState(void)
{
    return GPIO_READ(FT600_RXF_N);
}


bool Usb_GetTransmitEnableState(void)
{
    return GPIO_READ(MCU_TXE_INT_N);
}


void Usb_Ft601_OutputEnable(void)
{
    GPIO_RESET(MCU_FMC_CE_N);
}

void Usb_Ft601_OutputDisable(void)
{
    GPIO_SET(MCU_FMC_CE_N);
}

void Usb_Ft601_ReadEnable(void)
{
    GPIO_RESET(MCU_FMC_RE_N);
}

void Usb_Ft601_ReadDisable(void)
{
    GPIO_SET(MCU_FMC_RE_N);
}


void Usb_Ft601_WriteEnable(void)
{
    GPIO_RESET(MCU_FMC_WE_N);
}


void Usb_Ft601_WriteDisable(void)
{
    GPIO_SET(MCU_FMC_WE_N);
}


void Usb_Read(MemoryBuffer_t *buffer)
{
    uint32_t            numberReads;
    __IO uint16_t const *readUsb;
    uint16_t            *destination;
    uint32_t            maximumReads;
    uint32_t            ft600RxfN;
    
    destination = (uint16_t *)buffer->address;
    maximumReads = buffer->length / sizeof(*destination);
    readUsb = (__IO uint16_t *)NAND_COMMON_DATA;
    numberReads = maximumReads;
    
    Usb_Ft601_OutputEnable();
    Usb_Ft601_ReadEnable();
    
    do
    {
        *destination++ = *readUsb;
        ft600RxfN = FT600_RXF_N_GPIO_Port->IDR & FT600_RXF_N_Pin;
    } while ((ft600RxfN == 0) && (--maximumReads));
    
    /* Wait for FT601 FIFO to be emptied. */
    while (ft600RxfN == 0)
    {
        ft600RxfN = FT600_RXF_N_GPIO_Port->IDR & FT600_RXF_N_Pin;
    }
    
    Usb_Ft601_ReadDisable();
    Usb_Ft601_OutputDisable();
    
    buffer->contentSize = (numberReads - maximumReads) * sizeof(*destination);
}


bool Usb_Write(MemoryBuffer_t *buffer)
{
    bool            writeComplete;
    uint32_t        numberWrites;
    __IO uint16_t   *writeUsb;
    uint16_t const  *source;
    uint32_t        ft600TxeN;
    
    source = (uint16_t *)buffer->address;
    writeUsb =  (__IO uint16_t *)NAND_ATTRIB_DATA;
    numberWrites = buffer->contentSize / sizeof(uint16_t);
    writeComplete = true; // Assume the best
    
    /* Don't need to assert FMC_NCE as that controls the FTDI OE_N and is not required for MCU writes */
    Usb_Ft601_WriteEnable();
    
    /* Only write as much as we need to or until the FTDI deasserts TXE_N */
    do
    {
        *writeUsb = *source++;
        ft600TxeN = MCU_TXE_INT_N_GPIO_Port->IDR & MCU_TXE_INT_N_Pin;
    } while ((--numberWrites) && (ft600TxeN == 0));
    
    // TODO - abort FIFO if TXE_N de-asserted, and get WR_N high ASAP? No way to abort FMC FIFO...
    
    /* Wait for FMC FIFO to complete writes */
    while ((FMC_Bank3_R->SR & FMC_SR_FEMPT_Msk) == 0)
    {
    }
    
    /* Now can de-assert FT600 WR_N */
    Usb_Ft601_WriteDisable();
    
    if (numberWrites > 0)
    {
        /* Write aborted by FT600 */
        writeComplete = false;
    }
    
    return writeComplete;
}

