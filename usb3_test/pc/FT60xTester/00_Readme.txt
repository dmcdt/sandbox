This code is based on the FT600DataStreamerDemoApp v1.3.0.2.

Project and code has been converted to VS2019, include CRT behaviour.
Warnings cleaned up.
Using "official" release of FTD3XX library rather than the one supplied with the demo (which was different for the same version number!).
Fixed mixed runtime library conflicts.

Added functionality to check the data as it is being streamed in.
