/*
 *  Program to test communications from PC to MCU over USB3.
 */


// This needs to be defined to match the FTDI on the target board, either zero for the Mainboard (FT600) or 1 for the UMFT601 eval (FT601)
#define FT600_DEVICE    0
#define FT601_DEVICE    1
#define FTTYPE          FT600_DEVICE

#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <fstream>

#include <algorithm>

#include <FTD3xx.h>

// Not including a main here
//#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include "crc16_modbus.h"


typedef std::map<FT_STATUS, std::string> StatusDescriptionLookup_t;
StatusDescriptionLookup_t gFtdiErrorDescriptions;


void RegisterErrorDescriptions(void)
{
    gFtdiErrorDescriptions.insert(std::make_pair(FT_OK, "No error"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_INVALID_HANDLE, "Invalid handle"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_DEVICE_NOT_FOUND, "Device not found"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_DEVICE_NOT_OPENED, "Device not opened"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_IO_ERROR, "I/O error"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_INSUFFICIENT_RESOURCES, "Insufficient resources"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_INVALID_PARAMETER, "Invalid parameter"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_INVALID_BAUD_RATE, "Invalid baud rate"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_DEVICE_NOT_OPENED_FOR_ERASE, "Device not opened for erase"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_DEVICE_NOT_OPENED_FOR_WRITE, "Device not opened for write"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_FAILED_TO_WRITE_DEVICE, "Failed to write to device"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_EEPROM_READ_FAILED, "EEPROM read failed"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_EEPROM_WRITE_FAILED, "EEPROM write failed"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_EEPROM_ERASE_FAILED, "EEPROM erase failed"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_EEPROM_NOT_PRESENT, "EEPROM not present"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_EEPROM_NOT_PROGRAMMED, "EEPROM not programmed"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_INVALID_ARGS, "Invalid args"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_NOT_SUPPORTED, "Not supported"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_NO_MORE_ITEMS, "No more items"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_TIMEOUT, "Timeout"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_OPERATION_ABORTED, "Operation aborted"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_RESERVED_PIPE, "Reserved pipe"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_INVALID_CONTROL_REQUEST_DIRECTION, "Invalid control request direction"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_INVALID_CONTROL_REQUEST_TYPE, "Invalid control request type"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_IO_PENDING, "I/O pending"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_IO_INCOMPLETE, "I/O complete"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_HANDLE_EOF, "Handle EOF"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_BUSY, "Busy"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_NO_SYSTEM_RESOURCES, "No system resources"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_DEVICE_LIST_NOT_READY, "Device list not ready"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_DEVICE_NOT_CONNECTED, "Device not connected"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_INCORRECT_DEVICE_PATH, "Incorrect device path"));
    gFtdiErrorDescriptions.insert(std::make_pair(FT_OTHER_ERROR, "Other error"));
}

void ShowError(FT_STATUS code, std::string const& msg)
{
    std::string description;
    StatusDescriptionLookup_t::const_iterator lookup = gFtdiErrorDescriptions.find(code);
    if (lookup != gFtdiErrorDescriptions.cend())
    {
        description = lookup->second;
    }
    else
    {
        description = "Unknown error";
    }
    std::cerr << "Error " << code << " during " << msg.c_str() << ": " << description << std::endl;
}


size_t my_strlen_s(const char* str, size_t strsz)
{
    size_t  len = 0;

    if (str)
    {
        while ((*str++ != '\0') && (strsz--))
        {
            ++len;
        }
    }

    return len;
}


std::string GetStringFromArray(char const* arr, size_t maximumLength)
{
    size_t actualLength = my_strlen_s(arr, maximumLength);
    return std::string(arr, actualLength);
}


#define FTD3XXSERIALNUMBER_SIZE (sizeof(((FT_DEVICE_LIST_INFO_NODE*)0)->SerialNumber) / sizeof(((FT_DEVICE_LIST_INFO_NODE*)0)->SerialNumber[0]))
#define FTD3XXDESCRIPTION_SIZE  (sizeof(((FT_DEVICE_LIST_INFO_NODE*)0)->Description) / sizeof(((FT_DEVICE_LIST_INFO_NODE*)0)->Description[0]))

constexpr size_t OVERSAMPLE_RATIO = 32;

#if FTTYPE == FT600_DEVICE
// For FT600 on the High Speed board, since it is a 2 byte parallel FIFO bus
typedef std::vector<uint16_t> ExpandedBuffer_t;
#elif FTTYPE == FT601_DEVICE
// For FT601 on the FTDI eval board, since it is a 4 byte parallel FIFO bus
typedef std::vector<uint32_t> ExpandedBuffer_t;
#else
#pragma error('Unknown FTDI device type')
#endif


void CreateWriteBuffer(std::vector<uint8_t> const& data, ExpandedBuffer_t& writeBuffer)
{
    writeBuffer.resize(data.size() * OVERSAMPLE_RATIO);

    for (size_t index = 0; index < data.size(); index++)
    {
        for (size_t repeatIndex = 0; repeatIndex < OVERSAMPLE_RATIO; repeatIndex++)
        {
            writeBuffer[index * OVERSAMPLE_RATIO + repeatIndex] = (index << 8) | data[index];
            //writeBuffer[index * OVERSAMPLE_RATIO + repeatIndex] = (index & 1 ? 0xFF00 : 0x0000) | data[index];
        }
    }
}


void ExtractResponse(ExpandedBuffer_t const& readBuffer, std::vector<uint8_t>& response)
{
    response.clear();
    if (!readBuffer.empty())
    {
        response.reserve(readBuffer.size());
        uint8_t lastMessageIndex;

        //lastMessageIndex = readBuffer[0] >> 8;
        //response.push_back(readBuffer[0] & 0xFFU);

        //for (size_t readIndex = 1; readIndex < readBuffer.size(); readIndex++)
        //{
        //    uint8_t messageIndex = readBuffer[readIndex] >> 8;
        //    if (messageIndex == lastMessageIndex)
        //    {
        //        // Same as last byte, ignore
        //    }
        //    else if (messageIndex == (lastMessageIndex + 1))
        //    {
        //        // Next message byte
        //        lastMessageIndex = messageIndex;
        //        response.push_back(readBuffer[readIndex] & 0xFFU);
        //    }
        //    else
        //    {
        //        // Error - missed byte in message. Probably shouldn't happen in the PC side as it'll be fast enough.
        //        // CRC will end up mismatch anyway, so don't need to do anything here
        //    }
        //}

        // PC processing needs to be a bit more rigid since it can have more spurious data.
        // Only start when the index byte is 0. Only accept bytes when move onto the next
        // expected index byte. Take a majority vote on the data bytes for a sequence of
        // the same index bytes.

        //  While data to process
        //      Find expected index
        //      Find last expected index
        //      Find "mode" of data values -OR- just the midpoint as the data should be stable
        //      Move onto next message index

        uint8_t expectedIndex = 0;
        for (ExpandedBuffer_t::const_iterator it = readBuffer.cbegin(); it != readBuffer.cend(); /* Incremented in loop */)
        {
            ExpandedBuffer_t::const_iterator    expectedIt;
            expectedIt = std::find_if(it, readBuffer.cend(), [expectedIndex](ExpandedBuffer_t::value_type value) { return (value >> 8) == expectedIndex; } );
            if (expectedIt != readBuffer.cend())
            {
                ExpandedBuffer_t::const_iterator    nextIndexIt;
                nextIndexIt = std::find_if_not(expectedIt + 1, readBuffer.cend(), [expectedIndex](ExpandedBuffer_t::value_type value) { return (value >> 8) == expectedIndex; });

                expectedIt += std::distance(expectedIt, nextIndexIt) / 2;
                response.push_back(*expectedIt & 0xFFU);

                ++expectedIndex;
                it = nextIndexIt;
            }
            else
            {
                it = readBuffer.cend();
            }
        }

        //for (size_t readBufferIndex = 0; readBufferIndex < readBuffer.size(); /* Increased within loop */)
        //{

        //}
    }
}


void SaveBuffer(const ExpandedBuffer_t& buffer)
{
    std::string     filename;

    std::cout << "Enter filename to save to: ";

    std::cin.ignore();
    std::getline(std::cin, filename);
    if (!filename.empty())
    {
        std::ofstream   fs(filename);
        if (fs.is_open())
        {
            for (ExpandedBuffer_t::const_iterator it = buffer.begin(); it != buffer.end(); ++it)
            {
                fs << "0x" << std::setw(4) << std::setfill('0') << std::hex << static_cast<unsigned int>(*it) << std::endl;
            }
            fs.close();
        }
    }
}


void Download(ExpandedBuffer_t readBuffer)
{
}


#if defined(CATCH_CONFIG_MAIN)
/*

Received 228 bytes, 114 elements: ffff ffff 4023 0021 0021 0021 0021 0021 0021 0021 0021 0021 0021 0021 0021 0021 0021 0021 fff3 01ad 01ad 01ad 01ad 01ad 01ad 01ad 01ad 01ad 01ad 01ad 01ad 01ad 01ad 01ad 01ad ffff 02be 02be 02be 02be 02be 02be 02be 02be 02be 02be 02be 02be 02be 02be 02be 02be 03ef 03ef 03ef 03ef 03ef 03ef 03ef 03ef 03ef 03ef 03ef 03ef 03ef 03ef 03ef 03ef ffff 04ab 04ab 04ab 04ab 04ab 04ab 04ab 04ab 04ab 04ab 04ab 04ab 04ab 04ab 04ab 04ab ffff 05d5 05d5 05d5 05d5 05d5 05d5 05d5 05d5 05d5 05d5 05d5 05d5 05d5 05d5 05d5 05d5 fff7 fdf7 fdf7 ffff fff7 fff7 fff7 fff7 fff7 fff7 fff7 fff7 Packet too short for valid response 1

*/
TEST_CASE("Real response extracted", "[ExtractResponse]")
{
    uint16_t    testData[] = {
                                0xffff, 0xffff, 0x4023, 0x0021, 0x0021, 0x0021, 0x0021, 0x0021,
                                0x0021, 0x0021, 0x0021, 0x0021, 0x0021, 0x0021, 0x0021, 0x0021,
                                0x0021, 0x0021, 0xfff3, 0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad,
                                0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad,
                                0x01ad, 0x01ad, 0x01ad, 0xffff, 0x02be, 0x02be, 0x02be, 0x02be,
                                0x02be, 0x02be, 0x02be, 0x02be, 0x02be, 0x02be, 0x02be, 0x02be,
                                0x02be, 0x02be, 0x02be, 0x02be, 0x03ef, 0x03ef, 0x03ef, 0x03ef,
                                0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef,
                                0x03ef, 0x03ef, 0x03ef, 0x03ef, 0xffff, 0x04ab, 0x04ab, 0x04ab,
                                0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab,
                                0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab, 0xffff, 0x05d5, 0x05d5,
                                0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5,
                                0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0xfff7, 0xfdf7,
                                0xfdf7, 0xffff, 0xfff7, 0xfff7, 0xfff7, 0xfff7, 0xfff7, 0xfff7,
                                0xfff7, 0xfff7
    };

    uint8_t     expected[] = { 0x21, 0xAD, 0xBE, 0xEF, 0xAB, 0xD5 };

    ExpandedBuffer_t        received(std::begin(testData), std::end(testData));
    std::vector<uint8_t>    response;

    ExtractResponse(received, response);

    REQUIRE(memcmp(expected, &response[0], sizeof expected) == 0);
}


TEST_CASE("Perfect response extracted", "[ExtractResponse]")
{
    uint16_t    testData[] = {
                                0x0021, 0x0021, 0x0021, 0x0021, 0x0021, 0x0021, 0x0021, 0x0021,
                                0x0021, 0x0021, 0x0021, 0x0021, 0x0021, 0x0021, 0x0021, 0x0021,
                                0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad,
                                0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad,
                                0x02be, 0x02be, 0x02be, 0x02be, 0x02be, 0x02be, 0x02be, 0x02be,
                                0x02be, 0x02be, 0x02be, 0x02be, 0x02be, 0x02be, 0x02be, 0x02be,
                                0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef,
                                0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef,
                                0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab,
                                0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab,
                                0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5,
                                0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5
    };

    uint8_t     expected[] = { 0x21, 0xAD, 0xBE, 0xEF, 0xAB, 0xD5 };

    ExpandedBuffer_t        received(std::begin(testData), std::end(testData));
    std::vector<uint8_t>    response;

    ExtractResponse(received, response);

    REQUIRE(memcmp(expected, &response[0], sizeof expected) == 0);
}


TEST_CASE("Short response extracted", "[ExtractResponse]")
{
    uint16_t    testData[] = {
                                0x0021,
                                0x01ad,
                                0x02be,
                                0x03ef,
                                0x04ab,
                                0x05d5
    };

    uint8_t     expected[] = { 0x21, 0xAD, 0xBE, 0xEF, 0xAB, 0xD5 };

    ExpandedBuffer_t        received(std::begin(testData), std::end(testData));
    std::vector<uint8_t>    response;

    ExtractResponse(received, response);

    REQUIRE(memcmp(expected, &response[0], sizeof expected) == 0);
}


TEST_CASE("Corrupt data response extracted", "[ExtractResponse]")
{
    // Test data modified to show case of slow to change data bits, so that index is correct
    // but data values are different.
    uint16_t    testData[] = {
                                0xffff, 0xffff, 0x4023, 0x0023, 0x0021, 0x0021, 0x0021, 0x0021,
                                0x0021, 0x0021, 0x0021, 0x0021, 0x0021, 0x0021, 0x0021, 0x0021,
                                0x0021, 0x00F3, 0xfff3, 0x01F3, 0x01ad, 0x01ad, 0x01ad, 0x01ad,
                                0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad,
                                0x01ad, 0x01ad, 0x01FF, 0xffff, 0x02FF, 0x02be, 0x02be, 0x02be,
                                0x02be, 0x02be, 0x02be, 0x02be, 0x02be, 0x02be, 0x02be, 0x02be,
                                0x02be, 0x02be, 0x02be, 0x02EF, 0x03ef, 0x03ef, 0x03ef, 0x03ef,
                                0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef,
                                0x03ef, 0x03ef, 0x03ef, 0x03FF, 0xffff, 0x04FF, 0x04ab, 0x04ab,
                                0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab,
                                0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04FF, 0xffff, 0x05FF, 0x05d5,
                                0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5,
                                0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05F7, 0xfff7, 0xfdf7,
                                0xfdf7, 0xffff, 0xfff7, 0xfff7, 0xfff7, 0xfff7, 0xfff7, 0xfff7,
                                0xfff7, 0xfff7
    };

    uint8_t     expected[] = { 0x21, 0xAD, 0xBE, 0xEF, 0xAB, 0xD5 };

    ExpandedBuffer_t        received(std::begin(testData), std::end(testData));
    std::vector<uint8_t>    response;

    ExtractResponse(received, response);

    REQUIRE(memcmp(expected, &response[0], sizeof expected) == 0);
}


TEST_CASE("Corrupt index response extracted", "[ExtractResponse]")
{
    uint16_t    testData[] = {
                                0xffff, 0xffff, 0x4023, 0x0021, 0x4021, 0x0021, 0x0021, 0x0021,
                                0x0021, 0x0021, 0x0021, 0x0021, 0x0021, 0x0021, 0x0021, 0x0021,
                                0x0021, 0x0021, 0xfff3, 0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad,
                                0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad, 0x01ad,
                                0x01ad, 0x01ad, 0x01ad, 0xffff, 0x02be, 0x02be, 0x02be, 0x02be,
                                0x02be, 0x02be, 0x02be, 0x02be, 0x02be, 0x02be, 0x02be, 0x02be,
                                0x02be, 0x02be, 0x02be, 0x02be, 0x03ef, 0x03ef, 0x03ef, 0x03ef,
                                0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef, 0x03ef,
                                0x03ef, 0x03ef, 0x03ef, 0x03ef, 0xffff, 0x04ab, 0x04ab, 0x04ab,
                                0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab,
                                0x04ab, 0x04ab, 0x04ab, 0x04ab, 0x04ab, 0xffff, 0x05d5, 0x05d5,
                                0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5,
                                0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0x05d5, 0xfff7, 0xfdf7,
                                0xfdf7, 0xffff, 0xfff7, 0xfff7, 0xfff7, 0xfff7, 0xfff7, 0xfff7,
                                0xfff7, 0xfff7
    };

    uint8_t     expected[] = { 0x21, 0xAD, 0xBE, 0xEF, 0xAB, 0xD5 };

    ExpandedBuffer_t        received(std::begin(testData), std::end(testData));
    std::vector<uint8_t>    response;

    ExtractResponse(received, response);

    REQUIRE(memcmp(expected, &response[0], sizeof expected) == 0);
}

// This fails but it should pass.

/*
Received 476 bytes, 119 elements: ffcf fe0d ff8f 0001 0001 0001 0001 0001 0001 0001 0001 0001 0001 0001 0001 0011 fd3f 0100 0100 0100 0100 0100 0100 0100 0100 0100 0100 0100 0010 0a21 0221 0221 0221 0221 0221 0221 0221 0221 0221 0221 0221 1091 03ad 03ad 03ad 03ad 03ad 03ad 03ad 03ad 03ad 03ad 03ad 03ad 3ebd 04be 04be 04be 04be 04be 04be 04be 04be 04be 04be 04be 04be 3bbe 05ef 05ef 05ef 05ef 05ef 05ef 05ef 05ef 05ef 05ef 05ef 05ef 3f7f 06d3 06d3 06d3 06d3 06d3 06d3 06d3 06d3 06d3 06d3 06d3 06d3 3bf3 07c0 07c0 07c0 07c0 07c0 07c0 07c0 07c0 07c0 07c0 07c0 07c0 3870 ffff ffff f3ff f7ff ffff ffff ffff ffff ffff ffff ffff ffff
*/

#else

int main()
{
    FT_STATUS   ftStatus;
    DWORD       numberDevices;
    DWORD       openIndex = MAXDWORD;

    RegisterErrorDescriptions();

    do
    {
        //FT_DEVICE_LIST_INFO_NODE    node;
        //size_t descriptionLength;

        //memset(&node.Description[0], 'A', 32);
        //descriptionLength = my_strlen_s(&node.Description[0], FTD3XXDESCRIPTION_SIZE);

        //memset(&node.Description[0], 0, 32);
        //memset(&node.Description[0], 'B', 8);
        //descriptionLength = my_strlen_s(&node.Description[0], FTD3XXDESCRIPTION_SIZE);

        ftStatus = FT_CreateDeviceInfoList(&numberDevices);
        if (ftStatus == FT_OK)
        {
            std::cout << "There are " << numberDevices << " devices attached." << std::endl;
            if (numberDevices > 0)
            {
                std::vector<FT_DEVICE_LIST_INFO_NODE>   deviceList(numberDevices);
                ftStatus = FT_GetDeviceInfoList(&deviceList[0], &numberDevices);
                if (ftStatus == FT_OK)
                {
                    for (size_t deviceIndex = 0; deviceIndex < numberDevices; deviceIndex++)
                    {
                        std::string serialNumber = GetStringFromArray(deviceList[deviceIndex].SerialNumber, FTD3XXSERIALNUMBER_SIZE);
                        std::string description = GetStringFromArray(deviceList[deviceIndex].Description, FTD3XXDESCRIPTION_SIZE);
                        std::cout << deviceIndex << ". S/N: " << serialNumber << " - \"" << description << "\"" << std::endl;
                    }

                    std::cout << "Enter device index to open (0 ... " << (numberDevices - 1) << ") or -1 to exit: ";
                    std::cin >> openIndex;
                }
                else
                {
                    ShowError(ftStatus, "get device list");
                }
            }
        }
        else
        {
            ShowError(ftStatus, "create device list");
        }

        if (openIndex != MAXDWORD)
        {
            FT_HANDLE   handle;
            ftStatus = FT_Create(reinterpret_cast<PVOID>(openIndex), FT_OPEN_BY_INDEX, &handle);
            if (ftStatus == FT_OK)
            {
                int operation;

                ULONG ulBytesToWrite;
                ULONG ulBytesWritten;
                ULONG ulBytesRead;
                ULONG ulElementsRead;
                std::vector<uint8_t> command;
                uint16_t crc;
                ExpandedBuffer_t writeBuffer;

                ExpandedBuffer_t readBuffer(128);
                std::vector<uint8_t> response;


                // TODO - do stuff
                //ftStatus = FT_AbortPipe(handle, 0x02);
                //if (ftStatus != FT_OK)
                //{
                //    ShowError(ftStatus, "Aborting pipe");
                //}

                //ftStatus = FT_ResetDevicePort(handle);
                //if (ftStatus != FT_OK)
                //{
                //    ShowError(ftStatus, "resetting device");
                //}

                //ftStatus = FT_ClearStreamPipe(handle, TRUE, TRUE, 0x02);
                //if (ftStatus != FT_OK)
                //{
                //    ShowError(ftStatus, "resetting device");
                //}

                do
                {
                    std::cout << "1. Write" << std::endl
                        << "2. Read" << std::endl
                        << "3. Echo" << std::endl
                        << "4. Download" << std::endl
                        << "5. Abort write" << std::endl
                        << "6. Abort read" << std::endl
                        << "7. Save raw readbuffer to file" << std::endl
                        << "0. Close" << std::endl;
                    std::cin >> operation;

                    switch (operation)
                    {
                    case 1:
                        command.resize(7);
                        command[0] = 0x01;
                        command[1] = 0xDE;
                        command[2] = 0xAD;
                        command[3] = 0xBE;
                        command[4] = 0xEF;
                        crc = CRC16Modbus_Calculate(&command[0], command.size() - 2);
                        command[5] = crc & 0xFF;
                        command[6] = crc >> 8;

                        CreateWriteBuffer(command, writeBuffer);

                        ulBytesToWrite = writeBuffer.size() * sizeof(writeBuffer[0]) / sizeof(UCHAR);

                        //memset(&writeBuffer[0], 0xFF, ulBytesToWrite);

                        ftStatus = FT_WritePipe(handle, 0x02, reinterpret_cast<PUCHAR>(&writeBuffer[0]), ulBytesToWrite, &ulBytesWritten, NULL);
                        if (ftStatus != FT_OK)
                        {
                            ShowError(ftStatus, "writing");
                        }

                        break;

                    case 2:
                        readBuffer.resize(1024);
                        ftStatus = FT_ReadPipe(handle, 0x82, reinterpret_cast<PUCHAR>(&readBuffer[0]), readBuffer.size() * sizeof(readBuffer[0]) / sizeof(UCHAR), &ulBytesRead, NULL);

                        ulElementsRead = ulBytesRead / sizeof(readBuffer[0]);
                        std::cout << "Received " << ulBytesRead << " bytes, " << ulElementsRead << " elements: ";
                        for (DWORD i = 0; i < ulElementsRead; i++)
                        {
                            std::cout << std::hex << std::setw(4) << std::setfill('0') << (readBuffer[i] & 0xFFFFU) << ' ';
                        }
                        std::cout << std::endl << std::endl;

                        if (ftStatus == FT_OK)
                        {
                            ExtractResponse(readBuffer, response);
                            if (response.size() > 2)
                            {
                                uint16_t    crcCalculated = CRC16Modbus_Calculate(&response[0], response.size() - 2);
                                uint16_t    crcReceived = (response[response.size() - 1] << 8) | response[response.size() - 2];
                                if (crcCalculated == crcReceived)
                                {
                                    std::cout << "Received: ";
                                    for (size_t index = 0; index < response.size(); index++)
                                    {
                                        std::cout << "0x" << std::hex << std::setw(2) << std::setfill('0') << (int)response[index] << ' ';
                                    }
                                    std::cout << std::endl;
                                }
                                else
                                {
                                    std::cerr << "CRC mismatch calc " << crcCalculated << " != recv " << crcReceived << std::endl;
                                }
                            }
                            else
                            {
                                std::cerr << "Packet too short for valid response " << response.size() << std::endl;
                            }
                        }
                        else
                        {
                            ShowError(ftStatus, "reading");
                        }

                        std::cout << std::dec << std::setw(0);

                        break;

                    case 3:
                        command.resize(7);
                        command[0] = 0x01;
                        command[1] = 0xDE;
                        command[2] = 0xAD;
                        command[3] = 0xBE;
                        command[4] = 0xEF;
                        crc = CRC16Modbus_Calculate(&command[0], command.size() - 2);
                        command[5] = crc & 0xFF;
                        command[6] = crc >> 8;

                        CreateWriteBuffer(command, writeBuffer);

                        ulBytesToWrite = writeBuffer.size() * sizeof(writeBuffer[0]) / sizeof(UCHAR);

                        //memset(&writeBuffer[0], 0xFF, ulBytesToWrite);

                        ftStatus = FT_WritePipe(handle, 0x02, reinterpret_cast<PUCHAR>(&writeBuffer[0]), ulBytesToWrite, &ulBytesWritten, NULL);
                        if (ftStatus != FT_OK)
                        {
                            ShowError(ftStatus, "echo write");
                        }

                        Sleep(1);

                        readBuffer.resize(1024);
                        ftStatus = FT_ReadPipe(handle, 0x82, reinterpret_cast<PUCHAR>(&readBuffer[0]), readBuffer.size() * sizeof(readBuffer[0]) / sizeof(UCHAR), &ulBytesRead, NULL);

                        ulElementsRead = ulBytesRead / sizeof(readBuffer[0]);
                        std::cout << "Received " << ulBytesRead << " bytes, " << ulElementsRead << " elements: ";
                        for (DWORD i = 0; i < ulElementsRead; i++)
                        {
                            std::cout << std::hex << std::setw(4) << std::setfill('0') << (readBuffer[i] & 0xFFFFU) << ' ';
                        }

                        if (ftStatus == FT_OK)
                        {
                            ExtractResponse(readBuffer, response);
                            if (response.size() > 3)
                            {
                                uint16_t    crcCalculated = CRC16Modbus_Calculate(&response[0], response.size() - 2);
                                uint16_t    crcReceived = (response[response.size() - 1] << 8) | response[response.size() - 2];
                                if (crcCalculated == crcReceived)
                                {
                                    std::cout << std::endl << std::endl << "Received: ";
                                    for (size_t index = 0; index < response.size(); index++)
                                    {
                                        std::cout << "0x" << std::hex << std::setw(2) << std::setfill('0') << (int)response[index] << ' ';
                                    }
                                    std::cout << std::endl;
                                }
                                else
                                {
                                    std::cerr << "CRC mismatch calc " << crcCalculated << " != recv " << crcReceived << std::endl;
                                }
                            }
                            else
                            {
                                std::cerr << "Packet too short for valid response " << response.size() << std::endl;
                            }
                        }
                        else
                        {
                            ShowError(ftStatus, "echo read");
                        }

                        std::cout << std::dec << std::setw(0);

                        break;

                    case 4:
                        command.resize(11);
                        command[0] = 0x04;
                        command[1] = 0x00;
                        command[2] = 0x00;
                        command[3] = 0x00;
                        command[4] = 0x00;
                        command[5] = 0x01;
                        command[6] = 0x00;
                        command[7] = 0x00;
                        command[8] = 0x00;
                        crc = CRC16Modbus_Calculate(&command[0], command.size() - 2);
                        command[9] = crc & 0xFF;
                        command[10] = crc >> 8;

                        CreateWriteBuffer(command, writeBuffer);

                        ulBytesToWrite = writeBuffer.size() * sizeof(writeBuffer[0]) / sizeof(UCHAR);

                        //memset(&writeBuffer[0], 0xFF, ulBytesToWrite);

                        // 8640 bytes per page
                        // 2 NAND devices per pair = 2 * 8640 = 17,280 bytes per page
                        // 8 NAND pairs per row = 17,280 * 8 = 138,240 bytes per row
                        // Read 2 bytes for each read so Number of NAND reads = 138,240 / 2 = 69,120
                        // 8 FTCLKS per NAND read = 69,120 * 8 = 552,960 FTCLKs, so number of elements in target array must match
                        // True read length is xxxx bytes, we want to read extra for MCU <-> NAND switching times
                        readBuffer.resize(552960 + 100);

                        ftStatus = FT_WritePipe(handle, 0x02, reinterpret_cast<PUCHAR>(&writeBuffer[0]), ulBytesToWrite, &ulBytesWritten, NULL);
                        if (ftStatus != FT_OK)
                        {
                            ShowError(ftStatus, "download write");
                        }

                        Sleep(1);

                        ftStatus = FT_ReadPipe(handle, 0x82, reinterpret_cast<PUCHAR>(&readBuffer[0]), readBuffer.size() * sizeof(readBuffer[0]) / sizeof(UCHAR), &ulBytesRead, NULL);

                        ulElementsRead = ulBytesRead / sizeof(readBuffer[0]);
                        std::cout << "Received " << ulBytesRead << " bytes, " << ulElementsRead << " elements: ";
                        //for (DWORD i = 0; i < ulElementsRead; i++)
                        //{
                        //    std::cout << std::hex << std::setw(4) << std::setfill('0') << readBuffer[i] << ' ';
                        //}

                        if (ftStatus == FT_OK)
                        {
                            ExtractResponse(readBuffer, response);
                            if (response.size() > 3)
                            {
                                uint16_t    crcCalculated = CRC16Modbus_Calculate(&response[0], response.size() - 2);
                                uint16_t    crcReceived = (response[response.size() - 1] << 8) | response[response.size() - 2];
                                if (crcCalculated == crcReceived)
                                {
                                    //std::cout << std::endl << std::endl << "Received: ";
                                    //for (size_t index = 0; index < response.size(); index++)
                                    //{
                                    //    std::cout << "0x" << std::hex << std::setw(2) << std::setfill('0') << (int)response[index] << ' ';
                                    //}
                                    //std::cout << std::endl;
                                }
                                else
                                {
                                    std::cerr << "CRC mismatch calc " << crcCalculated << " != recv " << crcReceived << std::endl;
                                }
                            }
                            else
                            {
                                std::cerr << "Packet too short for valid response " << response.size() << std::endl;
                            }
                        }
                        else
                        {
                            ShowError(ftStatus, "download read");
                        }
                        break;

                    case 5:
                        ftStatus = FT_AbortPipe(handle, 0x02);
                        if (ftStatus != FT_OK)
                        {
                            ShowError(ftStatus, "abort write");
                        }
                        break;

                    case 6:
                        ftStatus = FT_AbortPipe(handle, 0x82);
                        if (ftStatus != FT_OK)
                        {
                            ShowError(ftStatus, "abort read");
                        }
                        break;

                    case 7:
                        SaveBuffer(readBuffer);
                        break;
                    }
                } while (operation != 0);


                ftStatus = FT_Close(handle);
                if (ftStatus != FT_OK)
                {
                    ShowError(ftStatus, "closing device");
                }
            }
            else
            {
                ShowError(ftStatus, "opening device");
            }
        }
    } while (openIndex != MAXDWORD);

    std::cout << "Press enter to exit";
    std::string waitForEnter;
    std::getline(std::cin, waitForEnter);
    return 0;
}
#endif
