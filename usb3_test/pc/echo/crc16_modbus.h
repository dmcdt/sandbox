#ifndef CRC16_MODBUS_H
#define CRC16_MODBUS_H


#include <stdint.h>


/* Initialise and calculate Modbus CRC */
uint16_t CRC16Modbus_Calculate(uint8_t const *data, uint16_t length);

/* Initialise for performing the Modbus CRC */
uint16_t CRC16Modbus_Initialise(void);

/* Update the CRC calculation for a single byte */
uint16_t CRC16Modbus_Update(uint16_t crc, uint8_t data);


#endif
