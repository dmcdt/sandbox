#include <fstream>
#include <iostream>
#include <string>
#include <iomanip>
#include <vector>
#include <cstdint>


typedef std::vector<uint16_t> ExpandedBuffer_t;

const int oneNandReadCycleCount = 8;        // Number of FTDI clock cycles / transfers that one NAND chip is enabled for
const int oneNandReadIndex = 4;             // Index within the number of FTDI clock cycles that we use as the valid data
const int ftdiTransferBufferSize = 4096;    // Byte length of the FTDI FIFO transfer buffer
const int transferCountPerBuffer = ftdiTransferBufferSize / sizeof(ExpandedBuffer_t::value_type);
const int oneNandTransferCountPerBuffer = transferCountPerBuffer / oneNandReadCycleCount;

const int nandDeviceCount = 8;
const int pageSize = (8192 + 448);


// Extracts the download data from the data read over the USB bus.
// Currently (depending on CPLD implementation) it extracts the
// 4th element from every 8 in the data read over the USB.
void UsbCommand_ExtractDownloadData(ExpandedBuffer_t const& readBuffer, std::vector<uint16_t>& download)
{
    download.resize(readBuffer.size() / oneNandReadCycleCount);
    for (size_t downloadIndex = 0; downloadIndex < download.size(); ++downloadIndex)
    {
        // Original unworking indexing
        //download[index] = readBuffer[8 * index + 4 - (index % 4096)];

        // Figured out from inspecting raw binary dump.
        // BD35/256 is the interval at which we drop a byte due to the FIFO buffer becoming full
        // 1280 is the position of the indexed value.
        // 2049 is the period over which the indexed item is inside the TXE_N deasserted period.
        // =8*BD35+4-ROUNDDOWN((BD35-ROUNDDOWN((BD35+(2049-1280))/2049,0))/256,0)

        size_t  readIndex;
        size_t  indexAdjust;
        
        indexAdjust = (downloadIndex - (downloadIndex + ((transferCountPerBuffer + 1) - ((oneNandReadIndex + 1) * oneNandTransferCountPerBuffer))) / (transferCountPerBuffer + 1)) / oneNandTransferCountPerBuffer;
        readIndex = oneNandReadCycleCount * downloadIndex + oneNandReadIndex - indexAdjust;

        download[downloadIndex] = readBuffer[readIndex];
    }
}


void UsbCommand_DeinterleaveExtractedData(std::vector<uint16_t> const& extracted, std::vector<uint16_t>& flattened)
{
    flattened.resize(extracted.size());

    for (size_t extractedIndex = 0; extractedIndex < extracted.size(); ++extractedIndex)
    {
        size_t flattenedIndex;

        flattenedIndex = (extractedIndex % nandDeviceCount) * pageSize + (extractedIndex / nandDeviceCount);

        flattened[flattenedIndex] = extracted[extractedIndex];
    }
}


int main(void)
{
    std::vector<uint16_t>   rawData;
    std::string             rawFilename;
    std::ifstream           rawStr;

    rawFilename = R"(C:\Users\dm\Documents\dev\cereus\downloader\build-CereusDownloader-Desktop_Qt_6_1_1_MSVC2019_64bit-Debug\dump.raw)";
    rawStr.open(rawFilename, std::ios::binary);
    if (rawStr.is_open())
    {
        rawStr.seekg(0, std::ios::end);
        std::streampos rawFileLength = rawStr.tellg();

        rawData.resize(rawFileLength / sizeof(std::vector<uint16_t>::value_type));

        rawStr.seekg(0, std::ios::beg);
        rawStr.read(reinterpret_cast<char*>(&rawData[0]), rawData.size() * sizeof(rawData[0]));
        rawStr.close();

        // Extract the memory contents from the downloaded values
        std::vector<uint16_t>   extractedData;
        UsbCommand_ExtractDownloadData(rawData, extractedData);

        // Reduce extracted data to an exact page size * number of devices
        extractedData.resize((extractedData.size() / (nandDeviceCount * pageSize)) * (nandDeviceCount * pageSize));

        if (!extractedData.empty())
        {
            std::string             extractedFilename;
            std::ofstream           extractedStr;

            extractedFilename = R"(C:\Users\dm\Documents\dev\cereus\downloader\build-CereusDownloader-Desktop_Qt_6_1_1_MSVC2019_64bit-Debug\dump2.bin)";
            extractedStr.open(extractedFilename, std::ios::binary);
            if (extractedStr)
            {
                extractedStr.write(reinterpret_cast<char*>(&extractedData[0]), extractedData.size() * sizeof(extractedData[0]));
                if (!extractedStr)
                {
                    std::cerr << "Failed to write to extracted file" << std::endl;
                }
                extractedStr.close();
            }
            else
            {
                std::cerr << "Could not open file for writing: " << extractedFilename << std::endl;
            }


            std::vector<uint16_t>   flattened;
            UsbCommand_DeinterleaveExtractedData(extractedData, flattened);
        }
        else
        {
            std::cerr << "No data to write." << std::endl;
        }
    }
    else
    {
        std::cerr << "Could not open file for reading: " << rawFilename << std::endl;
    }

    std::cout << "Press enter to exit";
    std::string waitForKey;
    std::getline(std::cin, waitForKey);

    return 0;
}

